const target =
  process.env.TRAPPER_BACKEND_URL || 'https://dev.trapper-project.org/';

console.log('Proxying to: ' + target);

const https = require('https');
const agent = new https.Agent({
  rejectUnauthorized: false,
});

const isHttps = target.startsWith('https://');

const options = {
  target,
  secure: isHttps,
  changeOrigin: true,
  logLevel: 'debug',
  agent: isHttps ? agent : false,
  headers: {
    Referer: target,
  },
  onProxyReq: (proxyReq, req) => {
    proxyReq.setHeader('X-Forwarded-For', req.ip);
    let host = req.hostname;
    if (req.socket.localPort !== 80 && req.socket.localPort !== 443) {
      host += ':' + req.socket.localPort;
    }
    proxyReq.setHeader('X-Forwarded-Host', host);
    proxyReq.setHeader('X-Trapper-Forwarded-Host', host);
    proxyReq.setHeader('X-Forwarded-Proto', req.protocol);
    proxyReq.setHeader('X-Trapper-Forwarded-Proto', req.protocol);
  },
  preserveHeaderKeyCase: true,
};

const endpoints = ['/api', '/media', '/storage', '/_allauth', '/account'];

const proxyConfig = {};
for (const endpoint of endpoints) {
  proxyConfig[endpoint] = options;
}

module.exports = proxyConfig;

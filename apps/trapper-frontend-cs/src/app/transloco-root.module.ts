import { TranslocoModule, provideTransloco } from '@jsverse/transloco';
import { isDevMode, NgModule } from '@angular/core';
import { availableLanguages } from '@trapper/translations';

const availableLangs = availableLanguages.slice();

@NgModule({
  exports: [TranslocoModule],
  providers: [
    provideTransloco({
      config: {
        availableLangs,
        defaultLang: 'en',
        reRenderOnLangChange: true,
        prodMode: !isDevMode(),
        missingHandler: {
          allowEmpty: true,
        },
      },
    }),
  ],
})
export class TranslocoRootModule {}

import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationStart,
  Router,
} from '@angular/router';
import { TranslocoService } from '@jsverse/transloco';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';
import { LanguageSelectionService } from '@trapper/translations';
import { tap } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  loading = true;

  additionalStyles$ = this.siteCustomizationsService.additionalStyles$;
  siteCustomizationReady$ =
    this.siteCustomizationsService.siteCustomizationReady$;

  siteCustomizationReady = false;

  constructor(
    private transloco: TranslocoService,
    private languageSelectionService: LanguageSelectionService,
    private router: Router,
    private siteCustomizationsService: CsSiteCustomizationService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.loading = true;
        this.changeDetectorRef.detectChanges();
      } else if (
        event instanceof NavigationEnd ||
        event instanceof NavigationCancel
      ) {
        this.loading = false;
        this.changeDetectorRef.detectChanges();
      }
    });
  }

  ngOnInit() {
    this.languageSelectionService.localeChange$
      .pipe(
        tap((locale) => {
          this.transloco.setActiveLang(locale);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.additionalStyles$.pipe(untilDestroyed(this)).subscribe((styles) => {
      this.setAdditionalStyles(styles);
    });

    this.siteCustomizationReady$
      .pipe(untilDestroyed(this))
      .subscribe((ready) => {
        this.siteCustomizationReady = ready;
        this.changeDetectorRef.detectChanges();
      });
  }

  ngAfterViewInit() {
    const spinner = document.getElementById(
      'root__trapper_cs_spinner__wrapper',
    );
    if (spinner) {
      spinner.style.display = 'none';
    }
  }

  setAdditionalStyles(styles: string) {
    const disableStyleCustomization = localStorage.getItem(
      'DEBUG_DISABLE_STYLE_CUSTOMIZATION',
    );
    if (disableStyleCustomization) {
      return;
    }
    const styleElement = document.getElementById(
      'trapper-cs-additional-styles',
    );
    if (styleElement) {
      styleElement.innerHTML = styles;
    }
  }
}

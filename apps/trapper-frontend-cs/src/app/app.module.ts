import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClientXsrfModule,
} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { UnauthorizedInterceptor } from '@trapper/auth-token';

import { TrapperTranslationsModule } from '@trapper/translations';

import { NgScrollbarModule } from 'ngx-scrollbar';
import { TranslocoRootModule } from './transloco-root.module';

const routes: Routes = [
  {
    path: 'cs',
    loadChildren: () =>
      import('@trapper/cs-layout').then((m) => m.csLayoutRoutes),
  },
  {
    path: 'password-reset/:uid/:token',
    redirectTo: '/cs/accounts/password-reset/:uid/:token',
  },
  {
    path: 'activate/:uid/:token',
    redirectTo: '/cs/accounts/activate/:uid/:token',
  },
  {
    path: '',
    redirectTo: 'cs/projects/active-project',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'cs/projects/active-project',
  },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
      scrollPositionRestoration: 'top',
      scrollOffset: [0, 0],
      preloadingStrategy: PreloadAllModules,
    }),
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    }),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TrapperTranslationsModule,
    NgScrollbarModule,
    TranslocoRootModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

#!/bin/bash

version=$(git describe --tags --always --dirty)
app_name=trapper-frontend-cs

index_file_path="dist/apps/$app_name/index.html"

replacement="<script>var TRAPPER_FE_VERSION = '$version';</script>"

sed --in-place "s#<!-- VERSION INFO -->#$replacement#g" $index_file_path

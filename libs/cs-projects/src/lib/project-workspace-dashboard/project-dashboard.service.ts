import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';

export interface PieChartItem {
  total: number;
  english_name: string;
}

export interface BarChartItem {
  english_name: string;
  images: number;
  sequences: number;
}

export interface ProjectStatistics {
  active_users: number;
  locations: number;
  deployments: number;
  images: number;
  recordings: number;
  camera_trap_days: number;
  pie_chart: PieChartItem[];
  bar_chart: BarChartItem[];
}

export interface OwnStatistics {
  locations: number;
  deployments: number;
  images: number;
  recordings: number;
  camera_trap_days: number;
  total_species: number;
  pie_chart: PieChartItem[];
  bar_chart: BarChartItem[];
}

export interface ProjectDashboardData {
  start_date: string;
  joined_date: string;
  project_statistics: ProjectStatistics;
  own_statistics: OwnStatistics;
}

@Injectable()
export class ProjectDashboardService extends BaseBackendConnector<
  void,
  ProjectDashboardData
> {
  loadDashboardData(projectSlug: string) {
    return this.get(`/api/cs/${projectSlug}/dashboard/`, {
      clearPreviousData: true,
    });
  }
}

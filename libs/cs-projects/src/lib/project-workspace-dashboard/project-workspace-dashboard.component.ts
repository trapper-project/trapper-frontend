import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDashboardService } from './project-dashboard.service';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, combineLatest, map, shareReplay, switchMap } from 'rxjs';
import { RecentProjectService } from '@trapper/profile';
import { inOutAnimation } from '@trapper/animations';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { ProjectDataService } from '../services/project-data.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PageTitleService } from '@trapper/page-title-management';
import { DashboardMessagesService } from '../services/dashboard-messages.service';
import { MatIconModule } from '@angular/material/icon';
import { PaginatorComponent } from '@trapper/ui';
import { NgxChartsModule, ScaleType } from '@swimlane/ngx-charts';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-dashboard',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    MatTooltipModule,
    MatIconModule,
    PaginatorComponent,
    NgxChartsModule,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './project-workspace-dashboard.component.html',
  styleUrls: ['./project-workspace-dashboard.component.scss'],
  providers: [ProjectDashboardService, DashboardMessagesService],
  animations: [inOutAnimation],
})
export class ProjectWorkspaceDashboardComponent implements OnInit {
  colors = Array.from(
    { length: 5 },
    (_, i) => `var(--trapper-color-dashboard-plot-${i})`,
  );
  plotProjectTotal$ = this.dashboardService.data$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.project_statistics.pie_chart
        .map((item) => item.total)
        .reduce((a, b) => a + b, 0);
    }),
    shareReplay(1),
  );
  plotOwnTotal$ = this.dashboardService.data$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.own_statistics.pie_chart
        .map((item) => item.total)
        .reduce((a, b) => a + b, 0);
    }),
    shareReplay(1),
  );
  projectName$ = this.projectDataService.data$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.name;
    }),
    shareReplay(1),
  );

  barPlotLabelImagesTranslation$ = this.translocoService.selectTranslate(
    'dashboard.barPlotLabelImages',
    {},
    this.translocoScope,
  );
  barPlotLabelSequencesTranslation$ = this.translocoService.selectTranslate(
    'dashboard.barPlotLabelSequences',
    {},
    this.translocoScope,
  );

  barChartData$ = combineLatest([
    this.dashboardService.data$,
    this.barPlotLabelImagesTranslation$,
    this.barPlotLabelSequencesTranslation$,
  ]).pipe(
    map(([data, imagesTranslation, sequencesTranslation]) => {
      if (!data) {
        return null;
      }
      return data.project_statistics.bar_chart.map((item) => {
        return {
          name: item.english_name,
          series: [
            {
              name: imagesTranslation,
              value: item.images,
            },
            {
              name: sequencesTranslation,
              value: item.sequences,
            },
          ],
        };
      });
    }),
    shareReplay(1),
  );

  pieChartData$ = this.dashboardService.data$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.project_statistics.pie_chart.map((item) => {
        return {
          name: item.english_name,
          value: item.total,
        };
      });
    }),
    shareReplay(1),
  );

  barChartOwnData$ = combineLatest([
    this.dashboardService.data$,
    this.barPlotLabelImagesTranslation$,
    this.barPlotLabelSequencesTranslation$,
  ]).pipe(
    map(([data, imagesTranslation, sequencesTranslation]) => {
      if (!data) {
        return null;
      }
      return data.own_statistics.bar_chart.map((item) => {
        return {
          name: item.english_name,
          series: [
            {
              name: imagesTranslation,
              value: item.images,
            },
            {
              name: sequencesTranslation,
              value: item.sequences,
            },
          ],
        };
      });
    }),
    shareReplay(1),
  );

  pieChartOwnData$ = this.dashboardService.data$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.own_statistics.pie_chart.map((item) => {
        return {
          name: item.english_name,
          value: item.total,
        };
      });
    }),
    shareReplay(1),
  );

  messages$ = this.dashboardMessagesService.messages$;
  pagination$ = this.dashboardMessagesService.pagination$;

  // bar plot color scheme by bar
  colorScheme = {
    domain: this.colors,
    name: 'barColorScheme',
    selectable: true,
    group: ScaleType.Ordinal,
  };

  constructor(
    public dashboardService: ProjectDashboardService,
    private route: ActivatedRoute,
    private recentProjectService: RecentProjectService,
    private projectDataService: ProjectDataService,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    private dashboardMessagesService: DashboardMessagesService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  @HostBinding('@inOutAnimation') inOutAnimation = true;
  ngOnInit(): void {
    this.pageTitleService.resetTitle();

    this.route.paramMap
      .pipe(
        switchMap((paramMap) => {
          const projectId = paramMap.get('projectId');
          if (!projectId) {
            return EMPTY;
          }
          this.recentProjectService.updateRecentProject(projectId);
          this.dashboardMessagesService.getMessages();
          return this.dashboardService.loadDashboardData(projectId);
        }),
      )
      .subscribe();

    combineLatest([
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'dashboardPageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([data, translation]) => {
        if (!data) {
          return;
        }
        this.pageTitleService.setSectionTitle([translation, data.name]);
      });
  }

  markAsReceived(messageId: number) {
    this.dashboardMessagesService.markAsReceived(messageId);
  }

  pageChanged(page: number) {
    this.dashboardMessagesService.getMessages(page);
  }
}
export default ProjectWorkspaceDashboardComponent;

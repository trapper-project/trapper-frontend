import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { inOutAnimation } from '@trapper/animations';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import {
  BadgeComponent,
  ButtonComponent,
  CheckboxComponent,
  DateRange,
  DateRangeInputComponent,
  FormFieldComponent,
  LabelComponent,
  PaginatorComponent,
  SelectBoxInputComponent,
  TextInputComponent,
  UserMiniAvatarComponent,
} from '@trapper/ui';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { ProjectDataService } from '../services/project-data.service';
import {
  DeploymentListingItem,
  ProjectDeploymentListingService,
} from '../services/project-deployments-listing.service';
import {
  Observable,
  combineLatest,
  distinctUntilChanged,
  map,
  shareReplay,
  tap,
} from 'rxjs';
import { format, parse } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { DeploymentListingTableComponent } from '../deployment-listing-table/deployment-listing-table.component';
import { DeploymentListingMapComponent } from '../deployment-listing-map/deployment-listing-map.component';
import { DeploymentListingMapService } from '../services/deployment-listing-map.service';
import {
  TeamShareDeploymentsComponentData,
  TeamsShareDeploymentsComponent,
} from '../teams-share-deployments/teams-share-deployments.component';
import { Dialog } from '@angular/cdk/dialog';
import { PageTitleService } from '@trapper/page-title-management';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-deployments',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    TextInputComponent,
    FormsModule,
    FormFieldComponent,
    LabelComponent,
    SelectBoxInputComponent,
    DateRangeInputComponent,
    PaginatorComponent,
    BadgeComponent,
    CheckboxComponent,
    NgScrollbarModule,
    ScrollingModule,
    ButtonComponent,
    RouterModule,
    NgxSkeletonLoaderModule,
    MatTooltipModule,
    UserMiniAvatarComponent,
    MatIconModule,
    DeploymentListingTableComponent,
    DeploymentListingMapComponent,
  ],
  templateUrl: './project-workspace-deployments.component.html',
  styleUrls: ['./project-workspace-deployments.component.scss'],
  animations: [inOutAnimation],
  providers: [ProjectDeploymentListingService, DeploymentListingMapService],
})
export class ProjectWorkspaceDeploymentsComponent implements OnInit {
  @HostBinding('@inOutAnimation') inOutAnimation = true;
  availableFilters$ = this.projectDataService.availableFilters$;

  listingItems$ = this.deploymentListingService.data$;
  pagination$ = this.deploymentListingService.pagination$;

  deployments$: Observable<DeploymentListingItem[]> = this.listingItems$.pipe(
    map((dto) => dto?.results || []),
  );
  noItems$ = this.listingItems$.pipe(map((dto) => dto?.results.length === 0));

  mapModeEnabled = false;

  selectedDeployments: Set<DeploymentListingItem> = new Set();

  currentDateRange$ = this.deploymentListingService.filter$.pipe(
    map((filters) => {
      const startString = filters['sdate_from'] || null;
      const endString = filters['sdate_to'] || null;
      return { startString, endString };
    }),
    distinctUntilChanged(
      (a, b) => a.startString === b.startString && a.endString === b.endString,
    ),
    map(({ startString, endString }) => {
      const start = startString
        ? parse(startString, 'yyyy-MM-dd', new Date())
        : null;
      const end = endString ? parse(endString, 'yyyy-MM-dd', new Date()) : null;
      return { start, end };
    }),
    shareReplay(1),
  );

  constructor(
    private projectDataService: ProjectDataService,
    private router: Router,
    private deploymentListingService: ProjectDeploymentListingService,
    private route: ActivatedRoute,
    private dialog: Dialog,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit(): void {
    this.deploymentListingService
      .connect()
      .pipe(untilDestroyed(this))
      .subscribe();

    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const projectId = params.get('projectId');
      if (projectId) {
        this.deploymentListingService.projectSlug$.next(projectId);
      }
    });

    combineLatest([
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'deploymentsPageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([project, title]) => {
        const titleParts = [title];
        if (project) {
          titleParts.push(project.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.route.queryParamMap
      .pipe(
        tap((query) => {
          const filters = Object.fromEntries(
            query.keys.filter((k) => k !== 'map').map((k) => [k, query.get(k)]),
          );

          if (query.has('map')) {
            this.mapModeEnabled = true;
          } else {
            this.mapModeEnabled = false;
          }

          this.deploymentListingService.filter$.next(filters);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  pageChanged(page: number) {
    this.router.navigate([], {
      queryParams: { page },
      queryParamsHandling: 'merge',
    });
  }

  filterChanged(filterName: string, filterValue: string | null) {
    this.router.navigate([], {
      queryParams: {
        [filterName]: filterValue,
        page: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  currentFilters$(field: string) {
    return this.deploymentListingService.filter$.pipe(
      map((filters) => filters[field] || null),
    );
  }

  dateRangeChanged(range: DateRange) {
    const sdate_from = range.start ? format(range.start, 'yyyy-MM-dd') : null;
    const sdate_to = range.end ? format(range.end, 'yyyy-MM-dd') : null;
    const queryParams = {
      sdate_from,
      sdate_to,
      page: null,
    };
    this.router.navigate([], {
      queryParams,
      queryParamsHandling: 'merge',
    });
  }

  deploymentSelected(deploymentId: number, selected: boolean) {
    this.deploymentListingService.toggleSelection(deploymentId, selected);
  }

  toggleMapMode() {
    if (!this.mapModeEnabled) {
      this.router.navigate([], {
        queryParams: { map: true },
        queryParamsHandling: 'merge',
      });
    } else {
      this.disableMapMode();
    }
  }

  disableMapMode() {
    this.router.navigate([], {
      queryParams: { map: null },
      queryParamsHandling: 'merge',
    });
  }

  myDeploymentsFilterChanged(selected: boolean) {
    this.router.navigate([], {
      queryParams: {
        by_me: selected ? '1' : null,
        map: selected && this.mapModeEnabled ? true : null,
        page: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  shareSelectedWithTeams() {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: TeamShareDeploymentsComponentData = {
      projectSlug: this.projectDataService.currentProjectSlug,
      deployments: Array.from(this.selectedDeployments),
    };
    this.dialog
      .open(TeamsShareDeploymentsComponent, {
        data,
        width: 'auto',
        height: 'auto',
        maxHeight: '90vh',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.selectedDeployments = new Set();
        }
      });
  }
}
export default ProjectWorkspaceDeploymentsComponent;

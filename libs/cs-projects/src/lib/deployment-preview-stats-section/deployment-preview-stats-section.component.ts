import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeploymentPreview } from '../services/deployment-preview.service';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-frontend-deployment-preview-stats-section',
  standalone: true,
  imports: [CommonModule, TranslocoModule],
  templateUrl: './deployment-preview-stats-section.component.html',
  styleUrls: ['./deployment-preview-stats-section.component.scss'],
})
export class DeploymentPreviewStatsSectionComponent {
  @Input() deployment?: DeploymentPreview;
}

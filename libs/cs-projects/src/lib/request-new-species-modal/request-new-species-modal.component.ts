import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { trapperCsProjectsTranslocoScope } from '../transloco.loader';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { DIALOG_DATA, DialogModule, DialogRef } from '@angular/cdk/dialog';
import {
  ButtonComponent,
  CardComponent,
  FormFieldComponent,
  LabelComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { RequestNewSpeciesService } from '../services/request-new-species.service';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';

export interface RequestNewSpeciesModalComponentData {
  currentProjectSlug: string;
  classificationId: number;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-request-new-species-modal',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    DialogModule,
    FormFieldComponent,
    TextInputComponent,
    ServerErrorsComponent,
    LabelComponent,
    CardComponent,
    ButtonComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './request-new-species-modal.component.html',
  styleUrls: ['./request-new-species-modal.component.scss'],
  providers: [trapperCsProjectsTranslocoScope, RequestNewSpeciesService],
})
export class RequestNewSpeciesModalComponent {
  inProgress$ = this.requestNewSpeciesService.inProgress$;
  formGroup = this.requestNewSpeciesService.formGroup;

  constructor(
    private dialogRef: DialogRef,
    private requestNewSpeciesService: RequestNewSpeciesService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private scope: TranslocoScope,
    @Inject(DIALOG_DATA) data: RequestNewSpeciesModalComponentData,
    private toastr: ToastrService,
  ) {
    this.requestNewSpeciesService.currentProjectSlug = data.currentProjectSlug;
    this.requestNewSpeciesService.classificationId = data.classificationId;
  }

  cancel() {
    this.dialogRef.close();
  }

  ok() {
    this.requestNewSpeciesService
      .performCreate()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.translocoService
          .selectTranslate(
            'requestNewSpeciesModal.successMessage',
            {},
            this.scope,
          )
          .pipe(take(1))
          .subscribe((message) => {
            this.toastr.success(message);
          });
        this.dialogRef.close();
      });
  }
}

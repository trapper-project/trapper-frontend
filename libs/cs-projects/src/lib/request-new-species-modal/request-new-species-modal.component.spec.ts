import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RequestNewSpeciesModalComponent } from './request-new-species-modal.component';

describe('RequestNewSpeciesModalComponent', () => {
  let component: RequestNewSpeciesModalComponent;
  let fixture: ComponentFixture<RequestNewSpeciesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RequestNewSpeciesModalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestNewSpeciesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

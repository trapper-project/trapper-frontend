import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectWorkspaceSidebarComponent } from '../project-workspace-sidebar/project-workspace-sidebar.component';
import { CardComponent, CompactModeService } from '@trapper/ui';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { inOutAnimation } from '@trapper/animations';
import { WorkspaceSelectorService } from '@trapper/workspace-selector';
import { ProjectDataService } from '../services/project-data.service';
import { EMPTY, combineLatest, map, switchMap } from 'rxjs';
import { LanguageSelectionService } from '@trapper/translations';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { trapperCsProjectsTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';
import { CurrentUserProfileService } from '@trapper/current-profile-data';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-active-workspace',
  standalone: true,
  imports: [
    CommonModule,
    ProjectWorkspaceSidebarComponent,
    CardComponent,
    RouterModule,
    TranslocoModule,
  ],
  templateUrl: './project-active-workspace.component.html',
  styleUrls: ['./project-active-workspace.component.scss'],
  animations: [inOutAnimation],
  providers: [ProjectDataService, trapperCsProjectsTranslocoScope],
})
export class ProjectActiveWorkspaceComponent implements OnInit, OnDestroy {
  profileData$ = this.profileService.data$;
  projectData$ = this.projectDataService.data$;

  @HostBinding('class.compact-mode') compactMode = false;

  constructor(
    private profileService: CurrentUserProfileService,
    private workspaceSelectorService: WorkspaceSelectorService,
    private projectDataService: ProjectDataService,
    private route: ActivatedRoute,
    private router: Router,
    private languageSelectionService: LanguageSelectionService,
    private compactModeService: CompactModeService,
  ) {}
  ngOnDestroy(): void {
    this.workspaceSelectorService.workspaceSelectorEnabled = false;
  }
  ngOnInit(): void {
    this.profileService.reloadUserProfile().subscribe();
    this.workspaceSelectorService.workspaceSelectorEnabled = true;

    this.projectDataService.notFound$
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.router.navigate(['/cs/projects']);
      });

    const projectSlug$ = this.route.paramMap.pipe(
      map((params) => {
        const projectId = params.get('projectId');

        return projectId;
      }),
    );

    combineLatest([projectSlug$, this.languageSelectionService.localeChange$])
      .pipe(
        switchMap(([projectSlug]) => {
          if (!projectSlug) {
            return EMPTY;
          }

          return this.projectDataService.fetchProjectData(projectSlug);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.compactModeService.compactMode$
      .pipe(untilDestroyed(this))
      .subscribe((compactMode) => {
        this.compactMode = compactMode;
      });
  }
}

export default ProjectActiveWorkspaceComponent;

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassificationDataService } from '../services/classification-data.service';
import { ProjectDataService } from '../services/project-data.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { DialogRef } from '@angular/cdk/dialog';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@jsverse/transloco';
import { MarkAsFavouriteService } from '../services/mark-as-favourite.service';
import { EMPTY, combineLatest, map, switchMap, take } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ResourceAnnotatorComponent } from '@trapper/resource-annotator';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-media-resource-preview',
  standalone: true,
  imports: [
    CommonModule,
    MatTooltipModule,
    MatIconModule,
    RouterModule,
    TranslocoModule,
    ResourceAnnotatorComponent,
  ],
  templateUrl: './media-resource-preview.component.html',
  styleUrls: ['./media-resource-preview.component.scss'],
  providers: [MarkAsFavouriteService],
})
export class MediaResourcePreviewComponent {
  currentItem$ = this.ClassificationDataService.data$;
  currentProject$ = this.projectDataService.data$;
  bboxes$ = this.ClassificationDataService.bboxes$;
  objects$ = this.ClassificationDataService.objects$;
  constructor(
    private ClassificationDataService: ClassificationDataService,
    private projectDataService: ProjectDataService,
    private dialogRef: DialogRef,
    private markAsFavouriteService: MarkAsFavouriteService,
  ) {}

  close() {
    this.dialogRef.close();
  }

  markAsFavourite() {
    combineLatest([this.currentProject$, this.currentItem$])
      .pipe(
        take(1),
        switchMap(([project, item]) => {
          if (!project || !item) {
            return EMPTY;
          }
          return this.markAsFavouriteService
            .markAsFavourite(project.slug, item.pk, !item.is_favorite)
            .pipe(
              map(() => {
                return { project, item };
              }),
            );
        }),
        switchMap(({ project, item }) => {
          if (!project || !item) {
            return EMPTY;
          }
          return this.ClassificationDataService.reloadMediaData(
            project.slug,
            item.pk,
          );
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }
}

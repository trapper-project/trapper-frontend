import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CheckboxComponent,
  PaginatorComponent,
  ResponsiveTableComponent,
  TableCellDefDirective,
  TextInputComponent,
  UserMiniAvatarComponent,
} from '@trapper/ui';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';
import {
  TeamListingItem,
  TeamListingService,
  TeamMember,
} from '../services/team-listing.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { map, tap } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ProfilePreviewService } from '@trapper/profile-preview';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { inOutAnimation } from '@trapper/animations';
import { ProjectDataService } from '../services/project-data.service';
import {
  TeamDeleteComponent,
  TeamDeleteComponentData,
} from '../team-delete/team-delete.component';
import { PageTitleService } from '@trapper/page-title-management';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-listing',
  standalone: true,
  imports: [
    CommonModule,
    TextInputComponent,
    PaginatorComponent,
    TranslocoModule,
    RouterModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    MatIconModule,
    UserMiniAvatarComponent,
    MatTooltipModule,
    NgScrollbarModule,
    ScrollingModule,
    DialogModule,
    ButtonComponent,
    CheckboxComponent,
    ResponsiveTableComponent,
    TableCellDefDirective,
  ],
  templateUrl: './team-listing.component.html',
  styleUrls: ['./team-listing.component.scss'],
  providers: [TeamListingService, ProfilePreviewService],
  animations: [inOutAnimation],
})
export class TeamListingComponent implements OnInit {
  pagination$ = this.teamsListingService.pagination$;
  data$ = this.teamsListingService.data$;
  listingItems$ = this.teamsListingService.listingItems$;
  noItems$ = this.teamsListingService.data$.pipe(
    map((data) => data && data.results.length === 0),
  );
  constructor(
    private teamsListingService: TeamListingService,
    private route: ActivatedRoute,
    private router: Router,
    private profilePreviewDialogService: ProfilePreviewService,
    private projectDataService: ProjectDataService,
    private dialog: Dialog,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
  ) {}

  ngOnInit(): void {
    this.teamsListingService.connect().pipe(untilDestroyed(this)).subscribe();

    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const projectId = params.get('projectId');
      if (projectId) {
        this.teamsListingService.projectSlug$.next(projectId);
      }
    });

    this.projectDataService.data$.pipe(untilDestroyed(this)).subscribe((d) => {
      const titleParts = [
        this.translocoService.translate(
          'teamListing.pageTitle',
          {},
          'csProjects',
        ),
      ];
      if (d) {
        titleParts.push(d.name);
      }
      this.pageTitleService.setSectionTitle(titleParts);
    });

    this.route.queryParamMap
      .pipe(
        tap((query) => {
          const filters = Object.fromEntries(
            query.keys.map((k) => [k, query.get(k)]),
          );

          this.teamsListingService.filter$.next(filters);
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  pageChanged(page: number) {
    this.router.navigate([], {
      queryParams: { page },
      queryParamsHandling: 'merge',
    });
  }

  filterChanged(filterName: string, filterValue: string | null) {
    this.router.navigate([], {
      queryParams: {
        [filterName]: filterValue,
        page: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  currentFilters$(field: string) {
    return this.teamsListingService.filter$.pipe(
      map((filters) => filters[field] || null),
    );
  }

  openUserDetails(member: TeamMember) {
    this.profilePreviewDialogService.open(member.id);
  }

  deleteTeam(team: TeamListingItem) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: TeamDeleteComponentData = {
      teamId: team.id,
      teamName: team.name,
      projectSlug: this.projectDataService.currentProjectSlug,
    };
    this.dialog
      .open(TeamDeleteComponent, {
        data,
        width: '500px',
        height: 'auto',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.teamsListingService.refresh();
        }
      });
  }
}

export default TeamListingComponent;

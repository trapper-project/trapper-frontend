import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';

export interface NewSpeciesRequestPayload {
  species: string;
  comment: string;
}

@Injectable()
export class RequestNewSpeciesService extends BaseBackendFormConnector<
  ModelFormGroupControls<NewSpeciesRequestPayload>,
  void
> {
  public currentProjectSlug?: string;
  public classificationId?: number;
  get endpoint() {
    return `/api/media-classifications/${this.currentProjectSlug}/request-new-species/${this.classificationId}/`;
  }

  constructor(private fb: NonNullableFormBuilder) {
    super(
      fb.group({
        species: fb.control(''),
        comment: fb.control(''),
      }),
    );
  }
}

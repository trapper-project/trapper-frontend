import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  Subject,
  asyncScheduler,
  catchError,
  combineLatest,
  map,
  shareReplay,
  switchMap,
  throttleTime,
} from 'rxjs';

export interface PaginationData {
  page: number;
  page_size: number;
  pages: number;
  count: number;
  total: number;
}

export interface TeamMember {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  name: string;
}

export interface TeamListingItem {
  id: number;
  name: string;
  description: string;
  team_leader: TeamMember;
  members_count: number;
  can_manage: boolean;
  date_created: string;
}

export interface TeamListingDto {
  pagination: PaginationData;
  results: TeamListingItem[];
}

const defaultFilters: Record<string, string | null> = {};

@Injectable()
export class TeamListingService {
  inProgress$ = new BehaviorSubject(false);
  _data$ = new BehaviorSubject<TeamListingDto | null>(null);
  filter$ = new BehaviorSubject(defaultFilters);
  projectSlug$ = new Subject<string>();

  reloads$ = new BehaviorSubject<unknown>({});

  data$ = this._data$.asObservable();

  listingItems$ = this._data$.pipe(
    map((dto) => dto?.results || null),
    shareReplay(1),
  );

  pagination$ = this._data$.pipe(
    map((dto) => dto?.pagination || null),
    shareReplay(1),
  );

  constructor(private httpClient: HttpClient) {}

  connect(): Observable<void> {
    return combineLatest([this.filter$, this.reloads$]).pipe(
      map(([filters]) => filters),
      throttleTime(500, asyncScheduler, { leading: true, trailing: true }),
      switchMap((filters) => {
        const params: Record<string, string> = {};
        Object.entries(filters).forEach(([k, v]) => {
          if (v) {
            params[k] = v;
          }
        });
        if (
          params['page_size'] &&
          ![10, 50, 100, 200].includes(+params['page_size'])
        ) {
          delete params['page_size'];
        }
        this.inProgress$.next(true);
        this._data$.next(null);
        return this.httpClient.get<TeamListingDto>(`/api/teams/`, {
          params,
        });
      }),
      map((data) => {
        this._data$.next(data);
        this.inProgress$.next(false);
      }),
      catchError(() => {
        this.inProgress$.next(false);
        return EMPTY;
      }),
    );
  }

  refresh() {
    this.reloads$.next({});
  }
}

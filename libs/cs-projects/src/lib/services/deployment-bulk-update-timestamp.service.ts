import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, tap } from 'rxjs';

@Injectable()
export class DeploymentBulkUpdateTimestampService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  bulkUpdateTimestamp(
    projectSlug: string,
    deploymentId: number,
    time_offset: number,
  ) {
    this.inProgress$.next(true);
    return this.httpClient
      .put(
        `/api/deployments/${projectSlug}/${deploymentId}/update_timestamps/`,
        { time_offset },
      )
      .pipe(
        tap(() => this.inProgress$.next(false)),
        catchError((error) => {
          this.toastr.error(
            error.error.detail ||
              this.translocoService.translate(
                'deploymentBulkUpdateTimestamp.deploymentBulkUpdateTimestampUnknownError',
                {},
                'csProjects',
              ),
          );
          this.inProgress$.next(false);
          return NEVER;
        }),
        tap(() =>
          this.toastr.success(
            this.translocoService.translate(
              'deploymentBulkUpdateTimestamp.deploymentBulkUpdateTimestampSuccess',
              {},
              'csProjects',
            ),
          ),
        ),
      );
  }
}

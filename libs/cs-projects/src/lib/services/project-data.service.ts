import { inject, Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { SelectBoxInputItem } from '@trapper/ui';
import { EMPTY, map, Observable, shareReplay } from 'rxjs';
import { TagColorsService } from './tag-colors.service';

export interface Owner {
  name: string;
  id: number;
}

export interface Species {
  id: string;
  english_name: string;
  latin_name: string;
  __str__: string;
}

export interface Location {
  id: number;
  name: string;
}

export interface Deployment {
  id: number;
  deployment_id: string;
  deployment_code: string;
  my_deployment: boolean;
}

export interface ClassificationSpecAttibuteChoice {
  id: string | number;
  name: string;
}

export interface ClassificationSpecAttribute {
  show?: boolean;
  label: string;
  name: string;
  type: 'boolean' | 'choices' | 'string' | 'integer' | 'float';
  choices?: ClassificationSpecAttibuteChoice[];
  selectBoxChoices?: SelectBoxInputItem[];
  colorMapping?: Map<string, string>;
  required?: boolean;
  description?: string;
  confidence_warning_threshold?: number;
}

export interface ClassificationSpec {
  static: ClassificationSpecAttribute[];
  dynamic: ClassificationSpecAttribute[];
}

export interface IdName {
  id: string | number;
  name: string;
}

function idNameToSBI(idName?: IdName[]): SelectBoxInputItem[] | undefined {
  if (!idName) {
    return undefined;
  }
  return idName.map((idName) => ({
    id: idName.id.toString(),
    name: idName.name,
  }));
}

export interface ProjectMediaFilters {
  // observation_type?: Record<string, string>;
  observation_type?: IdName[];
  classification_status?: IdName[];
  age?: IdName[];
  sex?: IdName[];
  behavior?: IdName[];
  species: Species[];
  owner?: Owner[];
  date_range: boolean;
  locations: Location[];
  locations_upload: Location[];
  deployments: Deployment[];
  bait_type?: IdName[];
  default_bait_type?: string;
  feature_type?: IdName[];
  available_teams?: { id: number; name: string }[];
}

export interface ProjectMediaFiltersInternal {
  observation_type?: SelectBoxInputItem[];
  species?: SelectBoxInputItem[];
  sex?: SelectBoxInputItem[];
  age?: SelectBoxInputItem[];
  behaviour?: SelectBoxInputItem[];
  classification_status?: SelectBoxInputItem[];
  media_owner?: SelectBoxInputItem[];
  location?: SelectBoxInputItem[];
  locations_upload?: SelectBoxInputItem[];
  deployment?: SelectBoxInputItem[];
  bait_type?: SelectBoxInputItem[];
  feature_type?: SelectBoxInputItem[];
  my_deployments?: SelectBoxInputItem[];
  all_deployments?: SelectBoxInputItem[];
  teams?: SelectBoxInputItem[];
  // date_range: SelectBoxInputItem[];
}
export interface ProjectData {
  name: string;
  slug: string;
  available_filters: ProjectMediaFilters;
  classification_spec: ClassificationSpec;
  video_support_enabled: boolean;
  is_current_user_errorproof: boolean;
  blur_humans_and_vehicles_immediately: boolean;
  hide_classification_attributes_for_non_animals: boolean;
}

export interface ProjectMediaFilter {
  label: string;
  placeholder: string;
  choices: SelectBoxInputItem[];
  name: string;
  is_multiselect: boolean;
  wide?: boolean;
}

@Injectable()
export class ProjectDataService extends BaseBackendConnector<
  void,
  ProjectData
> {
  REFRESH_THRESHOLD = 500;
  tagColorService = inject(TagColorsService);
  currentProjectSlug?: string;
  lastRefreshed: Date | null = null;
  availableFilters$ = this.data$.pipe(
    map((dto) => {
      if (!dto) {
        return null;
      }
      const filters: ProjectMediaFiltersInternal = {
        observation_type: idNameToSBI(dto.available_filters.observation_type),
        classification_status: idNameToSBI(
          dto.available_filters.classification_status,
        ),
        sex: idNameToSBI(dto.available_filters.sex),
        age: idNameToSBI(dto.available_filters.age),
        behaviour: idNameToSBI(dto.available_filters.behavior),
        species: dto.available_filters.species.map((species) => ({
          id: species.id.toString(),
          name: species.__str__,
        })),
        media_owner: idNameToSBI(dto.available_filters.owner),
        location: idNameToSBI(dto.available_filters.locations),
        locations_upload: idNameToSBI(dto.available_filters.locations_upload),
        deployment: dto.available_filters.deployments.map((deployment) => ({
          id: deployment.id.toString(),
          name: deployment.deployment_id,
        })),
        my_deployments: dto.available_filters.deployments
          .filter((deployment) => deployment.my_deployment)
          .map((deployment) => ({
            id: deployment.id.toString(),
            name: deployment.deployment_id,
          })),
        all_deployments: dto.available_filters.deployments.map(
          (deployment) => ({
            id: deployment.id.toString(),
            name: deployment.deployment_id,
          }),
        ),
        bait_type: idNameToSBI(dto.available_filters.bait_type),
        feature_type: idNameToSBI(dto.available_filters.feature_type),
        teams: idNameToSBI(dto.available_filters.available_teams),
      };

      return filters;
    }),
    shareReplay(1),
  );

  classificationSpec$: Observable<ClassificationSpec | null> = this.data$.pipe(
    map((dto) => dto?.classification_spec),
    map((spec) => {
      if (!spec) {
        return null;
      }
      const showTypes = ['choices', 'boolean', 'string', 'integer', 'float'];
      const snakeToTitleCase = (str: string | undefined) =>
        str
          ? str
              .split('_')
              .map((part) => part[0].toUpperCase() + part.slice(1))
              .join(' ')
          : str;
      const staticAttributes = spec.static.map((attr) => {
        return {
          ...attr,
          show: showTypes.includes(attr.type),
          label: attr.label || snakeToTitleCase(attr.name) || '',
          selectBoxChoices:
            attr.type === 'choices'
              ? attr.choices?.map((choice) => ({
                  id: choice.id.toString(),
                  name: choice.name,
                }))
              : undefined,
        };
      });
      const dynamicAttributes = spec.dynamic.map((attr) => {
        const colorMapping = this.tagColorService.getColorMappingForTagType(
          attr.name,
        );
        return {
          ...attr,
          show: showTypes.includes(attr.type) && attr.name !== 'bboxes',
          label: attr.label || snakeToTitleCase(attr.name) || '',
          selectBoxChoices:
            attr.type === 'choices'
              ? attr.choices?.map((choice) => ({
                  id: choice.id.toString(),
                  name: choice.name,
                }))
              : undefined,
          colorMapping,
        };
      });
      return {
        static: staticAttributes,
        dynamic: dynamicAttributes,
      };
    }),
    shareReplay(1),
  );

  classificationSpecDictionaries$ = this.classificationSpec$.pipe(
    map((spec) => {
      if (!spec) {
        return null;
      }
      const dictionaries: Record<string, Record<string, string>> = {};
      spec.static.forEach((attr) => {
        if (attr.selectBoxChoices) {
          dictionaries[attr.name] = Object.fromEntries(
            attr.selectBoxChoices.map((choice) => [choice.id, choice.name]),
          );
        }
      });
      spec.dynamic.forEach((attr) => {
        if (attr.selectBoxChoices) {
          dictionaries[attr.name] = Object.fromEntries(
            attr.selectBoxChoices.map((choice) => [choice.id, choice.name]),
          );
        }
      });
      return dictionaries;
    }),
    shareReplay(1),
  );

  classificationSpecFilters$ = this.classificationSpec$.pipe(
    map((spec) => {
      if (!spec) {
        return [];
      }
      const filters: ProjectMediaFilter[] = [];

      const single_select_names = ['observation_type'];

      for (const attr of spec.dynamic) {
        if (!attr.show) {
          continue;
        }
        if (attr.type !== 'choices') {
          continue;
        }
        filters.push({
          label: attr.label,
          placeholder: attr.label,
          choices: attr.selectBoxChoices || [],
          name: attr.name,
          is_multiselect: !single_select_names.includes(attr.name),
        });
      }

      return filters;
    }),
    shareReplay(1),
  );

  defaultBaitType$ = this.data$.pipe(
    map((dto) => dto?.available_filters.default_bait_type),
    shareReplay(1),
  );

  locationChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.location),
  );
  locationUploadChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.locations_upload),
  );
  deploymentChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.deployment),
  );
  myDeploymentChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.my_deployments),
  );
  baitTypeChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.bait_type),
  );

  featureTypeChoices$ = this.availableFilters$.pipe(
    map((filters) => filters?.feature_type),
  );

  teamChoices$ = this.availableFilters$.pipe(map((filters) => filters?.teams));

  videoSupportEnabled$ = this.data$.pipe(
    map((dto) => dto?.video_support_enabled),
    shareReplay(1),
  );

  isErrorProof$ = this.data$.pipe(
    map((dto) => dto?.is_current_user_errorproof),
    shareReplay(1),
  );

  setProjectSlug(projectSlug: string) {
    this.currentProjectSlug = projectSlug;
  }

  fetchProjectData(projectSlug: string) {
    this.setProjectSlug(projectSlug);
    this.lastRefreshed = new Date();
    return this.get(`/api/cs/${projectSlug}/`, { clearPreviousData: true });
  }

  reloadProjectData(): Observable<void> {
    if (!this.currentProjectSlug) {
      return EMPTY;
    }
    if (
      this.lastRefreshed &&
      new Date().getTime() - this.lastRefreshed.getTime() <
        this.REFRESH_THRESHOLD
    ) {
      return EMPTY;
    }
    this.lastRefreshed = new Date();
    return this.get(`/api/cs/${this.currentProjectSlug}/`, {
      clearPreviousData: false,
    });
  }
}

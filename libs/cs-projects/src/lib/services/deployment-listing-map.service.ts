import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, catchError, map, tap } from 'rxjs';

export interface DeploymentListingMapItem {
  pk: number;
  name: string;
  latitude: number;
  longitude: number;
  deployments_count: number;
}

@Injectable()
export class DeploymentListingMapService {
  data$ = new BehaviorSubject<DeploymentListingMapItem[] | null>(null);
  loading$ = new BehaviorSubject<boolean>(false);
  error$ = new BehaviorSubject<string | null>(null);

  constructor(private httpClient: HttpClient) {}

  fetchMapData(projectSlug: string, filters: Record<string, string>) {
    this.loading$.next(true);
    return this.httpClient
      .get<
        DeploymentListingMapItem[]
      >(`/api/deployments/${projectSlug}/locations/`, { params: filters })
      .pipe(
        map((data) => {
          const alreadyPresent = new Set();
          const newData: DeploymentListingMapItem[] = [];
          data.forEach((item) => {
            if (alreadyPresent.has(item.pk)) {
              return;
            }
            alreadyPresent.add(item.pk);
            newData.push(item);
          });
          return newData;
        }),
        tap((data) => this.data$.next(data)),
        catchError((error) => {
          this.error$.next(error);
          return EMPTY;
        }),
      );
  }
}

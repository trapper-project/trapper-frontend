import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {
  BehaviorSubject,
  Observable,
  catchError,
  map,
  of,
  switchMap,
  take,
  tap,
} from 'rxjs';
import {
  ObjectBoundingBoxes,
  AnnotationState,
  AnnotationStateService,
} from './annotation-state.service';
import { ClassificationDataService } from './classification-data.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  TRANSLOCO_SCOPE,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { ClassificationSpec } from './project-data.service';

type DynamicAttr = Record<string, string | number | null | undefined> & {
  bboxes?: ObjectBoundingBoxes;
};

export interface ClassificationRemotePayload {
  static: Record<string, string | number | null | undefined>;
  dynamic: DynamicAttr[];
}

export interface ClassificationRemoteErrorResponse {
  static?: Record<string, string[]>;
  dynamic?: Record<string, string[]>[];
  non_field_errors?: string[];
  detail?: string;
}

export interface ClassificationErrors {
  static?: Record<string, string[]>;
  dynamic?: Record<string, Record<string, string[]>>;
  non_field_errors?: string[];
}

@UntilDestroy()
@Injectable()
export class ClassificationService {
  error$ = new BehaviorSubject<ClassificationRemoteErrorResponse | null>(null);
  inProgress$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private toastrService: ToastrService,
    private annotationStateService: AnnotationStateService,
    private ClassificationDataService: ClassificationDataService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private scope: TranslocoScope,
  ) {}

  private _prepareClassificationRequest(
    projectSlug: string,
    classificationId: string | number,
    classificationIds: (string | number)[],
    payload: ClassificationRemotePayload,
    isFeedback: boolean,
    filters: Record<string, string | number>,
  ): Observable<unknown> {
    const action = isFeedback ? 'feedback' : 'classify';
    const params = new HttpParams({
      fromObject: filters,
    });
    if (classificationIds.length > 1) {
      return this.httpClient.post(
        `/api/media-classifications/${projectSlug}/${action}/`,
        {
          classification_ids: classificationIds,
          ...payload,
        },
        {
          params,
        },
      );
    }

    return this.httpClient.post(
      `/api/media-classifications/${projectSlug}/${action}/${classificationId}/`,
      payload,
      {
        params,
      },
    );
  }

  postClassification(
    projectSlug: string,
    classificationId: string | number,
    classificationIds: (string | number)[],
    annotationState: AnnotationState,
    isFeedback: boolean,
    filters: Record<string, string | number>,
    classificationSpec: ClassificationSpec,
  ): Observable<boolean> {
    const allowedAttributes = classificationSpec.dynamic.map(
      (dynamic) => dynamic.name,
    );
    const entries = Object.values(annotationState.state);
    const dynamic = [];
    for (const value of entries) {
      const bboxes = value.bboxes;
      const rawValues = value.values;
      // const isAnimal = rawValues['observation_type'] === 'animal';

      // TODO: Consider including `ProjectData` with `hide_classification_attributes_for_non_animals`
      // TODO: as a mechanism for excluding other form fields in the case of non-Animal classifications.
      const values: Record<string, string | number | null | undefined> = {};
      for (const key of allowedAttributes) {
        values[key] = rawValues[key];
      }

      // if (!isAnimal) {
      //   // if this is not an animal, we are interested only in the observation_type
      //   values['observation_type'] = rawValues['observation_type'];
      // } else {
      //   for (const key of allowedAttributes) {
      //     values[key] = rawValues[key];
      //   }
      // }

      const v: DynamicAttr = {
        ...values,
        bboxes,
      } as DynamicAttr;
      dynamic.push(v);
    }

    if (classificationIds.length > 1 && dynamic.length > 1) {
      dynamic.splice(1, dynamic.length - 1);
    }

    if (dynamic.length === 0) {
      dynamic.push({
        observation_type: 'blank',
      });
    }

    const payload: ClassificationRemotePayload = {
      static: {},
      dynamic,
    };

    this.annotationStateService.clearErrors();

    this.inProgress$.next(true);
    return this._prepareClassificationRequest(
      projectSlug,
      classificationId,
      classificationIds,
      payload,
      isFeedback,
      filters,
    ).pipe(
      switchMap(() => {
        if (isFeedback) {
          this.translocoService
            .selectTranslate(
              'csProjects.feedbackSavedSuccessfully',
              {},
              this.scope,
            )
            .pipe(take(1), untilDestroyed(this))
            .subscribe((translation) => {
              this.toastrService.success(translation);
            });
        } else {
          this.translocoService
            .selectTranslate(
              'csProjects.classificationSavedSuccessfully',
              {},
              this.scope,
            )
            .pipe(take(1), untilDestroyed(this))
            .subscribe((translation) => {
              this.toastrService.success(translation);
            });
        }
        return this.ClassificationDataService.reloadMediaData(
          projectSlug,
          classificationId,
        );
      }),
      map(() => true),
      catchError((err) => {
        this.inProgress$.next(false);
        if (err instanceof HttpErrorResponse && err.status === 400) {
          if (err.error.detail) {
            this.toastrService.error(err.error.detail, 'Error');
          } else if (err.error.non_field_errors) {
            this.toastrService.error(
              err.error.non_field_errors.join('\n'),
              'Error',
            );
          } else {
            this.toastrService.warning(
              'Please fix the errors and try again.',
              'Error',
            );
          }
          this.annotationStateService.setErrors(err.error.dynamic || []);
          this.error$.next(err.error);
        } else if (err instanceof HttpErrorResponse) {
          this.toastrService.error(
            `An error occurred while saving the classification: ${err.message}`,
            'Error',
          );
        }
        return of(false);
      }),
      tap(() => {
        this.inProgress$.next(false);
      }),
    );
  }
}

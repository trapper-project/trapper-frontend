import { inject, Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { combineLatestWith, map, Observable, shareReplay } from 'rxjs';
import { LatLngTuple } from 'leaflet';
import { AnnotatedObject } from '@trapper/resource-annotator';
import { ProjectDataService } from './project-data.service';
export interface DeploymentData {
  deployment_code: string;
  deployment_id: string;
  location_name: string;
  coordinates: string;
  id: number;
}

export interface Owner {
  name: string;
  avatar: string;
  id: number;
}

export interface ClassifiedIn {
  name: string;
  slug: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface StaticAttrs {
  // void
}

export type DynamicAttr = Record<string, string>;

export interface Bbox {
  label: string;
  bboxes: number[][];
}

export interface Bbox2 {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface BboxWithLabel {
  label: string;
  left: number;
  top: number;
  width: number;
  height: number;
}

export type DynamicAttrWithBbox = DynamicAttr & {
  bboxes?: Bbox2 | null;
  bboxes_raw?: Array<Array<number> | null>;
  attr_confidence?: Record<string, number>;
};

export interface Classification {
  pk: number;
  static: StaticAttrs;
  dynamic: DynamicAttrWithBbox[];
}

export interface SequenceData {
  id: number;
  sequence_id: number;
}

export interface ProjectMediaData {
  pk: number;
  name: string;
  deployment: DeploymentData;
  file_name: string;
  resource_type: string;
  date_recorded: string;
  date_uploaded: string;
  owner: Owner;
  classified_in: ClassifiedIn[];
  // classifications: Classification[];
  thumbnail_url: string;
  original_url: string;
  classification: Classification;
  user_classification?: Classification | null;
  sequence: SequenceData;
  is_favorite: boolean;
  is_approved: boolean;
  is_classified_by_me: boolean;
  has_feedback: boolean;
  is_current_users_classification_approved: boolean;
}

export interface ClassificationTableRow {
  id: string;
  observation_type?: string;
  species?: string | number;
  sex?: string;
  age?: string;
}

@Injectable()
export class ClassificationDataService extends BaseBackendConnector<
  void,
  ProjectMediaData
> {
  projectDataService = inject(ProjectDataService);

  classificationSpecDictionaries$ =
    this.projectDataService.classificationSpecDictionaries$;

  classificationLoaded$: Observable<boolean> = this.data$.pipe(
    map((m) => !!m),
    shareReplay(1),
  );

  isApproved$: Observable<boolean> = this.data$.pipe(
    map((m) => {
      if (!m) {
        return false;
      }

      return m.is_approved;
    }),
    shareReplay(1),
  );

  isCurrentUserClassificationApproved$: Observable<boolean> = this.data$.pipe(
    map((m) => {
      if (!m) {
        return false;
      }

      return m.is_current_users_classification_approved;
    }),
    shareReplay(1),
  );

  hasUserClassification$: Observable<boolean> = this.data$.pipe(
    map((m) => {
      if (!m) {
        return false;
      }

      return !!m.user_classification;
    }),
    shareReplay(1),
  );

  bboxes$: Observable<BboxWithLabel[] | null> = this.data$.pipe(
    map((m) => {
      if (!m) {
        return null;
      }

      // in previews, we use user_classification bounding boxes if it exists
      const classification = m.user_classification || m.classification;

      return classification.dynamic
        .map((attr, idx) => {
          if (!attr.bboxes) {
            return null;
          }
          return {
            label: 'bbox' + (idx + 1),
            ...attr.bboxes,
          };
        })
        .filter((x) => x !== null) as BboxWithLabel[];
    }),
    shareReplay(1),
  );

  objects$: Observable<AnnotatedObject[] | null> = this.data$.pipe(
    combineLatestWith(this.classificationSpecDictionaries$),
    map(([m, dictionaries]) => {
      if (!m || !dictionaries) {
        return null;
      }
      const speciesMap = dictionaries['species'] || {};
      const observationTypeMap = dictionaries['observation_type'] || {};

      // in previews, we use user_classification bounding boxes if it exists
      const classification = m.user_classification || m.classification;

      return classification.dynamic.map((attr, idx) => {
        const observationTypeId = attr['observation_type'];
        const speciesId = attr['species'];
        const bboxId = `${idx + 1}`;
        let label = bboxId;
        if (speciesId) {
          const species = speciesMap[speciesId] || `#${speciesId}`;
          label += '. ' + species;
        } else if (observationTypeId) {
          const observation_type = observationTypeMap[observationTypeId];
          label += '. ' + observation_type;
        }
        return {
          id: '' + (idx + 1),
          bboxes: attr.bboxes_raw || [],
          label,
        };
      });
    }),
    shareReplay(1),
  );

  locationCoordinates$: Observable<LatLngTuple | null> = this.data$.pipe(
    map((data) => {
      const coords = data?.deployment.coordinates
        .split(' ')
        .slice(-2)
        .map((x) => x.replace('(', '').replace(')', ''))
        .map(parseFloat)
        .reverse();

      if (!coords) {
        return null;
      }

      return [coords[0], coords[1]] as LatLngTuple;
    }),
    shareReplay(1),
  );

  reloadMediaData(
    projectSlug: string,
    mediaSlug: string | number,
  ): Observable<void> {
    return this.get(`/api/cs/${projectSlug}/media/${mediaSlug}/`, {
      clearPreviousData: true,
    });
  }
}

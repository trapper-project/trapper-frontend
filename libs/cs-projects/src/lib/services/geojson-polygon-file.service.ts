import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import * as geojson from 'geojson';
import { ToastrService } from 'ngx-toastr';
import { parseGPX } from '@we-gold/gpxjs';

@Injectable({
  providedIn: 'root',
})
export class GeojsonPolygonFileService {
  constructor(
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}
  private _parseGeojsonPolygon(
    geojsonObject: geojson.GeoJSON,
  ): geojson.Polygon | null {
    if (!geojsonObject.type) {
      this.toastr.error(
        this.translocoService.translate(
          'polygonLoader.polygonFileNotGeojsonError',
          {},
          'csProjects',
        ),
      );
      return null;
    }

    if (geojsonObject.type === 'FeatureCollection') {
      if (geojsonObject.features.length > 1) {
        this.toastr.warning(
          this.translocoService.translate(
            'polygonLoader.polygonFileFeatureCollectionWarning',
            {},
            'csProjects',
          ),
        );
      }
      geojsonObject = geojsonObject.features[0];
    }

    if (geojsonObject.type === 'Feature') {
      geojsonObject = geojsonObject.geometry;
    }

    if (geojsonObject.type !== 'Polygon') {
      this.toastr.error(
        this.translocoService.translate(
          'polygonLoader.polygonFileNotPolygonError',
          {},
          'csProjects',
        ),
      );
      return null;
    }

    return geojsonObject as geojson.Polygon;
  }

  private _loadFromGeojsonFile(str: string): geojson.Polygon | null {
    try {
      const geojsonObject = JSON.parse(str);
      return this._parseGeojsonPolygon(geojsonObject);
    } catch (e) {
      this.toastr.error(
        this.translocoService.translate(
          'polygonLoader.polygonFileNotParsedError',
          {},
          'csProjects',
        ),
      );
      return null;
    }
  }

  private _loadFromGpxFile(str: string): geojson.Polygon | null {
    const [parsedFile, error] = parseGPX(str);
    if (error) {
      throw new Error(error.message);
    }
    const tracks = parsedFile.tracks;
    if (tracks.length === 0) {
      throw new Error('No tracks found');
    }
    const track = tracks[0];
    if (track.points.length === 0) {
      throw new Error('No points found');
    }
    const polygon: geojson.Polygon = {
      type: 'Polygon',
      coordinates: [],
    };
    const coordinates: geojson.Position[] = [];
    for (const point of track.points) {
      coordinates.push([point.longitude, point.latitude]);
    }

    // save the first point as the last point to close the polygon
    coordinates.push(coordinates[0]);

    polygon.coordinates.push(coordinates);

    return polygon;
  }

  private _isGpxFile(filename: string): boolean {
    return filename.toLowerCase().endsWith('.gpx');
  }

  private _isGeojsonFile(filename: string): boolean {
    return filename.toLowerCase().endsWith('.geojson');
  }

  selectPolygonFileFromDisk(): Promise<geojson.Polygon | null> {
    return new Promise<geojson.Polygon | null>((resolve) => {
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = '*.geojson,*.gpx';
      input.onchange = (event) => {
        const target = event.target as HTMLInputElement;
        if (!target.files?.length) {
          this.toastr.error(
            this.translocoService.translate(
              'polygonLoader.polygonFileNotSelectedError',
              {},
              'csProjects',
            ),
          );
          resolve(null);
          return;
        }
        const file = target.files[0];
        const reader = new FileReader();
        reader.onload = (event) => {
          const result = event.target?.result;
          if (typeof result !== 'string') {
            resolve(null);
            return;
          }
          try {
            let polygon: geojson.Polygon | null = null;

            if (this._isGeojsonFile(file.name)) {
              polygon = this._loadFromGeojsonFile(result);
            } else if (this._isGpxFile(file.name)) {
              polygon = this._loadFromGpxFile(result);
            } else {
              throw new Error('Unsupported file type');
            }

            resolve(polygon);
          } catch (e) {
            this.toastr.error(
              this.translocoService.translate(
                'polygonLoader.polygonFileNotParsedError',
                {},
                'csProjects',
              ),
            );
            resolve(null);
            return;
          }
        };
        reader.readAsText(file);
      };
      input.click();
    });
  }
}

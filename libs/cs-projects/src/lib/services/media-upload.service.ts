import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FileSystemFileEntry } from 'ngx-file-drop';
import {
  BehaviorSubject,
  catchError,
  finalize,
  forkJoin,
  from,
  map,
  mergeMap,
  Observable,
  of,
  ReplaySubject,
  retry,
  Subject,
  takeUntil,
  tap,
} from 'rxjs';
import * as R from 'remeda';
import { MediaUploadInitializationResponse } from './media-create-upload.service';
import { v4 as uuidv4 } from 'uuid';

const CHUNK_SIZE = 3;
const CONCURENCY = 4;

export interface SingleFileEntry {
  fileId: number;
  relativePath: string;
  fileEntry: FileSystemFileEntry;
}

function getImageFileExtension(filename: string): string {
  const parts = filename.split('.');
  return parts.length > 1 ? parts[parts.length - 1] : 'jpg';
}

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class MediaUploadService {
  totalFiles$ = new BehaviorSubject(0);
  processedFileCount = 0;
  successCount = 0;
  failureCount = 0;
  totalCount = 0;
  inProgress$ = new BehaviorSubject(false);
  cancel$ = new Subject<void>();
  finished$ = new Subject<void>();
  uploadStartTime = new Date();
  toFinalize$ = new Subject<{
    projectSlug: string;
    collectionData: MediaUploadInitializationResponse;
  }>();
  constructor(private httpClient: HttpClient) {}
  uploadMultipleFiles(
    projectSlug: string,
    files: SingleFileEntry[],
    collectionData: MediaUploadInitializationResponse,
  ): Observable<void> {
    this.totalCount = files.length;
    this.inProgress$.next(true);
    this.reset();
    const chunks = R.chunk(files, CHUNK_SIZE);
    this.uploadStartTime = new Date();

    return from(chunks).pipe(
      mergeMap(
        (fileEntries) =>
          this._uploadChunk(projectSlug, fileEntries, collectionData),
        CONCURENCY,
      ),
      map((cnt) => {
        this.processedFileCount += cnt;
      }),
      takeUntil(this.cancel$),
      finalize(() => {
        this.inProgress$.next(false);
        this.toFinalize$.next({ projectSlug, collectionData });
        if (this.processedFileCount === this.totalCount) {
          const endTime = new Date();
          console.info(
            'Upload time (seconds):',
            (endTime.getTime() - this.uploadStartTime.getTime()) / 1000,
          );
        }
      }),
    );
  }

  private _uploadChunk(
    projectSlug: string,
    fileEntries: SingleFileEntry[],
    collectionData: MediaUploadInitializationResponse,
  ): Observable<number> {
    const subs: Observable<{ file: File; relativePath: string }>[] =
      fileEntries.map((entry) => {
        const sub = new ReplaySubject<{ file: File; relativePath: string }>(1);

        entry.fileEntry.file((file: File) => {
          sub.next({ file, relativePath: entry.relativePath });
          sub.complete();
        });

        return sub;
      });

    return forkJoin(subs).pipe(
      mergeMap((files) => {
        const formData = new FormData();
        formData.append('deployment', collectionData.deployment_id.toString());
        formData.append('collection', collectionData.collection_id.toString());
        interface FileMetadata {
          name: string;
          size: number;
          type: string;
          last_modified: number;
          last_modified_date: string;
        }
        const metadata: Record<string, FileMetadata> = {};
        files.forEach(({ file, relativePath }) => {
          const file_uuid = uuidv4();
          const ext = getImageFileExtension(relativePath);
          const file_metadata = {
            name: file.name,
            size: file.size,
            type: file.type,
            last_modified: file.lastModified,
            last_modified_date: new Date(file.lastModified).toISOString(),
          };
          metadata[file_uuid] = file_metadata;

          formData.append('file', file, `${file_uuid}.${ext}`);
        });
        formData.append('metadata', JSON.stringify(metadata));

        return this.httpClient
          .post(`/api/cs/${projectSlug}/upload-media/`, formData)
          .pipe(
            retry(3),
            tap(() => {
              this.successCount += files.length;
            }),
            catchError(() => {
              this.failureCount += files.length;
              return of(files.length);
            }),
            map(() => files.length),
            untilDestroyed(this),
          );
      }),
    );
  }

  cancel() {
    this.cancel$.next();
    this.reset();
  }

  reset() {
    this.processedFileCount = 0;
    this.successCount = 0;
    this.failureCount = 0;
  }

  finalizeUpload(
    projectSlug: string,
    collectionData: MediaUploadInitializationResponse,
  ) {
    return this.httpClient
      .post(`/api/cs/${projectSlug}/complete-upload/`, {
        deployment: collectionData.deployment_id,
        collection: collectionData.collection_id,
      })
      .pipe(
        retry(3),
        map(() => {
          this.finished$.next();
        }),
      );
  }
}

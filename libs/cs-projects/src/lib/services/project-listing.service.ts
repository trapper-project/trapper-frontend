import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';

export interface Project {
  slug: string;
  name: string;
  image_background: string;
  description: string;
  tags: string[];
}

export interface ProjectListingModel {
  projects: Project[];
}

@Injectable()
export class ProjectListingService extends BaseBackendConnector<
  unknown,
  Project[]
> {
  loadProjects() {
    return this.get('/api/cs/projects/');
  }
}

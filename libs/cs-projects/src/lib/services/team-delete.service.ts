import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, tap } from 'rxjs';

@Injectable()
export class TeamDeleteService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  deleteTeam(teamId: number, projectSlug: string) {
    this.inProgress$.next(true);
    return this.httpClient.delete(`/api/teams/${projectSlug}/${teamId}/`).pipe(
      tap(() => this.inProgress$.next(false)),
      catchError((error) => {
        this.toastr.error(
          error.error.detail ||
            this.translocoService.translate(
              'teamDelete.teamDeleteUnknownError',
              {},
              'csProjects',
            ),
        );
        this.inProgress$.next(false);
        return NEVER;
      }),
      tap(() =>
        this.toastr.success(
          this.translocoService.translate(
            'teamDelete.teamDeleteSuccess',
            {},
            'csProjects',
          ),
        ),
      ),
    );
  }
}

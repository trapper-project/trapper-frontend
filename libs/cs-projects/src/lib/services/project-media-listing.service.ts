import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import * as R from 'remeda';

import {
  asyncScheduler,
  BehaviorSubject,
  catchError,
  combineLatest,
  EMPTY,
  map,
  Observable,
  shareReplay,
  startWith,
  Subject,
  switchMap,
  throttleTime,
  withLatestFrom,
} from 'rxjs';
import { Bbox2 } from './classification-data.service';
import { ProjectDataService } from './project-data.service';

export interface PaginationData {
  page: number;
  page_size: number;
  pages: number;
  count: number;
  total: number;
}
export interface Species {
  english_name: string;
  latin_name: string;
  __str__: string;
  genus: string;
  family: string;
}

export interface SpeciesDisplay {
  name: string;
  count: number;
  fullName: string;
  isOther?: boolean;
}

export interface ObservationTypeDisplay {
  name: string;
  count: number;
  isOther?: boolean;
}

export interface LocationData {
  pk: number;
  deployment_code: string;
  deployment_id: string;
  location: number;
  location_id: string;
  start_date: Date;
  end_date: Date;
  owner: string;
  owner_profile: string;
  research_project: string;
  tags: string[];
  correct_setup: boolean;
  correct_tstamp: boolean;
  detail_data: string;
  update_data: string;
  delete_data: string;
}

export interface Owner {
  name: string;
  avatar: string;
}

export interface MediaListingItemDeploymentData {
  pk: number;
  deployment_code: string;
  deployment_id: string;
}

export interface Sequence {
  id: number;
  sequence_id: number;
}

export interface MediaListingItem {
  date_recorded: string;
  pk: number;
  name: string;
  thumbnail_url: string;
  original_url: string;
  location_name: string;
  observation_type: string[];
  species: Species[];
  species_display?: SpeciesDisplay[];
  observation_type_display?: ObservationTypeDisplay[];
  owner: Owner;
  deployment?: MediaListingItemDeploymentData;
  bboxes: Array<Bbox2 | null>;
  bboxes_raw?: Array<number[] | null>[];
  sequence?: Sequence;
  next_classification_sequence?: number | null;
  previous_classification_sequence?: number | null;
  is_favorite: boolean;
  is_approved: boolean;
  is_classified_by_me: boolean;
  has_feedback: boolean;
  resource_type: string;
}

export interface MediaListingDto {
  pagination: PaginationData;
  results: MediaListingItem[];
}

const defaultFilters: Record<string, string | null> = {};

@Injectable()
export class ProjectMediaListingService {
  inProgress$ = new BehaviorSubject(false);
  dataRaw$ = new BehaviorSubject<MediaListingDto | null>(null);
  filter$ = new BehaviorSubject(defaultFilters);
  reload$ = new Subject<void>();
  projectSlug$ = new Subject<string>();

  error$ = new BehaviorSubject<string | null>(null);

  data$ = this.dataRaw$.pipe(
    withLatestFrom(this.projectDataService.classificationSpecDictionaries$),
    map(([data, classificationSpecDictionaries]) => {
      if (!data || !classificationSpecDictionaries) {
        return null;
      }
      const processed = data.results.map((item) => {
        const species = item.species;

        const groupped: Record<string, Species[]> = R.groupBy(
          species,
          (item) => item.english_name,
        );

        const sorted = R.sortBy(
          Object.entries(groupped),
          ([, v]) => v.length,
        ).reverse();

        const finalSpecies: SpeciesDisplay[] = [];
        if (sorted.length > 3) {
          const topSpecies: SpeciesDisplay[] = sorted
            .slice(0, 2)
            .map(([k, v]) => {
              return {
                name: k,
                count: v.length,
                fullName: v[0].__str__,
              };
            });

          const remainingSpecies = sorted.slice(2);

          const otherSpecies: SpeciesDisplay = {
            name: this.translocoService.translate(
              'mediaListing.otherSpecies',
              {},
              'csProjects',
            ),
            fullName: this.translocoService.translate(
              'mediaListing.otherSpecies',
              {},
              'csProjects',
            ),
            count: remainingSpecies.reduce((acc, [, v]) => acc + v.length, 0),
            isOther: true,
          };
          finalSpecies.push(...topSpecies, otherSpecies);
        } else {
          finalSpecies.push(
            ...sorted.map(([k, v]) => {
              return {
                name: k,
                count: v.length,
                fullName: v[0].__str__,
              };
            }),
          );
        }

        const observationTypes = item.observation_type;

        const grouppedObservationTypes: Record<string, string[]> = R.groupBy(
          observationTypes,
          (item) => item,
        );

        const sortedObservationTypes = R.sortBy(
          Object.entries(grouppedObservationTypes),
          ([, v]) => v.length,
        ).reverse();

        const finalObservationTypes: ObservationTypeDisplay[] = [];
        if (sortedObservationTypes.length > 3) {
          const topObservationTypes: ObservationTypeDisplay[] =
            sortedObservationTypes.slice(0, 2).map(([k, v]) => {
              const name =
                classificationSpecDictionaries['observation_type'][k];
              return {
                key: k,
                name,
                count: v.length,
              };
            });

          const remainingObservationTypes = sortedObservationTypes.slice(2);

          const otherObservationTypes: ObservationTypeDisplay = {
            name: this.translocoService.translate(
              'mediaListing.otherObservationTypes',
              {},
              'csProjects',
            ),
            count: remainingObservationTypes
              .map(([, v]) => v.length)
              .reduce((a, b) => a + b, 0),
            isOther: true,
          };
          finalObservationTypes.push(
            ...topObservationTypes,
            otherObservationTypes,
          );
        } else {
          finalObservationTypes.push(
            ...sortedObservationTypes.map(([k, v]) => {
              const name =
                classificationSpecDictionaries['observation_type'][k];
              return {
                key: k,
                name,
                count: v.length,
              };
            }),
          );
        }

        return {
          ...item,
          species_display: finalSpecies,
          observation_type_display: finalObservationTypes,
        };
      });
      return {
        ...data,
        results: processed,
      };
    }),
    shareReplay(1),
  );

  pagination$ = this.data$.pipe(
    map((dto) => dto?.pagination || null),
    shareReplay(1),
  );

  constructor(
    private httpClient: HttpClient,
    private translocoService: TranslocoService,
    private projectDataService: ProjectDataService,
  ) {}

  connect(): Observable<void> {
    return combineLatest([
      this.reload$.pipe(startWith(null)),
      this.filter$,
    ]).pipe(
      map(([, filters]) => filters),
      throttleTime(500, asyncScheduler, { leading: true, trailing: true }),
      withLatestFrom(
        this.projectSlug$,
        this.projectDataService.classificationSpecFilters$,
      ),
      switchMap(([filters, slug, classificationSpecFilters]) => {
        const params: Record<string, string | string[]> = {};
        Object.entries(filters).forEach(([k, v]) => {
          if (!v) {
            return;
          }
          const spec = classificationSpecFilters.find((f) => f.name === k);

          if (!Array.isArray(v) && spec?.is_multiselect) {
            params[k] = v.split(',');
          } else {
            params[k] = v;
          }
        });
        if (
          params['page_size'] &&
          ![10, 50, 100, 200].includes(+params['page_size'])
        ) {
          delete params['page_size'];
        }
        this.inProgress$.next(true);
        this.dataRaw$.next(null);
        this.error$.next(null);
        return this.httpClient
          .get<MediaListingDto>(`/api/cs/${slug}/media/`, {
            params,
          })
          .pipe(
            catchError((err) => {
              this.inProgress$.next(false);
              this.dataRaw$.next(null);
              if (err instanceof HttpErrorResponse) {
                if (typeof err.error === 'string') {
                  this.error$.next('An error occurred. Please try again.');
                  return EMPTY;
                }
                const error = Object.entries(err.error).reduce(
                  (acc, [k, v]) => {
                    if (k !== 'detail') {
                      acc += `${k}: ${v}`;
                    } else {
                      acc += v;
                    }
                    return acc;
                  },
                  '',
                );
                this.error$.next(error);
              } else {
                this.error$.next('Unknown error');
              }
              return EMPTY;
            }),
          );
      }),
      map((data) => {
        this.dataRaw$.next(data);
        this.inProgress$.next(false);
      }),
    );
  }
}

import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';

export interface TeamMemberInvitationPayload {
  users: number[] | null;
  motivation: string;
}

@UntilDestroy()
@Injectable()
export class TeamMebmerInviteService extends BaseBackendFormConnector<
  ModelFormGroupControls<TeamMemberInvitationPayload>,
  void
> {
  public currentProjectSlug?: string;
  public currentTeamId?: string;
  get endpoint() {
    return `/api/teams/${this.currentTeamId}/members/invite/`;
  }

  constructor(fb: NonNullableFormBuilder) {
    super(
      fb.group({
        users: fb.control<number[] | null>([]),
        motivation: fb.control(''),
      }),
    );
  }

  setProjectSlug(slug: string) {
    this.currentProjectSlug = slug;
  }
  setTeamId(id: string) {
    this.currentTeamId = id;
  }
}

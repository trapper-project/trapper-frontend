import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';

export interface GeoJsonGeometry {
  type: string;
  coordinates: number[][][];
}

export interface TeamEditPayload {
  name: string;
  description: string;
  polygon: GeoJsonGeometry | null;
}

export interface TeamMember {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  name: string;
}

export type MembershipRole = Record<string, string>;
export type MembershipStatus = Record<string, string>;

export interface TeamMembership {
  id: number;
  user: TeamMember;
  role: MembershipRole;
  status: MembershipStatus;
  motivation: string;
  date_created: string;
  date_modified: string;
}

export interface TeamEditResponse {
  id: number;
  name: string;
  description: string;
  team_leader: TeamMember;
  members: TeamMembership[];
  can_manage: boolean;
  date_created: string;
  date_modified: string;
  polygon?: GeoJsonGeometry;
}

@UntilDestroy()
@Injectable()
export class TeamEditService extends BaseBackendFormConnector<
  ModelFormGroupControls<TeamEditPayload, GeoJsonGeometry>,
  TeamEditResponse
> {
  public currentProjectSlug?: string;
  public currentTeamId?: string;
  get endpoint() {
    return `/api/teams/${this.currentProjectSlug}/${this.currentTeamId}/`;
  }

  constructor(fb: NonNullableFormBuilder) {
    super(
      fb.group({
        name: fb.control(''),
        description: fb.control(''),
        polygon: fb.control<GeoJsonGeometry | null>(null),
      }),
    );
  }

  setProjectSlug(slug: string) {
    this.currentProjectSlug = slug;
  }
  setTeamId(id: string) {
    this.currentTeamId = id;
  }
}

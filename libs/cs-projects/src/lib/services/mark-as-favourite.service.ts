import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

@Injectable()
export class MarkAsFavouriteService {
  constructor(private httpClient: HttpClient) {}

  markAsFavourite(
    projectSlug: string,
    classificationId: number | string,
    value: boolean,
  ): Observable<void> {
    return this.httpClient
      .post(
        `/api/media-classifications/${projectSlug}/mark-as-favorite/${classificationId}/`,
        {
          favorite: value,
        },
      )
      .pipe(
        map(() => {
          return;
        }),
      );
  }
}

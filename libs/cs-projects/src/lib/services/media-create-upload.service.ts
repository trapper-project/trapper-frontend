import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateRange } from '@trapper/ui';
import { catchError, EMPTY, Observable } from 'rxjs';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function setFormErrors(formGroup: FormGroup, errors: any) {
  Object.keys(errors).forEach((key) => {
    const control = formGroup.get(key);
    const value: unknown = errors[key];
    if (!control || !value) {
      return;
    }

    if (control instanceof FormGroup) {
      setFormErrors(control, value);
    } else {
      control.setErrors({ serverErrors: value });
    }
  });
}

export interface CreateLocationModel {
  name: string | null;
  latitude: string | null;
  longitude: string | null;
}

export interface CreateDeploymentModel {
  deployment_code: string;
  start_date: string;
  end_date: string;
  bait_type: number;
  camera_id: string;
  camera_model: string;
  camera_interval: number;
  camera_height: number;
  camera_tilt: number;
  camera_heading: number;
  detection_distance: number;
  timestamp_issues: boolean;
  feature_type: string;
  habitat: string;
  comments: string;
  is_incomplete: boolean;
}

export interface MediaUploadInitializationModel {
  location: CreateLocationModel;
  deployment: CreateDeploymentModel;
}

export interface MediaUploadInitializationResponse {
  deployment_id: string;
  collection_id: string;
}

type AdditionalFieldsT =
  | 'camera_id'
  | 'camera_model'
  | 'camera_interval'
  | 'camera_height'
  | 'camera_tilt'
  | 'camera_heading'
  | 'detection_distance'
  | 'timestamp_issues'
  | 'feature_type'
  | 'habitat';

@Injectable()
export class MediaCreateUploadService {
  formGroup = new FormGroup({
    location: new FormGroup({
      name: new FormControl(''),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
    }),
    deployment: new FormGroup({
      deployment_code: new FormControl(''),
      start_date: new FormControl(undefined),
      end_date: new FormControl(undefined),
      date_range: new FormControl<DateRange | undefined>(undefined),
      bait_type: new FormControl<string | undefined>(undefined),
      camera_id: new FormControl(''),
      camera_model: new FormControl(''),
      camera_interval: new FormControl(undefined),
      camera_height: new FormControl(undefined),
      camera_tilt: new FormControl(undefined),
      camera_heading: new FormControl(undefined),
      detection_distance: new FormControl(undefined),
      timestamp_issues: new FormControl(false),
      feature_type: new FormControl(undefined),
      habitat: new FormControl(''),
    }),
    location_id: new FormControl<string | undefined>(undefined),
    deployment_id: new FormControl<string | undefined>(undefined),
    is_private: new FormControl(true),
    comments: new FormControl(''),
    is_incomplete: new FormControl(false),
  });

  formInitialValues = this.formGroup.getRawValue();

  additionalFields: AdditionalFieldsT[] = [
    'camera_id',
    'camera_model',
    'camera_interval',
    'camera_height',
    'camera_tilt',
    'camera_heading',
    'detection_distance',
    'timestamp_issues',
    'feature_type',
    'habitat',
  ];

  constructor(private httpClient: HttpClient) {}

  disableAdditionalFields() {
    this.additionalFields.forEach((field) => {
      this.formGroup
        .get(`deployment.${field}`)
        ?.setValue(this.formInitialValues.deployment[field], {
          emitEvent: false,
        });
      this.formGroup.get(`deployment.${field}`)?.disable();
    });
  }

  enableAdditionalFields() {
    this.additionalFields.forEach((field) => {
      this.formGroup.get(`deployment.${field}`)?.enable();
    });
  }

  initializeUpload(
    projectSlug: string,
    config: { createNewLocation: boolean; createNewDeployment: boolean },
  ): Observable<MediaUploadInitializationResponse> {
    this.formGroup.setErrors(null);
    this.formGroup.markAsPristine();
    const rawPayload = this.formGroup.value;
    const payload = {
      ...rawPayload,
      location: config.createNewLocation ? rawPayload.location : undefined,
      deployment: config.createNewDeployment
        ? {
            ...rawPayload.deployment,
            start_date:
              rawPayload?.deployment?.date_range?.start?.toISOString() || null,
            end_date:
              rawPayload?.deployment?.date_range?.end?.toISOString() || null,
            date_range: undefined,
          }
        : undefined,
      location_id:
        !config.createNewLocation && config.createNewDeployment
          ? rawPayload.location_id
          : undefined,
      deployment_id: !config.createNewDeployment
        ? rawPayload.deployment_id
        : undefined,
    };
    return this.httpClient
      .post<MediaUploadInitializationResponse>(
        `/api/cs/${projectSlug}/create-upload/`,
        payload,
      )
      .pipe(
        catchError((err) => {
          setFormErrors(this.formGroup, err.error);
          const start_date_errors = err.error?.deployment?.start_date || [];
          const end_date_errors = err.error?.deployment?.end_date || [];
          const uniqueDateRangeErrors = [
            ...new Set([...start_date_errors, ...end_date_errors]),
          ];
          if (uniqueDateRangeErrors.length > 0) {
            this.formGroup
              .get('deployment.date_range')
              ?.setErrors({ serverErrors: uniqueDateRangeErrors });
          }
          this.scrollToFirstError();
          return EMPTY;
        }),
      );
  }

  resetForm() {
    this.formGroup.reset(this.formInitialValues);
  }

  clearSelectedLocation() {
    this.formGroup.get('location_id')?.setValue(undefined);
  }

  clearSelectedDeployment() {
    this.formGroup.get('deployment_id')?.setValue(undefined);
  }

  clearDeploymentForm() {
    this.formGroup.get('deployment')?.reset(this.formInitialValues.deployment);
  }

  clearLocationForm() {
    this.formGroup.get('location')?.reset(this.formInitialValues.location);
  }

  scrollToFirstError() {
    setTimeout(() => {
      const firstError = document.querySelector(
        'trapper-ui-form-field:has(.ng-invalid)',
      );
      if (firstError) {
        firstError.scrollIntoView({ behavior: 'smooth' });
      }
    }, 100);
  }
}

import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';
import { DateRange } from '@trapper/ui';

export interface DeploymentEditModel {
  deployment_id?: string | null;
  deployment_code?: string | null;
  location_id?: number;
  location?: {
    name: string;
    latitude: number;
    longitude: number;
  };
  start_date?: string;
  end_date?: string;
  owner?: {
    id: number;
    name: string;
    avatar: string;
  };
  camera_id?: string;
  camera_model?: string;
  camera_interval?: number;
  camera_height?: string;
  camera_tilt?: number;
  camera_heading?: number;
  detection_distance?: string;
  feature_type?: string;
  habitat?: string;
  correct_tstamp?: boolean;
  comments?: string;
  status?: number;
  is_incomplete?: boolean;
}

export interface Location {
  name: string | null;
  latitude: string | null;
  longitude: string | null;
}

export interface DeploymentEditModePayload {
  deployment_code: string;
  location_id: null | string;
  location: Location;
  date_range: DateRange | null;
  start_date: string;
  end_date: string;
  camera_id: string;
  camera_model: string | null;
  camera_interval: number;
  camera_height: string | null;
  camera_tilt: number;
  camera_heading: number;
  detection_distance: string | null;
  feature_type: string | null;
  habitat: string | null;
  comments: string | null;
  is_incomplete: boolean;
  timestamp_issues: boolean;
  is_private: boolean;
  bait_type: string | null;
}

export interface DeploymentExtraData extends Record<string, unknown> {
  media: {
    pk: number;
    thumbnail: string;
  }[];
  resources_count: number;
  pk: number | string;
  deployment_id: string;
}

@UntilDestroy()
@Injectable()
export class DeploymentEditService extends BaseBackendFormConnector<
  ModelFormGroupControls<DeploymentEditModePayload>,
  DeploymentExtraData
> {
  private currentProjectSlug?: string;
  private currentDeploymentId?: string;
  get endpoint() {
    return `/api/deployments/${this.currentProjectSlug}/${this.currentDeploymentId}/`;
  }

  constructor(fb: NonNullableFormBuilder) {
    super(
      fb.group({
        deployment_code: fb.control(''),
        location_id: fb.control<string | null>(null),
        location: fb.group({
          name: fb.control<string | null>(null),
          latitude: fb.control<string | null>(null),
          longitude: fb.control<string | null>(null),
        }),
        date_range: fb.control<DateRange | null>(null),
        start_date: fb.control(''),
        end_date: fb.control(''),
        camera_id: fb.control(''),
        camera_model: fb.control<string | null>(null),
        camera_interval: fb.control(0),
        camera_height: fb.control<string | null>(null),
        camera_tilt: fb.control(0),
        camera_heading: fb.control(0),
        detection_distance: fb.control<string | null>(null),
        feature_type: fb.control<string | null>(null),
        habitat: fb.control<string | null>(null),
        comments: fb.control<string | null>(null),
        is_incomplete: fb.control(false),
        timestamp_issues: fb.control(false),
        is_private: fb.control(true),
        bait_type: fb.control<string | null>(null),
      }),
    );
  }

  override patchFormValue(value: Partial<DeploymentEditModePayload>) {
    this.formGroup.patchValue({
      ...value,
      location_id: value.location_id?.toString() || null,
      bait_type: value.bait_type?.toString() || null,
      date_range: {
        start: value.start_date ? new Date(value.start_date) : null,
        end: value.end_date ? new Date(value.end_date) : null,
      },
    });
  }

  override get payload() {
    const value = this.formGroup.value;
    return {
      ...value,
      start_date: value.date_range?.start?.toISOString() || '',
      end_date: value.date_range?.end?.toISOString() || '',
    };
  }

  setProjectSlug(slug: string | undefined) {
    this.currentProjectSlug = slug;
  }

  setDeploymentId(id: string | undefined) {
    this.currentDeploymentId = id;
  }

  reset() {
    this.currentProjectSlug = undefined;
    this.currentDeploymentId = undefined;
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  Subject,
  bufferTime,
  combineLatest,
  filter,
  finalize,
  map,
  shareReplay,
  takeUntil,
  tap,
} from 'rxjs';
import { MediaListingItem, Sequence } from './project-media-listing.service';
import { sortBy, uniqBy } from 'remeda';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

export interface MediaRollDto {
  results: MediaListingItem[];
  next: string | null;
  previous: string | null;
}

export interface ImageRollitem extends MediaListingItem {
  selected: boolean;
  sequenceHighlightVariant: number | null;
}

export interface SequenceWithImages {
  id: number;
  sequence_id: number;
  images: ImageRollitem[];
}

interface SequenceData {
  id: number;
  sequence_id: number;
  classifications: number[];
}

@UntilDestroy()
@Injectable()
export class ImageRollService {
  page_size = 30;
  fetchingNextPage = false;
  fetchingPreviousPage = false;
  reloading = false;
  canFetchNextPage = true;
  canFetchPreviousPage = true;
  currentFilters: Record<string, string | null> = {};
  firstPage = 1;
  lastPage = 1;
  projectSlug = '';
  data: MediaListingItem[] = [];
  classificationPageSource = new Map<number, { pk: number; rev: boolean }>();
  classificationIdsFromPage = new Map<string, number[]>();
  loadedIds = new Set<number>();
  data$ = new BehaviorSubject<ImageRollitem[]>([]);
  dataBySequenceRaw$ = new BehaviorSubject<SequenceWithImages[]>([]);
  rollReloaded$ = new Subject<void>();
  rollReloadTriggered$ = new Subject<void>();
  filtersOrProjectChanged$ = new Subject<void>();

  sequenceReloadRequested$ = new Subject<number[]>();

  groupClassificationMode$ = new BehaviorSubject<boolean>(false);

  previousPageLoadingStarted$ = new Subject<{
    previousFirstClassificationId: number;
  }>();
  previousPageLoadingFinished$ = new Subject<{
    previousFirstClassificationId: number;
    currentFirstClassificationId: number;
  }>();

  currentClassificationId: number | null = null;
  filtersSet = false;

  selectedItems = new Set<number>();
  itemSelectedByEntering: number | null = null;

  sequenceAllImageIds: Map<number, number[]> = new Map();
  sequenceAllImageIds$ = new BehaviorSubject<Map<number, number[]>>(new Map());

  currentClassificationIdChanged$ = new BehaviorSubject<number | null>(null);

  requestsInFlight: Set<Record<string, string>> = new Set();

  hasPreviousSequence$ = combineLatest([
    this.data$,
    this.currentClassificationIdChanged$,
  ]).pipe(
    map(([data, currentClassificationId]) => {
      if (!currentClassificationId) {
        return false;
      }
      const currentClassification = data.find(
        (item) => item.pk === currentClassificationId,
      );

      if (
        !currentClassification ||
        !currentClassification.previous_classification_sequence
      ) {
        return false;
      }

      return true;
    }),
    shareReplay(1),
  );

  hasNextSequence$ = combineLatest([
    this.data$,
    this.currentClassificationIdChanged$,
  ]).pipe(
    map(([data, currentClassificationId]) => {
      if (!currentClassificationId) {
        return false;
      }

      const currentClassification = data.find(
        (item) => item.pk === currentClassificationId,
      );

      if (
        !currentClassification ||
        !currentClassification.next_classification_sequence
      ) {
        return false;
      }

      return true;
    }),
    shareReplay(1),
  );

  hasNextClassification$ = combineLatest([
    this.data$,
    this.currentClassificationIdChanged$,
  ]).pipe(
    map(([data, currentClassificationId]) => {
      if (!currentClassificationId) {
        return false;
      }
      const index = data.findIndex(
        (item) => item.pk === currentClassificationId,
      );
      return index < data.length - 1;
    }),
    shareReplay(1),
  );

  hasPreviousClassification$ = combineLatest([
    this.data$,
    this.currentClassificationIdChanged$,
  ]).pipe(
    map(([data, currentClassificationId]) => {
      if (!currentClassificationId) {
        return false;
      }
      const index = data.findIndex(
        (item) => item.pk === currentClassificationId,
      );
      return index > 0;
    }),
    shareReplay(1),
  );

  dataBySequence$ = combineLatest([
    this.dataBySequenceRaw$,
    this.sequenceAllImageIds$,
  ]).pipe(
    map(([dataBySequence, sequenceAllImageIds]) => {
      return dataBySequence.map((item) => {
        return {
          ...item,
          isSequenceDataLoaded: sequenceAllImageIds.has(item.id),
        };
      });
    }),
    shareReplay(1),
  );

  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ) {
    this.filtersOrProjectChanged$.pipe(untilDestroyed(this)).subscribe(() => {
      this.requestsInFlight.clear();
    });

    this.sequenceReloadRequested$
      .pipe(
        bufferTime(500),
        filter((v) => v.length > 0),
        untilDestroyed(this),
      )
      .subscribe((sequenceIdLists) => {
        const uniqueSequenceIds = Array.from(new Set(sequenceIdLists.flat()));
        this._fetchSequencesData(uniqueSequenceIds).subscribe(
          (sequenceData) => {
            for (const item of sequenceData) {
              this.sequenceAllImageIds.set(
                Number(item.id),
                item.classifications,
              );
            }
            this.sequenceAllImageIds$.next(this.sequenceAllImageIds);
          },
        );
      });
  }

  private _fetchSequenceData(sequenceId: number): Observable<SequenceData> {
    const params: Record<string, string> = {};
    for (const [k, v] of Object.entries(this.currentFilters)) {
      if (v) {
        params[k] = v;
      }
    }
    return this.httpClient.get<SequenceData>(
      `/api/media-classifications/${this.projectSlug}/sequence/${sequenceId}/`,
      {
        params,
      },
    );
  }

  private _fetchSequencesData(
    sequenceIds: number[],
  ): Observable<SequenceData[]> {
    const params: Record<string, string> = {};
    for (const [k, v] of Object.entries(this.currentFilters)) {
      if (v) {
        params[k] = v;
      }
    }
    params['id'] = sequenceIds.join(',');
    return this.httpClient.get<SequenceData[]>(
      `/api/media-classifications/${this.projectSlug}/sequences/`,
      {
        params,
      },
    );
  }

  private _fetchPage(
    pk: number,
    rev: boolean,
    page_size: number | undefined = undefined,
  ): Observable<MediaRollDto> {
    if (page_size === undefined) {
      page_size = this.page_size;
    }
    const params: Record<string, string> = {};
    for (const [k, v] of Object.entries(this.currentFilters)) {
      if (v) {
        params[k] = v;
      }
    }
    let cursorData = `p=${pk}`;
    if (rev) {
      cursorData += '&r=1';
    }

    const cursor = window.btoa(cursorData);
    params['cursor'] = cursor;
    params['page_size'] = String(page_size);

    if (this.requestsInFlight.has(params)) {
      return EMPTY;
    }

    this.requestsInFlight.add(params);

    return this.httpClient
      .get<MediaRollDto>(`/api/cs/${this.projectSlug}/media/scroll/`, {
        params,
      })
      .pipe(
        tap((dto) => {
          const classificationIds = dto.results.map((item) => item.pk);
          this.classificationIdsFromPage.set(`${pk}-${rev}`, classificationIds);
          for (const item of dto.results) {
            this.classificationPageSource.set(item.pk, {
              pk,
              rev,
            });
          }
        }),
        finalize(() => {
          this.requestsInFlight.delete(params);
        }),
        takeUntil(this.filtersOrProjectChanged$),
      );
  }

  reloadClassificationRoll(): Observable<void> {
    this.canFetchNextPage = true;
    this.canFetchPreviousPage = true;
    this.fetchingNextPage = false;
    this.fetchingPreviousPage = false;
    this.reloading = true;
    this.data = [];
    this.loadedIds = new Set();
    this.classificationPageSource = new Map();
    this.classificationIdsFromPage = new Map();
    this._emitNewData();
    if (!this.currentClassificationId || !this.projectSlug) {
      return EMPTY;
    }
    this.rollReloadTriggered$.next();
    return this._fetchPage(this.currentClassificationId, false).pipe(
      takeUntil(this.rollReloadTriggered$),
      map((dto) => {
        this.reloading = false;
        this._updateData(dto?.results || []);
        this.fetchNextPage().subscribe();
        this.fetchPreviousPage().subscribe();
      }),
      tap(() => {
        this.rollReloaded$.next();
      }),
    );
  }

  setFilters(filters: Record<string, string | null>) {
    this.currentFilters = filters;
    this.firstPage = 1;
    this.lastPage = 1;
    this.filtersSet = true;
    this.filtersOrProjectChanged$.next();

    if (this.currentClassificationId && this.projectSlug) {
      this.reloadClassificationRoll().subscribe();
    }
  }

  getFilters() {
    return this.currentFilters;
  }

  setClassification(classificationId: number) {
    this.currentClassificationId = classificationId;
    this.currentClassificationIdChanged$.next(classificationId);

    if (
      this.itemSelectedByEntering !== null &&
      classificationId !== this.itemSelectedByEntering
    ) {
      // we do not deselect the item if we are in group classification mode
      if (this.selectedItems.size === 1) {
        this.selectedItems.delete(this.itemSelectedByEntering);
      }
      this.itemSelectedByEntering = null;
    }

    this.selectedItems.add(classificationId);
    this.itemSelectedByEntering = classificationId;

    this._emitNewData();

    if (!this.filtersSet) {
      return;
    }

    if (!this.loadedIds.has(classificationId)) {
      this.reloadClassificationRoll().subscribe();
      return;
    }

    const index = this.data.findIndex((item) => item.pk === classificationId);
    if (index === -1) {
      return;
    }

    if (index + this.page_size >= this.data.length) {
      this.fetchNextPage().subscribe();
    }

    if (index - this.page_size <= 0) {
      this.fetchPreviousPage().subscribe();
    }
  }

  setClassificationAndProject(classificationId: number, projectSlug: string) {
    if (this.projectSlug !== projectSlug) {
      this.filtersOrProjectChanged$.next();
    }
    this.projectSlug = projectSlug;

    this.setClassification(classificationId);
  }

  reloadUpdatedClassifications(classificationIds: number[]) {
    const classificationIdToIndex = new Map<number, number>();
    for (const [i, item] of this.data.entries()) {
      classificationIdToIndex.set(item.pk, i);
    }
    let firstClassification = classificationIds[0];
    let firstClassifcationIndex =
      classificationIdToIndex.get(firstClassification);
    if (firstClassifcationIndex === undefined) {
      return;
    }
    let lastClassificationIndex = firstClassifcationIndex;
    for (const classificationId of classificationIds) {
      const index = classificationIdToIndex.get(classificationId);
      if (index === undefined) {
        continue;
      }
      if (index < firstClassifcationIndex) {
        firstClassifcationIndex = index;
        firstClassification = classificationId;
      }
      if (index > lastClassificationIndex) {
        lastClassificationIndex = index;
      }
    }

    const minimumPageSize =
      lastClassificationIndex - firstClassifcationIndex + 1;

    if (minimumPageSize < 0) {
      return;
    }

    this._fetchPage(firstClassification, false, minimumPageSize)
      .pipe(
        takeUntil(this.rollReloadTriggered$),
        map((dto) => {
          this._updateData(dto?.results || [], classificationIds);
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  _updateData(newData: MediaListingItem[], staleDataToRemove: number[] = []) {
    const staleDataToRemoveSet = new Set(staleDataToRemove);
    this.data = this.data.filter((item) => !staleDataToRemoveSet.has(item.pk));
    this.data = [...newData, ...this.data];
    this.loadedIds = new Set(this.data.map((item) => item.pk));

    this.data = sortBy(
      this.data,
      (item) => new Date(item.date_recorded),
      // (item) => item.name,
      (item) => item.pk,
    );
    this.data = uniqBy(this.data, (item) => item.pk);

    for (const item of this.data) {
      if (!item.sequence) {
        continue;
      }
      if (!this.sequenceAllImageIds.has(item.sequence.id)) {
        continue;
      }
      this.sequenceAllImageIds.delete(item.sequence.id);
    }

    const allSequenceIds = [];
    for (const item of this.data) {
      if (!item.sequence) {
        continue;
      }
      allSequenceIds.push(item.sequence.id);
    }

    const uniqueSequenceIds = Array.from(new Set(allSequenceIds));
    this.sequenceReloadRequested$.next(uniqueSequenceIds);

    this._emitNewData();
  }

  _emitNewData() {
    const data = this.data.map((item) => ({
      ...item,
      selected: this.selectedItems.has(item.pk),
      sequenceHighlightVariant: item.sequence
        ? item.sequence.sequence_id % 4
        : null,
    }));
    const dataBySequence: Record<number, ImageRollitem[]> = {};
    const sequenceData: Record<number, Sequence> = {};
    for (const item of data) {
      const sequence = item.sequence || {
        id: 0,
        sequence_id: 0,
      };
      if (!dataBySequence[sequence.id]) {
        dataBySequence[sequence.id] = [];
        sequenceData[sequence.id] = sequence;
      }
      dataBySequence[sequence.id].push(item);
    }

    const dataBySequenceArray = Object.entries(dataBySequence).map(
      ([sequence_pk, images]) => ({
        id: Number(sequence_pk),
        sequence_id: sequenceData[Number(sequence_pk)].sequence_id,
        images,
      }),
    );
    dataBySequenceArray.sort((a, b) => a.sequence_id - b.sequence_id);
    this.dataBySequenceRaw$.next(dataBySequenceArray);

    this.data$.next(data);

    const groupClassificationMode = this.selectedItems.size > 1;
    if (groupClassificationMode !== this.groupClassificationMode$.value) {
      this.groupClassificationMode$.next(groupClassificationMode);
    }
  }

  fetchNextPage(): Observable<void> {
    if (this.fetchingNextPage || !this.canFetchNextPage) {
      return EMPTY;
    }
    if (!this.currentClassificationId || !this.projectSlug) {
      return EMPTY;
    }
    const lastItem = this.data[this.data.length - 1];
    if (!lastItem) {
      return EMPTY;
    }

    this.fetchingNextPage = true;
    return this._fetchPage(lastItem.pk, false).pipe(
      takeUntil(this.rollReloadTriggered$),
      map((dto) => {
        this._updateData(dto?.results || []);
        this.fetchingNextPage = false;
        if (dto?.results?.length === 0 || dto?.next === null) {
          this.canFetchNextPage = false;
        }
      }),
    );
  }

  fetchPreviousPage(): Observable<void> {
    if (this.fetchingPreviousPage || !this.canFetchPreviousPage) {
      return EMPTY;
    }
    if (!this.currentClassificationId || !this.projectSlug) {
      return EMPTY;
    }
    const firstItem = this.data[0];
    if (!firstItem) {
      return EMPTY;
    }

    this.fetchingPreviousPage = true;
    const previousFirstClassificationId = firstItem.pk;
    this.previousPageLoadingStarted$.next({
      previousFirstClassificationId,
    });
    return this._fetchPage(firstItem.pk, true).pipe(
      takeUntil(this.rollReloadTriggered$),
      map((dto) => {
        this._updateData(dto?.results || []);
        this.fetchingPreviousPage = false;
        if (dto?.results?.length === 0 || dto?.previous === null) {
          this.canFetchPreviousPage = false;
        }
        const currentFirstClassificationId = this.data[0].pk;

        this.previousPageLoadingFinished$.next({
          previousFirstClassificationId,
          currentFirstClassificationId,
        });
      }),
    );
  }

  goToClassification(pk: number) {
    if (!this.projectSlug) {
      return;
    }
    return this.router.navigate(
      ['/cs/projects/', this.projectSlug, 'classify', pk],
      {
        queryParamsHandling: 'preserve',
      },
    );
  }

  getNextNotSelectedClassificationId(): number | null {
    if (!this.currentClassificationId || !this.projectSlug) {
      return null;
    }
    const index = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    if (index === -1 || index === this.data.length - 1) {
      return null;
    }
    for (let i = index + 1; i < this.data.length; i++) {
      if (!this.selectedItems.has(this.data[i].pk)) {
        return this.data[i].pk;
      }
    }
    return null;
  }

  goToNextClassification() {
    if (!this.currentClassificationId || !this.projectSlug) {
      return;
    }
    const index = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    if (index === -1 || index === this.data.length - 1) {
      return;
    }
    const nextItem = this.data[index + 1];
    return this.goToClassification(nextItem.pk);
  }

  goToPreviousClassification() {
    if (!this.currentClassificationId || !this.projectSlug) {
      return;
    }
    const index = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    if (index === -1 || index === 0) {
      return;
    }
    const previousItem = this.data[index - 1];
    return this.goToClassification(previousItem.pk);
  }

  hasPreviousClassification() {
    if (!this.currentClassificationId || !this.projectSlug) {
      return false;
    }
    const index = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    return index > 0;
  }

  hasNextClassification() {
    if (!this.currentClassificationId || !this.projectSlug) {
      return false;
    }
    const index = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    return index < this.data.length - 1;
  }

  toggleItemSelection(pk: number) {
    if (this.selectedItems.has(pk)) {
      this.selectedItems.delete(pk);
    } else {
      this.selectedItems.add(pk);
    }

    if (
      this.currentClassificationId &&
      !this.selectedItems.has(this.currentClassificationId)
    ) {
      this.selectedItems.add(pk);
    }

    this._emitNewData();
  }

  selectRange(pk: number) {
    if (!this.currentClassificationId) {
      return;
    }
    const currentClassificationIndex = this.data.findIndex(
      (item) => item.pk === this.currentClassificationId,
    );
    if (currentClassificationIndex === -1) {
      return;
    }
    const clickedItemIndex = this.data.findIndex((item) => item.pk === pk);
    if (clickedItemIndex === -1) {
      return;
    }
    const [start, end] =
      currentClassificationIndex < clickedItemIndex
        ? [currentClassificationIndex, clickedItemIndex]
        : [clickedItemIndex, currentClassificationIndex];

    this.selectedItems.clear();
    for (let i = start; i <= end; i++) {
      this.selectedItems.add(this.data[i].pk);
    }
    this._emitNewData();
  }

  clearSelection() {
    this.selectedItems.clear();
    if (this.currentClassificationId) {
      this.selectedItems.add(this.currentClassificationId);
    }
    this._emitNewData();
  }

  selectSequence(sequenceId: number) {
    const sequenceImageIds = this.sequenceAllImageIds.get(sequenceId);

    if (!sequenceImageIds || sequenceImageIds.length === 0) {
      return;
    }

    const allSelected = sequenceImageIds.every((id) =>
      this.selectedItems.has(id),
    );

    if (allSelected) {
      for (const id of sequenceImageIds) {
        this.selectedItems.delete(id);
      }
    } else {
      for (const id of sequenceImageIds) {
        this.selectedItems.add(id);
      }
    }

    // make sure the current classification is always selected
    if (this.currentClassificationId) {
      this.selectedItems.add(this.currentClassificationId);
    }

    this._emitNewData();
  }

  selectCurrentClassificationSequence() {
    const currentClassification = this.data.find(
      (item) => item.pk === this.currentClassificationId,
    );

    if (!currentClassification || !currentClassification.sequence) {
      return;
    }

    this.selectSequence(currentClassification.sequence.id);
  }

  get groupClassificationMode() {
    return this.selectedItems.size > 1;
  }

  goToNextSequence() {
    this.clearSelection();
    if (!this.currentClassificationId || !this.projectSlug) {
      return;
    }
    const currentClassification = this.data.find(
      (item) => item.pk === this.currentClassificationId,
    );

    if (
      !currentClassification ||
      !currentClassification.next_classification_sequence
    ) {
      return;
    }

    const newClassificationId =
      currentClassification.next_classification_sequence;
    return this.goToClassification(newClassificationId);
  }

  goToPreviousSequence() {
    this.clearSelection();
    if (!this.currentClassificationId || !this.projectSlug) {
      return;
    }
    const currentClassification = this.data.find(
      (item) => item.pk === this.currentClassificationId,
    );

    if (
      !currentClassification ||
      !currentClassification.previous_classification_sequence
    ) {
      return;
    }

    // this.clearSelection();

    const newClassificationId =
      currentClassification.previous_classification_sequence;
    return this.goToClassification(newClassificationId);
  }
}

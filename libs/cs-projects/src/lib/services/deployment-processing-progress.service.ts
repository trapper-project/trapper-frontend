import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BaseBackendConnector } from '@trapper/backend';
import {
  EMPTY,
  catchError,
  filter,
  interval,
  map,
  shareReplay,
  startWith,
  switchMap,
  takeUntil,
} from 'rxjs';

export interface DeploymentProcessingProgress {
  pk: number;
  deployment_id: string;
  progress: number;
  finished: boolean;
}

@UntilDestroy()
@Injectable()
export class DeploymentProcessingProgressService extends BaseBackendConnector<
  void,
  DeploymentProcessingProgress
> {
  finished$ = this.data$.pipe(
    map((data) => data?.finished || false),
    shareReplay(1),
  );
  progress$ = this.data$.pipe(
    map((data) => data?.progress),
    shareReplay(1),
  );

  fetchDeploymentProcessingProgress(projectSlug: string, deploymentId: string) {
    return this.get(
      `/api/deployments/${projectSlug}/${deploymentId}/progress/`,
    ).pipe(catchError(() => EMPTY));
  }

  fetchProgressDataUntilFinished(projectSlug: string, deploymentId: string) {
    this.clear();
    return interval(7000).pipe(
      startWith(0),
      switchMap(() =>
        this.fetchDeploymentProcessingProgress(projectSlug, deploymentId),
      ),
      takeUntil(this.finished$.pipe(filter((finished) => finished))),
      untilDestroyed(this),
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, tap } from 'rxjs';

export interface TeamAvailableForSharing {
  id: number;
  name: string;
}

@Injectable()
export class TeamsAvailableForSharingService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  team$ = new BehaviorSubject<TeamAvailableForSharing[] | null>(null);
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  fetchAvailableTeams() {
    this.inProgress$.next(true);
    this.team$.next(null);
    return this.httpClient
      .get<TeamAvailableForSharing[]>(`/api/teams/available_for_sharing/`)
      .pipe(
        tap(() => this.inProgress$.next(false)),
        catchError((error) => {
          this.toastr.error(
            error.error.detail ||
              this.translocoService.translate(
                'teamsShareDeployment.teamsAvailableForSharingUnknownError',
                {},
                'csProjects',
              ),
          );
          this.inProgress$.next(false);
          return NEVER;
        }),
        tap((data) => this.team$.next(data)),
      );
  }
}

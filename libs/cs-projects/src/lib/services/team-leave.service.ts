import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, tap } from 'rxjs';

@Injectable()
export class TeamLeaveService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  leaveTeam(teamId: number) {
    this.inProgress$.next(true);
    return this.httpClient.post(`/api/teams/${teamId}/members/leave/`, {}).pipe(
      tap(() => this.inProgress$.next(false)),
      catchError((error) => {
        this.toastr.error(
          error.error.detail ||
            this.translocoService.translate(
              'teamLeave.teamLeaveUnknownError',
              {},
              'csProjects',
            ),
        );
        this.inProgress$.next(false);
        return NEVER;
      }),
      tap(() =>
        this.toastr.success(
          this.translocoService.translate(
            'teamLeave.teamLeaveSuccess',
            {},
            'csProjects',
          ),
        ),
      ),
    );
  }
}

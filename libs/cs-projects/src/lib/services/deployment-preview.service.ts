import { Injectable, inject } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable, combineLatest, map, shareReplay } from 'rxjs';
import { ProjectDataService } from './project-data.service';

export interface DeploymentPreviewThumbnailBbox {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface DeploymentPreviewThumbnail {
  pk: number;
  thumbnail: string;
  bboxes: DeploymentPreviewThumbnailBbox[];
}

export interface DeploymentPreview {
  pk: number;
  deployment_id: string;
  location_id: number;
  location: {
    name: string;
    latitude: number | null;
    longitude: number | null;
  };

  start_date: string;
  end_date: string;
  owner: {
    id: number;
    name: string;
    avatar: string;
  };
  camera_id: string;
  camera_model: string;
  camera_interval: number;
  camera_height: string;
  camera_tilt: number;
  camera_heading: number;
  detection_distance: string;
  feature_type: string;
  habitat: string;
  timestamp_issues: boolean;
  comments: string;
  status: number;
  is_incomplete: boolean;
  thumbnails: DeploymentPreviewThumbnail[];
  species: {
    total: number;
    latin_name: string;
    english_name: string;
  }[];
  deployment_code: string;
  sequences: number;
  resources_count: number;
  classified_ai_count: number;
  classified_by_me_count: number;
  classified_expert_count: number;
  can_delete: boolean;
  can_update: boolean;
  can_classify?: boolean;
  have_additional_fields?: boolean;
  bait_type: string;
  is_private: boolean;
}

@Injectable()
export class DeploymentPreviewService extends BaseBackendConnector<
  void,
  DeploymentPreview
> {
  private projectDataService: ProjectDataService = inject(ProjectDataService);
  override data$: Observable<DeploymentPreview | null> = this.data$.pipe(
    map((deployment) => {
      if (!deployment) {
        return null;
      }

      return {
        ...deployment,
        can_classify: deployment.thumbnails.length > 0,
        have_additional_fields: !!(
          deployment.camera_id ||
          deployment.camera_model ||
          deployment.camera_interval ||
          deployment.camera_height ||
          deployment.camera_tilt ||
          deployment.camera_heading ||
          deployment.detection_distance ||
          deployment.feature_type ||
          deployment.habitat ||
          deployment.timestamp_issues !== null
        ),
      };
    }),
  );

  baitType$ = combineLatest([
    this.data$,
    this.projectDataService.availableFilters$,
  ]).pipe(
    map(([deployment, filters]) => {
      if (
        !deployment ||
        !filters ||
        deployment.bait_type === null ||
        !filters.bait_type
      ) {
        return null;
      }
      if (!filters) {
        return null;
      }
      return (
        filters.bait_type.find(
          (filter) => filter.id.toString() === deployment.bait_type.toString(),
        ) || null
      );
    }),
    shareReplay(1),
  );

  fetchDeploymentPreview(projectSlug: string, deploymentId: string) {
    return this.get(`/api/deployments/${projectSlug}/${deploymentId}/`);
  }
}

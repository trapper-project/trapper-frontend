import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';
import * as geojson from 'geojson';
export interface TeamMember {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  name: string;
}

export interface TeamMembership {
  id: number;
  user: TeamMember;
  role: Record<string, string>;
  status: Record<string, string>;
  motivation: string;
  date_created: string;
  date_modified: string;
}

export interface TeamProjectDeployments {
  classification_project_slug: string;
  classification_project_name: string;
  deployments: number;
}

export interface Team {
  id: number;
  name: string;
  description: string;
  team_leader: TeamMember;
  members: TeamMembership[];
  can_manage: boolean;
  date_created: string;
  date_modified: string;
  polygon: geojson.Polygon | null;
  projects: TeamProjectDeployments[];
}

@Injectable()
export class TeamDetailsService extends BaseBackendConnector<void, Team> {
  constructor() {
    super();
  }

  fetchTeamDetails(projectSlug: string, teamId: string): Observable<void> {
    return this.get(`/api/teams/${projectSlug}/${teamId}/`);
  }
}

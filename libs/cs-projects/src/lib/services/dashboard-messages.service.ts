import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  TRANSLOCO_SCOPE,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, take } from 'rxjs';

export interface DashboardMessage {
  pk: number;
  subject: string;
  text: string;
  date_sent: string;
}

export interface DashboardMessagePagination {
  page: number;
  page_size: number;
  pages: number;
  count: number;
  total: number;
}

export interface DashboardMessageResponse {
  pagination: DashboardMessagePagination;
  results: DashboardMessage[];
}

@UntilDestroy()
@Injectable()
export class DashboardMessagesService {
  messages$ = new BehaviorSubject<DashboardMessage[] | null>(null);
  pagination$ = new BehaviorSubject<DashboardMessagePagination | null>(null);

  constructor(
    private httpClient: HttpClient,
    private toastrService: ToastrService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  getMessages(page?: number) {
    page = page || 1;
    this.httpClient
      .get<DashboardMessageResponse>(`/api/cs/inbox/?page=${page}`)
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        this.messages$.next(response.results);
        this.pagination$.next(response.pagination);
      });
  }

  markAsReceived(messageId: number) {
    this.httpClient
      .patch(`/api/cs/inbox/${messageId}/mark-as-received/`, {})
      .subscribe(() => {
        this.translocoService
          .selectTranslate('messageMarkedAsReceived', {}, this.translocoScope)
          .pipe(take(1))
          .subscribe((message) => {
            this.toastrService.success(message);
          });
        this.getMessages();
      });
  }
}

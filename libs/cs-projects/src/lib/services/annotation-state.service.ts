import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, Subject, map } from 'rxjs';

export interface AnnotationBbox {
  left: number;
  top: number;
  width: number;
  height: number;
}

export type ObjectSingleBoundingBox = number[];
export type ObjectBoundingBoxes = Array<ObjectSingleBoundingBox | null>;

export interface AnnotationBboxState {
  bboxId: string;
  values: Record<string, string | number | null>;
  confidence: Record<string, number>;
  errors: Record<string, string[] | null>;
  bboxes: ObjectBoundingBoxes;
}

export interface AnnotationState {
  state: Record<string, AnnotationBboxState>;
  dirty: boolean;
  selectedBboxes: string[];
}

export abstract class AnnotationEvent {
  abstract handle(state: AnnotationState): AnnotationState;
}

export class ClearAnnotationEvent implements AnnotationEvent {
  handle(): AnnotationState {
    return { state: {}, dirty: false, selectedBboxes: [] };
  }
}

export class UpdateAttributesAnnotationEvent implements AnnotationEvent {
  constructor(
    public bboxIds: string[],
    public updatedValues: Record<string, string | number | null>,
  ) {}

  private updateSingleBbox(
    state: AnnotationBboxState,
    updatedValues: Record<string, string | number | null>,
  ): AnnotationBboxState {
    const newValues = { ...state.values };
    for (const [key, value] of Object.entries(updatedValues)) {
      if (value === null) {
        newValues[key] = null;
      } else {
        newValues[key] = value.toString();
      }
    }

    // clear confidence for updated values
    const newConfidence = { ...state.confidence };
    for (const key of Object.keys(updatedValues)) {
      delete newConfidence[key];
    }

    return {
      ...state,
      values: newValues,
      confidence: newConfidence,
      errors: {},
    };
  }

  handle(state: AnnotationState): AnnotationState {
    const newBboxesState = { ...state.state };
    for (const bboxId of this.bboxIds) {
      const bbox = newBboxesState[bboxId];
      if (!bbox) {
        continue;
      }
      newBboxesState[bboxId] = this.updateSingleBbox(bbox, this.updatedValues);
    }

    return {
      state: newBboxesState,
      dirty: true,
      selectedBboxes: state.selectedBboxes,
    };
  }
}

export class AddAnnotationEvent implements AnnotationEvent {
  constructor(
    public bboxId: string,
    public bboxes: ObjectBoundingBoxes,
    public values: Record<string, string | number | null>,
    public confidence: Record<string, number> = {},
  ) {}

  handle(state: AnnotationState): AnnotationState {
    for (const [key, value] of Object.entries(this.values)) {
      if (value !== null) {
        this.values[key] = value.toString();
      }
    }
    return {
      ...state,
      state: {
        ...state.state,
        [this.bboxId]: {
          bboxId: this.bboxId,
          bboxes: this.bboxes,
          values: this.values,
          confidence: this.confidence,
          errors: {},
        },
      },
    };
  }
}

export class DeleteAnnotationEvent implements AnnotationEvent {
  constructor(public bboxId: string) {}

  handle(state: AnnotationState): AnnotationState {
    const newState = { ...state.state };
    delete newState[this.bboxId];
    return {
      ...state,
      dirty: true,
      state: newState,
      selectedBboxes: [],
    };
  }
}

export class UpdateBboxLocation implements AnnotationEvent {
  constructor(
    public bboxId: string,
    public bboxes: ObjectBoundingBoxes,
  ) {}

  handle(state: AnnotationState): AnnotationState {
    const newState = { ...state.state };
    const bbox = newState[this.bboxId];
    if (!bbox) {
      return state;
    }
    newState[this.bboxId] = {
      ...bbox,
      bboxes: this.bboxes,
    };
    return {
      ...state,
      dirty: true,
      state: newState,
    };
  }
}

export class AddNewEmptyBbox implements AnnotationEvent {
  constructor(public bboxes: ObjectBoundingBoxes) {}

  handle(state: AnnotationState): AnnotationState {
    let bboxIdx = 0;
    while (state.state[generateBboxId(bboxIdx)]) {
      bboxIdx++;
    }

    const bboxId = generateBboxId(bboxIdx);

    return {
      dirty: true,
      state: {
        ...state.state,
        [bboxId]: {
          bboxId,
          bboxes: this.bboxes,
          values: {},
          confidence: {},
          errors: {},
        },
      },
      selectedBboxes: [bboxId],
    };
  }
}

export class SetErrors implements AnnotationEvent {
  constructor(public errors: Record<string, string[]>[]) {}

  handle(state: AnnotationState): AnnotationState {
    const newState = { ...state.state };
    for (let i = 0; i < this.errors.length; i++) {
      const error = this.errors[i];
      const bboxId = generateBboxId(i);
      newState[bboxId].errors = error;
    }

    return {
      ...state,
      state: newState,
    };
  }
}

export class ClearErrors implements AnnotationEvent {
  handle(state: AnnotationState): AnnotationState {
    const newState = { ...state.state };
    for (const bboxId in newState) {
      newState[bboxId].errors = {};
    }

    return {
      ...state,
      state: newState,
    };
  }
}

export class MakePristine implements AnnotationEvent {
  handle(state: AnnotationState): AnnotationState {
    return {
      ...state,
      dirty: false,
    };
  }
}

export class MarkAsDirty extends AnnotationEvent {
  override handle(state: AnnotationState): AnnotationState {
    return {
      ...state,
      dirty: true,
    };
  }
}

export class SelectBboxes extends AnnotationEvent {
  constructor(public bboxIds: string[]) {
    super();
  }

  override handle(state: AnnotationState): AnnotationState {
    return {
      ...state,
      selectedBboxes: this.bboxIds,
    };
  }
}

export class SelectAllBboxes extends AnnotationEvent {
  override handle(state: AnnotationState): AnnotationState {
    const selectedBboxes = Object.keys(state.state);
    return {
      ...state,
      selectedBboxes,
    };
  }
}

export class SelectNextBbox extends AnnotationEvent {
  override handle(state: AnnotationState): AnnotationState {
    if (state.selectedBboxes.length === 0) {
      const bboxIds = Object.keys(state.state);
      if (bboxIds.length === 0) {
        return state;
      }
      return {
        ...state,
        selectedBboxes: [bboxIds[0]],
      };
    }
    let selectedBboxes = [...state.selectedBboxes];
    selectedBboxes.sort();
    const lastSelectedBbox = selectedBboxes[selectedBboxes.length - 1];
    const bboxIds = Object.keys(state.state);
    const idx = bboxIds.indexOf(lastSelectedBbox);
    if (idx === -1) {
      return state;
    }
    const nextBboxId = bboxIds[idx + 1];
    if (!nextBboxId) {
      return state;
    }
    selectedBboxes = [nextBboxId];

    return {
      ...state,
      selectedBboxes,
    };
  }
}

export class SelectPreviousBbox extends AnnotationEvent {
  override handle(state: AnnotationState): AnnotationState {
    if (state.selectedBboxes.length === 0) {
      const bboxIds = Object.keys(state.state);
      if (bboxIds.length === 0) {
        return state;
      }
      return {
        ...state,
        selectedBboxes: [bboxIds[bboxIds.length - 1]],
      };
    }
    let selectedBboxes = [...state.selectedBboxes];
    selectedBboxes.sort();
    const firstSelectedBbox = selectedBboxes[0];
    const bboxIds = Object.keys(state.state);
    const idx = bboxIds.indexOf(firstSelectedBbox);
    if (idx === -1) {
      return state;
    }
    const previousBboxId = bboxIds[idx - 1];
    if (!previousBboxId) {
      return state;
    }
    selectedBboxes = [previousBboxId];

    return {
      ...state,
      selectedBboxes,
    };
  }
}

export class ToggleBbboxSelection extends AnnotationEvent {
  constructor(public bboxId: string) {
    super();
  }

  override handle(state: AnnotationState): AnnotationState {
    const selectedBboxes = [...state.selectedBboxes];
    const idx = selectedBboxes.indexOf(this.bboxId);
    if (idx === -1) {
      selectedBboxes.push(this.bboxId);
    } else {
      selectedBboxes.splice(idx, 1);
    }
    return {
      ...state,
      selectedBboxes,
    };
  }
}

export class MarkAsDirtyIfEmpty extends AnnotationEvent {
  override handle(state: AnnotationState): AnnotationState {
    if (Object.keys(state.state).length === 0) {
      return {
        ...state,
        dirty: true,
      };
    }
    return state;
  }
}

export function generateBboxId(bboxIdx: number) {
  return `${bboxIdx + 1}`;
}

@UntilDestroy()
@Injectable()
export class AnnotationStateService {
  private _currentState: AnnotationState = {
    state: {},
    dirty: false,
    selectedBboxes: [],
  };

  public get currentState(): AnnotationState {
    return this._currentState;
  }

  annotationEvents$ = new Subject<AnnotationEvent>();
  annotationState$ = new BehaviorSubject<AnnotationState>(this._currentState);

  bboxes$ = this.annotationState$.pipe(
    map((state) => {
      const bboxes: AnnotationBboxState[] = [];
      for (const bboxId in state.state) {
        bboxes.push(state.state[bboxId]);
      }
      return bboxes;
    }),
  );

  isDirty$ = this.annotationState$.pipe(map((state) => state.dirty));
  get isDirty() {
    return this._currentState.dirty;
  }

  constructor() {
    this.annotationEvents$.pipe(untilDestroyed(this)).subscribe((event) => {
      this._currentState = event.handle(this._currentState);
      this.annotationState$.next(this._currentState);
    });
  }

  clearAll() {
    this.annotationEvents$.next(new ClearAnnotationEvent());
  }

  updateAttributes(
    bboxIds: string[],
    updatedValues: Record<string, string | null>,
  ) {
    this.annotationEvents$.next(
      new UpdateAttributesAnnotationEvent(bboxIds, updatedValues),
    );
  }
  updateAllAttributes(updatedValues: Record<string, string | null>) {
    const bboxIds = Object.keys(this._currentState.state);
    this.annotationEvents$.next(
      new UpdateAttributesAnnotationEvent(bboxIds, updatedValues),
    );
  }

  addObject(
    bboxId: string,
    bboxes: ObjectBoundingBoxes,
    values: Record<string, string | null>,
    confidence: Record<string, number> = {},
  ) {
    this.annotationEvents$.next(
      new AddAnnotationEvent(bboxId, bboxes, values, confidence),
    );
  }

  deleteBbox(bboxId: string) {
    this.annotationEvents$.next(new DeleteAnnotationEvent(bboxId));
  }

  addNewEmptyBbox(bboxes: ObjectBoundingBoxes) {
    this.annotationEvents$.next(new AddNewEmptyBbox(bboxes));
  }

  updateBboxLocation(bboxId: string, bboxes: ObjectBoundingBoxes) {
    this.annotationEvents$.next(new UpdateBboxLocation(bboxId, bboxes));
  }

  setErrors(errors: Record<string, string[]>[]) {
    this.annotationEvents$.next(new SetErrors(errors));
  }

  clearErrors() {
    this.annotationEvents$.next(new ClearErrors());
  }

  makePristine() {
    this.annotationEvents$.next(new MakePristine());
  }

  markAsDirty() {
    this.annotationEvents$.next(new MarkAsDirty());
  }

  markAsDirtyIfEmpty() {
    this.annotationEvents$.next(new MarkAsDirtyIfEmpty());
  }

  selectBboxes(bboxIds: string[]) {
    this.annotationEvents$.next(new SelectBboxes(bboxIds));
  }

  toggleBboxSelection(bboxId: string) {
    this.annotationEvents$.next(new ToggleBbboxSelection(bboxId));
  }

  selectAllBboxes() {
    this.annotationEvents$.next(new SelectAllBboxes());
  }

  selectNextBbox() {
    this.annotationEvents$.next(new SelectNextBbox());
  }

  selectPreviousBbox() {
    this.annotationEvents$.next(new SelectPreviousBbox());
  }

  get currentConsensusValues() {
    const consensusValues: Record<string, string | null> = {};
    const bboxes = this._currentState.state;
    const allPossibleKeys = new Set<string>();
    for (const bboxId in bboxes) {
      for (const key in bboxes[bboxId].values) {
        allPossibleKeys.add(key);
      }
    }
    const possibleValuesForKeys: Record<string, Set<string>> = {};
    for (const key of allPossibleKeys) {
      possibleValuesForKeys[key] = new Set();
    }
    for (const bboxId in bboxes) {
      for (const key in bboxes[bboxId].values) {
        const value = bboxes[bboxId].values[key];
        if (value === null || value === undefined) {
          continue;
        }
        possibleValuesForKeys[key].add(value.toString());
      }
    }
    for (const key in possibleValuesForKeys) {
      const values = Array.from(possibleValuesForKeys[key]);
      if (values.length === 1) {
        consensusValues[key] = values[0];
      } else {
        consensusValues[key] = null;
      }
    }
    return consensusValues;
  }
}

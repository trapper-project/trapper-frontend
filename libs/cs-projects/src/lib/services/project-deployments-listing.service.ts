import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';

import {
  asyncScheduler,
  BehaviorSubject,
  catchError,
  combineLatest,
  EMPTY,
  map,
  Observable,
  scan,
  shareReplay,
  Subject,
  switchMap,
  throttleTime,
  withLatestFrom,
} from 'rxjs';

export interface PaginationData {
  page: number;
  page_size: number;
  pages: number;
  count: number;
  total: number;
}

export interface DeploymentListingItemOwner {
  name: string;
  avatar: string;
  id: number;
}

export interface Species {
  english_name: string;
  latin_name: string;
  total: number;
}

export interface DeploymentListingItemThumbnailBbox {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface DeploymentListingItemThumbnail {
  pk: number;
  thumbnail: string;
  bboxes: DeploymentListingItemThumbnailBbox[];
}
export interface DeploymentListingItem {
  pk: number;
  thumbnails: DeploymentListingItemThumbnail[];
  thumbnail_urls?: string[];

  deployment_code: string;
  deployment_id: string;
  location_name: string;
  species: Species[];
  start_date: string;
  end_date: string;
  sequences: number;
  owner: DeploymentListingItemOwner;
  can_delete: boolean;
  can_update: boolean;

  selected?: boolean;
}

export interface DeploymentListingDto {
  pagination: PaginationData;
  results: DeploymentListingItem[];
}

const defaultFilters: Record<string, string | null> = {};

interface SelectionEvent {
  deploymentIds: Set<number>;
  action: 'add' | 'remove' | 'clear';
}

@Injectable()
export class ProjectDeploymentListingService {
  inProgress$ = new BehaviorSubject(false);
  _data$ = new BehaviorSubject<DeploymentListingDto | null>(null);
  filter$ = new BehaviorSubject(defaultFilters);
  projectSlug$ = new Subject<string>();
  selectionEvent$ = new BehaviorSubject<SelectionEvent>({
    deploymentIds: new Set(),
    action: 'clear',
  });
  reloads$ = new BehaviorSubject<unknown>({});

  selectedDeploymentIds$ = this.selectionEvent$.pipe(
    scan((acc, event) => {
      if (event.action === 'clear') {
        return new Set();
      } else if (event.action === 'add') {
        return new Set([...acc, ...event.deploymentIds]);
      } else if (event.action === 'remove') {
        return new Set([...acc].filter((id) => !event.deploymentIds.has(id)));
      }
      return acc;
    }, new Set<number>()),
  );

  data$ = combineLatest([this._data$, this.selectedDeploymentIds$]).pipe(
    map(([data, selectedIds]) => {
      if (!data) {
        return null;
      }
      return {
        ...data,
        results: data.results.map((item) => ({
          ...item,
          selected: selectedIds.has(item.pk),
          thumbnail_urls: item.thumbnails.map((t) => t.thumbnail),
        })),
      };
    }),
  );

  pagination$ = this._data$.pipe(
    map((dto) => dto?.pagination || null),
    shareReplay(1),
  );

  constructor(
    private httpClient: HttpClient,
    private translocoService: TranslocoService,
  ) {}

  connect(): Observable<void> {
    return combineLatest([this.filter$, this.reloads$]).pipe(
      map(([filters]) => filters),
      throttleTime(500, asyncScheduler, { leading: true, trailing: true }),
      withLatestFrom(this.projectSlug$),
      switchMap(([filters, slug]) => {
        const params: Record<string, string> = {};
        Object.entries(filters).forEach(([k, v]) => {
          if (v) {
            params[k] = v;
          }
        });
        if (
          params['page_size'] &&
          ![10, 50, 100, 200].includes(+params['page_size'])
        ) {
          delete params['page_size'];
        }
        this.inProgress$.next(true);
        this._data$.next(null);
        return this.httpClient.get<DeploymentListingDto>(
          `/api/deployments/${slug}/`,
          {
            params,
          },
        );
      }),
      map((data) => {
        this._data$.next(data);
        this.inProgress$.next(false);
      }),
      catchError(() => {
        this.inProgress$.next(false);
        return EMPTY;
      }),
    );
  }

  toggleSelection(deploymentId: number, selected: boolean) {
    this.selectionEvent$.next({
      deploymentIds: new Set([Number(deploymentId)]),
      action: selected ? 'add' : 'remove',
    });
  }

  refresh() {
    this.reloads$.next({});
  }
}

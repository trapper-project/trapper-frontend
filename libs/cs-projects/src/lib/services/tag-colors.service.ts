import { Injectable } from '@angular/core';

export interface TagWithColor {
  tag: string;
  color: string;
}

@Injectable({ providedIn: 'root' })
export class TagColorsService {
  private colors = new Map([
    [
      'observation_type',
      new Map<string, string>([
        ['human', 'var(--trapper-color-tag-0)'],
        ['animal', 'var(--trapper-color-tag-1)'],
        ['vehicle', 'var(--trapper-color-tag-2)'],
        ['unknown', 'var(--trapper-color-tag-3)'],
        ['unclassified', 'var(--trapper-color-tag-4)'],
        ['blank', 'var(--trapper-color-tag-5)'],
      ]),
    ],
  ]);

  getColorMappingForTagType(tagType: string): Map<string, string> {
    return this.colors.get(tagType) || new Map();
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, tap } from 'rxjs';

@Injectable()
export class DeploymentDeleteService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  deleteDeployment(projectSlug: string, deploymentId: number) {
    this.inProgress$.next(true);
    return this.httpClient
      .delete(`/api/deployments/${projectSlug}/${deploymentId}/`)
      .pipe(
        tap(() => this.inProgress$.next(false)),
        catchError((error) => {
          this.toastr.error(
            error.error.detail ||
              this.translocoService.translate(
                'deploymentDelete.deploymentDeleteUnknownError',
                {},
                'csProjects',
              ),
          );
          this.inProgress$.next(false);
          return NEVER;
        }),
        tap(() =>
          this.toastr.success(
            this.translocoService.translate(
              'deploymentDelete.deploymentDeleteSuccess',
              {},
              'csProjects',
            ),
          ),
        ),
      );
  }
}

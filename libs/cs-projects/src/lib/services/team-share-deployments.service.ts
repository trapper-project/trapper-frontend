import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import {
  BehaviorSubject,
  NEVER,
  catchError,
  map,
  shareReplay,
  tap,
} from 'rxjs';

export interface DeploymentSharingResult {
  id: number;
  deployment_code: string;
  rejection_reason: string | null;
}

export interface DeploymentSharingResultsDto {
  deployments: DeploymentSharingResult[];
}

@Injectable()
export class TeamShareDeploymentsService {
  inProgress$ = new BehaviorSubject<boolean>(false);
  results$ = new BehaviorSubject<DeploymentSharingResultsDto | null>(null);
  deployments$ = this.results$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }
      return data.deployments;
    }),
    shareReplay(1),
  );
  rejected$ = this.results$.pipe(
    map((data) => {
      if (!data) {
        return null;
      }

      return data.deployments.filter((item) => item.rejection_reason);
    }),
    shareReplay(1),
  );
  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  shareDeployments(
    projectSlug: string,
    teamId: number,
    shareLocation: boolean,
    deploymentIds: number[],
  ) {
    this.inProgress$.next(true);
    return this.httpClient
      .post<DeploymentSharingResultsDto>(
        `/api/teams/${projectSlug}/${teamId}/share_deployments/`,
        {
          deployments: deploymentIds,
          share_location: shareLocation,
        },
      )
      .pipe(
        tap(() => this.inProgress$.next(false)),
        catchError((error) => {
          this.toastr.error(
            error.error.detail ||
              this.translocoService.translate(
                'teamsShareDeployment.teamsShareDeploymentUnknownError',
                {},
                'csProjects',
              ),
          );
          this.inProgress$.next(false);
          return NEVER;
        }),
        tap((data) => this.results$.next(data)),
      );
  }
}

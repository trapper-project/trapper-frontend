import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertComponent,
  CheckboxComponent,
  FormFieldComponent,
  LabelComponent,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { ProjectDataService } from '../services/project-data.service';
import { MatIconModule } from '@angular/material/icon';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { inOutAnimation } from '@trapper/animations';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-frontend-deployment-edit-additional-fields',
  standalone: true,
  imports: [
    CommonModule,
    TextInputComponent,
    SelectBoxInputComponent,
    FormFieldComponent,
    ServerErrorsComponent,
    LabelComponent,
    MatIconModule,
    ReactiveFormsModule,
    TranslocoModule,
    CheckboxComponent,
    AlertComponent,
  ],
  templateUrl: './deployment-edit-additional-fields.component.html',
  styleUrls: ['./deployment-edit-additional-fields.component.scss'],
  animations: [inOutAnimation],
})
export class DeploymentEditAdditionalFieldsComponent {
  featureTypeChoices$ = this.projectDataService.featureTypeChoices$;

  @Input() formGroup!: FormGroup;
  @Input() expanded = false;
  @Output() expandedChange = new EventEmitter<boolean>();

  @Input() inUploadView = false;

  constructor(private projectDataService: ProjectDataService) {}

  toggleAdditionalFields() {
    this.expanded = !this.expanded;
    this.expandedChange.emit(this.expanded);
  }
}

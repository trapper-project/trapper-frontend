import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import {
  DeploymentListingMapItem,
  DeploymentListingMapService,
} from '../services/deployment-listing-map.service';
import {
  DetailsCardItemComponent,
  MapComponent,
  MapMarker,
  SpinnerComponent,
} from '@trapper/ui';
import { BehaviorSubject, EMPTY, combineLatest, map, switchMap } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslocoModule } from '@jsverse/transloco';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-listing-map',
  standalone: true,
  imports: [
    CommonModule,
    MapComponent,
    DetailsCardItemComponent,
    RouterModule,
    TranslocoModule,
    SpinnerComponent,
  ],
  templateUrl: './deployment-listing-map.component.html',
  styleUrls: ['./deployment-listing-map.component.scss'],
})
export class DeploymentListingMapComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private deploymentListingMapService: DeploymentListingMapService,
    private router: Router,
  ) {}

  mapData$ = this.deploymentListingMapService.data$;

  selectedMarkerId$ = new BehaviorSubject<string | null>(null);
  selectedLocation$ = combineLatest([
    this.mapData$,
    this.selectedMarkerId$,
  ]).pipe(
    map(([data, selectedMarkerId]) => {
      if (!data) {
        return null;
      }
      return (
        data.find((item) => item.pk.toString() === selectedMarkerId) || null
      );
    }),
    map((item) => {
      if (!item) {
        return null;
      }
      return {
        ...item,
        latitudeAbs: Math.abs(item.latitude),
        longitudeAbs: Math.abs(item.longitude),
      };
    }),
  );

  markers$ = combineLatest([this.mapData$, this.selectedMarkerId$]).pipe(
    map(([data, selectedMarkerId]) => {
      if (!data) {
        return null;
      }
      const markers = data.map((item) => {
        return {
          lat: item.latitude,
          lng: item.longitude,
          label: item.name,
          id: item.pk.toString(),
          isHighlighted: item.pk.toString() === selectedMarkerId,
        };
      });
      return markers;
    }),
  );

  ngOnInit() {
    combineLatest([this.route.paramMap, this.route.queryParamMap])
      .pipe(
        switchMap(([params, queryParams]) => {
          const projectId = params.get('projectId');
          const filters: Record<string, string> = {};
          queryParams.keys.forEach((key) => {
            if (key === 'page' || key === 'map' || key === 'page_size') {
              return;
            }
            const value = queryParams.get(key);
            if (value) {
              filters[key] = value;
            }
          });
          if (projectId) {
            return this.deploymentListingMapService.fetchMapData(
              projectId,
              filters,
            );
          }
          return EMPTY;
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  onMarkerClicked(marker: MapMarker | null) {
    this.selectedMarkerId$.next(marker?.id || null);
  }

  onViewDeploymentsClicked(location: DeploymentListingMapItem) {
    this.router.navigate([], {
      queryParams: { location: location.pk, by_me: 1, page: null, map: null },
      queryParamsHandling: 'merge',
    });
  }

  onViewResourcesClicked(location: DeploymentListingMapItem) {
    this.router.navigate(['../media'], {
      queryParams: { locations_map: location.pk, by_me: 1, page: null },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DeploymentEditService } from '../services/deployment-edit.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DeploymentEditLocationSelectionComponent } from '../deployment-edit-location-selection/deployment-edit-location-selection.component';
import { DeploymentEditAdditionalFieldsComponent } from '../deployment-edit-additional-fields/deployment-edit-additional-fields.component';
import { DeploymentEditAdditionalInformationsComponent } from '../deployment-edit-additional-informations/deployment-edit-additional-informations.component';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import {
  ButtonComponent,
  DateRangeInputComponent,
  FormFieldComponent,
  LabelComponent,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectDataService } from '../services/project-data.service';
import { inOutAnimation } from '@trapper/animations';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { combineLatest } from 'rxjs';
import { PageTitleService } from '@trapper/page-title-management';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-edit',
  standalone: true,
  imports: [
    CommonModule,
    DeploymentEditLocationSelectionComponent,
    DeploymentEditAdditionalFieldsComponent,
    DeploymentEditAdditionalInformationsComponent,
    TranslocoModule,
    TextInputComponent,
    SelectBoxInputComponent,
    ButtonComponent,
    ReactiveFormsModule,
    TextInputComponent,
    SelectBoxInputComponent,
    LabelComponent,
    ServerErrorsComponent,
    FormFieldComponent,
    DateRangeInputComponent,
    RouterModule,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './deployment-edit.component.html',
  styleUrls: ['./deployment-edit.component.scss'],
  providers: [DeploymentEditService],
  animations: [inOutAnimation],
})
export class DeploymentEditComponent implements OnInit {
  currentProjectSlug?: string | null;
  currentDeploymentId?: string | null;
  formGroup = this.deploymentEditService.formGroup;

  today = new Date();

  deploymentChoices$ = this.projectDataService.deploymentChoices$;
  baitTypeChoices$ = this.projectDataService.baitTypeChoices$;

  data$ = this.deploymentEditService.data$;

  constructor(
    private route: ActivatedRoute,
    private deploymentEditService: DeploymentEditService,
    private projectDataService: ProjectDataService,
    private router: Router,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.pageTitleService.resetTitle();
    combineLatest([
      this.data$,
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'deploymentEdit.pageTitlePrefix',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([deployment, project, title]) => {
        const titleParts = [];
        if (deployment) {
          titleParts.push(`${title} - ${deployment.deployment_id}`);
        }
        if (project) {
          titleParts.push(project.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const projectSlug = params.get('projectId');
      const deploymentId = params.get('deploymentId');
      this.currentProjectSlug = projectSlug;
      this.currentDeploymentId = deploymentId;
      this.deploymentEditService.setProjectSlug(projectSlug || undefined);
      this.deploymentEditService.setDeploymentId(deploymentId || undefined);
      if (!projectSlug || !deploymentId) {
        return;
      }
      this.locationEditingEnabledChange(false);
    });
    this.deploymentEditService.fetchCurrentValue().subscribe();
    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  submit() {
    this.deploymentEditService
      .performUpdate()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.router.navigate(['../details'], { relativeTo: this.route });
      });
  }

  locationEditingEnabledChange(creatingNew: boolean) {
    if (creatingNew) {
      this.formGroup.controls.location_id.disable();
      // this.formGroup.controls.location_id.reset();
      this.formGroup.controls.location.enable();
      this.formGroup.controls.location.reset();
    } else {
      this.formGroup.controls.location_id.enable();
      this.formGroup.controls.location.disable();
      this.formGroup.controls.location.reset();
    }
  }
}
export default DeploymentEditComponent;

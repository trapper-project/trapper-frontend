import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';

const colors = Array.from(
  { length: 10 },
  (_, i) => `var(--trapper-color-tag-${i})`,
);
export interface TagWithColor {
  tag: string;
  color: string;
}

@Injectable({ providedIn: 'root' })
export class ProjectListingTagColorsService {
  private colorForTag$ = new BehaviorSubject(new Map<string, string>());

  updateColors(tags: string[]) {
    const colorForTag = new Map();
    for (const tag of tags) {
      if (!colorForTag.has(tag)) {
        const color = colors[colorForTag.size % colors.length];
        if (color) {
          colorForTag.set(tag, color);
        }
      }
    }

    this.colorForTag$.next(colorForTag);
  }

  getColorForTags$(tags: string[]): Observable<TagWithColor[]> {
    return this.colorForTag$.pipe(
      map((colorForTag) =>
        tags.map((tag) => ({ tag, color: colorForTag.get(tag) || colors[0] })),
      ),
    );
  }
}

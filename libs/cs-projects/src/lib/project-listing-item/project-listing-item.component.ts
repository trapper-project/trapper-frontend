import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Project } from '../services/project-listing.service';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { BadgeComponent } from '@trapper/ui';
import {
  ProjectListingTagColorsService,
  TagWithColor,
} from './project-listing-tag-colors.service';
import { Observable } from 'rxjs';
import { EllipsisModule } from 'ngx-ellipsis';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'trapper-frontend-project-listing-item',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule,
    BadgeComponent,
    EllipsisModule,
    MatTooltipModule,
  ],
  templateUrl: './project-listing-item.component.html',
  styleUrls: ['./project-listing-item.component.scss'],
})
export class ProjectListingItemComponent {
  @Input() project?: Project;

  descriptionDisplayed = false;
  tagsDisplayed = false;

  constructor(
    private projectListingTagColorsService: ProjectListingTagColorsService,
  ) {}

  tag$(tags: string[]): Observable<TagWithColor[]> {
    return this.projectListingTagColorsService.getColorForTags$(tags);
  }
}

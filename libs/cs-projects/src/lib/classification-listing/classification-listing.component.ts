import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { combineLatest, map, tap } from 'rxjs';
import { ProjectMediaListingService } from '../services/project-media-listing.service';
import { ProjectDataService } from '../services/project-data.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { TagColorsService } from '../services/tag-colors.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  ButtonComponent,
  CheckboxComponent,
  FormFieldComponent,
  LabelComponent,
  PaginatorComponent,
  RetryImageOnErrorDirective,
  SelectBoxInputComponent,
  ThumbnailWithBboxesComponent,
} from '@trapper/ui';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { inOutAnimation } from '@trapper/animations';
import { PageTitleService } from '@trapper/page-title-management';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-classification-listing',
  standalone: true,
  imports: [
    CommonModule,
    SelectBoxInputComponent,
    CheckboxComponent,
    ButtonComponent,
    FormsModule,
    MatIconModule,
    PaginatorComponent,
    NgxSkeletonLoaderModule,
    FormsModule,
    RouterModule,
    MatTooltipModule,
    RetryImageOnErrorDirective,
    TranslocoModule,
    FormFieldComponent,
    LabelComponent,
    ThumbnailWithBboxesComponent,
  ],
  templateUrl: './classification-listing.component.html',
  styleUrls: ['./classification-listing.component.scss'],
  providers: [ProjectMediaListingService, TagColorsService],
  animations: [inOutAnimation],
})
export class ClassificationListingComponent implements OnInit {
  availableFilters$ = this.projectDataService.availableFilters$;
  classifierFilters$ = this.projectDataService.classificationSpecFilters$;

  listingItems$ = this.projectMediaListingService.data$;
  pagination$ = this.projectMediaListingService.pagination$;

  results$ = this.listingItems$.pipe(map((dto) => dto?.results));
  noItems$ = this.listingItems$.pipe(map((dto) => dto?.results.length === 0));

  error$ = this.projectMediaListingService.error$;

  constructor(
    private projectMediaListingService: ProjectMediaListingService,
    private projectDataService: ProjectDataService,
    private route: ActivatedRoute,
    private router: Router,
    private tagColorService: TagColorsService,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  ngOnInit(): void {
    this.pageTitleService.resetTitle();
    this.projectMediaListingService
      .connect()
      .pipe(untilDestroyed(this))
      .subscribe();

    this.route.paramMap
      .pipe(
        tap((params) => {
          const projectSlug = params.get('projectId');
          if (projectSlug) {
            this.projectMediaListingService.projectSlug$.next(projectSlug);
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.route.queryParamMap
      .pipe(
        tap((query) => {
          const filters = Object.fromEntries(
            query.keys.map((k) => [k, query.get(k)]),
          );
          // if (!filters['deployment']) {
          //   return;
          // }
          this.projectMediaListingService.filter$.next(filters);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();

    combineLatest([
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'classificatinListingPageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([projectData, title]) => {
        const titleParts = [title];
        if (projectData) {
          titleParts.push(projectData.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });
  }

  pageChanged(page: number) {
    this.router.navigate([], {
      queryParams: { page },
      queryParamsHandling: 'merge',
    });
  }

  filterChanged(filterName: string, filterValue: string | null) {
    if (filterName === 'deployment') {
      this.router.navigate([], {
        queryParams: {
          [filterName]: filterValue,
          observation_type: filterValue ? 'animal' : null,
          page: null,
        },
        queryParamsHandling: '',
      });
      return;
    }
    this.router.navigate([], {
      queryParams: {
        [filterName]: filterValue,
        page: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  currentFilters$(field: string) {
    return this.projectMediaListingService.filter$.pipe(
      map((filters) => filters[field] || null),
    );
  }
}

export default ClassificationListingComponent;

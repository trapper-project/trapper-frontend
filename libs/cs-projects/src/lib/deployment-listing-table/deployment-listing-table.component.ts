import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import {
  BadgeComponent,
  CheckboxComponent,
  ResponsiveTableComponent,
  TableCellDefDirective,
  ThumbnailGridMiniComponent,
  UserMiniAvatarComponent,
} from '@trapper/ui';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import {
  DeploymentListingItem,
  DeploymentListingItemOwner,
  ProjectDeploymentListingService,
} from '../services/project-deployments-listing.service';
import { FormsModule } from '@angular/forms';
import { map } from 'rxjs';
import { inOutAnimation } from '@trapper/animations';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  DeploymentDeleteComponentData,
  DeploymentDeleteConfirmationComponent,
} from '../deployment-delete-confirmation/deployment-delete-confirmation.component';
import { ProjectDataService } from '../services/project-data.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  CsProfilePreviewModule,
  ProfilePreviewService,
} from '@trapper/profile-preview';
import { ToastrService } from 'ngx-toastr';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-listing-table',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    NgScrollbarModule,
    ScrollingModule,
    NgxSkeletonLoaderModule,
    CheckboxComponent,
    UserMiniAvatarComponent,
    RouterModule,
    MatIconModule,
    FormsModule,
    ThumbnailGridMiniComponent,
    DialogModule,
    BadgeComponent,
    MatTooltipModule,
    CsProfilePreviewModule,
    ResponsiveTableComponent,
    TableCellDefDirective,
  ],
  templateUrl: './deployment-listing-table.component.html',
  styleUrls: ['./deployment-listing-table.component.scss'],
  animations: [inOutAnimation],
})
export class DeploymentListingTableComponent implements OnInit {
  @Output() selectedItemsChange = new EventEmitter<
    Set<DeploymentListingItem>
  >();
  private profilePreviewDialogService = inject(ProfilePreviewService);
  loading$ = this.projectDeploymentListingService.inProgress$;
  listingItems$ = this.projectDeploymentListingService.data$;
  deployments$ = this.projectDeploymentListingService.data$.pipe(
    map((data) => data?.results || null),
  );

  @Input() selectedItems = new Set<DeploymentListingItem>();

  constructor(
    private projectDeploymentListingService: ProjectDeploymentListingService,
    private dialog: Dialog,
    private projectDataService: ProjectDataService,
    private translocoService: TranslocoService,
    private toastrService: ToastrService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit(): void {
    this.deployments$.pipe(untilDestroyed(this)).subscribe(() => {
      this.selectedItems.clear();
      this.selectedItemsChange.emit(this.selectedItems);
    });
  }

  deleteDeployment(deployment: DeploymentListingItem) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: DeploymentDeleteComponentData = {
      deploymentId: deployment.pk,
      deploymentName: deployment.deployment_id,
      projectSlug: this.projectDataService.currentProjectSlug,
    };
    this.dialog
      .open(DeploymentDeleteConfirmationComponent, {
        data,
        width: '500px',
        height: 'auto',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.projectDeploymentListingService.refresh();
        }
      });
  }

  openUserDetails(owner: DeploymentListingItemOwner) {
    this.profilePreviewDialogService.open(owner.id);
  }

  onSelectedItemsChange(selectedItems: Set<DeploymentListingItem>) {
    this.selectedItems = selectedItems;
    this.selectedItemsChange.emit(this.selectedItems);
  }

  rowSelectionEnabledFunction(item: DeploymentListingItem) {
    return item.can_update;
  }
}

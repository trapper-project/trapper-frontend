import { Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DeploymentPreview,
  DeploymentPreviewService,
} from '../services/deployment-preview.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatIconModule } from '@angular/material/icon';
import { TranslocoModule } from '@jsverse/transloco';
import {
  BadgeComponent,
  DetailsCardItemComponent,
  MapComponent,
  ProgressBarComponent,
  SimpleCheckmarkComponent,
  UserMiniAvatarComponent,
  UserProfileLinkComponent,
  YesNoFlagComponent,
} from '@trapper/ui';
import { DeploymentPreviewStatsSectionComponent } from '../deployment-preview-stats-section/deployment-preview-stats-section.component';
import { inOutAnimation } from '@trapper/animations';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import {
  DeploymentDeleteComponentData,
  DeploymentDeleteConfirmationComponent,
} from '../deployment-delete-confirmation/deployment-delete-confirmation.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  CsProfilePreviewModule,
  ProfilePreviewService,
} from '@trapper/profile-preview';
import {
  BulkUpdateTimestampsComponent,
  BulkUpdateTimestampsComponentData,
} from '../bulk-update-timestamps/bulk-update-timestamps.component';
import {
  EMPTY,
  combineLatest,
  filter,
  map,
  shareReplay,
  switchMap,
} from 'rxjs';
import { ProjectDataService } from '../services/project-data.service';
import { PageTitleService } from '@trapper/page-title-management';
import { DeploymentProcessingProgressService } from '../services/deployment-processing-progress.service';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-preview',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    TranslocoModule,
    UserMiniAvatarComponent,
    MapComponent,
    DeploymentPreviewStatsSectionComponent,
    DetailsCardItemComponent,
    UserProfileLinkComponent,
    YesNoFlagComponent,
    RouterModule,
    DialogModule,
    MatTooltipModule,
    SimpleCheckmarkComponent,
    CsProfilePreviewModule,
    BadgeComponent,
    ProgressBarComponent,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './deployment-preview.component.html',
  styleUrls: ['./deployment-preview.component.scss'],
  providers: [DeploymentPreviewService, DeploymentProcessingProgressService],
  animations: [inOutAnimation],
})
export class DeploymentPreviewComponent implements OnInit {
  data$ = this.deploymentPreviewService.data$;
  baitType$ = this.deploymentPreviewService.baitType$;
  currentProjectSlug?: string | null;
  currentDeploymentId?: string | null;

  progress$ = this.deploymentProcessingProgressService.progress$.pipe(
    map((progress) => {
      if (progress === null || progress === undefined) {
        return undefined;
      }
      return progress * 100;
    }),
    shareReplay(1),
  );
  finished$ = this.deploymentProcessingProgressService.finished$;

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  constructor(
    private deploymentPreviewService: DeploymentPreviewService,
    private deploymentProcessingProgressService: DeploymentProcessingProgressService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: Dialog,
    private profilePreviewService: ProfilePreviewService,
    private titleService: PageTitleService,
    private projectDataService: ProjectDataService,
  ) {}

  ngOnInit(): void {
    this.titleService.resetTitle();
    combineLatest([
      this.deploymentPreviewService.data$,
      this.projectDataService.data$,
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([deployment, project]) => {
        const titleParts = [];
        if (deployment) {
          titleParts.push(deployment.deployment_id);
        }
        if (project) {
          titleParts.push(project.name);
        }
        this.titleService.setSectionTitle(titleParts);
      });

    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const projectSlug = params.get('projectId');
      const deploymentId = params.get('deploymentId');
      this.currentProjectSlug = projectSlug;
      this.currentDeploymentId = deploymentId;
      if (!projectSlug || !deploymentId) {
        return;
      }
      this.deploymentPreviewService
        .fetchDeploymentPreview(projectSlug, deploymentId)
        .pipe(untilDestroyed(this))
        .subscribe();
    });

    this.deploymentPreviewService.data$
      .pipe(
        switchMap((data) => {
          if (!data) {
            return EMPTY;
          }

          const probablyFinished = data.thumbnails.length > 0;
          if (probablyFinished) {
            return EMPTY;
          }

          if (!this.currentProjectSlug || !this.currentDeploymentId) {
            return EMPTY;
          }

          return this.deploymentProcessingProgressService.fetchProgressDataUntilFinished(
            this.currentProjectSlug,
            this.currentDeploymentId,
          );
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.finished$
      .pipe(
        filter((finished) => finished),
        switchMap(() => {
          if (!this.currentProjectSlug || !this.currentDeploymentId) {
            return EMPTY;
          }
          return this.deploymentPreviewService.fetchDeploymentPreview(
            this.currentProjectSlug,
            this.currentDeploymentId,
          );
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  deleteDeployment(deployment: DeploymentPreview) {
    if (!this.currentProjectSlug) {
      return;
    }
    const data: DeploymentDeleteComponentData = {
      deploymentId: deployment.pk,
      deploymentName: deployment.deployment_id,
      projectSlug: this.currentProjectSlug,
    };
    this.dialog
      .open(DeploymentDeleteConfirmationComponent, {
        data,
        width: '500px',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['../../'], { relativeTo: this.route });
        }
      });
  }

  updateTime(deployment: DeploymentPreview) {
    if (!this.currentProjectSlug) {
      return;
    }

    const data: BulkUpdateTimestampsComponentData = {
      deploymentId: deployment.pk,
      deploymentName: deployment.deployment_id,
      projectSlug: this.currentProjectSlug,
    };

    this.dialog
      .open(BulkUpdateTimestampsComponent, {
        data,
        width: '500px',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result && this.currentProjectSlug && this.currentDeploymentId) {
          this.deploymentPreviewService
            .fetchDeploymentPreview(
              this.currentProjectSlug,
              this.currentDeploymentId,
            )
            .pipe(untilDestroyed(this))
            .subscribe();
        }
      });
  }

  viewProfile(userId: number) {
    this.profilePreviewService.open(userId);
  }

  classifyDeployment(deployment: DeploymentPreview) {
    if (!this.currentProjectSlug) {
      return;
    }
    const firstThumbnailId = deployment.thumbnails[0]?.pk;
    if (!firstThumbnailId) {
      return;
    }
    this.router.navigate([`../../../classify`, firstThumbnailId], {
      relativeTo: this.route,
      queryParams: {
        deployment: deployment.pk,
      },
    });
  }
}
export default DeploymentPreviewComponent;

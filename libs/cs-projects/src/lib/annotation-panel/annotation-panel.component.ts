import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnnotationStateService } from '../services/annotation-state.service';
import {
  Observable,
  combineLatest,
  distinctUntilChanged,
  map,
  shareReplay,
} from 'rxjs';
import {
  BadgeComponent,
  ButtonComponent,
  CheckboxComponent,
  FormFieldComponent,
  LabelComponent,
  SelectBoxInputComponent,
  SwitchComponent,
  TextInputComponent,
} from '@trapper/ui';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import {
  ClassificationSpec,
  ProjectDataService,
} from '../services/project-data.service';
import { FormsModule } from '@angular/forms';
import { TranslocoModule } from '@jsverse/transloco';
import { BboxDeleteConfirmationComponent } from '../bbox-delete-confirmation/bbox-delete-confirmation.component';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import {
  RequestNewSpeciesModalComponent,
  RequestNewSpeciesModalComponentData,
} from '../request-new-species-modal/request-new-species-modal.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { bootstrapExclamationTriangleFill } from '@ng-icons/bootstrap-icons';
import { ClassificationDataService } from '../services/classification-data.service';

export interface SelectedBboxesAttributeValue {
  attribute: string;
  value: string | null | number;
  sameValueForAll: boolean;
  errors: string[] | null;
  confidence: number | null;
}

export interface AnnotationFormField {
  value: string;
  label: string;
}

export interface AnnotationForm {
  fields: AnnotationFormField[];
}

function hideObservationTypeBlank(
  spec: ClassificationSpec,
): ClassificationSpec {
  const dynamic = spec.dynamic.map((attr) => {
    const selectBoxChoices =
      attr.name === 'observation_type'
        ? attr.selectBoxChoices?.filter((choice) => choice.id !== 'blank')
        : attr.selectBoxChoices;
    return {
      ...attr,
      selectBoxChoices,
    };
  });

  return {
    ...spec,
    dynamic,
  };
}

function hideClassifierOptionsWhenNotAnimal(
  annotationState: Record<string, SelectedBboxesAttributeValue>,
  spec: ClassificationSpec,
) {
  const isAnimal = annotationState['observation_type']?.value === 'animal';

  const dynamic = spec.dynamic.map((attr) => {
    return {
      ...attr,
      show: attr.show && (attr.name === 'observation_type' || isAnimal),
    };
  });

  return {
    ...spec,
    dynamic,
  };
}

@Component({
  selector: 'trapper-frontend-annotation-panel',
  standalone: true,
  imports: [
    CommonModule,
    BadgeComponent,
    MatTooltipModule,
    MatIconModule,
    FormFieldComponent,
    SelectBoxInputComponent,
    CheckboxComponent,
    LabelComponent,
    FormsModule,
    ButtonComponent,
    TranslocoModule,
    DialogModule,
    OverlayModule,
    SwitchComponent,
    TextInputComponent,
    NgScrollbarModule,
    NgIconComponent,
  ],
  providers: [
    provideIcons({
      bootstrapExclamationTriangleFill,
    }),
  ],
  templateUrl: './annotation-panel.component.html',
  styleUrls: ['./annotation-panel.component.scss'],
})
export class AnnotationPanelComponent {
  @Input() canSaveClassification = false;
  @Input() groupClassificationMode = false;
  @Input() currentClassificationId?: number | null;
  @Input() savingInProgress = false;

  @Output() saveClassification = new EventEmitter<boolean>();

  @Input() proceedToNextClassificationOnSave = false;
  @Output() proceedToNextClassificationOnSaveChange =
    new EventEmitter<boolean>();

  annotationState$ = this.annotationStateService.annotationState$;
  bboxes$ = this.annotationStateService.bboxes$.pipe(
    map((bboxes) => {
      return bboxes.map((bbox) => {
        return {
          id: bbox.bboxId,
          hasErrors: bbox.errors && Object.entries(bbox.errors).length > 0,
        };
      });
    }),
  );
  haveBboxes$ = this.annotationState$.pipe(
    map((state) => Object.keys(state.state).length > 0),
  );
  selectedBboxesIds$ = this.annotationState$.pipe(
    map((state) => new Set(state.selectedBboxes)),
  );
  selectedBboxes$ = combineLatest([this.bboxes$, this.selectedBboxesIds$]).pipe(
    map(([bboxes, selectedBboxesIds]) => {
      return bboxes.filter((bbox) => selectedBboxesIds.has(bbox.id));
    }),
  );

  classificationSpec$ = this.projectDataService.classificationSpec$;
  isErrorProof$ = this.projectDataService.isErrorProof$;
  hasUserClassification$ =
    this.classificationDataService.hasUserClassification$;
  isApproved$ = this.classificationDataService.isApproved$;
  classificationLoaded$ = this.classificationDataService.classificationLoaded$;
  isCurrentUserClassificationApproved$ =
    this.classificationDataService.isCurrentUserClassificationApproved$;

  actionsAvailable$ = combineLatest([
    this.classificationLoaded$,
    this.haveBboxes$,
    this.isApproved$,
    this.isErrorProof$,
    this.isCurrentUserClassificationApproved$,
  ]).pipe(
    map(
      ([
        classificationLoaded,
        haveBboxes,
        isApproved,
        isErrorProof,
        isCurrentUserClassificationApproved,
      ]) => {
        if (!classificationLoaded) {
          return null;
        }
        const actions = {
          classify: false,
          classifyEmpty: false,
          feedback: false,
          feedbackEmpty: false,
        };

        let canHaveFeedback = false;
        let canBeClassified = false;

        // classification can only have feedback if it is approved
        if (isApproved) {
          // error proof user cannot send feedback to their own classification
          if (isErrorProof && !isCurrentUserClassificationApproved) {
            canHaveFeedback = true;
          } else if (!isErrorProof) {
            canHaveFeedback = true;
          }
        }

        if (!isApproved || isErrorProof) {
          canBeClassified = true;
        }

        if (canHaveFeedback) {
          actions.feedback = haveBboxes;
          actions.feedbackEmpty = !haveBboxes;
        }

        if (canBeClassified) {
          actions.classify = haveBboxes;
          actions.classifyEmpty = !haveBboxes;
        }

        return actions;
      },
    ),
  );

  currentAnnotationState$: Observable<
    Record<string, SelectedBboxesAttributeValue>
  > = combineLatest([this.annotationState$, this.classificationSpec$]).pipe(
    distinctUntilChanged(),
    map(([annotationState, spec]) => {
      const attirbutes = spec?.dynamic.map((dynamic) => dynamic.name) || [];

      const selectedBboxes: { label: string; hasErrors: boolean }[] = [];
      for (const [label, bboxState] of Object.entries(annotationState.state)) {
        if (!annotationState.selectedBboxes.includes(label)) {
          continue;
        }
        let hasErrors = false;
        if (bboxState?.errors && Object.entries(bboxState.errors).length > 0) {
          hasErrors = true;
        }
        selectedBboxes.push({
          label,
          hasErrors,
        });
      }

      const selectedBboxesState = (selectedBboxes || []).map((bbox) => {
        const bboxState = annotationState.state[bbox.label];
        return bboxState;
      });

      const attributeValues = attirbutes.map((attribute) => {
        const allValues = selectedBboxesState.map((bboxState) => {
          if (!bboxState) {
            return null;
          }
          return bboxState.values[attribute];
        });
        const allDistinctValues = [...new Set(allValues)];
        const sameValueForAll = allDistinctValues.length === 1;
        const value = sameValueForAll ? allDistinctValues[0] : null;
        const allErrors = selectedBboxesState[0]?.errors;
        const errors =
          sameValueForAll && allErrors ? allErrors[attribute] : null;
        const confidence =
          allValues.length !== 1
            ? null
            : selectedBboxesState[0]?.confidence[attribute] || null;

        return {
          attribute,
          value,
          sameValueForAll,
          errors,
          confidence,
        };
      });

      return Object.fromEntries(
        attributeValues.map((attributeValue) => [
          attributeValue.attribute,
          attributeValue,
        ]),
      );
    }),
  );

  viewData$ = combineLatest([
    this.currentAnnotationState$,
    this.classificationSpec$,
    this.selectedBboxes$,
    this.projectDataService.data$,
  ]).pipe(
    map(([annotationState, spec, selectedBboxes, projectData]) => {
      if (!spec || !selectedBboxes || !annotationState) {
        return null;
      }
      let newSpec = hideObservationTypeBlank(spec);
      if (projectData?.hide_classification_attributes_for_non_animals) {
        newSpec = hideClassifierOptionsWhenNotAnimal(annotationState, newSpec);
      }

      const fields = newSpec.dynamic.map((dynamic) => {
        const attrState = annotationState[dynamic.name];
        const value = attrState?.value;
        const errors = attrState?.errors;
        const sameValueForAll = attrState?.sameValueForAll;
        const confidence = attrState?.confidence
          ? Math.round(attrState.confidence * 100)
          : null;

        const confidenceWarning =
          confidence &&
          dynamic.confidence_warning_threshold &&
          confidence < dynamic.confidence_warning_threshold * 100;

        const willBeBlurredWarning =
          projectData?.blur_humans_and_vehicles_immediately &&
          dynamic.name === 'observation_type' &&
          ['human', 'vehicle'].includes(value as string);

        return {
          ...dynamic,
          value,
          errors,
          sameValueForAll,
          confidence,
          confidenceWarning,
          willBeBlurredWarning,
        };
      });

      return {
        fields,
        selectedBboxes,
      };
    }),
    shareReplay(1),
  );

  constructor(
    private annotationStateService: AnnotationStateService,
    private projectDataService: ProjectDataService,
    private classificationDataService: ClassificationDataService,
    private dialog: Dialog,
    private overlay: Overlay,
  ) {}

  updateBboxAttribute(
    bboxes: { id: string }[],
    attribute: string,
    value: string | null,
  ) {
    const bboxIds = bboxes.map((bbox) => bbox.id);
    this.annotationStateService.updateAttributes(bboxIds, {
      [attribute]: value,
    });
  }

  saveClassificationClick(feedback: boolean) {
    this.saveClassification.emit(feedback);
  }

  deleteBboxes(bboxes: { id: string }[]) {
    const dialog = this.dialog.open(BboxDeleteConfirmationComponent, {
      positionStrategy: this.overlay
        .position()
        .global()
        .top('54px')
        .centerHorizontally(),
      disableClose: true,
    });

    dialog.closed.subscribe((result) => {
      if (!result) {
        return;
      }
      const bboxIds = bboxes.map((bbox) => bbox.id);
      bboxIds.forEach((bboxId) => {
        this.annotationStateService.deleteBbox(bboxId);
      });
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  activateInputForField(_fieldName: string) {
    // TODO: figure out how to do this
  }

  requestNewSpeciesClick() {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    if (!this.currentClassificationId) {
      return;
    }
    const data: RequestNewSpeciesModalComponentData = {
      currentProjectSlug: this.projectDataService.currentProjectSlug,
      classificationId: this.currentClassificationId,
    };
    this.dialog.open(RequestNewSpeciesModalComponent, {
      data,
      positionStrategy: this.overlay
        .position()
        .global()
        .top('54px')
        .centerHorizontally(),
    });
  }
}

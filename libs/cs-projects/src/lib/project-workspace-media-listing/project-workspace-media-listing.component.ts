import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  BadgeComponent,
  ButtonComponent,
  CheckboxComponent,
  DateRange,
  DateRangeInputComponent,
  FormFieldComponent,
  LabelComponent,
  PaginatorComponent,
  ResponsiveTableComponent,
  RetryImageOnErrorDirective,
  SelectBoxInputComponent,
  TableCellDefDirective,
  TextInputComponent,
  ThumbnailWithBboxesComponent,
  UserMiniAvatarComponent,
  YesNoFlagComponent,
} from '@trapper/ui';
import {
  combineLatest,
  combineLatestWith,
  distinctUntilChanged,
  map,
  shareReplay,
  take,
  tap,
} from 'rxjs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ProjectDataService } from '../services/project-data.service';
import {
  MediaListingItem,
  ProjectMediaListingService,
} from '../services/project-media-listing.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EllipsisModule } from 'ngx-ellipsis';
import { TagColorsService } from '../services/tag-colors.service';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
// import { LightboxModule } from 'ng-gallery/lightbox';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { inOutAnimation } from '@trapper/animations';
import { MediaListingGalleryComponent } from '../media-listing-gallery/media-listing-gallery.component';
import { Overlay } from '@angular/cdk/overlay';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { format, parse } from 'date-fns';
import {
  CsProfilePreviewModule,
  ProfilePreviewService,
} from '@trapper/profile-preview';
import { PageTitleService } from '@trapper/page-title-management';
import { MarkAsFavouriteService } from '../services/mark-as-favourite.service';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-media-listing',
  standalone: true,
  imports: [
    CommonModule,
    SelectBoxInputComponent,
    TextInputComponent,
    MatDatepickerModule,
    MatNativeDateModule,
    CheckboxComponent,
    ButtonComponent,
    FormsModule,
    BadgeComponent,
    MatIconModule,
    YesNoFlagComponent,
    PaginatorComponent,
    NgxSkeletonLoaderModule,
    FormsModule,
    RouterModule,
    MatTooltipModule,
    EllipsisModule,
    RetryImageOnErrorDirective,
    DialogModule,
    // LightboxModule,
    TranslocoModule,
    FormFieldComponent,
    LabelComponent,
    NgScrollbarModule,
    ScrollingModule,
    DateRangeInputComponent,
    UserMiniAvatarComponent,
    CsProfilePreviewModule,
    ThumbnailWithBboxesComponent,
    ResponsiveTableComponent,
    TableCellDefDirective,
  ],
  providers: [
    ProjectMediaListingService,
    TagColorsService,
    MarkAsFavouriteService,
  ],
  templateUrl: './project-workspace-media-listing.component.html',
  styleUrls: ['./project-workspace-media-listing.component.scss'],
  animations: [inOutAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectWorkspaceMediaListingComponent implements OnInit {
  @ViewChild('galleryBoxTemplate') galleryBoxTemplate?: TemplateRef<void>;
  galleryId = 'trapper-media-listing-gallery';
  species$ = this.projectDataService.availableFilters$.pipe(
    map((filters) => filters?.observation_type),
  );

  showAdvancedFilters = false;

  availableFilters$ = this.projectDataService.availableFilters$;
  classifierFilters$ = this.projectDataService.classificationSpecFilters$;

  classifierFiltersBasic$ = this.classifierFilters$.pipe(
    map((filters) => filters.filter((f) => f.name === 'species')),
  );
  classifierFiltersAdvanced$ = this.classifierFilters$.pipe(
    map((filters) => filters.filter((f) => f.name !== 'species')),
  );

  locationChoices$ = this.projectDataService.locationChoices$;
  deploymentChoices$ = this.projectDataService.deploymentChoices$;
  teamChoices$ = this.projectDataService.teamChoices$;

  listingItems$ = this.projectMediaListingService.data$;
  pagination$ = this.projectMediaListingService.pagination$;

  results$ = this.listingItems$.pipe(
    map((dto) => dto?.results),
    shareReplay(1),
  );
  noItems$ = this.listingItems$.pipe(
    map((dto) => dto?.results.length === 0),
    shareReplay(1),
  );

  error$ = this.projectMediaListingService.error$;

  currentDateRange$ = this.projectMediaListingService.filter$.pipe(
    map((filters) => {
      const startString = filters['rdate_from'] || null;
      const endString = filters['rdate_to'] || null;
      return { startString, endString };
    }),
    distinctUntilChanged(
      (a, b) => a.startString === b.startString && a.endString === b.endString,
    ),
    map(({ startString, endString }) => {
      const start = startString
        ? parse(startString, 'yyyy-MM-dd', new Date())
        : null;
      const end = endString ? parse(endString, 'yyyy-MM-dd', new Date()) : null;
      return { start, end };
    }),
    shareReplay(1),
  );

  constructor(
    private projectMediaListingService: ProjectMediaListingService,
    private projectDataService: ProjectDataService,
    private route: ActivatedRoute,
    private router: Router,
    private tagColorService: TagColorsService,
    private dialog: Dialog,
    private overlay: Overlay,
    private profilePreviewService: ProfilePreviewService,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    private markAsFavouriteService: MarkAsFavouriteService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  ngOnInit(): void {
    combineLatest([
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'mediaListingPageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([projectData, title]) => {
        const titleParts = [];
        titleParts.push(title);
        if (projectData) {
          titleParts.push(projectData.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.projectMediaListingService
      .connect()
      .pipe(untilDestroyed(this))
      .subscribe();

    this.route.paramMap
      .pipe(
        tap((params) => {
          const projectSlug = params.get('projectId');
          if (projectSlug) {
            this.projectMediaListingService.projectSlug$.next(projectSlug);
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.route.queryParamMap
      .pipe(
        take(1),
        tap((query) => {
          const nonAdvancedFilters = ['location', 'deployment', 'page'];
          const advancedFilters = query.keys.filter(
            (k) => !nonAdvancedFilters.includes(k),
          );
          this.showAdvancedFilters = advancedFilters.length > 0;
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.route.queryParamMap
      .pipe(
        tap((query) => {
          const filters = Object.fromEntries(
            query.keys.map((k) => [k, query.get(k)]),
          );
          this.projectMediaListingService.filter$.next(filters);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  pageChanged(page: number) {
    this.router.navigate([], {
      queryParams: { page },
      queryParamsHandling: 'merge',
    });
  }

  filterChanged(filterName: string, filterValue: string | null | string[]) {
    if (filterValue instanceof Array) {
      filterValue = filterValue.join(',');
    }
    this.router.navigate([], {
      queryParams: {
        [filterName]: filterValue,
        page: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  currentFilters$(field: string) {
    return this.projectMediaListingService.filter$.pipe(
      map((filters) => filters[field] || null),
    );
  }

  currentFiltersMultiByField$ = this.projectMediaListingService.filter$.pipe(
    combineLatestWith(this.classifierFilters$),
    map(([filters, classifierFilters]) => {
      const data = classifierFilters.map((f) => {
        const currentValue = filters[f.name];
        if (!currentValue) {
          return [f.name, null];
        }
        if (f.is_multiselect && typeof currentValue === 'string') {
          return [f.name, currentValue.split(',')];
        }
        return [f.name, currentValue];
      });

      return Object.fromEntries(data);
    }),
    shareReplay(1),
  );

  currentFiltersMulti$(field: string) {
    return this.currentFiltersMultiByField$.pipe(
      map((filters) => filters[field] || null),
    );
  }

  dateRangeChanged(range: DateRange) {
    const rdate_from = range.start ? format(range.start, 'yyyy-MM-dd') : null;
    const rdate_to = range.end ? format(range.end, 'yyyy-MM-dd') : null;
    const queryParams = {
      rdate_from,
      rdate_to,
      page: null,
    };
    this.router.navigate([], {
      queryParams,
      queryParamsHandling: 'merge',
    });
  }

  obsTypeTagColors(obsType: string) {
    return this.tagColorService
      .getColorMappingForTagType('observation_type')
      .get(obsType);
  }

  showGalleryDialog(itemId: number) {
    this.dialog.open(MediaListingGalleryComponent, {
      data: {
        currentId: itemId,
      },
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
      providers: [
        {
          provide: ProjectMediaListingService,
          useValue: this.projectMediaListingService,
        },
        {
          provide: ProjectDataService,
          useValue: this.projectDataService,
        },
        {
          provide: MarkAsFavouriteService,
          useValue: this.markAsFavouriteService,
        },
      ],
    });
  }

  viewProfile(userId: number) {
    this.profilePreviewService.open(userId);
  }

  markAsFavourite(item: MediaListingItem) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    this.markAsFavouriteService
      .markAsFavourite(
        this.projectDataService.currentProjectSlug,
        item.pk,
        !item.is_favorite,
      )
      .pipe(untilDestroyed(this))
      .subscribe(() => this.projectMediaListingService.reload$.next());
  }
}
export default ProjectWorkspaceMediaListingComponent;

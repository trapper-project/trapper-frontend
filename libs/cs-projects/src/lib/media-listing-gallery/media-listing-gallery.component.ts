import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MediaListingItem,
  ProjectMediaListingService,
} from '../services/project-media-listing.service';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import {
  BehaviorSubject,
  Subject,
  combineLatest,
  delay,
  map,
  of,
  shareReplay,
  withLatestFrom,
} from 'rxjs';
import { ThumbnailWithBboxesComponent } from '@trapper/ui';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  CdkVirtualScrollViewport,
  ScrollingModule,
} from '@angular/cdk/scrolling';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HotkeysService } from '@ngneat/hotkeys';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslocoModule } from '@jsverse/transloco';
import { ProjectDataService } from '../services/project-data.service';
import { MarkAsFavouriteService } from '../services/mark-as-favourite.service';
import {
  AnnotatedObject,
  ResourceAnnotatorComponent,
} from '@trapper/resource-annotator';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-media-listing-gallery',
  standalone: true,
  imports: [
    CommonModule,
    ThumbnailWithBboxesComponent,
    NgScrollbarModule,
    InfiniteScrollModule,
    ScrollingModule,
    MatIconModule,
    RouterModule,
    MatTooltipModule,
    TranslocoModule,
    ResourceAnnotatorComponent,
  ],
  templateUrl: './media-listing-gallery.component.html',
  styleUrls: ['./media-listing-gallery.component.scss'],
})
export class MediaListingGalleryComponent implements OnInit {
  @ViewChild('scrollViewport') scrollViewport?: CdkVirtualScrollViewport;
  @ViewChild('scrolledContent') scrolledContent?: ElementRef<HTMLElement>;
  @ViewChild('rollContainer') rollContainer?: ElementRef<HTMLElement>;

  itemWidth = 112 + 24;

  listingItems$ = this.projectMediaListingService.data$.pipe(
    map((data) => data?.results || null),
  );

  currentItemId$ = new BehaviorSubject<number | null>(null);

  currentItem$ = combineLatest([this.listingItems$, this.currentItemId$]).pipe(
    map(([items, itemId]) => {
      if (itemId === null || !items) {
        return null;
      }
      return items.find((item) => item.pk === itemId) || null;
    }),
    shareReplay(1),
  );

  objects$ = this.currentItem$.pipe(
    map((item): AnnotatedObject[] => {
      if (!item || !item.bboxes_raw) {
        return [];
      }
      return item.bboxes_raw.map((bboxes, index) => ({
        id: `bbox${index}`,
        bboxes,
        label: `bbox${index}`,
      }));
    }),
  );

  goToNext$ = new Subject<boolean>();
  goToPrev$ = new Subject<boolean>();

  currentProject$ = this.projectDataService.data$;

  goToFirstAfterLoad = false;
  goToLastAfterLoad = false;

  constructor(
    private projectMediaListingService: ProjectMediaListingService,
    private projectDataService: ProjectDataService,
    private markAsFavouriteService: MarkAsFavouriteService,
    private hotkeys: HotkeysService,
    public route: ActivatedRoute,
    private dialogRef: DialogRef,
    private router: Router,
    @Inject(DIALOG_DATA) data: { currentId: number },
  ) {
    this.setCurrentItem(data.currentId);

    this.listingItems$.pipe(untilDestroyed(this)).subscribe((items) => {
      if (this.goToFirstAfterLoad && items && items.length > 0) {
        this.setCurrentItem(items[0].pk);
        this.goToFirstAfterLoad = false;
      }
      if (this.goToLastAfterLoad && items && items.length > 0) {
        this.setCurrentItem(items[items.length - 1].pk);
        this.goToLastAfterLoad = false;
      }
    });

    combineLatest([this.currentItemId$, this.listingItems$])
      .pipe(delay(0), untilDestroyed(this))
      .subscribe(([currentItem, items]) => {
        if (!currentItem || !items) {
          return;
        }
        if (!items.find((item) => item.pk === currentItem)) {
          if (items.length > 0) {
            this.setCurrentItem(items[0].pk);
          } else {
            this.close();
          }
        }
      });

    combineLatest([this.currentItem$, this.listingItems$])
      .pipe(delay(0), untilDestroyed(this))
      .subscribe(([currentItem, items]) => {
        if (!currentItem || !items) {
          return;
        }
        this.scrollToItem(currentItem.pk, items);
      });

    this.goToNext$
      .pipe(
        withLatestFrom(this.currentItem$, this.listingItems$),
        untilDestroyed(this),
      )
      .subscribe(([, currentItem, items]) => {
        if (!currentItem || !items) {
          return;
        }
        const itemIndex = items.findIndex((item) => item.pk === currentItem.pk);
        if (itemIndex === -1 || itemIndex === items.length - 1) {
          this.goToFirstAfterLoad = true;
          this.requestNextPage();
          return;
        }
        const nextItem = items[itemIndex + 1];
        this.setCurrentItem(nextItem.pk);
      });

    this.goToPrev$
      .pipe(
        withLatestFrom(this.currentItem$, this.listingItems$),
        untilDestroyed(this),
      )
      .subscribe(([, currentItem, items]) => {
        if (!currentItem || !items) {
          return;
        }
        const itemIndex = items.findIndex((item) => item.pk === currentItem.pk);
        if (itemIndex === -1 || itemIndex === 0) {
          this.goToLastAfterLoad = true;
          this.requestPrevPage();
          return;
        }
        const prevItem = items[itemIndex - 1];
        this.setCurrentItem(prevItem.pk);
      });
  }

  scrollToItem(itemId: number, items: MediaListingItem[]) {
    const itemIndex = items.findIndex((item) => item.pk === itemId);
    if (itemIndex === -1) {
      return;
    }
    // center the item in the scroll viewport
    const containerWidth = this.rollContainer?.nativeElement.clientWidth || 0;
    const itemWidth = this.itemWidth;
    const itemOffset = itemIndex * itemWidth;
    const scrollOffset = itemOffset - (containerWidth - itemWidth) / 2;
    this.scrollViewport?.scrollToOffset(scrollOffset, 'smooth');
  }

  setCurrentItem(itemId: number) {
    this.currentItemId$.next(itemId);
  }

  onScroll(event: WheelEvent) {
    if (this.scrollViewport) {
      const currentOffset = this.scrollViewport.measureScrollOffset();
      const delta = event.deltaY * 0.3;
      const newOffset = currentOffset + delta;

      this.scrollViewport.scrollToOffset(newOffset);

      event.preventDefault();
    }
  }

  initHotkeys() {
    this.hotkeys
      .addShortcut({ keys: 'right' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.goToNext$.next(true);
      });

    this.hotkeys
      .addShortcut({ keys: 'left' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.goToPrev$.next(true);
      });
  }

  ngOnInit(): void {
    this.initHotkeys();
  }

  close() {
    this.dialogRef.close();
  }

  onPrev() {
    this.goToPrev$.next(true);
  }

  onNext() {
    this.goToNext$.next(true);
  }

  markAsFavourite(item: MediaListingItem) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    this.markAsFavouriteService
      .markAsFavourite(
        this.projectDataService.currentProjectSlug,
        item.pk,
        !item.is_favorite,
      )
      .pipe(untilDestroyed(this))
      .subscribe(() => this.projectMediaListingService.reload$.next());
  }

  requestNextPage() {
    of(1)
      .pipe(
        withLatestFrom(this.projectMediaListingService.pagination$),
        map(([, pagination]) => pagination),
        untilDestroyed(this),
      )
      .subscribe((pagination) => {
        if (pagination && pagination.page < pagination.pages) {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              page: pagination.page + 1,
            },
            queryParamsHandling: 'merge',
          });
        }
      });
  }

  requestPrevPage() {
    of(1)
      .pipe(
        withLatestFrom(this.projectMediaListingService.pagination$),
        map(([, pagination]) => pagination),
        untilDestroyed(this),
      )
      .subscribe((pagination) => {
        if (pagination && pagination.page > 1) {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              page: pagination.page - 1,
            },
            queryParamsHandling: 'merge',
          });
        }
      });
  }
}

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-frontend-bbox-delete-confirmation',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    DialogModule,
    TranslocoModule,
  ],
  templateUrl: './bbox-delete-confirmation.component.html',
  styleUrls: ['./bbox-delete-confirmation.component.scss'],
})
export class BboxDeleteConfirmationComponent {
  constructor(private dialogRef: DialogRef) {}

  ok() {
    this.dialogRef.close(true);
  }

  cancel() {
    this.dialogRef.close(false);
  }
}

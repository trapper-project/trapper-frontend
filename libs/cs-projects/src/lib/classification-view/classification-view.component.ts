import {
  Component,
  HostBinding,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  BehaviorSubject,
  EMPTY,
  combineLatest,
  combineLatestWith,
  filter,
  map,
  of,
  shareReplay,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs';
import { ProjectDataService } from '../services/project-data.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { TagColorsService } from '../services/tag-colors.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  BadgeComponent,
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  CompactModeService,
  FormFieldComponent,
  LabelComponent,
  RetryImageOnErrorDirective,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  SidebarStateService,
  ThumbnailWithBboxesComponent,
} from '@trapper/ui';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import {
  ProjectMediaData,
  ClassificationDataService,
} from '../services/classification-data.service';
import { inOutAnimation } from '@trapper/animations';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { DragDropModule } from '@angular/cdk/drag-drop';

import {
  AnnotationStateService,
  generateBboxId,
} from '../services/annotation-state.service';
import { ClassificationService } from '../services/classification.service';
import { ImageRollService } from '../services/image-roll.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Hotkey, HotkeysService } from '@ngneat/hotkeys';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import { ImageRollComponent } from '../image-roll/image-roll.component';
import { AnnotationPanelComponent } from '../annotation-panel/annotation-panel.component';
import { PageTitleService } from '@trapper/page-title-management';
import {
  AnnotatedObject,
  BboxClickEvent,
  ResourceAnnotatorComponent,
} from '@trapper/resource-annotator';
import { BboxDeleteConfirmationComponent } from '../bbox-delete-confirmation/bbox-delete-confirmation.component';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import {
  bootstrapChevronDoubleLeft,
  bootstrapChevronDoubleRight,
  bootstrapChevronLeft,
  bootstrapChevronRight,
} from '@ng-icons/bootstrap-icons';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-classification-view',
  standalone: true,
  imports: [
    CommonModule,
    SelectBoxInputComponent,
    CheckboxComponent,
    ButtonComponent,
    FormsModule,
    MatIconModule,
    NgxSkeletonLoaderModule,
    FormsModule,
    RouterModule,
    MatTooltipModule,
    RetryImageOnErrorDirective,
    TranslocoModule,
    FormFieldComponent,
    LabelComponent,
    CardComponent,
    BadgeComponent,
    NgScrollbarModule,
    DragDropModule,
    ThumbnailWithBboxesComponent,
    ServerErrorsComponent,
    InfiniteScrollModule,
    DialogModule,
    OverlayModule,
    ImageRollComponent,
    AnnotationPanelComponent,
    ResourceAnnotatorComponent,
    DialogModule,
    OverlayModule,
    NgIconComponent,
  ],
  templateUrl: './classification-view.component.html',
  styleUrls: ['./classification-view.component.scss'],
  providers: [
    TagColorsService,
    ClassificationDataService,
    AnnotationStateService,
    ClassificationService,
    ImageRollService,
    provideIcons({
      bootstrapChevronDoubleLeft,
      bootstrapChevronDoubleRight,
      bootstrapChevronLeft,
      bootstrapChevronRight,
    }),
  ],
  animations: [inOutAnimation],
})
export class ClassificationViewComponent implements OnInit, OnDestroy {
  @ViewChild(AnnotationPanelComponent, { static: false })
  annotationPanel?: AnnotationPanelComponent;
  annotationState$ = this.annotationStateService.annotationState$;
  sequenceClassificationMode = false;
  private registeredHotkeys: string[] = [];
  savingInProgress$ = this.classificationService.inProgress$;

  proceedToNextClassificationOnSave = true;

  classificationSpec$ = this.projectDataService.classificationSpec$;

  currentClassificationData$ = this.classificationDataService.data$;

  deploymentPk$ = new BehaviorSubject<string | null>(null);
  currentDeployment$ = this.currentClassificationData$.pipe(
    filter((data): data is ProjectMediaData => !!data),
    map((data) => data.deployment),
    combineLatestWith(this.deploymentPk$),
    map(([deployment, deploymentPk]) => {
      if (String(deployment.id) === String(deploymentPk)) {
        return deployment;
      }
      return null;
    }),
    shareReplay(1),
  );

  deploymentAndFileName$ = this.currentDeployment$.pipe(
    combineLatestWith(this.currentClassificationData$),
    map(([deployment, data]) => {
      if (!deployment || !data) {
        return null;
      }
      return `${deployment.deployment_id} / ${data.file_name}`;
    }),
    shareReplay(1),
  );

  isDirty$ = this.annotationStateService.isDirty$;
  groupClassificationMode$ = this.imageRollService.groupClassificationMode$;

  haveBboxes$ = this.annotationState$.pipe(
    map((state) => Object.keys(state.state).length > 0),
  );

  canSaveClassification$ = combineLatest([
    this.annotationState$,
    this.currentClassificationData$,
    this.groupClassificationMode$,
    this.projectDataService.isErrorProof$,
  ]).pipe(
    map(
      ([
        annotationState,
        currentClassificationData,
        groupClassificationMode,
        isErrorProof,
      ]) => {
        if (!currentClassificationData) {
          return false;
        }

        if (annotationState.dirty || isErrorProof) {
          return true;
        }

        const haveBboxes = Object.keys(annotationState.state).length !== 0;

        if (
          !haveBboxes &&
          !currentClassificationData.user_classification &&
          !currentClassificationData.is_approved
        ) {
          return true;
        }

        if (groupClassificationMode) {
          return true;
        }

        return false;
      },
    ),
    shareReplay(1),
  );

  hasPreviousSequence$ = this.imageRollService.hasPreviousSequence$;
  hasNextSequence$ = this.imageRollService.hasNextSequence$;
  hasNextClassification$ = this.imageRollService.hasNextClassification$;
  hasPreviousClassification$ = this.imageRollService.hasPreviousClassification$;

  get itemsSelected() {
    return this.imageRollService.selectedItems.size;
  }

  classificationSpecDictionaries$ =
    this.projectDataService.classificationSpecDictionaries$;

  bboxes$ = this.annotationStateService.bboxes$.pipe(
    combineLatestWith(this.classificationSpecDictionaries$),
    map(([bboxes, dictionaries]) => {
      if (!bboxes || !dictionaries) {
        return null;
      }
      const speciesMap = dictionaries['species'] || {};
      const observationTypeMap = dictionaries['observation_type'] || {};
      return bboxes.map((bbox) => {
        let label = bbox.bboxId;
        const speciesId = bbox.values['species'];
        const observationTypeId = bbox.values['observation_type'];
        if (speciesId) {
          const species = speciesMap[speciesId] || `#${speciesId}`;
          label += '. ' + species;
        } else if (observationTypeId) {
          const observation_type = observationTypeMap[observationTypeId];
          label += '. ' + observation_type;
        }
        return {
          id: bbox.bboxId,
          label,
          bboxes: bbox.bboxes,
          hasErrors: bbox.errors && Object.entries(bbox.errors).length > 0,
        };
      });
    }),
  );

  selectedBboxesIds$ = this.annotationState$.pipe(
    map((state) => new Set(state.selectedBboxes)),
  );

  _projectSlug?: string | null;
  currentClassificationId?: number | null;
  private _sidebarPresentInitial = false;

  constructor(
    private projectDataService: ProjectDataService,
    private route: ActivatedRoute,
    private router: Router,
    private classificationDataService: ClassificationDataService,
    private annotationStateService: AnnotationStateService,
    private classificationService: ClassificationService,
    private imageRollService: ImageRollService,
    private hotkeys: HotkeysService,
    private pageTitleService: PageTitleService,
    private dialog: Dialog,
    private overlay: Overlay,
    private sidebarStateService: SidebarStateService,
    private compactModeService: CompactModeService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  ngOnInit(): void {
    this.compactModeService.setCompactMode(true);
    this._sidebarPresentInitial = this.sidebarStateService.sidebarExpanded;
    this.sidebarStateService.collapseSidebar();
    this.pageTitleService.resetTitle();
    this.restoreSavedProceedToNextClassificationOnSaveValue();
    combineLatest([
      this.classificationDataService.data$,
      this.translocoService.selectTranslate<string>(
        'classifyResourcePageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([data, title]) => {
        const titleParts = [];
        if (data) {
          titleParts.push(data.name);
        }
        titleParts.push(title);
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.route.paramMap
      .pipe(
        tap((params) => {
          this._projectSlug = params.get('projectId');

          const classificationIdStr = params.get('classificationId');

          this.currentClassificationId = classificationIdStr
            ? +classificationIdStr || null
            : null;

          if (this._projectSlug && this.currentClassificationId) {
            if (!this.sequenceClassificationMode) {
              this.annotationStateService.clearAll();
              this.bboxSelectionChanged(new Set());
            }
            this.classificationDataService
              .reloadMediaData(this._projectSlug, this.currentClassificationId)
              .pipe(take(1))
              .subscribe();

            this.imageRollService.setClassificationAndProject(
              +this.currentClassificationId,
              this._projectSlug,
            );
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.route.queryParamMap
      .pipe(
        tap((query) => {
          const filters = Object.fromEntries(
            query.keys.map((k) => [k, query.get(k)]),
          );
          if (filters['deployment']) {
            this.deploymentPk$.next(filters['deployment'] || null);
          }
          this.imageRollService.setFilters(filters);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();

    combineLatest([
      this.currentClassificationData$,
      this.groupClassificationMode$,
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([data, groupClassificationMode]) => {
        if (!data) {
          return;
        }
        const annotations =
          data.user_classification && !data.is_approved
            ? data.user_classification
            : data.classification;
        if (!annotations) {
          return;
        }
        const currentConsensusValues =
          this.annotationStateService.currentConsensusValues;
        this.annotationStateService.clearAll();
        for (let i = 0; i < annotations.dynamic.length; i++) {
          const attr = annotations.dynamic[i];
          if (attr['observation_type'] === 'blank') {
            continue;
          }
          const bboxLabel = generateBboxId(i);
          const bboxAttributes: Record<string, string | null> = {};
          for (const [key, value] of Object.entries(
            attr as Record<string, string | null>,
          )) {
            if (
              key === 'bboxes' ||
              key === 'bboxes_raw' ||
              key === 'attr_confidence'
            ) {
              continue;
            }
            bboxAttributes[key] = value;
          }
          // const bbox = attr.bboxes ? { ...attr.bboxes } : undefined;
          const bboxes = attr.bboxes_raw ? [...attr.bboxes_raw] : undefined;

          if (!bboxes) {
            continue;
          }

          this.annotationStateService.addObject(
            bboxLabel,
            bboxes,
            bboxAttributes,
            attr.attr_confidence || {},
          );
        }
        this.annotationStateService.makePristine();

        if (!data.user_classification && !data.is_approved) {
          this.annotationStateService.markAsDirtyIfEmpty();
        }

        if (groupClassificationMode) {
          this.annotationStateService.selectAllBboxes();
          this.annotationStateService.updateAllAttributes(
            currentConsensusValues,
          );
          this.annotationStateService.markAsDirty();
        }

        // setTimeout(() => {
        //   this.scrollToActiveThumbnail();
        // }, 150);
      });

    this.imageRollService.groupClassificationMode$
      .pipe(untilDestroyed(this))
      .subscribe((mode) => {
        this.sequenceClassificationMode = mode;
        if (mode) {
          this.annotationStateService.selectAllBboxes();
        } else {
          this.annotationStateService.selectBboxes([]);
        }
      });

    this.initHotkeys();
  }

  ngOnDestroy() {
    this.compactModeService.setCompactMode(false);
    if (this._sidebarPresentInitial) {
      this.sidebarStateService.expandSidebar();
    } else {
      this.sidebarStateService.collapseSidebar();
    }
    this.clearHotkeys();
  }

  bboxClicked($event: BboxClickEvent) {
    const bboxId = $event.bboxId;

    if (this.sequenceClassificationMode) {
      return;
    }

    if ($event.event.ctrlKey) {
      this.annotationStateService.toggleBboxSelection(bboxId);
      return;
    }

    this.annotationStateService.selectBboxes([bboxId]);
  }

  bboxSelectionChanged($event: Set<string> | string[]) {
    if (this.sequenceClassificationMode) {
      return;
    }
    this.annotationStateService.selectBboxes(Array.from($event));
  }

  bboxCreated(bbox: AnnotatedObject) {
    this.annotationStateService.addNewEmptyBbox(bbox.bboxes);
  }

  bboxLocationUpdated(bbox: AnnotatedObject) {
    this.annotationStateService.updateBboxLocation(bbox.id, bbox.bboxes);
  }

  saveClassification(isFeedback: boolean | null = null) {
    if (!this._projectSlug || !this.currentClassificationId) {
      return;
    }
    of(1)
      .pipe(
        withLatestFrom(
          this.annotationStateService.annotationState$,
          this.currentClassificationData$,
          this.canSaveClassification$,
          this.classificationSpec$,
        ),
        switchMap(
          ([
            ,
            annotationState,
            currentClassificationData,
            canSaveClassification,
            classificationSpec,
          ]) => {
            if (
              !this._projectSlug ||
              !this.currentClassificationId ||
              !currentClassificationData ||
              !canSaveClassification ||
              !classificationSpec
            ) {
              return EMPTY;
            }

            const classificationIds = Array.from(
              this.imageRollService.selectedItems,
            );

            if (isFeedback === null) {
              isFeedback = currentClassificationData.is_approved;
            }

            const filters: Record<string, string | number> = {};
            for (const [key, value] of Object.entries(
              this.imageRollService.currentFilters,
            )) {
              if (value !== null && value !== undefined) {
                filters[key] = value;
              }
            }

            let nextClassificationToLoad: number | null = null;
            if (classificationIds.length !== 1) {
              nextClassificationToLoad =
                this.imageRollService.getNextNotSelectedClassificationId();
            }

            return this.classificationService
              .postClassification(
                this._projectSlug,
                this.currentClassificationId,
                classificationIds,
                annotationState,
                isFeedback,
                filters,
                classificationSpec,
              )
              .pipe(
                tap((saved) => {
                  if (
                    this.currentClassificationId &&
                    !classificationIds.includes(this.currentClassificationId)
                  ) {
                    classificationIds.push(this.currentClassificationId);
                  }
                  if (saved) {
                    this.imageRollService.clearSelection();
                    if (this.proceedToNextClassificationOnSave) {
                      if (classificationIds.length === 1) {
                        this.imageRollService.goToNextClassification();
                      } else if (nextClassificationToLoad) {
                        this.imageRollService.goToClassification(
                          nextClassificationToLoad,
                        );
                      }
                    }
                  }
                  return this.imageRollService.reloadUpdatedClassifications(
                    classificationIds,
                  );
                }),
              );
          },
        ),
      )
      .subscribe();
  }

  clearHotkeys() {
    this.hotkeys.removeShortcuts(this.registeredHotkeys);
  }

  addShortcut(options: Hotkey) {
    this.registeredHotkeys.push(options.keys);
    return this.hotkeys.addShortcut(options);
  }

  initHotkeys() {
    this.addShortcut({ keys: 'right' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.goToNextClassification();
      });

    this.addShortcut({ keys: 'left' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.goToPreviousClassification();
      });

    this.addShortcut({ keys: 'meta.right' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.goToNextSequence();
      });

    this.addShortcut({ keys: 'meta.left' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.goToPreviousSequence();
      });

    this.addShortcut({ keys: 'shift.right' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.annotationStateService.selectNextBbox();
      });

    this.addShortcut({ keys: 'shift.left' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.annotationStateService.selectPreviousBbox();
      });

    this.addShortcut({ keys: 'meta.a' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.annotationStateService.selectAllBboxes();
      });

    this.addShortcut({ keys: 'meta.shift.a' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.selectCurrentClassificationSequence();
      });

    this.addShortcut({ keys: 'meta.d' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        if (this.sequenceClassificationMode) {
          return;
        }
        this.annotationStateService.selectBboxes([]);
      });

    this.addShortcut({ keys: 'escape' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.annotationStateService.selectBboxes([]);
      });

    this.addShortcut({ keys: 'meta.escape' })
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.imageRollService.clearSelection();
      });

    this.addShortcut({ keys: 'meta.s' }).subscribe(() => {
      this.saveClassification();
    });

    this.addShortcut({ keys: 'meta.down' }).subscribe(() => {
      if (!this.annotationPanel) {
        return;
      }
      this.annotationPanel.activateInputForField('species');
    });
  }

  selectPreviousSequence() {
    this.imageRollService.goToPreviousSequence();
  }

  selectNextSequence() {
    this.imageRollService.goToNextSequence();
  }

  clearSelection() {
    this.imageRollService.clearSelection();
  }

  selectPreviousClassification() {
    this.imageRollService.goToPreviousClassification();
  }

  selectNextClassification() {
    this.imageRollService.goToNextClassification();
  }

  deleteBboxes(bboxIds: string[]) {
    const dialog = this.dialog.open(BboxDeleteConfirmationComponent, {
      positionStrategy: this.overlay
        .position()
        .global()
        .top('54px')
        .centerHorizontally(),
      disableClose: true,
    });

    dialog.closed.subscribe((result) => {
      if (!result) {
        return;
      }
      for (const bboxId of bboxIds) {
        this.annotationStateService.deleteBbox(bboxId);
      }
    });
  }

  resourcePreviewRequested() {
    let url = this.router.serializeUrl(
      this.router.createUrlTree(
        ['../../', 'media', this.currentClassificationId],
        { relativeTo: this.route },
      ),
    );

    if (url.startsWith('/')) {
      url = url.substring(1);
    }

    let baseHref = document.getElementsByTagName('base')[0].href;
    if (baseHref.endsWith('/')) {
      baseHref = baseHref.substring(0, baseHref.length - 1);
    }

    url = baseHref + '/' + url;

    window.open(url, '_blank');
  }

  restoreSavedProceedToNextClassificationOnSaveValue() {
    const rawValue = localStorage.getItem('proceedToNextClassificationOnSave');
    if (!rawValue) {
      return;
    }
    if (rawValue === 'true') {
      this.proceedToNextClassificationOnSave = true;
    } else {
      this.proceedToNextClassificationOnSave = false;
    }
  }

  saveProceedToNextClassificationOnSaveValue() {
    localStorage.setItem(
      'proceedToNextClassificationOnSave',
      this.proceedToNextClassificationOnSave ? 'true' : 'false',
    );
  }
}

export default ClassificationViewComponent;

import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoModule } from '@jsverse/transloco';
import {
  CheckboxComponent,
  FormFieldComponent,
  LabelComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'trapper-frontend-deployment-edit-additional-informations',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    TextInputComponent,
    CheckboxComponent,
    FormFieldComponent,
    ServerErrorsComponent,
    LabelComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './deployment-edit-additional-informations.component.html',
  styleUrls: ['./deployment-edit-additional-informations.component.scss'],
})
export class DeploymentEditAdditionalInformationsComponent {
  @Input() formGroup!: FormGroup;
  @Input() canChangePrivacySettings = false;
}

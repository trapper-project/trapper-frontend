import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';
import { MatIconModule } from '@angular/material/icon';
import {
  Team,
  TeamDetailsService,
  TeamMember,
} from '../services/team-details.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import {
  ButtonComponent,
  FormFieldComponent,
  IconButtonComponent,
  LabelComponent,
  MapComponent,
  ResponsiveTableComponent,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  TableCellDefDirective,
  TextInputComponent,
  UserProfileLinkComponent,
} from '@trapper/ui';
import { TeamEditService } from '../services/team-edit.service';
import { ReactiveFormsModule } from '@angular/forms';
import {
  BehaviorSubject,
  Observable,
  Subject,
  catchError,
  combineLatest,
  distinctUntilChanged,
  map,
  of,
  switchMap,
} from 'rxjs';
import * as geojson from 'geojson';
import { ToastrService } from 'ngx-toastr';
import { GeojsonPolygonFileService } from '../services/geojson-polygon-file.service';
import { HttpClient } from '@angular/common/http';
import { TeamMebmerInviteService } from '../services/team-member-invite.service';
import { ProjectDataService } from '../services/project-data.service';
import {
  TeamDeleteComponent,
  TeamDeleteComponentData,
} from '../team-delete/team-delete.component';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import {
  TeamMembershipDeleteComponentData,
  TeamRemoveTeammateComponent,
} from '../team-remove-teammate/team-remove-teammate.component';
import {
  TeamLeaveComponent,
  TeamLeaveComponentData,
} from '../team-leave/team-leave.component';
import { PageTitleService } from '@trapper/page-title-management';

export interface TypeaheadUser {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  name: string;
}

interface TeamMembershipDisplay {
  id: number;
  user: TeamMember;
  role: string;
  role_id: number;
  status: string;
  status_id: number;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-details',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    MatIconModule,
    NgxSkeletonLoaderModule,
    FormFieldComponent,
    TextInputComponent,
    ServerErrorsComponent,
    LabelComponent,
    ButtonComponent,
    ReactiveFormsModule,
    MapComponent,
    SelectBoxInputComponent,
    ResponsiveTableComponent,
    TableCellDefDirective,
    UserProfileLinkComponent,
    IconButtonComponent,
    DialogModule,
    RouterLink,
  ],
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss'],
  providers: [TeamDetailsService, TeamEditService, TeamMebmerInviteService],
})
export class TeamDetailsComponent implements OnInit {
  editing = false;
  mapEditing = false;
  team$ = this.teamDetailsService.data$;
  formGroup = this.teamEditService.formGroup;

  invitationFormGroup = this.teamMemberInviteService.formGroup;
  teamMemberInvitationInProgress$ = this.teamMemberInviteService.inProgress$;
  userTypeahead$ = new Subject<string>();
  users$ = new BehaviorSubject<TypeaheadUser[]>([]);

  userSelectBoxItems$ = this.users$;

  editInProgress$ = this.teamEditService.inProgress$;

  newPolygon: geojson.Polygon | null = null;
  mapPolygonEdited = false;

  teamProjects$ = this.teamDetailsService.data$.pipe(
    map((team) => {
      if (!team) {
        return null;
      }
      return team.projects;
    }),
  );

  teamMembers$: Observable<TeamMembershipDisplay[]> =
    this.teamDetailsService.data$.pipe(
      map((team) => {
        if (!team) {
          return [];
        }
        return team.members.map((membership) => ({
          id: membership.id,
          user: membership.user,
          role: Object.values(membership.role)[0],
          role_id: +Object.keys(membership.role)[0],
          status: Object.values(membership.status)[0],
          status_id: +Object.keys(membership.status)[0],
        }));
      }),
    );

  constructor(
    private teamDetailsService: TeamDetailsService,
    private teamEditService: TeamEditService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
    private geojsonPolygonFileService: GeojsonPolygonFileService,
    private httpClient: HttpClient,
    private teamMemberInviteService: TeamMebmerInviteService,
    private projectDataService: ProjectDataService,
    private dialog: Dialog,
    private router: Router,
    private pageTitleService: PageTitleService,
  ) {}

  ngOnInit(): void {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((paramMap) => {
      const teamSlug = paramMap.get('teamId');
      const projectSlug = paramMap.get('projectId');
      if (projectSlug && teamSlug) {
        this.teamDetailsService
          .fetchTeamDetails(projectSlug, teamSlug)
          .subscribe();
        this.teamEditService.setProjectSlug(projectSlug);
        this.teamEditService.setTeamId(teamSlug);
        this.teamMemberInviteService.setProjectSlug(projectSlug);
        this.teamMemberInviteService.setTeamId(teamSlug);
      }
    });

    combineLatest([
      this.projectDataService.data$,
      this.teamDetailsService.data$,
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([project, team]) => {
        const titleParts = [];
        if (team) {
          titleParts.push(team.name);
        }
        if (project) {
          titleParts.push(project.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.editInProgress$.pipe(untilDestroyed(this)).subscribe((inProgress) => {
      if (inProgress) {
        // this.formGroup.disable();
      } else {
        this.formGroup.enable();
      }
    });

    this.teamMemberInviteService.inProgress$
      .pipe(untilDestroyed(this))
      .subscribe((inProgress) => {
        if (!inProgress) {
          this.invitationFormGroup.enable();
        }
      });

    this.userTypeahead$
      .pipe(
        distinctUntilChanged(),
        switchMap((searchTerm) => {
          if (!searchTerm) {
            return of([]);
          }
          return this.httpClient
            .get<
              TypeaheadUser[]
            >(`/api/teams/members/search/?user=${searchTerm}`)
            .pipe(
              catchError(() => {
                return of([]);
              }),
            );
        }),
        untilDestroyed(this),
      )
      .subscribe((users) => {
        this.users$.next(users);
      });
  }

  startEditing(currentTeamValue: Team) {
    this.editing = true;
    this.mapEditing = false;
    this.formGroup.patchValue(currentTeamValue);
    this.formGroup.disable();
    this.formGroup.controls.name.enable();
    this.formGroup.controls.description.enable();
  }

  startMapEditing() {
    this.mapEditing = true;
    this.editing = false;
    this.formGroup.disable();
    this.formGroup.controls.polygon.enable();
  }

  save() {
    this.teamEditService.performUpdate().subscribe(() => {
      this.editing = false;
      this.translocoService
        .selectTranslate(
          'teamDisplay.teamUpdatedToastMessage',
          {},
          'csProjects',
        )
        .pipe(untilDestroyed(this))
        .subscribe((translation) => {
          this.toastr.success(translation);
        });
      if (
        !this.teamEditService.currentProjectSlug ||
        !this.teamEditService.currentTeamId
      ) {
        return;
      }
      this.teamDetailsService
        .fetchTeamDetails(
          this.teamEditService.currentProjectSlug,
          this.teamEditService.currentTeamId,
        )
        .subscribe();
    });
    this.formGroup.disable();
  }

  polygonChanged(polygon: geojson.Polygon | null) {
    this.newPolygon = polygon;
    this.mapPolygonEdited = true;
  }

  savePolygon() {
    this.formGroup.disable();
    this.formGroup.controls.polygon.enable();
    this.formGroup.controls.polygon.setValue(this.newPolygon);
    this.formGroup.controls.polygon.markAsDirty();
    this.mapEditing = false;
    this.teamEditService.performUpdate().subscribe(() => {
      this.translocoService
        .selectTranslate(
          'teamDisplay.teamPolygonUpdatedToastMessage',
          {},
          'csProjects',
        )
        .pipe(untilDestroyed(this))
        .subscribe((translation) => {
          this.toastr.success(translation);
        });
      this.editing = false;
      this.newPolygon = null;
      this.mapPolygonEdited = false;
      if (
        !this.teamEditService.currentProjectSlug ||
        !this.teamEditService.currentTeamId
      ) {
        return;
      }
      this.teamDetailsService
        .fetchTeamDetails(
          this.teamEditService.currentProjectSlug,
          this.teamEditService.currentTeamId,
        )
        .subscribe();
    });
  }

  selectPolygonFileFromDisk() {
    this.geojsonPolygonFileService
      .selectPolygonFileFromDisk()
      .then((polygon) => {
        this.formGroup.controls.polygon.setValue(polygon);
        this.formGroup.controls.polygon.markAsDirty();
        this.mapPolygonEdited = true;
        this.newPolygon = polygon;
      })
      .catch(() => {
        this.toastr.error(
          this.translocoService.translate(
            'teamDisplay.teamPolygonFileErrorToastMessage',
            {},
            'csProjects',
          ),
        );
      });
  }

  invite() {
    this.teamMemberInviteService
      .performCreate()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.toastr.success(
          this.translocoService.translate(
            'teamDisplay.teamMemberInvitationSentToastMessage',
            {},
            'csProjects',
          ),
        );
        this.invitationFormGroup.reset();
        if (
          this.teamEditService.currentProjectSlug &&
          this.teamEditService.currentTeamId
        ) {
          this.teamDetailsService
            .fetchTeamDetails(
              this.teamEditService.currentProjectSlug,
              this.teamEditService.currentTeamId,
            )
            .subscribe();
        }
      });
    this.invitationFormGroup.disable();
  }

  deleteTeam(team: Team) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: TeamDeleteComponentData = {
      teamId: team.id,
      teamName: team.name,
      projectSlug: this.projectDataService.currentProjectSlug,
    };
    this.dialog
      .open(TeamDeleteComponent, {
        data,
        width: '500px',
        height: 'auto',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['../'], { relativeTo: this.route });
        }
      });
  }

  leaveTeam(team: Team) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: TeamLeaveComponentData = {
      teamId: team.id,
      teamName: team.name,
      projectSlug: this.projectDataService.currentProjectSlug,
    };
    this.dialog
      .open(TeamLeaveComponent, {
        data,
        width: '500px',
        height: 'auto',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['../'], { relativeTo: this.route });
        }
      });
  }

  deleteTeamMember(team: Team, membership: TeamMembershipDisplay) {
    if (!this.projectDataService.currentProjectSlug) {
      return;
    }
    const data: TeamMembershipDeleteComponentData = {
      teamId: team.id,
      membershipId: membership.id,
      projectSlug: this.projectDataService.currentProjectSlug,
      teamName: team.name,
      userName: membership.user.name,
    };
    this.dialog
      .open(TeamRemoveTeammateComponent, {
        data,
        width: '500px',
        height: 'auto',
        panelClass: 'dialog',
      })
      .closed.pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (
          result &&
          this.teamEditService.currentProjectSlug &&
          this.teamEditService.currentTeamId
        ) {
          this.teamDetailsService
            .fetchTeamDetails(
              this.teamEditService.currentProjectSlug,
              this.teamEditService.currentTeamId,
            )
            .subscribe();
        }
      });
  }

  removeSelectedUserFromInvitationForm(userId: number) {
    this.invitationFormGroup.controls.users.setValue(
      this.invitationFormGroup.controls.users.value?.filter(
        (id: number) => id !== userId,
      ) || [],
    );
  }
}

export default TeamDetailsComponent;

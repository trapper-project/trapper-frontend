import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  DynamicAttrWithBbox,
  ProjectMediaData,
  ClassificationDataService,
} from '../services/classification-data.service';
import { ActivatedRoute, RouterModule } from '@angular/router';
import {
  EMPTY,
  combineLatest,
  combineLatestWith,
  map,
  shareReplay,
  switchMap,
  take,
} from 'rxjs';
import {
  BadgeComponent,
  CardComponent,
  MapComponent,
  ResponsiveTableComponent,
  TableCellDefDirective,
  UserProfileLinkComponent,
  YesNoFlagComponent,
} from '@trapper/ui';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatIconModule } from '@angular/material/icon';
import { CdkTableModule } from '@angular/cdk/table';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { inOutAnimation } from '@trapper/animations';
import {
  CsProfilePreviewModule,
  ProfilePreviewService,
} from '@trapper/profile-preview';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { MediaResourcePreviewComponent } from '../media-resource-preview/media-resource-preview.component';
import {
  ClassificationSpecAttribute,
  ProjectDataService,
} from '../services/project-data.service';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { MarkAsFavouriteService } from '../services/mark-as-favourite.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { trapperCsProjectsTranslocoScope } from '../transloco.loader';
import { ResourceAnnotatorComponent } from '@trapper/resource-annotator';
import { provideIcons } from '@ng-icons/core';
import { bootstrapFullscreen } from '@ng-icons/bootstrap-icons';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-media-display',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    NgxSkeletonLoaderModule,
    MatIconModule,
    CdkTableModule,
    YesNoFlagComponent,
    BadgeComponent,
    LeafletModule,
    NgScrollbarModule,
    CsProfilePreviewModule,
    UserProfileLinkComponent,
    MapComponent,
    DialogModule,
    TranslocoModule,
    ResponsiveTableComponent,
    TableCellDefDirective,
    RouterModule,
    MatTooltipModule,
    ResourceAnnotatorComponent,
  ],
  templateUrl: './project-workspace-media-display.component.html',
  styleUrls: ['./project-workspace-media-display.component.scss'],
  providers: [
    ClassificationDataService,
    MarkAsFavouriteService,
    trapperCsProjectsTranslocoScope,
    provideIcons({
      bootstrapFullscreen,
    }),
  ],
  animations: [inOutAnimation],
})
export class ProjectWorkspaceMediaDisplayComponent implements OnInit {
  currentProjectSlug: string | null = null;

  media$ = this.ClassificationDataService.data$;

  bboxes$ = this.ClassificationDataService.bboxes$;
  objects$ = this.ClassificationDataService.objects$;

  locationCoordinates$ = this.ClassificationDataService.locationCoordinates$;

  classificationTable$ = this.projectDataService.classificationSpec$.pipe(
    combineLatestWith(
      this.ClassificationDataService.data$,
      this.projectDataService.classificationSpecDictionaries$,
      this.translocoService.selectTranslate(
        'unknownBadge',
        {},
        this.translocoScope,
      ),
    ),
    map(([spec, data, dictionaries, unknownBadge]) => {
      if (!spec || !data || !dictionaries) {
        return null;
      }

      const columns = spec.dynamic.filter((col) => col.show);

      const rows = data.classification.dynamic;

      const bodyRows = this.extractBodyRows(
        rows,
        columns,
        unknownBadge,
        dictionaries,
      );

      return {
        columns,
        bodyRows,
      };
    }),
    shareReplay(1),
  );

  userClassificationTable$ = this.projectDataService.classificationSpec$.pipe(
    combineLatestWith(
      this.ClassificationDataService.data$,
      this.projectDataService.classificationSpecDictionaries$,
      this.translocoService.selectTranslate(
        'unknownBadge',
        {},
        this.translocoScope,
      ),
    ),
    map(([spec, data, dictionaries, unknownBadge]) => {
      if (!spec || !data || !dictionaries) {
        return null;
      }

      const userClassification = data.user_classification;
      if (!userClassification) {
        return null;
      }

      const columns = spec.dynamic.filter((col) => col.show);

      const rows = userClassification.dynamic;

      const bodyRows = this.extractBodyRows(
        rows,
        columns,
        unknownBadge,
        dictionaries,
      );

      return {
        columns,
        bodyRows,
      };
    }),
  );

  isApproved$ = this.ClassificationDataService.isApproved$;

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  constructor(
    private ClassificationDataService: ClassificationDataService,
    private projectDataService: ProjectDataService,
    private route: ActivatedRoute,
    private profilePreviewService: ProfilePreviewService,
    private dialog: Dialog,
    private pageTitleService: PageTitleService,
    private markAsFavouriteService: MarkAsFavouriteService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: string,
  ) {}
  private extractBodyRows(
    rows: DynamicAttrWithBbox[],
    columns: ClassificationSpecAttribute[],
    unknownBadge: string,
    dictionaries: Record<string, Record<string, string>>,
  ) {
    return rows.map((row, index) => {
      const values: Record<
        string,
        {
          value: string;
          color?: string;
          confidence?: number;
        }
      > = {};
      for (const col of columns) {
        let val = row[col.name];
        if (val === null || val === undefined) {
          val = unknownBadge;
        } else if (col.type === 'choices') {
          const dictionary = dictionaries[col.name] || {};
          val = dictionary[val] || '#' + val;
        }
        const colorMapping = col.colorMapping;
        const color = colorMapping
          ? colorMapping.get(row[col.name])
          : undefined;

        const confidence = row.attr_confidence
          ? row.attr_confidence[col.name]
          : undefined;

        values[col.name] = {
          value: val,
          color,
          confidence,
        };
      }
      values['id'] = {
        value: `bbox${index + 1}`,
      };
      return values;
    });
  }

  ngOnInit(): void {
    this.pageTitleService.resetTitle();
    this.route.paramMap
      .pipe(
        switchMap((paramMap) => {
          const projectSlug = paramMap.get('projectId');
          this.currentProjectSlug = projectSlug;
          const mediaSlug = paramMap.get('mediaSlug');
          if (projectSlug && mediaSlug) {
            return this.ClassificationDataService.reloadMediaData(
              projectSlug,
              mediaSlug,
            );
          }

          return EMPTY;
        }),
        untilDestroyed(this),
      )
      .subscribe();

    combineLatest([
      this.ClassificationDataService.data$,
      this.projectDataService.data$,
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([mediaData, projectData]) => {
        const titleParts = [];
        if (mediaData) {
          titleParts.push(mediaData.file_name);
        }
        if (projectData) {
          titleParts.push(projectData.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });
  }

  viewProfile(userId: string | number) {
    this.profilePreviewService.open(userId);
  }

  openMediaResourcePreview() {
    this.dialog.open(MediaResourcePreviewComponent, {
      providers: [
        {
          provide: ClassificationDataService,
          useValue: this.ClassificationDataService,
        },
        {
          provide: ProjectDataService,
          useValue: this.projectDataService,
        },
      ],
    });
  }

  saveResourceToFile(resourceData: ProjectMediaData) {
    const imageUrl = resourceData.original_url;
    const imageFileName = resourceData.file_name;
    const link = document.createElement('a');
    link.href = imageUrl;
    link.download = imageFileName;
    link.click();
  }

  markAsFavourite() {
    this.media$
      .pipe(
        take(1),
        switchMap((item) => {
          if (!item || !this.currentProjectSlug) {
            return EMPTY;
          }
          return this.markAsFavouriteService
            .markAsFavourite(
              this.currentProjectSlug,
              item.pk,
              !item.is_favorite,
            )
            .pipe(
              map(() => {
                return { item };
              }),
            );
        }),
        switchMap(({ item }) => {
          if (!this.currentProjectSlug || !item) {
            return EMPTY;
          }
          return this.ClassificationDataService.reloadMediaData(
            this.currentProjectSlug,
            item.pk,
          );
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }
}

export default ProjectWorkspaceMediaDisplayComponent;

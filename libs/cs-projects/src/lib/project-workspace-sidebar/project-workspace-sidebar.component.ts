import {
  Component,
  HostBinding,
  HostListener,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { ProjectDataService } from '../services/project-data.service';
import { map } from 'rxjs';
import { inOutAnimation } from '@trapper/animations';
import { TranslocoModule } from '@jsverse/transloco';
import { BreakpointObserver } from '@angular/cdk/layout';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CompactModeService, SidebarStateService } from '@trapper/ui';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-sidebar',
  standalone: true,
  imports: [CommonModule, RouterModule, MatIconModule, TranslocoModule],
  templateUrl: './project-workspace-sidebar.component.html',
  styleUrls: ['./project-workspace-sidebar.component.scss'],
  animations: [inOutAnimation],
})
export class ProjectWorkspaceSidebarComponent implements OnInit, OnDestroy {
  @HostBinding('class.sidebar-expanded')
  get sidebarExpanded() {
    return this.sidebarStateService.sidebarExpanded;
  }

  @HostBinding('style.--sidebar-top-offset.px')
  currentSidebarOffset = 0;

  @HostBinding('class.wide-screen')
  wideScreen = true;

  @HostBinding('class.sidebar-compact-mode')
  sidebarCompactMode = false;

  constructor(
    private projectDataService: ProjectDataService,
    private breakpointObserver: BreakpointObserver,
    private sidebarStateService: SidebarStateService,
    private compactModeService: CompactModeService,
    private router: Router,
  ) {}

  projectName$ = this.projectDataService.data$.pipe(
    map((project) => project?.name),
  );

  toggleSidebar() {
    this.sidebarStateService.toggleSidebar();
  }

  expandSidebar() {
    this.sidebarStateService.expandSidebar();
  }

  collapseSidebar() {
    this.sidebarStateService.collapseSidebar();
  }

  ngOnInit(): void {
    this.sidebarStateService.setSidebarPresent(true);
    this.sidebarStateService.currentSidebarOffset$
      .pipe(untilDestroyed(this))
      .subscribe((offset) => {
        this.currentSidebarOffset = offset;
      });

    this.sidebarStateService.wideScreen$
      .pipe(untilDestroyed(this))
      .subscribe((wideScreen) => {
        this.wideScreen = wideScreen;
      });

    this.router.events.pipe(untilDestroyed(this)).subscribe(() => {
      if (!this.wideScreen) {
        this.collapseSidebar();
      }
    });
    this.compactModeService.compactMode$
      .pipe(untilDestroyed(this))
      .subscribe((compactMode) => {
        this.sidebarCompactMode = compactMode;
      });
  }

  ngOnDestroy() {
    this.sidebarStateService.setSidebarPresent(false);
  }

  @HostListener('wheel', ['$event'])
  handleWheel(event: WheelEvent) {
    if (this.sidebarExpanded) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  @HostListener('touchmove', ['$event'])
  handleTouchMove(event: TouchEvent) {
    if (this.sidebarExpanded) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  @HostListener('click')
  handleClick() {
    this.collapseSidebar();
  }

  clickOnSidebar(event: MouseEvent) {
    event.stopPropagation();
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormFieldComponent,
  LabelComponent,
  MapComponent,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CreateLocationModel } from '../services/media-create-upload.service';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

import { BehaviorSubject, distinctUntilChanged, map, tap } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';
import { ProjectDataService } from '../services/project-data.service';
import { inOutAnimation } from '@trapper/animations';
import { ModelFormGroupControls } from '@trapper/backend';
import { LeafletMouseEvent } from 'leaflet';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';

function str2float(s: string | null | undefined): number | null {
  if (s === null || s === undefined) {
    return null;
  }

  const val = parseFloat(s);

  if (isNaN(val)) {
    return null;
  }

  return val;
}

function isInvalidLatitute(lat: number | null): boolean {
  return lat === null || lat < -90 || lat > 90;
}

function isInvalidLongitude(lng: number | null): boolean {
  return lng === null || lng < -180 || lng > 180;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-edit-location-selection',
  standalone: true,
  imports: [
    CommonModule,
    FormFieldComponent,
    TextInputComponent,
    SelectBoxInputComponent,
    ServerErrorsComponent,
    LeafletModule,
    ReactiveFormsModule,
    LabelComponent,
    TranslocoModule,
    MapComponent,
  ],
  templateUrl: './deployment-edit-location-selection.component.html',
  styleUrls: ['./deployment-edit-location-selection.component.scss'],
  animations: [inOutAnimation],
})
export class DeploymentEditLocationSelectionComponent implements OnInit {
  private _locationEditingEnabled = false;
  @Input()
  public get locationEditingEnabled() {
    return this._locationEditingEnabled;
  }
  public set locationEditingEnabled(value) {
    this._locationEditingEnabled = value;

    if (value) {
      if (this.locationEditFormGroup) {
        this.locationEditFormGroup.enable();
      }
      if (this.locationSelectionFormControl) {
        this.locationSelectionFormControl.reset();
        this.locationSelectionFormControl.disable();
      }
    } else {
      if (this.locationEditFormGroup) {
        this.locationEditFormGroup.reset();
        this.locationEditFormGroup.disable();
      }
      if (this.locationSelectionFormControl) {
        this.locationSelectionFormControl.enable();
      }
    }
  }
  @Output() locationEditingEnabledChange = new EventEmitter<boolean>();

  locationChoices$ = this.projectDataService.locationUploadChoices$;

  // formGroup = this.mediaCreateUploadService.formGroup;
  @Input() locationEditFormGroup?: FormGroup<
    ModelFormGroupControls<CreateLocationModel>
  >;

  @Input() locationSelectionFormControl?: FormControl<
    string | null | undefined
  >;

  latLong$ = new BehaviorSubject<[number, number] | null>(null);

  defaultLocation$ = this.siteCustomizationService.defaultLocation$;

  markers$ = this.latLong$.pipe(
    distinctUntilChanged(),
    map((coords) => {
      return coords
        ? [
            {
              id: 'current',
              lat: coords[0],
              lng: coords[1],
              label: '',
              isHighlighted: true,
            },
          ]
        : [];
    }),
  );

  constructor(
    // private mediaCreateUploadService: MediaCreateUploadService,
    private translocoService: TranslocoService,
    private projectDataService: ProjectDataService,
    private siteCustomizationService: CsSiteCustomizationService,
  ) {}

  ngOnInit() {
    this.siteCustomizationService.siteCustomization$
      .pipe(untilDestroyed(this))
      .subscribe((siteCustomization) => {
        if (
          siteCustomization?.default_latitude &&
          siteCustomization?.default_longitude
        ) {
          this.latLong$.next([
            siteCustomization.default_latitude,
            siteCustomization.default_longitude,
          ]);
        }
      });
    this.locationEditFormGroup?.valueChanges
      .pipe(
        tap((value) => {
          let lat = str2float(value?.latitude);
          let long = str2float(value?.longitude);

          if (lat !== null && isInvalidLatitute(lat)) {
            this.fixLatitude();
          }
          if (long !== null && isInvalidLongitude(long)) {
            this.fixLongitude();
          }

          lat = this.fixedLatitudeValue(lat);
          long = this.fixedLongitudeValue(long);

          if (lat !== null && long !== null) {
            this.latLong$.next([lat, long]);
          } else {
            this.latLong$.next(null);
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  toggleLocationEditing() {
    this.locationEditingEnabled = !this.locationEditingEnabled;
    this.locationEditingEnabledChange.emit(this.locationEditingEnabled);
  }

  leafletMapClick(e: LeafletMouseEvent) {
    if (!this.locationEditFormGroup) {
      return;
    }
    const location = this.locationEditFormGroup;
    location.patchValue({
      latitude: e.latlng.lat.toString(),
      longitude: e.latlng.lng.toString(),
    });
    this.fixLongitude();
    this.fixLatitude();
    const lat = str2float(location.controls.latitude.value);
    const long = str2float(location.controls.longitude.value);
    if (lat === null || long === null) {
      return;
    }
    this.latLong$.next([lat, long]);
  }

  fixedLongitudeValue(long: number | null) {
    if (long === null) {
      return null;
    }
    if (long < -180) {
      return (long % 360) + 360;
    } else if (long > 180) {
      return (long % 360) - 360;
    }
    return long;
  }

  fixedLatitudeValue(lat: number | null) {
    if (lat === null) {
      return null;
    }
    if (lat < -90) {
      return -90;
    } else if (lat > 90) {
      return 90;
    }
    return lat;
  }

  fixLongitude() {
    if (!this.locationEditFormGroup) {
      return;
    }
    const location = this.locationEditFormGroup;
    const longitude = location.controls.longitude;
    let value = str2float(longitude.value);
    if (value && value >= -180 && value <= 180) {
      return;
    }
    value = this.fixedLongitudeValue(value);
    if (value === null) {
      return;
    }
    longitude.setValue(value.toString());
  }

  fixLatitude() {
    if (!this.locationEditFormGroup) {
      return;
    }
    const location = this.locationEditFormGroup;
    const latitude = location.controls.latitude;
    let value = str2float(latitude.value);
    if (value && value >= -90 && value <= 90) {
      return;
    }
    value = this.fixedLatitudeValue(value);
    if (value === null) {
      return;
    }
    latitude.setValue(value.toString());
  }
}

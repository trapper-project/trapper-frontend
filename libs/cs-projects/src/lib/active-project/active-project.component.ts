import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { RecentProjectService } from '@trapper/profile';
import { SpinnerComponent } from '@trapper/ui';
import { inOutAnimation } from '@trapper/animations';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-active-project',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './active-project.component.html',
  styleUrls: ['./active-project.component.scss'],
  animations: [inOutAnimation],
})
export class ActiveProjectComponent implements OnInit {
  constructor(
    private router: Router,
    private recentProjectService: RecentProjectService,
  ) {}
  ngOnInit(): void {
    this.recentProjectService.reloadRecentProject();
    this.recentProjectService.currentActiveProject$
      .pipe(untilDestroyed(this))
      .subscribe((project) => {
        if (project) {
          this.router.navigate(['/cs/projects/', project]);
        } else {
          this.router.navigate(['/cs/projects']);
        }
      });
  }
}
export default ActiveProjectComponent;

import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { RecentProjectService } from '@trapper/profile';
import { SpinnerComponent } from '@trapper/ui';
import { inOutAnimation } from '@trapper/animations';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatestWith, map } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-active-project-team',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './active-project.component.html',
  styleUrls: ['./active-project.component.scss'],
  animations: [inOutAnimation],
})
export class ActiveProjectTeamComponent implements OnInit {
  constructor(
    private router: Router,
    private recentProjectService: RecentProjectService,
    private route: ActivatedRoute,
  ) {}
  ngOnInit(): void {
    this.recentProjectService.reloadRecentProject();
    const teamId$ = this.route.paramMap.pipe(
      map((params) => params.get('teamId')),
      map((teamId) => (teamId ? parseInt(teamId, 10) : null)),
    );
    this.recentProjectService.currentActiveProject$
      .pipe(combineLatestWith(teamId$), untilDestroyed(this))
      .subscribe(([project, teamId]) => {
        if (project && teamId) {
          this.router.navigate(['/cs/projects/', project, 'teams', teamId]);
        } else if (project) {
          this.router.navigate(['/cs/projects/', project, 'teams']);
        } else {
          this.router.navigate(['/cs/projects']);
        }
      });
  }
}
export default ActiveProjectTeamComponent;

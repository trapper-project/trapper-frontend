import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectListingService } from '../services/project-listing.service';
import { ProjectListingItemComponent } from '../project-listing-item/project-listing-item.component';
import {
  CardComponent,
  SpinnerComponent,
  TextInputComponent,
} from '@trapper/ui';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject, combineLatest, map, shareReplay, tap } from 'rxjs';
import { inOutAnimation } from '@trapper/animations';
import { ProjectListingTagColorsService } from '../project-listing-item/project-listing-tag-colors.service';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { trapperCsProjectsTranslocoScope } from '../transloco.loader';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-listing',
  standalone: true,
  imports: [
    CommonModule,
    ProjectListingItemComponent,
    CardComponent,
    TextInputComponent,
    FormsModule,
    SpinnerComponent,
    NgxSkeletonLoaderModule,
    TranslocoModule,
  ],
  templateUrl: './project-listing.component.html',
  styleUrls: ['./project-listing.component.scss'],
  providers: [ProjectListingService, trapperCsProjectsTranslocoScope],
  animations: [inOutAnimation],
})
export class ProjectListingComponent implements OnInit {
  profileData$ = this.profileService.data$;
  inProgress$ = this.projectListingService.inProgress$;
  searchText$ = new BehaviorSubject('');
  projects$ = combineLatest([
    this.projectListingService.data$,
    this.searchText$,
  ]).pipe(
    map(([data, q]) => {
      const qq = q.toLocaleLowerCase();
      return data
        ? data.filter(
            (proj) =>
              proj.name.trim().toLowerCase().includes(qq) ||
              proj.name.trim().toLocaleLowerCase().includes(qq) ||
              proj.tags.find((tag) =>
                tag.trim().toLocaleLowerCase().includes(qq),
              ),
          )
        : null;
    }),
    shareReplay(1),
  );

  constructor(
    private projectListingService: ProjectListingService,
    private userProfileService: CurrentUserProfileService,
    private projectListingTagColorsService: ProjectListingTagColorsService,
    private translocoService: TranslocoService,
    private titleService: PageTitleService,
    private profileService: CurrentUserProfileService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.projectListingService.loadProjects().subscribe();
    this.userProfileService.reloadUserProfile().subscribe();

    this.translocoService
      .selectTranslate('projectListingPageTitle', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.titleService.setSectionTitle(title);
      });

    this.projectListingService.data$
      .pipe(
        tap((projects) =>
          this.projectListingTagColorsService.updateColors(
            projects?.flatMap((project) => project.tags) || [],
          ),
        ),
      )
      .subscribe();
  }

  onSearchTextChange(q: string) {
    this.searchText$.next(q);
  }
}
export default ProjectListingComponent;

import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { TranslocoModule } from '@jsverse/transloco';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { TeamMembershipDeleteService } from '../services/teammate-delete.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

export interface TeamMembershipDeleteComponentData {
  teamId: number;
  membershipId: number;
  projectSlug: string;
  teamName: string;
  userName: string;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-remove-teammate',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    ButtonComponent,
    TranslocoModule,
  ],
  templateUrl: './team-remove-teammate.component.html',
  styleUrls: ['./team-remove-teammate.component.scss'],
  providers: [TeamMembershipDeleteService],
})
export class TeamRemoveTeammateComponent {
  inProgress = this.teamMembershipDeleteService.inProgress$;
  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: TeamMembershipDeleteComponentData,
    private teamMembershipDeleteService: TeamMembershipDeleteService,
  ) {}

  get teamName() {
    return this.data.teamName;
  }

  get userName() {
    return this.data.userName;
  }

  ok() {
    this.teamMembershipDeleteService
      .deleteTeamMember(this.data.teamId, this.data.membershipId)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.dialog.close(true);
      });
  }

  cancel() {
    this.dialog.close(false);
  }
}

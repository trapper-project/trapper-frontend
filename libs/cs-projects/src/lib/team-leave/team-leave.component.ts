import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamLeaveService } from '../services/team-leave.service';
import {
  ButtonComponent,
  CardComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { TranslocoModule } from '@jsverse/transloco';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

export interface TeamLeaveComponentData {
  teamId: number;
  projectSlug: string;
  teamName: string;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-leave',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    ButtonComponent,
    TranslocoModule,
  ],
  templateUrl: './team-leave.component.html',
  styleUrls: ['./team-leave.component.scss'],
  providers: [TeamLeaveService],
})
export class TeamLeaveComponent {
  inProgress = this.teamLeaveService.inProgress$;
  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: TeamLeaveComponentData,
    private teamLeaveService: TeamLeaveService,
  ) {}

  get teamName() {
    return this.data.teamName;
  }

  ok() {
    this.teamLeaveService
      .leaveTeam(this.data.teamId)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.dialog.close(true);
      });
  }

  cancel() {
    this.dialog.close(false);
  }
}

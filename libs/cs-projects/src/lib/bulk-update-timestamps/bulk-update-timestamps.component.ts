import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormFieldComponent,
  LabelComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { TranslocoModule } from '@jsverse/transloco';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { DeploymentBulkUpdateTimestampService } from '../services/deployment-bulk-update-timestamp.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormsModule } from '@angular/forms';

export interface BulkUpdateTimestampsComponentData {
  deploymentId: number;
  projectSlug: string;
  deploymentName: string;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-bulk-update-timestamps',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    ButtonComponent,
    TranslocoModule,
    FormFieldComponent,
    LabelComponent,
    FormsModule,
  ],
  templateUrl: './bulk-update-timestamps.component.html',
  styleUrls: ['./bulk-update-timestamps.component.scss'],
  providers: [DeploymentBulkUpdateTimestampService],
})
export class BulkUpdateTimestampsComponent {
  private _seconds = 0;
  private _minutes = 0;
  private _hours = 0;
  mode: 'add' | 'subtract' = 'add';
  inProgress = this.deploymentBulkUpdateTimestampService.inProgress$;
  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: BulkUpdateTimestampsComponentData,
    private deploymentBulkUpdateTimestampService: DeploymentBulkUpdateTimestampService,
  ) {}

  get deploymentName() {
    return this.data.deploymentName;
  }

  get timeOffset() {
    return this._seconds + this._minutes * 60 + this._hours * 3600;
  }

  ok() {
    const time_offset = this.timeOffset * (this.mode === 'add' ? 1 : -1);
    this.deploymentBulkUpdateTimestampService
      .bulkUpdateTimestamp(
        this.data.projectSlug,
        this.data.deploymentId,
        time_offset,
      )
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.dialog.close(true);
      });
  }

  cancel() {
    this.dialog.close(false);
  }

  get seconds() {
    if (this._seconds === 0) {
      return null;
    }
    return this._seconds.toString();
  }

  set seconds(value: string | null) {
    if (value === null) {
      this._seconds = 0;
      return;
    }
    this._seconds = parseInt(value, 10);
    if (this._seconds < 0) {
      this._seconds = 0;
    }
  }

  get minutes() {
    if (this._minutes === 0) {
      return null;
    }

    return this._minutes.toString();
  }

  set minutes(value: string | null) {
    if (value === null) {
      this._minutes = 0;
      return;
    }
    this._minutes = parseInt(value, 10);
  }

  get hours() {
    if (this._hours === 0) {
      return null;
    }
    return this._hours.toString();
  }

  set hours(value: string | null) {
    if (value === null) {
      this._hours = 0;
      return;
    }
    this._hours = parseInt(value, 10);
  }

  fixValues() {
    if (this._seconds < 0) {
      this._seconds = 0;
    }
    if (this._minutes < 0) {
      this._minutes = 0;
    }
    if (this._hours < 0) {
      this._hours = 0;
    }

    if (this._seconds > 59) {
      const minutes = Math.floor(this._seconds / 60);
      this._seconds = this._seconds % 60;
      this._minutes += minutes;
    }

    if (this._minutes > 59) {
      const hours = Math.floor(this._minutes / 60);
      this._minutes = this._minutes % 60;
      this._hours += hours;
    }
  }
}

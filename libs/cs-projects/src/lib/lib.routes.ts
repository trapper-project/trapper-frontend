import { Route } from '@angular/router';

export const routes: Route[] = [
  {
    path: '',
    loadComponent: () => import('./project-listing/project-listing.component'),
    pathMatch: 'full',
  },
  {
    path: 'active-project',
    loadComponent: () => import('./active-project/active-project.component'),
  },
  {
    path: 'teams/:teamId',
    loadComponent: () =>
      import('./active-project/active-project-team.component'),
  },
  {
    path: 'teams',
    loadComponent: () =>
      import('./active-project/active-project-team.component'),
  },
  {
    path: ':projectId',
    loadComponent: () =>
      import('./project-active-workspace/project-active-workspace.component'),
    children: [
      {
        path: 'dashboard',
        loadComponent: () =>
          import(
            './project-workspace-dashboard/project-workspace-dashboard.component'
          ),
      },
      {
        path: 'media/:mediaSlug',
        loadComponent: () =>
          import(
            './project-workspace-media-display/project-workspace-media-display.component'
          ),
      },
      {
        path: 'media',
        loadComponent: () =>
          import(
            './project-workspace-media-listing/project-workspace-media-listing.component'
          ),
      },
      {
        path: 'upload',
        loadComponent: () =>
          import(
            './project-workspace-upload/project-workspace-upload.component'
          ),
      },
      {
        path: 'deployments/:deploymentId/details',
        loadComponent: () =>
          import('./deployment-preview/deployment-preview.component'),
      },
      {
        path: 'deployments/:deploymentId/edit',
        loadComponent: () =>
          import('./deployment-edit/deployment-edit.component'),
      },
      {
        path: 'deployments',
        loadComponent: () =>
          import(
            './project-workspace-deployments/project-workspace-deployments.component'
          ),
      },
      {
        path: 'classify',
        loadComponent: () =>
          import('./classification-listing/classification-listing.component'),
      },
      {
        path: 'classify/:classificationId',
        loadComponent: () =>
          import('./classification-view/classification-view.component'),
      },
      {
        path: 'teams/create',
        loadComponent: () => import('./team-create/team-create.component'),
      },
      {
        path: 'teams/:teamId',
        loadComponent: () => import('./team-details/team-details.component'),
      },
      {
        path: 'teams',
        loadComponent: () => import('./team-listing/team-listing.component'),
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
    ],
  },
];

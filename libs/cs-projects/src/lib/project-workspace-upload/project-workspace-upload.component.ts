import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NgxFileDropEntry,
  NgxFileDropModule,
  FileSystemFileEntry,
} from 'ngx-file-drop';
import { MatIconModule } from '@angular/material/icon';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {
  AlertComponent,
  ButtonComponent,
  CheckboxComponent,
  DateInputComponent,
  DateRange,
  DateRangeInputComponent,
  FormFieldComponent,
  LabelComponent,
  ProgressBarComponent,
  SelectBoxInputComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import {
  MediaUploadService,
  SingleFileEntry,
} from '../services/media-upload.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EMPTY,
  combineLatest,
  finalize,
  map,
  mergeMap,
  switchMap,
  tap,
} from 'rxjs';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import { ToastrService } from 'ngx-toastr';
import { MediaCreateUploadService } from '../services/media-create-upload.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { ProjectDataService } from '../services/project-data.service';
import { MediaUploadProgressOverlayComponent } from '../media-upload-progress-overlay/media-upload-progress-overlay.component';
import { inOutAnimation } from '@trapper/animations';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { DeploymentEditAdditionalInformationsComponent } from '../deployment-edit-additional-informations/deployment-edit-additional-informations.component';
import { DeploymentEditAdditionalFieldsComponent } from '../deployment-edit-additional-fields/deployment-edit-additional-fields.component';
import { DeploymentEditLocationSelectionComponent } from '../deployment-edit-location-selection/deployment-edit-location-selection.component';
import { PageTitleService } from '@trapper/page-title-management';
import { MatTooltipModule } from '@angular/material/tooltip';

let nextFileId = 1;

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-project-workspace-upload',
  standalone: true,
  imports: [
    CommonModule,
    NgxFileDropModule,
    MatIconModule,
    NgScrollbarModule,
    ScrollingModule,
    ButtonComponent,
    ProgressBarComponent,
    DialogModule,
    OverlayModule,
    SelectBoxInputComponent,
    TextInputComponent,
    LabelComponent,
    FormFieldComponent,
    ReactiveFormsModule,
    ServerErrorsComponent,
    CdkAccordionModule,
    DateInputComponent,
    CheckboxComponent,
    DateRangeInputComponent,
    FormsModule,
    TranslocoModule,
    DeploymentEditAdditionalInformationsComponent,
    DeploymentEditAdditionalFieldsComponent,
    DeploymentEditLocationSelectionComponent,
    AlertComponent,
    MatTooltipModule,
  ],
  templateUrl: './project-workspace-upload.component.html',
  styleUrls: ['./project-workspace-upload.component.scss'],
  providers: [MediaCreateUploadService],
  animations: [inOutAnimation],
})
export class ProjectWorkspaceUploadComponent implements OnInit {
  public files: SingleFileEntry[] = [];
  currentProjectSlug: string | null = null;
  creatingNewLocation = false;
  creatingNewDeployment = true;
  today = new Date();
  additionalFieldsExpanded = false;

  deploymentChoices$ = this.projectDataService.myDeploymentChoices$;

  baitTypeChoices$ = this.projectDataService.baitTypeChoices$;

  videoSupportEnabled$ = this.projectDataService.videoSupportEnabled$;
  videoSupportEnabled = false;

  range?: DateRange;

  @HostBinding('@inOutAnimation') inOutAnimation = true;

  constructor(
    public mediaUploadService: MediaUploadService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: Dialog,
    private overlay: Overlay,
    private toastr: ToastrService,
    public mediaCreateUploadService: MediaCreateUploadService,
    private projectDataService: ProjectDataService,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.disableLocationForm();
    this.enableDeploymentForm();

    combineLatest([
      this.projectDataService.data$,
      this.translocoService.selectTranslate(
        'uploadPageTitle',
        {},
        this.translocoScope,
      ),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([d, title]) => {
        const titleParts = [];
        titleParts.push(title);
        if (d) {
          titleParts.push(d.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });

    this.route.paramMap
      .pipe(
        switchMap((paramMap) => {
          const projectSlug = paramMap.get('projectId');
          this.currentProjectSlug = projectSlug;
          this.mediaCreateUploadService.resetForm();

          return EMPTY;
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((query) => {
      const deployment = query.get('deployment');
      if (deployment) {
        this.mediaCreateUploadService.formInitialValues.deployment_id =
          deployment;

        if (this.creatingNewDeployment) {
          this.toggleCreatingNewDeployment();
        }

        this.mediaCreateUploadService.resetForm();
      }
    });

    this.mediaUploadService.toFinalize$
      .pipe(
        mergeMap(({ projectSlug, collectionData }) => {
          return this.mediaUploadService
            .finalizeUpload(projectSlug, collectionData)
            .pipe(map(() => ({ projectSlug, collectionData })));
        }),
        untilDestroyed(this),
      )
      .subscribe(({ projectSlug, collectionData }) => {
        this.router.navigate([
          '/cs/projects',
          projectSlug,
          'deployments',
          collectionData.deployment_id,
          'details',
        ]);
        this.toastr.success('Your images were uploaded');
      });

    this.mediaCreateUploadService.disableAdditionalFields();
    this.projectDataService
      .reloadProjectData()
      .pipe(untilDestroyed(this))
      .subscribe();

    this.projectDataService.defaultBaitType$
      .pipe(
        tap((baitType) => {
          if (!baitType) {
            return;
          }
          this.mediaCreateUploadService.formInitialValues.deployment.bait_type =
            baitType;
          if (
            !this.mediaCreateUploadService.formGroup.controls.deployment
              .controls.bait_type.value
          ) {
            this.mediaCreateUploadService.formGroup.controls.deployment.controls.bait_type.setValue(
              baitType,
            );
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.videoSupportEnabled$
      .pipe(untilDestroyed(this))
      .subscribe((videoSupportEnabled) => {
        this.videoSupportEnabled = videoSupportEnabled || false;
      });
  }
  public dropped(files: NgxFileDropEntry[]) {
    const parsedFiles: SingleFileEntry[] = files
      .filter((file) => file.fileEntry.isFile)
      .map((file) => ({
        fileId: nextFileId++,
        relativePath: file.relativePath,
        fileEntry: file.fileEntry as FileSystemFileEntry,
      }));

    // keep only images
    const images = parsedFiles.filter((file) => {
      const extension = file.relativePath.split('.').pop();
      const allowedExtensions = ['jpg', 'jpeg', 'png'];
      if (this.videoSupportEnabled) {
        allowedExtensions.push('mp4');
      }
      return extension && allowedExtensions.includes(extension.toLowerCase());
    });

    this.files = [...this.files, ...images];
  }

  upload() {
    if (this.files.length === 0) {
      return;
    }
    if (this.currentProjectSlug) {
      const projectSlug = this.currentProjectSlug;
      this.mediaCreateUploadService
        .initializeUpload(this.currentProjectSlug, {
          createNewDeployment: this.creatingNewDeployment,
          createNewLocation: this.creatingNewLocation,
        })
        .pipe(
          switchMap((collectionData) => {
            this.mediaCreateUploadService.formGroup.disable();
            const dialog = this.dialog.open(
              MediaUploadProgressOverlayComponent,
              {
                positionStrategy: this.overlay
                  .position()
                  .global()
                  .top('54px')
                  .centerHorizontally(),
                disableClose: true,
              },
            );

            return this.mediaUploadService
              .uploadMultipleFiles(projectSlug, this.files, collectionData)
              .pipe(
                finalize(() => {
                  dialog.close();
                }),
                untilDestroyed(this),
              );
          }),
          untilDestroyed(this),
        )
        .subscribe();
    }
  }

  cancel() {
    this.mediaUploadService.cancel();
  }

  enableDeploymentForm() {
    this.mediaCreateUploadService.clearSelectedDeployment();
    this.mediaCreateUploadService.formGroup.controls.deployment_id.disable();
  }

  disableDeploymentForm() {
    this.mediaCreateUploadService.formGroup.controls.deployment_id.enable();
    this.mediaCreateUploadService.clearDeploymentForm();
  }

  enableLocationForm() {
    this.mediaCreateUploadService.clearSelectedLocation();
    this.mediaCreateUploadService.formGroup.controls.location_id.disable();
  }

  disableLocationForm() {
    this.mediaCreateUploadService.clearLocationForm();
    this.mediaCreateUploadService.formGroup.controls.location_id.enable();
  }

  toggleCreatingNewLocation() {
    this.creatingNewLocation = !this.creatingNewLocation;
    if (this.creatingNewLocation) {
      this.enableLocationForm();
    } else {
      this.disableLocationForm();
    }
  }

  setCreatingNewLocation(createingNewLocation: boolean) {
    this.creatingNewLocation = createingNewLocation;
    if (this.creatingNewLocation) {
      this.enableLocationForm();
    } else {
      this.disableLocationForm();
    }
  }

  toggleCreatingNewDeployment() {
    if (this.creatingNewLocation && this.creatingNewDeployment) {
      this.toggleCreatingNewLocation();
    }
    this.creatingNewDeployment = !this.creatingNewDeployment;
    if (this.creatingNewDeployment) {
      this.enableDeploymentForm();
    } else {
      this.disableDeploymentForm();
    }
  }

  toggleAdditionalFields(additionalFieldsExpanded: boolean) {
    this.additionalFieldsExpanded = additionalFieldsExpanded;
    if (this.additionalFieldsExpanded) {
      this.mediaCreateUploadService.enableAdditionalFields();
    } else {
      this.mediaCreateUploadService.disableAdditionalFields();
    }
  }

  clearFiles() {
    this.files = [];
  }
}
export default ProjectWorkspaceUploadComponent;

import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeploymentListingItem } from '../services/project-deployments-listing.service';
import {
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  FormFieldComponent,
  LabelComponent,
  SelectBoxInputComponent,
  SimpleMessageComponent,
  YesNoFlagComponent,
} from '@trapper/ui';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';
import { DialogRef, DIALOG_DATA } from '@angular/cdk/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TeamShareDeploymentsService } from '../services/team-share-deployments.service';
import { TeamsAvailableForSharingService } from '../services/teams-available-for-sharing.service';
import { ToastrService } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';

export interface TeamShareDeploymentsComponentData {
  projectSlug: string;
  deployments: DeploymentListingItem[];
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-teams-share-deployments',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    ButtonComponent,
    TranslocoModule,
    FormFieldComponent,
    SelectBoxInputComponent,
    LabelComponent,
    FormsModule,
    YesNoFlagComponent,
    CheckboxComponent,
  ],
  templateUrl: './teams-share-deployments.component.html',
  styleUrls: ['./teams-share-deployments.component.scss'],
  providers: [TeamsAvailableForSharingService, TeamShareDeploymentsService],
})
export class TeamsShareDeploymentsComponent implements OnInit {
  selectedTeamId: number | null = null;
  shareLocation = false;
  shareInProgress$ = this.teamShareDeploymentsService.inProgress$;
  availableTeams$ = this.teamsAvailableForSharingService.team$;
  sharingResults$ = this.teamShareDeploymentsService.results$;

  rejected$ = this.teamShareDeploymentsService.rejected$;
  allDeployments$ = this.teamShareDeploymentsService.deployments$;

  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: TeamShareDeploymentsComponentData,
    private teamShareDeploymentsService: TeamShareDeploymentsService,
    private teamsAvailableForSharingService: TeamsAvailableForSharingService,
    private toastr: ToastrService,
    private translocoService: TranslocoService,
  ) {}

  ngOnInit() {
    this.teamsAvailableForSharingService
      .fetchAvailableTeams()
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  cancel() {
    this.dialog.close(false);
  }

  close() {
    this.dialog.close(true);
  }

  share() {
    if (!this.selectedTeamId) {
      this.toastr.error(
        this.translocoService.translate(
          'teamsShareDeployment.noTeamSelected',
          {},
          'csProjects',
        ),
      );
      return;
    }
    this.teamShareDeploymentsService
      .shareDeployments(
        this.data.projectSlug,
        this.selectedTeamId,
        this.shareLocation,
        this.data.deployments.map((d) => d.pk),
      )
      .pipe(untilDestroyed(this))
      .subscribe();
  }
}

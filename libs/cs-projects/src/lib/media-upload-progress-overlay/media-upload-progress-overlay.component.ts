import { Component, HostListener } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  ProgressBarComponent,
} from '@trapper/ui';
import { MediaUploadService } from '../services/media-upload.service';
import { Router } from '@angular/router';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-frontend-media-upload-progress-overlay',
  standalone: true,
  imports: [
    CommonModule,
    ProgressBarComponent,
    CardComponent,
    ButtonComponent,
    TranslocoModule,
  ],
  templateUrl: './media-upload-progress-overlay.component.html',
  styleUrls: ['./media-upload-progress-overlay.component.scss'],
})
export class MediaUploadProgressOverlayComponent {
  constructor(
    public mediaUploadService: MediaUploadService,
    private router: Router,
  ) {}

  cancel() {
    this.mediaUploadService.cancel();
  }

  @HostListener('window:beforeunload', ['$event'])
  handleClose($event: BeforeUnloadEvent) {
    $event.returnValue = false;
  }
}

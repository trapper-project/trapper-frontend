import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  FormFieldComponent,
  LabelComponent,
  MapComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { TeamsCreateService } from '../services/teams-create.service';
import { ReactiveFormsModule } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';
import * as geojson from 'geojson';
import { GeojsonPolygonFileService } from '../services/geojson-polygon-file.service';
import { ToastrService } from 'ngx-toastr';
import { PageTitleService } from '@trapper/page-title-management';
import { ProjectDataService } from '../services/project-data.service';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-create',
  standalone: true,
  imports: [
    CommonModule,
    FormFieldComponent,
    ServerErrorsComponent,
    TextInputComponent,
    LabelComponent,
    MapComponent,
    ReactiveFormsModule,
    ButtonComponent,
    TranslocoModule,
  ],
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.scss'],
  providers: [TeamsCreateService],
})
export class TeamCreateComponent implements OnInit {
  formGroup = this.teamsCreateService.formGroup;
  newPolygon: geojson.Polygon | null = null;

  inProgress$ = this.teamsCreateService.inProgress$;

  defaultLocation$ = this.siteCustomizationService.defaultLocation$;

  constructor(
    private teamsCreateService: TeamsCreateService,
    private route: ActivatedRoute,
    private router: Router,
    private geojsonPolygonFileService: GeojsonPolygonFileService,
    private translocoService: TranslocoService,
    private toastr: ToastrService,
    private pageTitleService: PageTitleService,
    private projectDataService: ProjectDataService,
    private siteCustomizationService: CsSiteCustomizationService,
  ) {}

  ngOnInit(): void {
    this.teamsCreateService.data$
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
        const teamId = data?.id;
        if (teamId) {
          this.router.navigate(['..', teamId], { relativeTo: this.route });
        }
      });

    this.projectDataService.data$
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
        const titleParts = [
          this.translocoService.translate(
            'teamCreate.pageTitle',
            {},
            'csProjects',
          ),
        ];
        if (data) {
          titleParts.push(data.name);
        }
        this.pageTitleService.setSectionTitle(titleParts);
      });
  }

  onSubmit() {
    this.teamsCreateService.performCreate().subscribe();
  }

  polygonChanged(polygon: geojson.Polygon | null) {
    this.formGroup.patchValue({ polygon });
  }

  selectPolygonFileFromDisk() {
    this.geojsonPolygonFileService
      .selectPolygonFileFromDisk()
      .then((polygon) => {
        this.formGroup.controls.polygon.setValue(polygon);
        this.formGroup.controls.polygon.markAsDirty();
        this.newPolygon = polygon;
      })
      .catch(() => {
        this.toastr.error(
          this.translocoService.translate(
            'teamCreate.teamPolygonFileErrorToastMessage',
            {},
            'csProjects',
          ),
        );
      });
  }
}

export default TeamCreateComponent;

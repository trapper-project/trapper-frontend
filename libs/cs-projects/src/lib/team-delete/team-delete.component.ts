import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { TeamDeleteService } from '../services/team-delete.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslocoModule } from '@jsverse/transloco';

export interface TeamDeleteComponentData {
  teamId: number;
  projectSlug: string;
  teamName: string;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-team-delete',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    ButtonComponent,
    TranslocoModule,
  ],
  templateUrl: './team-delete.component.html',
  styleUrls: ['./team-delete.component.scss'],
  providers: [TeamDeleteService],
})
export class TeamDeleteComponent {
  inProgress = this.teamDeleteService.inProgress$;
  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: TeamDeleteComponentData,
    private teamDeleteService: TeamDeleteService,
  ) {}

  get teamName() {
    return this.data.teamName;
  }

  ok() {
    this.teamDeleteService
      .deleteTeam(this.data.teamId, this.data.projectSlug)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.dialog.close(true);
      });
  }

  cancel() {
    this.dialog.close(false);
  }
}

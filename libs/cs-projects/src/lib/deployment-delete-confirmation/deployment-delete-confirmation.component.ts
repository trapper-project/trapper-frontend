import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  DetailsCardItemComponent,
  FormColumnComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { TranslocoModule } from '@jsverse/transloco';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { DeploymentDeleteService } from '../services/deployment-delete.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

export interface DeploymentDeleteComponentData {
  deploymentId: number;
  projectSlug: string;
  deploymentName: string;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-deployment-delete-confirmation',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    ButtonComponent,
    TranslocoModule,
    DetailsCardItemComponent,
  ],
  templateUrl: './deployment-delete-confirmation.component.html',
  styleUrls: ['./deployment-delete-confirmation.component.scss'],
  providers: [DeploymentDeleteService],
})
export class DeploymentDeleteConfirmationComponent {
  inProgress = this.deploymentDeleteService.inProgress$;
  constructor(
    private dialog: DialogRef,
    @Inject(DIALOG_DATA)
    private data: DeploymentDeleteComponentData,
    private deploymentDeleteService: DeploymentDeleteService,
  ) {}

  get deploymentName() {
    return this.data.deploymentName;
  }

  ok() {
    this.deploymentDeleteService
      .deleteDeployment(this.data.projectSlug, this.data.deploymentId)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.dialog.close(true);
      });
  }

  cancel() {
    this.dialog.close(false);
  }
}

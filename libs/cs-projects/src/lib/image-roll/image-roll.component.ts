import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgScrollbar, NgScrollbarModule } from 'ngx-scrollbar';
import {
  ImageRollService,
  SequenceWithImages,
} from '../services/image-roll.service';
import { delay, map } from 'rxjs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MediaListingItem } from '../services/project-media-listing.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckboxComponent, ThumbnailWithBboxesComponent } from '@trapper/ui';
import { TranslocoModule } from '@jsverse/transloco';
import { MatIconModule } from '@angular/material/icon';
import { PostNavigationScrollToTopStateService } from '@trapper/post-navigation-scroll-to-top';

function offsetLeftRelativeTo(element: HTMLElement, relativeTo: HTMLElement) {
  let offsetLeft = 0;
  let currentElement: HTMLElement | null = element;
  while (currentElement && currentElement !== relativeTo) {
    offsetLeft += currentElement.offsetLeft;
    currentElement = currentElement.offsetParent as HTMLElement | null;
  }
  return offsetLeft;
}

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-image-roll',
  standalone: true,
  imports: [
    CommonModule,
    InfiniteScrollModule,
    NgScrollbarModule,
    MatTooltipModule,
    ThumbnailWithBboxesComponent,
    TranslocoModule,
    MatIconModule,
    CheckboxComponent,
  ],
  templateUrl: './image-roll.component.html',
  styleUrls: ['./image-roll.component.scss'],
})
export class ImageRollComponent implements OnInit, OnDestroy {
  private _firstElementLoadingStartedOffset = 0;
  private _lastScrollToActiveTime = 0;
  private _scrollToOffsetDuration = 200;
  @ViewChild('imageRollScrollbar', { static: false }) scrollbar?: NgScrollbar;
  @ViewChild('imageRollContainer', { static: false })
  imageRollContainer?: ElementRef<HTMLElement>;

  private _currentClassificationId?: number | null | undefined;

  @ViewChild('imageRollScrollViewport', { static: false })
  imageRollScrollViewport?: ElementRef<HTMLElement>;

  results$ = this.imageRollService.dataBySequence$;
  noItems$ = this.imageRollService.data$.pipe(map((dto) => dto?.length === 0));

  @Input()
  public get currentClassificationId(): number | null | undefined {
    return this._currentClassificationId;
  }
  public set currentClassificationId(value: number | null | undefined) {
    if (value && value !== this._currentClassificationId) {
      setTimeout(() => {
        this.scrollToActiveThumbnail();
      }, 0);
    }
    this._currentClassificationId = value;
  }

  get fetchingPreviousPage() {
    return this.imageRollService.fetchingPreviousPage;
  }

  get fetchingNextPage() {
    return this.imageRollService.fetchingNextPage;
  }

  get itemsSelected() {
    return this.imageRollService.selectedItems.size;
  }

  get reloading() {
    return this.imageRollService.reloading;
  }

  constructor(
    private imageRollService: ImageRollService,
    private router: Router,
    private route: ActivatedRoute,
    private postNavigationScrollToTopStateService: PostNavigationScrollToTopStateService,
  ) {}

  ngOnInit(): void {
    this.postNavigationScrollToTopStateService.enabled = false;
    this.imageRollService.previousPageLoadingStarted$
      .pipe(untilDestroyed(this), delay(0))
      .subscribe(({ previousFirstClassificationId }) => {
        if (!this.imageRollScrollViewport) {
          return;
        }
        const previousFirstElement =
          this.imageRollScrollViewport.nativeElement.querySelector(
            `.image-roll-button[data-classification-id='${previousFirstClassificationId}']`,
          );

        if (
          !previousFirstElement ||
          !(previousFirstElement instanceof HTMLElement)
        ) {
          return;
        }

        const previousFirstElementOffset = offsetLeftRelativeTo(
          previousFirstElement,
          this.imageRollScrollViewport.nativeElement,
        );

        this._firstElementLoadingStartedOffset = previousFirstElementOffset;
      });

    this.imageRollService.previousPageLoadingFinished$
      .pipe(untilDestroyed(this), delay(0))
      .subscribe(
        ({ previousFirstClassificationId, currentFirstClassificationId }) => {
          // setTimeout(() => {
          this.onPreviousPageLoadingFinished(
            previousFirstClassificationId,
            currentFirstClassificationId,
          );
          // }, 0);
        },
      );

    this.imageRollService.rollReloaded$
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        setTimeout(() => {
          this.scrollToActiveThumbnail();
        }, 0);
      });

    this.imageRollService.data$
      .pipe(delay(0), untilDestroyed(this))
      .subscribe(() => {
        this.scrollbar?.update();
      });
  }

  ngOnDestroy(): void {
    this.postNavigationScrollToTopStateService.enabled = true;
  }

  imageRollTrackByFn(index: number, item: MediaListingItem) {
    return item.pk;
  }
  imageRollSequencesTrakcByFn(index: number, item: SequenceWithImages) {
    return item.sequence_id;
  }

  onScrollUp() {
    this.imageRollService.fetchPreviousPage().subscribe();
  }

  onScrollDown() {
    this.imageRollService.fetchNextPage().subscribe();
  }

  onScroll(event: WheelEvent) {
    if (this.imageRollScrollViewport) {
      const currentOffset =
        this.imageRollScrollViewport.nativeElement.scrollLeft;
      const delta = event.deltaY * 0.3;
      const newOffset = currentOffset + delta;

      this.scrollToOffsetImmediate(newOffset);

      event.preventDefault();
    }
  }

  private onPreviousPageLoadingFinished(
    previousFirstClassification: number,
    currentFirstClassification: number,
  ) {
    if (!this.imageRollScrollViewport) {
      return;
    }
    const previousFirstElement =
      this.imageRollScrollViewport?.nativeElement.querySelector(
        `.image-roll-button[data-classification-id='${previousFirstClassification}']`,
      );

    if (
      !previousFirstElement ||
      !(previousFirstElement instanceof HTMLElement)
    ) {
      return;
    }

    const currentFirstClassificationElement =
      this.imageRollScrollViewport?.nativeElement.querySelector(
        `.image-roll-button[data-classification-id='${currentFirstClassification}']`,
      );

    if (
      !currentFirstClassificationElement ||
      !(currentFirstClassificationElement instanceof HTMLElement)
    ) {
      return;
    }

    const previousFirstElementOffset = offsetLeftRelativeTo(
      previousFirstElement,
      this.imageRollScrollViewport.nativeElement,
    );

    const currentFirstElementOffset = offsetLeftRelativeTo(
      currentFirstClassificationElement,
      this.imageRollScrollViewport.nativeElement,
    );

    let delta = previousFirstElementOffset - currentFirstElementOffset;
    delta -= this._firstElementLoadingStartedOffset;

    const currentScrollPosition =
      this.imageRollScrollViewport.nativeElement.scrollLeft;

    this.scrollToOffsetImmediate(currentScrollPosition + delta);
  }

  scrollToActiveThumbnail() {
    const el = document.querySelector(
      '.active-thumbnail',
    ) as HTMLElement | null;
    if (el) {
      this.scrollToItem(el);
      this._lastScrollToActiveTime = Date.now();
    }
  }

  scrollToItem(element: HTMLElement) {
    const thumbnailWidth = 112;
    const imageRollGap = 24;

    let x = 0;
    if (this.imageRollContainer) {
      const rect =
        this.imageRollContainer.nativeElement.getBoundingClientRect();
      x = -(rect.width - thumbnailWidth) / 2 + imageRollGap;
    }

    if (!this.scrollbar) {
      return;
    }

    let offsetLeftRelativeToScrollContainer = 0;
    if (this.imageRollContainer) {
      offsetLeftRelativeToScrollContainer = offsetLeftRelativeTo(
        element,
        this.imageRollContainer.nativeElement,
      );
    }

    this.scrollToOffset(offsetLeftRelativeToScrollContainer + x);
  }

  scrollToOffset(offset: number) {
    if (!this.scrollbar) {
      return;
    }

    this.scrollbar.scrollTo({
      left: offset,
      duration: this._scrollToOffsetDuration,
    });
  }

  scrollToOffsetImmediate(offset: number) {
    if (!this.scrollbar) {
      return;
    }

    const lastScrollToActiveDelta = Date.now() - this._lastScrollToActiveTime;

    if (lastScrollToActiveDelta < this._scrollToOffsetDuration) {
      this.scrollToActiveThumbnail();
      return;
    }

    this.scrollbar.scrollTo({
      left: offset,
      duration: 0,
    });
  }

  rollItemSelectButtonClicked(item: MediaListingItem, $event: MouseEvent) {
    if ($event.shiftKey) {
      this.imageRollService.selectRange(item.pk);
      return;
    }
    this.imageRollService.toggleItemSelection(item.pk);
  }

  rollThumbnailClicked(item: MediaListingItem) {
    this.imageRollService.setClassification(item.pk);
    this.router.navigate(['..', item.pk], {
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  selectSequence(sequenceId: number) {
    this.imageRollService.selectSequence(sequenceId);
  }
}

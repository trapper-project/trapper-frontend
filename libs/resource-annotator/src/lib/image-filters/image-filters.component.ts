import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { bootstrapArrowCounterclockwise } from '@ng-icons/bootstrap-icons';
import { ToolButtonComponent } from '../tool-button/tool-button.component';
import { trapperResourceAnnotatorTranslocoScope } from '../transloco.loader';
import { MatTooltip } from '@angular/material/tooltip';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-image-filters',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgIconComponent,
    ToolButtonComponent,
    MatTooltip,
    TranslocoModule,
  ],
  providers: [
    trapperResourceAnnotatorTranslocoScope,
    provideIcons({
      bootstrapArrowCounterclockwise,
    }),
  ],
  templateUrl: './image-filters.component.html',
  styleUrl: './image-filters.component.scss',
})
export class ImageFiltersComponent {
  @Input() brightness = 100;
  @Input() contrast = 100;
  @Input() saturation = 100;

  @Output() brightnessChange = new EventEmitter<number>();
  @Output() contrastChange = new EventEmitter<number>();
  @Output() saturationChange = new EventEmitter<number>();

  setBrightness(value: number) {
    this.brightness = value;
    this.brightnessChange.emit(value);
  }

  setContrast(value: number) {
    this.contrast = value;
    this.contrastChange.emit(value);
  }

  setSaturation(value: number) {
    this.saturation = value;
    this.saturationChange.emit(value);
  }

  resetBrightness() {
    this.setBrightness(100);
  }
  resetContrast() {
    this.setContrast(100);
  }
  resetSaturation() {
    this.setSaturation(100);
  }
}

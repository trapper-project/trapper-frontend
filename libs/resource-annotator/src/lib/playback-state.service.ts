import { EventEmitter, Injectable, Signal, signal } from '@angular/core';

@Injectable()
export class PlaybackStateService {
  private videoElement: HTMLVideoElement | null = null;
  videoPlaying = signal(false);
  get currentAnnotationFrame(): Signal<number> {
    return this._currentAnnotationFrame.asReadonly();
  }
  get totalAnnotationFrames(): Signal<number> {
    return this._totalAnnotationFrames.asReadonly();
  }
  get annotationFramesPerSecond(): Signal<number> {
    return this._annotationFramesPerSecond.asReadonly();
  }
  private _currentAnnotationFrame = signal(0);
  private _totalAnnotationFrames = signal(0);
  private _annotationFramesPerSecond = signal(4);

  annotationFrameChanged = new EventEmitter<number>();
  videoFrameChanged = new EventEmitter<void>();

  setVideoElement(videoElement: HTMLVideoElement) {
    this.videoElement = videoElement;
  }
  clearVideoElement() {
    this.videoElement = null;
  }

  seekToFrame(frame: number) {
    if (!this.videoElement) {
      return;
    }
    // add a little offset to land "in the middle of the frame"
    // do not add offset if we are at the last frame
    const newTime =
      (frame + (frame === this._totalAnnotationFrames() - 1 ? 0 : 0.3)) /
      this._annotationFramesPerSecond();

    this.videoElement.currentTime = newTime;
  }

  setTotalAnnotationFrames(totalFrames: number) {
    this._totalAnnotationFrames.set(totalFrames);
  }

  setAnnotationFramesPerSecond(framesPerSecond: number) {
    this._annotationFramesPerSecond.set(framesPerSecond);
  }

  updateAnnotationFrameRate() {
    if (!this.videoElement) {
      this.setAnnotationFramesPerSecond(1);
      return;
    }
    const duration = this.videoElement.duration;
    const totalFrames = this.totalAnnotationFrames();
    this._annotationFramesPerSecond.set(totalFrames / duration);
  }

  nextFrame() {
    const totalFrames = this.totalAnnotationFrames();
    this.goToFrame((this.currentAnnotationFrame() + 1) % totalFrames);
  }

  previousFrame() {
    const totalFrames = this.totalAnnotationFrames();
    this.goToFrame(
      (this.currentAnnotationFrame() - 1 + totalFrames) % totalFrames,
    );
  }

  goToFrame(frame: number) {
    if (frame >= 0 && frame < this.totalAnnotationFrames()) {
      this._currentAnnotationFrame.set(frame);
      this.seekToFrame(this.currentAnnotationFrame());
      //   this.updateCurrentFrameBoundingBoxes();
      this.annotationFrameChanged.emit(frame);
    }
  }

  setCurrentAnnotationFrame(frame: number) {
    this._currentAnnotationFrame.set(frame);
  }

  reset() {
    this._currentAnnotationFrame.set(0);
    this._annotationFramesPerSecond.set(1);
  }

  playPause() {
    if (!this.videoElement) {
      this.videoPlaying.set(false);
      return;
    }
    const elem = this.videoElement;
    if (elem.paused) {
      elem.play();
      this.videoPlaying.set(true);
    } else {
      elem.pause();
      this.videoPlaying.set(false);
      this.seekToFrame(this.currentAnnotationFrame());
    }
  }

  onVideoFrame() {
    if (!this.videoElement || !this.videoPlaying()) {
      return;
    }
    const elem = this.videoElement;
    const frame = Math.floor(
      elem.currentTime * this.annotationFramesPerSecond(),
    );
    if (frame !== this.currentAnnotationFrame()) {
      this.setCurrentAnnotationFrame(frame);
      this.annotationFrameChanged.emit(frame);
    }
    // this.updateInterpolatedBoundingBoxes();
    this.videoFrameChanged.emit();

    requestAnimationFrame(() => {
      this.onVideoFrame();
    });
  }

  videoStarted() {
    this.videoPlaying.set(true);
    requestAnimationFrame(() => {
      this.onVideoFrame();
    });
  }

  videoTimeUpdate() {
    if (!this.videoElement) {
      return;
    }
    const elem = this.videoElement;
    const currentTime = elem.currentTime;
    console.log('currentTime', currentTime);
  }
}

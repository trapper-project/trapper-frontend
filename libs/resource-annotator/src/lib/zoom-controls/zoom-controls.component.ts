import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolButtonComponent } from '../tool-button/tool-button.component';
import { provideIcons } from '@ng-icons/core';
import { featherZoomIn, featherZoomOut } from '@ng-icons/feather-icons';
import { trapperResourceAnnotatorTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'trapper-zoom-controls',
  standalone: true,
  imports: [
    CommonModule,
    ToolButtonComponent,
    TranslocoModule,
    MatTooltipModule,
  ],
  templateUrl: './zoom-controls.component.html',
  styleUrl: './zoom-controls.component.scss',
  providers: [
    trapperResourceAnnotatorTranslocoScope,
    provideIcons({
      featherZoomOut,
      featherZoomIn,
    }),
  ],
})
export class ZoomControlsComponent {
  @Input() zoom = 1;
  @Output() zoomChange = new EventEmitter<number>();

  @Input() minZoom = 0.1;
  @Input() maxZoom = 10;

  zoomIn() {
    this.zoom += 0.1;
    if (this.zoom > this.zoom) {
      this.zoom = this.maxZoom;
    }
    this.zoomChange.emit(this.zoom);
  }

  zoomOut() {
    this.zoom -= 0.1;
    if (this.zoom < this.minZoom) {
      this.zoom = this.minZoom;
    }
    this.zoomChange.emit(this.zoom);
  }

  resetZoom() {
    this.zoom = 1;
    this.zoomChange.emit(this.zoom);
  }
}

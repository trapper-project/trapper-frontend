import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolButtonComponent } from '../tool-button/tool-button.component';
import { provideIcons } from '@ng-icons/core';
import {
  bootstrapPause,
  bootstrapPlay,
  bootstrapSkipBackward,
  bootstrapSkipForward,
} from '@ng-icons/bootstrap-icons';
import { PlaybackStateService } from '../playback-state.service';
import { FormsModule } from '@angular/forms';
import { Subject, throttleTime } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { trapperResourceAnnotatorTranslocoScope } from '../transloco.loader';

@UntilDestroy()
@Component({
  selector: 'trapper-video-controls',
  standalone: true,
  imports: [CommonModule, ToolButtonComponent, FormsModule],
  providers: [
    trapperResourceAnnotatorTranslocoScope,
    provideIcons({
      bootstrapPause,
      bootstrapPlay,
      bootstrapSkipBackward,
      bootstrapSkipForward,
    }),
  ],
  templateUrl: './video-controls.component.html',
  styleUrl: './video-controls.component.scss',
})
export class VideoControlsComponent implements OnInit {
  currentAnnotationFrame = this.playbackStateService.currentAnnotationFrame;
  totalAnnotationFrames = this.playbackStateService.totalAnnotationFrames;
  videoPlaying = this.playbackStateService.videoPlaying;

  goToFrameThrottled$ = new Subject<number>();

  constructor(private playbackStateService: PlaybackStateService) {}

  ngOnInit() {
    this.goToFrameThrottled$
      .pipe(throttleTime(50), untilDestroyed(this))
      .subscribe((frame) => this.playbackStateService.goToFrame(frame));
  }

  playPauseClicked() {
    this.playbackStateService.playPause();
  }

  nextFrameClicked() {
    this.playbackStateService.nextFrame();
  }

  previousFrameClicked() {
    this.playbackStateService.previousFrame();
  }

  goToFrame(frame: number) {
    this.goToFrameThrottled$.next(frame);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResourceAnnotatorComponent } from './resource-annotator.component';

describe('ResourceAnnotatorComponent', () => {
  let component: ResourceAnnotatorComponent;
  let fixture: ComponentFixture<ResourceAnnotatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResourceAnnotatorComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ResourceAnnotatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

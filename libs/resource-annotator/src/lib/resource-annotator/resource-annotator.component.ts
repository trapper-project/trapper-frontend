import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
  signal,
} from '@angular/core';
import { toObservable } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { NgxResizeObserverModule } from 'ngx-resize-observer';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { featherZoomIn, featherZoomOut } from '@ng-icons/feather-icons';
import { remixFocusMode, remixHdLine } from '@ng-icons/remixicon';
import {
  bootstrapBoundingBox,
  bootstrapBoundingBoxCircles,
  bootstrapArrowsMove,
  bootstrapTrash,
  bootstrapEye,
  bootstrapEyeFill,
  bootstrapImage,
  bootstrapArrowCounterclockwise,
  bootstrapPause,
  bootstrapPlay,
  bootstrapPlus,
  bootstrapSkipBackward,
  bootstrapSkipForward,
  bootstrapFullscreenExit,
  bootstrapArrowsFullscreen,
  bootstrapX,
  bootstrapExclamationLg,
} from '@ng-icons/bootstrap-icons';

import screenfull from 'screenfull';

import {
  matPanToolOutline,
  matHdOutline,
} from '@ng-icons/material-icons/outline';
import {
  matBrowserNotSupported,
  matSettingsBrightness,
} from '@ng-icons/material-icons/baseline';
import { lucideBoxSelect } from '@ng-icons/lucide';
import {
  combineLatest,
  delay,
  fromEvent,
  map,
  take,
  takeUntil,
  tap,
  throttleTime,
} from 'rxjs';
import { CdkContextMenuTrigger, CdkMenu, CdkMenuItem } from '@angular/cdk/menu';
import { SpinnerComponent } from '@trapper/ui';
import { OverlayModule } from '@angular/cdk/overlay';
import { FormsModule } from '@angular/forms';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ImageFiltersComponent } from '../image-filters/image-filters.component';
import { ToolButtonComponent } from '../tool-button/tool-button.component';
import { VideoControlsComponent } from '../video-controls/video-controls.component';
import { PlaybackStateService } from '../playback-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ZoomControlsComponent } from '../zoom-controls/zoom-controls.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { trapperResourceAnnotatorTranslocoScope } from '../transloco.loader';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';

export interface BoundingBox {
  left: number;
  top: number;
  width: number;
  height: number;
}

type Bbox = number[] | null;

export interface AnnotatedObject {
  id: string;
  label?: string;
  hasErrors?: boolean;
  bboxes: Bbox[];
}

export interface CurrentFrameBoundingBox extends BoundingBox {
  interpolated?: BoundingBox;
  id: string;
  label: string;
  hasErrors: boolean;
  nextFrame: BoundingBox | null;
  selected: boolean;
}

type BboxMode = 'create_new' | 'select_existing' | 'move_or_resize' | null;

interface ModeButtonDef {
  icon: string;
  mode: BboxMode;
  tooltip?: string;
}

export interface CustomToolButton {
  icon: string;
  tooltip: string;
  eventName: string;
}

export interface BboxClickEvent {
  bboxId: string;
  event: MouseEvent;
}

function clampRect(rect: BoundingBox): BoundingBox {
  let left = rect.left;
  let top = rect.top;
  let right = rect.left + rect.width;
  let bottom = rect.top + rect.height;
  if (left < 0) {
    left = 0;
  }
  if (top < 0) {
    top = 0;
  }
  if (right > 1) {
    right = 1;
  }
  if (bottom > 1) {
    bottom = 1;
  }
  return {
    left,
    top,
    width: right - left,
    height: bottom - top,
  };
}

@UntilDestroy()
@Component({
  selector: 'trapper-resource-annotator',
  standalone: true,
  imports: [
    CommonModule,
    NgxResizeObserverModule,
    NgIconComponent,
    CdkMenu,
    CdkContextMenuTrigger,
    CdkMenuItem,
    SpinnerComponent,
    OverlayModule,
    FormsModule,
    NgScrollbarModule,
    ImageFiltersComponent,
    ToolButtonComponent,
    VideoControlsComponent,
    ZoomControlsComponent,
    MatTooltipModule,
    TranslocoModule,
  ],
  templateUrl: './resource-annotator.component.html',
  styleUrls: ['./resource-annotator.component.scss'],
  providers: [
    trapperResourceAnnotatorTranslocoScope,
    provideIcons({
      featherZoomIn,
      featherZoomOut,
      remixFocusMode,
      lucideBoxSelect,
      bootstrapBoundingBox,
      bootstrapBoundingBoxCircles,
      bootstrapArrowsMove,
      bootstrapTrash,
      matPanToolOutline,
      matHdOutline,
      remixHdLine,
      bootstrapEye,
      bootstrapEyeFill,
      bootstrapImage,
      matBrowserNotSupported,
      matSettingsBrightness,
      bootstrapArrowCounterclockwise,
      bootstrapExclamationLg,
      bootstrapPause,
      bootstrapPlay,
      bootstrapPlus,
      bootstrapSkipBackward,
      bootstrapSkipForward,
      bootstrapFullscreenExit,
      bootstrapArrowsFullscreen,
      bootstrapX,
    }),
    PlaybackStateService,
  ],
})
export class ResourceAnnotatorComponent implements OnInit {
  edges: Array<'top' | 'bottom' | 'left' | 'right'> = [
    'top',
    'bottom',
    'left',
    'right',
  ];
  minZoom = 0.1;
  maxZoom = 5;
  internalresourceSrc: string | null = null;
  internalHdResourceSrc: string | null = null;
  hoveredId: string | null = null;
  alwaysShowLabels = false;
  alwaysShowLabelsLocked = true;

  hideBoundingBoxes = false;
  hideBoundingBoxesLocked = false;

  imageFiltersOpen = false;
  imageFiltersPosition: { x: number; y: number } = { x: 0, y: 0 };

  contrast = 100;
  brightness = 100;
  saturation = 100;

  videoPlaying = this.playbackState.videoPlaying;

  @Input() readOnly = false;
  @Input() showLabels = true;
  @Input() showObjectList = true;
  @Input() controlsOnHover = false;
  @Input() contextMenuEnabled = true;
  @Input() fullScreenToogleEnabled = true;

  @Input() customToolButtons: CustomToolButton[] = [];
  @Output() customToolButtonClicked = new EventEmitter<string>();

  @Input() set resourceSrc(value: string | null | undefined) {
    this.internalresourceSrc = value || null;
    this.playbackState.reset();
    this.loading = true;
    this.highResolution = false;
  }

  @Input() set hdResourceSrc(value: string | null | undefined) {
    this.internalHdResourceSrc = value || null;
    this.hdLoading = true;
  }

  _fileName?: string | null;

  @Input() set fileName(value: string | null | undefined) {
    this._fileName = value;
  }
  @Input() isVideo = false;

  @Input() autoFitPaddingTop = 48;
  @Input() autoFitPaddingBottom = 48;
  @Input() autoFitPaddingLeft = 216;
  @Input() autoFitPaddingRight = 0;

  _bboxInteactionDisabled = signal(false);
  @Input() set bboxInteactionDisabled(value: boolean | null | undefined) {
    if (!value) {
      this.currentMode = null;
    }
    this._bboxInteactionDisabled.set(value || false);
  }

  private _mainResource:
    | ElementRef<HTMLImageElement | HTMLVideoElement>
    | undefined;

  @ViewChild('mainResource')
  public get mainResource():
    | ElementRef<HTMLImageElement | HTMLVideoElement>
    | undefined {
    return this._mainResource;
  }
  public set mainResource(
    value: ElementRef<HTMLImageElement | HTMLVideoElement> | undefined,
  ) {
    this._mainResource = value;
    if (value && value.nativeElement instanceof HTMLVideoElement) {
      this.playbackState.setVideoElement(value.nativeElement);
    } else {
      this.playbackState.clearVideoElement();
    }
  }

  @ViewChild('workspace') workspace: ElementRef<HTMLDivElement> | undefined;

  @Output() bboxCreated = new EventEmitter<AnnotatedObject>();
  @Output() bboxUpdated = new EventEmitter<AnnotatedObject>();
  @Output() bboxesDeleted = new EventEmitter<string[]>();
  @Output() selectedBboxesChange = new EventEmitter<string[]>();
  @Output() resourcePreviewRequested = new EventEmitter();

  highResolution = false;

  private _selectedBboxes = new Set<string>();
  selectedObjectsCount = signal(0);
  @Input() set selectedBboxes(
    bboxes: string[] | undefined | null | Set<string>,
  ) {
    if (!bboxes) {
      this._selectedBboxes = new Set<string>();
    } else {
      this._selectedBboxes = new Set(bboxes);
    }
    if (this._selectedBboxes.size === 1) {
      this.selectedBboxId = this._selectedBboxes.values().next().value;
    } else {
      this.selectedBboxId = null;
    }
    this.selectedObjectsCount.set(this._selectedBboxes.size);
    this.recreateInternalBboxes();
  }

  get selectedBboxes(): string[] {
    return Array.from(this._selectedBboxes);
  }

  private _emitBboxesChange() {
    this.selectedBboxesChange.emit([...this._selectedBboxes]);
  }

  private _currentMode: BboxMode = null;
  public get currentMode(): BboxMode {
    return this._currentMode;
  }
  public set currentMode(value: BboxMode) {
    if (this.currentMode === value) {
      return;
    }
    this._currentMode = value;
    this.temporaryBboxRect = null;
  }

  modeButtons = signal<ModeButtonDef[]>([]);

  selectedBboxId: string | null = null;

  bboxDrawing = false;
  skipNextWorkspaceClick = false;
  temporaryBboxRect: BoundingBox | null = null;

  boundingBoxes: CurrentFrameBoundingBox[] = [];
  annotatedObjects: AnnotatedObject[] = [];

  currentObjectId: string | null = null;

  currentImageSizing = {
    imageWidth: 0,
    imageHeight: 0,
    width: 0,
    height: 0,
    x: 0,
    y: 0,
    realWidth: 0,
    realHeight: 0,
  };

  currentZoom = 1;
  loading = false;
  hdLoading = false;

  panX = 0;
  panY = 0;

  private panStartX = 0;
  private panStartY = 0;
  private panStartCursorX = 0;
  private panStartCursorY = 0;

  lastMouseX: number | null = null;
  lastMouseY: number | null = null;

  panInProgress = false;

  currentAnnotationFrame = this.playbackState.currentAnnotationFrame;
  totalAnnotationFrames = this.playbackState.totalAnnotationFrames;
  annotationFramesPerSecond = this.playbackState.annotationFramesPerSecond;

  isFullScreen = signal(false);

  @Input()
  set bboxes(annotatedObjects: AnnotatedObject[] | undefined | null) {
    this.annotatedObjects = annotatedObjects || [];
    if (!annotatedObjects) {
      this.boundingBoxes = [];
      return;
    }
    if (annotatedObjects.length > 0) {
      const first = annotatedObjects[0];
      if (first.bboxes.length > 0) {
        this.playbackState.setTotalAnnotationFrames(first.bboxes.length);
        // if (!this.loading) {
        this.updateAnntationFrameRate();
        // }
      }
    }
    this.boundingBoxes = [];
    this.updateCurrentFrameBoundingBoxes();
  }

  private updateCurrentFrameBoundingBoxes() {
    this.boundingBoxes = [];
    for (const obj of this.annotatedObjects) {
      const frame = obj.bboxes[this.currentAnnotationFrame()];
      const nextFrame = obj.bboxes[this.currentAnnotationFrame() + 1];
      if (!frame) {
        continue;
      }
      const [left, top, width, height] = frame;
      const nextFrameRect = nextFrame
        ? {
            left: nextFrame[0],
            top: nextFrame[1],
            width: nextFrame[2],
            height: nextFrame[3],
          }
        : null;
      this.boundingBoxes.push({
        id: obj.id,
        label: obj.label || '',
        hasErrors: obj.hasErrors || false,
        left,
        top,
        width,
        height,
        nextFrame: nextFrameRect,
        selected: this._selectedBboxes.has(obj.id),
      });
    }
  }

  private updateInterpolatedBoundingBoxes() {
    if (
      !this.mainResource ||
      !(this.mainResource.nativeElement instanceof HTMLVideoElement)
    ) {
      return;
    }

    const frameTime = 1 / this.annotationFramesPerSecond();
    const currentTime = this.mainResource.nativeElement.currentTime;
    const currentFrame = Math.floor(currentTime / frameTime);
    const currentFrameTime = currentFrame * frameTime;
    const frameProgress = (currentTime - currentFrameTime) / frameTime;

    for (const bbox of this.boundingBoxes) {
      if (!bbox.nextFrame) {
        continue;
      }
      const nextFrame = bbox.nextFrame;
      const left = bbox.left + (nextFrame.left - bbox.left) * frameProgress;
      const top = bbox.top + (nextFrame.top - bbox.top) * frameProgress;
      const width = bbox.width + (nextFrame.width - bbox.width) * frameProgress;
      const height =
        bbox.height + (nextFrame.height - bbox.height) * frameProgress;
      bbox.interpolated = {
        left,
        top,
        width,
        height,
      };
    }
  }

  clearInterpolatedBoundingBoxes() {
    for (const bbox of this.boundingBoxes) {
      bbox.interpolated = undefined;
    }
  }

  private recreateInternalBboxes() {
    this.updateCurrentFrameBoundingBoxes();
    // this.boundingBoxes = this.boundingBoxes.map((bbox) => ({
    //   ...bbox,
    //   selected: this._selectedBboxes.has(bbox.id),
    // }));
  }

  imageUpdated() {
    if (!this.loading) {
      return;
    }
    this.loading = false;
    this.brightness = 100;
    this.contrast = 100;
    this.saturation = 100;
    this.scaleToFit();
    // this.totalAnnotationFrames = this.getTotalAnnotationFrames();
    this.updateAnntationFrameRate();
  }

  imageSizeUpdated() {
    // void
  }

  private getWorkspaceOffset() {
    if (!this.workspace) {
      return { x: 0, y: 0 };
    }

    const workspaceRect = this.workspace.nativeElement.getBoundingClientRect();
    return { x: workspaceRect.left, y: workspaceRect.top };
  }

  private getHitTestedBbox(event: MouseEvent) {
    if (!this.mainResource) {
      return null;
    }

    const imageRect = this.mainResource.nativeElement.getBoundingClientRect();

    const x = (event.pageX - imageRect.left) / imageRect.width;
    const y = (event.pageY - imageRect.top) / imageRect.height;

    let currentBestHit: CurrentFrameBoundingBox | null = null;

    for (const bbox of this.boundingBoxes) {
      if (
        x >= bbox.left &&
        x <= bbox.left + bbox.width &&
        y >= bbox.top &&
        y <= bbox.top + bbox.height
      ) {
        if (
          !currentBestHit ||
          currentBestHit.width * currentBestHit.height >
            bbox.width * bbox.height
        ) {
          currentBestHit = bbox;
        }
      }
    }

    return currentBestHit;
  }

  wheel(event: WheelEvent) {
    const delta = event.deltaY;
    const ratio = 0.001;
    const previousZoom = this.currentZoom;

    this.currentZoom += delta * ratio;

    if (this.currentZoom < this.minZoom) {
      this.currentZoom = this.minZoom;
    }

    if (this.currentZoom > this.maxZoom) {
      this.currentZoom = this.maxZoom;
    }

    const workspaceOffset = this.getWorkspaceOffset();

    const cx = event.pageX;
    const cy = event.pageY;

    const x = cx - workspaceOffset.x;
    const y = cy - workspaceOffset.y;

    this.panX = x - (this.currentZoom / previousZoom) * (x - this.panX);
    this.panY = y - (this.currentZoom / previousZoom) * (y - this.panY);

    event.preventDefault();
    event.stopPropagation();
  }

  workspaceMouseDown(event: MouseEvent) {
    if (this.currentMode == 'create_new') {
      this.startBboxDrawing(event);
    } else if (this.currentMode === 'select_existing') {
      this.startBboxSelection(event);
    } else if (this.currentMode === null) {
      this.startPan(event);
    }

    event.preventDefault();
    event.stopPropagation();
  }

  startPan(event: MouseEvent) {
    this.panInProgress = true;
    this.panStartX = this.panX;
    this.panStartY = this.panY;
    this.panStartCursorX = event.pageX;
    this.panStartCursorY = event.pageY;

    event.preventDefault();
    event.stopPropagation();

    let moved = false;
    let totalMoved = 0;

    const mouseUpEvent = fromEvent(document, 'mouseup', { passive: true }).pipe(
      take(1),
      tap(() => {
        this.endPan();
        if (moved && totalMoved > 1) {
          this.skipNextWorkspaceClick = true;
        }
      }),
    );

    fromEvent(document, 'mousemove', { passive: true, capture: false })
      .pipe(takeUntil(mouseUpEvent))
      .subscribe((e) => {
        moved = true;
        const event = e as MouseEvent;
        totalMoved += Math.abs(event.movementX) + Math.abs(event.movementY);
        this.pan(event);
      });
  }

  endPan() {
    this.panInProgress = false;
  }

  pan(event: MouseEvent) {
    if (!this.panInProgress) {
      return;
    }

    const deltaX = event.pageX - this.panStartCursorX;
    const deltaY = event.pageY - this.panStartCursorY;

    this.panX = this.panStartX + deltaX;
    this.panY = this.panStartY + deltaY;

    event.preventDefault();
    event.stopPropagation();
  }

  private getResourceOriginalSize() {
    if (!this.mainResource) {
      return { width: 0, height: 0 };
    }

    const elem = this.mainResource.nativeElement;

    if (elem instanceof HTMLImageElement) {
      return { width: elem.naturalWidth, height: elem.naturalHeight };
    }

    return { width: elem.videoWidth, height: elem.videoHeight };
  }

  scaleToFit() {
    if (!this.mainResource || !this.workspace) {
      return;
    }

    const workspaceRect = this.workspace.nativeElement.getBoundingClientRect();

    const { width, height } = this.getResourceOriginalSize();

    const workspaceWidth =
      workspaceRect.width - this.autoFitPaddingLeft - this.autoFitPaddingRight;
    const workspaceHeight =
      workspaceRect.height - this.autoFitPaddingTop - this.autoFitPaddingBottom;

    const zoomX = workspaceWidth / width;
    const zoomY = workspaceHeight / height;

    const zoom = Math.min(zoomX, zoomY);

    this.currentZoom = zoom;

    const newImageWidth = width * zoom;
    const newImageHeight = height * zoom;

    this.panX = (workspaceWidth - newImageWidth) / 2;
    this.panY = (workspaceHeight - newImageHeight) / 2;

    this.panY += this.autoFitPaddingTop;
    this.panX += this.autoFitPaddingLeft;
  }

  workspaceMouseMoved(event: MouseEvent) {
    const offset = this.getWorkspaceOffset();
    this.lastMouseX = event.pageX - offset.x;
    this.lastMouseY = event.pageY - offset.y;
  }

  annotationLayerMouseMoved(event: MouseEvent) {
    const bbox = this.getHitTestedBbox(event);
    this.hoveredId = bbox ? bbox.id : null;
  }

  annotationLayerMouseDown(event: MouseEvent) {
    const bbox = this.getHitTestedBbox(event);
    if (!bbox) {
      return;
    }
    this.bboxMouseDown(event, bbox, 'move');
  }

  annotationLayerDoubleClicked(event: MouseEvent) {
    const bbox = this.getHitTestedBbox(event);
    if (!bbox) {
      return;
    }
    this.bboxDoubleClick(event, bbox);
  }

  annotationLayerClicked(event: MouseEvent) {
    const bbox = this.getHitTestedBbox(event);
    if (!bbox) {
      return;
    }
    this.bboxClicked(event, bbox);
  }

  workspaceMouseOut() {
    this.lastMouseX = null;
    this.lastMouseY = null;
  }

  getTotalAnnotationFrames() {
    if (!this.mainResource) {
      return 0;
    }

    const elem = this.mainResource.nativeElement;

    if (elem instanceof HTMLImageElement) {
      return 1;
    }

    return Math.floor(elem.duration * this.annotationFramesPerSecond()) + 1;
  }

  updateAnntationFrameRate() {
    this.playbackState.updateAnnotationFrameRate();
  }

  private _getDrawnRect(
    x0: number,
    y0: number,
    mouseEvent: MouseEvent,
    imageRect: DOMRect,
  ) {
    const x1 = (mouseEvent.pageX - imageRect.left) / imageRect.width;
    const y1 = (mouseEvent.pageY - imageRect.top) / imageRect.height;
    const left = Math.min(x0, x1);
    const top = Math.min(y0, y1);
    const width = Math.abs(x1 - x0);
    const height = Math.abs(y1 - y0);

    return {
      left,
      top,
      width,
      height,
    };
  }

  startBboxDrawing(event: MouseEvent) {
    if (!this.mainResource) {
      return;
    }
    this.bboxDrawing = true;
    const bboxDrawingStartX = event.pageX;
    const bboxDrawingStartY = event.pageY;

    const imageRect = this.mainResource.nativeElement.getBoundingClientRect();

    const x0 = (bboxDrawingStartX - imageRect.left) / imageRect.width;
    const y0 = (bboxDrawingStartY - imageRect.top) / imageRect.height;

    const mouseUpEvent = fromEvent(document, 'mouseup', { passive: true }).pipe(
      take(1),
      tap(() => {
        this.bboxDrawing = false;
        if (!this.temporaryBboxRect) {
          return;
        }
        this.temporaryBboxRect = clampRect(this.temporaryBboxRect);

        let newObject: AnnotatedObject | null = null;
        let createdNew = false;
        if (this.currentObjectId) {
          const obj = this.annotatedObjects.find(
            (obj) => obj.id === this.currentObjectId,
          );
          if (obj) {
            newObject = {
              ...obj,
              bboxes: [...obj.bboxes],
            };
          }
        }

        if (!newObject) {
          createdNew = true;
          newObject = {
            id: this.annotatedObjects.length.toString(),
            bboxes: new Array(this.totalAnnotationFrames()).fill(null),
          };
        }

        newObject.bboxes[this.currentAnnotationFrame()] = [
          this.temporaryBboxRect.left,
          this.temporaryBboxRect.top,
          this.temporaryBboxRect.width,
          this.temporaryBboxRect.height,
        ];

        if (createdNew) {
          this.bboxCreated.emit(newObject);
        } else {
          this.bboxUpdated.emit(newObject);
        }
        this.temporaryBboxRect = null;
        this.currentMode = 'move_or_resize';
      }),
    );

    fromEvent(document, 'mousemove', { passive: true, capture: false })
      .pipe(takeUntil(mouseUpEvent))
      .subscribe((e) => {
        const mouseEvent = e as MouseEvent;

        this.temporaryBboxRect = this._getDrawnRect(
          x0,
          y0,
          mouseEvent,
          imageRect,
        );

        this.temporaryBboxRect = clampRect(this.temporaryBboxRect);
        if (this.temporaryBboxRect.width * this.currentZoom < 0.01) {
          this.temporaryBboxRect = null;
          return;
        }
        if (this.temporaryBboxRect.height * this.currentZoom < 0.01) {
          this.temporaryBboxRect = null;
          return;
        }
      });
  }

  startBboxSelection(event: MouseEvent) {
    if (!this.mainResource) {
      return;
    }
    const bboxSelectionStartX = event.pageX;
    const bboxSelectionStartY = event.pageY;

    const imageRect = this.mainResource.nativeElement.getBoundingClientRect();

    const x0 = (bboxSelectionStartX - imageRect.left) / imageRect.width;
    const y0 = (bboxSelectionStartY - imageRect.top) / imageRect.height;

    let moved = false;
    let totalMoved = 0;

    const mouseUpEvent = fromEvent(document, 'mouseup', { passive: true }).pipe(
      take(1),
      tap(() => {
        // this.currentMode = null;
        // event.stopPropagation();
        this.temporaryBboxRect = null;
        if (moved && totalMoved > 1) {
          this.skipNextWorkspaceClick = true;
        }
      }),
    );

    fromEvent(document, 'mousemove', { passive: true, capture: false })
      .pipe(takeUntil(mouseUpEvent))
      .subscribe((e) => {
        const mouseEvent = e as MouseEvent;
        this.temporaryBboxRect = this._getDrawnRect(
          x0,
          y0,
          mouseEvent,
          imageRect,
        );
        const selectedBboxes: string[] = [];

        if (this.temporaryBboxRect.width * this.currentZoom < 0.01) {
          this.temporaryBboxRect = null;
          return;
        }
        if (this.temporaryBboxRect.height * this.currentZoom < 0.01) {
          this.temporaryBboxRect = null;
          return;
        }

        const sx0 = this.temporaryBboxRect.left;
        const sy0 = this.temporaryBboxRect.top;
        const sx1 = this.temporaryBboxRect.left + this.temporaryBboxRect.width;
        const sy1 = this.temporaryBboxRect.top + this.temporaryBboxRect.height;

        for (const bbox of this.boundingBoxes) {
          let bx0 = bbox.left;
          let by0 = bbox.top;
          let bx1 = bbox.left + bbox.width;
          let by1 = bbox.top + bbox.height;
          if (bx0 > bx1) {
            [bx0, bx1] = [bx1, bx0];
          }
          if (by0 > by1) {
            [by0, by1] = [by1, by0];
          }
          if (sx0 > bx1) {
            continue;
          }
          if (sx1 < bx0) {
            continue;
          }
          if (sy0 > by1) {
            continue;
          }
          if (sy1 < by0) {
            continue;
          }
          selectedBboxes.push(bbox.id);
        }
        this.selectedBboxes = selectedBboxes;
        this._emitBboxesChange();
        moved = true;
        totalMoved +=
          Math.abs(mouseEvent.movementX) + Math.abs(mouseEvent.movementY);
      });
  }

  bboxClicked(event: MouseEvent, bbox: CurrentFrameBoundingBox) {
    if (this._bboxInteactionDisabled()) {
      return;
    }
    if (event.ctrlKey || event.metaKey) {
      this._selectedBboxes.add(bbox.id);
      this._emitBboxesChange();
    } else {
      this.selectedBboxes = [bbox.id];
      this._emitBboxesChange();
    }

    if (this._selectedBboxes.size == 1) {
      this.selectedBboxId = bbox.id;
    } else {
      this.selectedBboxId = null;
    }

    this.recreateInternalBboxes();

    event.preventDefault();
    event.stopPropagation();
  }

  bboxDoubleClick(event: MouseEvent, bbox: CurrentFrameBoundingBox) {
    if (this.readOnly) {
      return;
    }
    this.selectedBboxes = new Set([bbox.id]);
    this.currentMode = 'move_or_resize';
    event.preventDefault();
    event.stopPropagation();

    this.recreateInternalBboxes();
  }

  bboxMouseDown(
    event: MouseEvent,
    bbox: CurrentFrameBoundingBox,
    mode:
      | 'top-left'
      | 'top-right'
      | 'bottom-left'
      | 'bottom-right'
      | 'move'
      | 'left'
      | 'right'
      | 'top'
      | 'bottom',
  ) {
    if (!this.mainResource) {
      return;
    }
    if (this.currentMode !== 'move_or_resize') {
      return;
    }
    event.preventDefault();
    event.stopPropagation();

    const imageRect = this.mainResource.nativeElement.getBoundingClientRect();

    const bboxStartX = bbox.left * imageRect.width;
    const bboxStartY = bbox.top * imageRect.height;
    const bboxStartWidth = bbox.width * imageRect.width;
    const bboxStartHeight = bbox.height * imageRect.height;

    const bboxStartMouseX = event.pageX;
    const bboxStartMouseY = event.pageY;

    const object = this.annotatedObjects.find((obj) => obj.id === bbox.id);
    if (!object) {
      return;
    }

    const mouseUpEvent = fromEvent(document, 'mouseup', {
      passive: true,
      capture: true,
    }).pipe(
      take(1),
      tap((event) => {
        const newObject = {
          ...object,
          bboxes: object.bboxes.map((frame, index) => {
            if (index !== this.currentAnnotationFrame()) {
              return frame;
            }
            return [bbox.left, bbox.top, bbox.width, bbox.height];
          }),
        };
        this.bboxUpdated.emit(newObject);
        event.stopPropagation();
      }),
    );

    fromEvent(document, 'mousemove', { passive: true, capture: false })
      .pipe(takeUntil(mouseUpEvent))
      .subscribe((e) => {
        const mouseEvent = e as MouseEvent;
        const deltaX = mouseEvent.pageX - bboxStartMouseX;
        const deltaY = mouseEvent.pageY - bboxStartMouseY;

        if (mode == 'move') {
          bbox.left = (bboxStartX + deltaX) / imageRect.width;
          bbox.top = (bboxStartY + deltaY) / imageRect.height;
        } else if (mode == 'top-left') {
          bbox.left = (bboxStartX + deltaX) / imageRect.width;
          bbox.top = (bboxStartY + deltaY) / imageRect.height;
          bbox.width = (bboxStartWidth - deltaX) / imageRect.width;
          bbox.height = (bboxStartHeight - deltaY) / imageRect.height;
        } else if (mode == 'top-right') {
          bbox.top = (bboxStartY + deltaY) / imageRect.height;
          bbox.width = (bboxStartWidth + deltaX) / imageRect.width;
          bbox.height = (bboxStartHeight - deltaY) / imageRect.height;
        } else if (mode == 'bottom-left') {
          bbox.left = (bboxStartX + deltaX) / imageRect.width;
          bbox.width = (bboxStartWidth - deltaX) / imageRect.width;
          bbox.height = (bboxStartHeight + deltaY) / imageRect.height;
        } else if (mode == 'bottom-right') {
          bbox.width = (bboxStartWidth + deltaX) / imageRect.width;
          bbox.height = (bboxStartHeight + deltaY) / imageRect.height;
        } else if (mode == 'top') {
          bbox.top = (bboxStartY + deltaY) / imageRect.height;
          bbox.height = (bboxStartHeight - deltaY) / imageRect.height;
        } else if (mode == 'bottom') {
          bbox.height = (bboxStartHeight + deltaY) / imageRect.height;
        } else if (mode == 'left') {
          bbox.left = (bboxStartX + deltaX) / imageRect.width;
          bbox.width = (bboxStartWidth - deltaX) / imageRect.width;
        } else if (mode == 'right') {
          bbox.width = (bboxStartWidth + deltaX) / imageRect.width;
        }
      });
  }

  deleteSelectedBboxes() {
    this.bboxesDeleted.emit(this.selectedBboxes);
  }

  workspaceClicked() {
    if (this.skipNextWorkspaceClick) {
      this.skipNextWorkspaceClick = false;
      return;
    }
    if (this._selectedBboxes.size > 0) {
      this.selectedBboxes = [];
      this._emitBboxesChange();
      if (this.currentMode === 'move_or_resize') {
        this.currentMode = null;
      }
    }
  }

  bboxMouseEnter(bbox: CurrentFrameBoundingBox) {
    this.hoveredId = bbox.id;
  }
  bboxMouseLeave(bbox: CurrentFrameBoundingBox) {
    if (this.hoveredId === bbox.id) {
      this.hoveredId = null;
    }
  }

  toggleImageFilters(event: MouseEvent, trigger: HTMLElement) {
    if (!this.workspace) {
      return;
    }
    if (this.imageFiltersOpen) {
      this.imageFiltersOpen = false;
      return;
    }
    this.imageFiltersOpen = true;
    const workspaceRect = this.workspace.nativeElement.getBoundingClientRect();
    const triggerRect = trigger.getBoundingClientRect();
    const xOffset = 0;
    const yOffset = -12;
    this.imageFiltersPosition = {
      x: workspaceRect.right - triggerRect.right - xOffset,
      y: triggerRect.height - yOffset,
    };
  }

  videoEnded() {
    console.info('video ended');
    this.videoPlaying.set(false);
    this.playbackState.goToFrame(0);
    this.clearInterpolatedBoundingBoxes();
  }

  videoPaused() {
    console.info('video paused');
    this.videoPlaying.set(false);
    this.playbackState.seekToFrame(this.currentAnnotationFrame());
    this.clearInterpolatedBoundingBoxes();
  }

  videoStarted() {
    this.playbackState.videoStarted();
  }

  videoTimeUpdate() {
    if (!this.mainResource) {
      return;
    }
    const elem = this.mainResource.nativeElement;
    if (!(elem instanceof HTMLVideoElement)) {
      return;
    }
    const currentTime = elem.currentTime;
    console.log('currentTime', currentTime);
  }

  seekToFrame(frame: number) {
    this.playbackState.seekToFrame(frame);
  }

  constructor(
    private playbackState: PlaybackStateService,
    private translocoService: TranslocoService,
    private injector: Injector,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit() {
    this.playbackState.annotationFrameChanged.subscribe(() => {
      this.updateCurrentFrameBoundingBoxes();
    });
    this.playbackState.videoFrameChanged.subscribe(() => {
      this.updateInterpolatedBoundingBoxes();
    });

    fromEvent(window, 'resize')
      .pipe(throttleTime(300), delay(100), untilDestroyed(this))
      .subscribe(() => {
        this.scaleToFit();
      });

    if (screenfull.isEnabled) {
      this.isFullScreen.set(screenfull.isFullscreen);
      fromEvent(screenfull, 'change')
        .pipe(untilDestroyed(this))
        .subscribe(() => {
          this.isFullScreen.set(screenfull.isFullscreen);
        });
    }

    const panMode$ = this.translocoService
      .selectTranslate<string>('modeButtons.pan', {}, this.translocoScope)
      .pipe(
        map((translation): ModeButtonDef => {
          return {
            icon: 'matPanToolOutline',
            mode: null,
            tooltip: translation,
          };
        }),
      );

    const groupSelectMode$ = this.translocoService
      .selectTranslate<string>(
        'modeButtons.groupSelect',
        {},
        this.translocoScope,
      )
      .pipe(
        map((translation): ModeButtonDef => {
          return {
            icon: 'lucideBoxSelect',
            mode: 'select_existing',
            tooltip: translation,
          };
        }),
      );

    const moveMode$ = this.translocoService
      .selectTranslate<string>('modeButtons.move', {}, this.translocoScope)
      .pipe(
        map((translation): ModeButtonDef => {
          return {
            icon: 'bootstrapArrowsMove',
            mode: 'move_or_resize',
            tooltip: translation,
          };
        }),
      );

    combineLatest([
      panMode$,
      groupSelectMode$,
      moveMode$,
      toObservable(this._bboxInteactionDisabled, {
        injector: this.injector,
      }),
    ])
      .pipe(untilDestroyed(this))
      .subscribe(([pan, groupSelect, move, disabled]) => {
        if (disabled) {
          this.modeButtons.set([pan]);
          return;
        } else {
          this.modeButtons.set([pan, groupSelect, move]);
        }
      });
  }

  toggleFullscreen() {
    if (screenfull.isEnabled) {
      screenfull.toggle();
    }
  }

  cancelCreateNew() {
    this.temporaryBboxRect = null;
    this.currentMode = null;
  }

  toggleObjectSelection(objectId: string, event: MouseEvent) {
    const isCtrl = event.ctrlKey || event.metaKey;
    if (!isCtrl) {
      this.selectedBboxes = [objectId];
    } else {
      if (this._selectedBboxes.has(objectId)) {
        this._selectedBboxes.delete(objectId);
      } else {
        this._selectedBboxes.add(objectId);
      }
      this._emitBboxesChange();
    }
    event.stopPropagation();
  }
}

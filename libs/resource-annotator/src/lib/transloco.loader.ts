import { TRANSLOCO_SCOPE, Translation } from '@jsverse/transloco';
import { availableLanguages } from '@trapper/translations';

export const loader = availableLanguages.reduce(
  (acc, lang) => {
    acc[lang] = () => import(`./i18n/${lang}.json`);
    return acc;
  },
  {} as Record<string, () => Promise<Translation>>,
);

export const trapperResourceAnnotatorTranslocoScope = {
  provide: TRANSLOCO_SCOPE,
  useValue: {
    scope: 'resourceAnnotator',
    loader,
  },
};

import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconType, NgIconComponent } from '@ng-icons/core';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'trapper-tool-button',
  standalone: true,
  imports: [CommonModule, NgIconComponent, MatTooltipModule],
  templateUrl: './tool-button.component.html',
  styleUrl: './tool-button.component.scss',
})
export class ToolButtonComponent {
  @Input() icon?: IconType;

  @Input()
  active = false;

  @Input() tooltip = '';
}

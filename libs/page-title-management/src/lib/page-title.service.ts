import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';
import {
  BehaviorSubject,
  combineLatest,
  map,
  shareReplay,
  startWith,
} from 'rxjs';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class PageTitleService {
  APP_TITLE = 'Trapper';

  customTitle$ = this.csSiteCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.project_name),
  );
  appTitle$ = this.customTitle$.pipe(
    startWith(this.APP_TITLE),
    map((title) => (title ? title : this.APP_TITLE)),
    shareReplay(1),
  );

  sectionTitle$ = new BehaviorSubject<string[]>([]);

  constructor(
    private title: Title,
    private csSiteCustomizationService: CsSiteCustomizationService,
  ) {
    combineLatest([this.appTitle$, this.sectionTitle$])
      .pipe(untilDestroyed(this))
      .subscribe(([appTitle, sectionTitle]) => {
        const parts = [...sectionTitle, appTitle];
        this.title.setTitle(parts.join(' | '));
      });
  }

  resetTitle(): void {
    this.title.setTitle(this.APP_TITLE);
  }

  setSectionTitle(sectionTitle: string | string[]): void {
    const titleParts: string[] = [];
    if (!Array.isArray(sectionTitle)) {
      titleParts.push(sectionTitle);
    } else {
      titleParts.push(...sectionTitle);
    }
    this.sectionTitle$.next(titleParts);
  }

  setTitle(title: string | string[]): void {
    this.setSectionTitle(title);
  }
}

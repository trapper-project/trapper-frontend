import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { BaseBackendConnector } from '@trapper/backend';

export interface UserProfileData {
  email: string;
  username: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  institution: string;
  about_me: string;
  gdpr: boolean;
  tos: boolean;
  avatar: string;
  default_cs_project?: string | null;
  is_cs_only: boolean;
}

@Injectable({ providedIn: 'root' })
export class CurrentUserProfileService extends BaseBackendConnector<
  void,
  UserProfileData
> {
  private router = inject(Router);
  constructor() {
    super();
    this.data$.subscribe((data) => {
      if (!data) {
        return;
      }

      if (!data.tos) {
        console.warn('TOS not accepted');
        this.router.navigate(['/cs/accounts/consents']);
      }
    });
  }
  reloadUserProfile() {
    return this.get('/api/accounts/profile/');
  }
}

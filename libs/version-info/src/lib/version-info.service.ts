import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, map } from 'rxjs';

export interface VersionInfo {
  fe_version: string;
  be_version: string;
}

@Injectable({
  providedIn: 'root',
})
@UntilDestroy()
export class VersionInfoService {
  versionInfo: VersionInfo = {
    fe_version: 'unknown',
    be_version: 'unknown',
  };
  versionInfo$ = new BehaviorSubject<VersionInfo>(this.versionInfo);
  constructor(private httpClient: HttpClient) {
    this.extractFrontendVersion();
    this.fetchBackendVersion();
  }

  extractFrontendVersion() {
    const wnd = window as { TRAPPER_FE_VERSION?: string };
    if (wnd.TRAPPER_FE_VERSION) {
      this.versionInfo.fe_version = wnd.TRAPPER_FE_VERSION;
      this.versionInfo$.next(this.versionInfo);
    }
  }

  fetchBackendVersion() {
    return this.httpClient
      .get<{ version: string }>('/api/cs/version')
      .pipe(
        map((data) => data.version),
        untilDestroyed(this),
      )
      .subscribe((ver) => {
        this.versionInfo.be_version = ver;
        this.versionInfo$.next(this.versionInfo);
      });
  }
}

import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'trapper-ui-user-mini-avatar',
  standalone: true,
  imports: [CommonModule, MatTooltipModule],
  templateUrl: './user-mini-avatar.component.html',
  styleUrls: ['./user-mini-avatar.component.scss'],
})
export class UserMiniAvatarComponent {
  @Input() avatarUrl? = '';
  @Input() name? = '';
  @Input() userId? = '';
}

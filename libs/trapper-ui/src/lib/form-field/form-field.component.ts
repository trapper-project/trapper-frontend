import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-ui-form-field',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
})
export class FormFieldComponent {}

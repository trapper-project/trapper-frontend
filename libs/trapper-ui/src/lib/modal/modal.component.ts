import { Component, ContentChild, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgScrollbarModule } from 'ngx-scrollbar';

@Component({
  selector: 'trapper-ui-modal',
  standalone: true,
  imports: [CommonModule, NgScrollbarModule],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  @ContentChild('footer', { read: TemplateRef })
  modalFooter?: TemplateRef<unknown>;
  @ContentChild('title', { read: TemplateRef })
  modalTitle?: TemplateRef<unknown>;
}

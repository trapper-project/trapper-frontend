import {
  Component,
  EventEmitter,
  Inject,
  Input,
  NgZone,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import * as L from 'leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DomEvent, Marker, latLng, tileLayer } from 'leaflet';
import 'leaflet.polylinemeasure';
import 'leaflet-draw';
import 'leaflet.locatecontrol';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import * as geojson from 'geojson';
import {
  TRANSLOCO_SCOPE,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { trapperUiTranslocoScope } from '../transloco.loader';

let trapperMapId = 0;

export interface MapMarker {
  lat: number;
  lng: number;
  label?: string;
  id: string;
  isHighlighted?: boolean;
}

export interface LayerData {
  id: string;
  layer: L.Layer;
}

const iconLargeSelectedTemplate = `
<svg
    class="map-marker-icon-large-selected"
    width="40"
    height="60"
    viewBox="0 0 40 60"
    fill="none" xmlns="http://www.w3.org/2000/svg">
  <path
    d="M38 20C38 22.3013 36.9164 25.7865 35.05 29.9497C33.2195 34.0329 30.7627 38.5001 28.2818 42.6639C25.805 46.821 23.3251 50.6411 21.4629 53.4239C20.9163 54.2407 20.4234 54.9673 20 55.5861C19.5766 54.9673 19.0837 54.2407 18.5371 53.4239C16.6749 50.6411 14.195 46.821 11.7182 42.6639C9.23735 38.5001 6.78048 34.0329 4.95001 29.9497C3.08365 25.7865 2 22.3013 2 20C2 10.0589 10.0589 2 20 2C29.9411 2 38 10.0589 38 20Z"
    fill="currentColor" stroke="currentColor" style="color: var(--trapper-color-map-marker);"
    stroke-width="4"
  />
  <circle cx="20.5" cy="18.5" r="7" fill="currentColor" stroke="currentColor" style="color: var(--trapper-color-map-marker-circle-selected);"/>
</svg>
`;

const iconLargeTemplate = `
<svg
    class="map-marker-icon-large-selected"
    width="40"
    height="60"
    viewBox="0 0 40 60"
    fill="none" xmlns="http://www.w3.org/2000/svg">
  <path
    d="M38 20C38 22.3013 36.9164 25.7865 35.05 29.9497C33.2195 34.0329 30.7627 38.5001 28.2818 42.6639C25.805 46.821 23.3251 50.6411 21.4629 53.4239C20.9163 54.2407 20.4234 54.9673 20 55.5861C19.5766 54.9673 19.0837 54.2407 18.5371 53.4239C16.6749 50.6411 14.195 46.821 11.7182 42.6639C9.23735 38.5001 6.78048 34.0329 4.95001 29.9497C3.08365 25.7865 2 22.3013 2 20C2 10.0589 10.0589 2 20 2C29.9411 2 38 10.0589 38 20Z"
    fill="currentColor" stroke="currentColor" style="color: var(--trapper-color-map-marker);"
    stroke-width="4"
  />
  <circle cx="20.5" cy="18.5" r="7" fill="currentColor" stroke="currentColor" style="color: var(--trapper-color-map-marker-circle);"/>
</svg>
`;

@UntilDestroy()
@Component({
  selector: 'trapper-ui-map',
  standalone: true,
  imports: [CommonModule, LeafletModule],
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [trapperUiTranslocoScope],
})
export class MapComponent {
  private _markers$ = new BehaviorSubject<MapMarker[]>([]);
  private _map?: L.Map;
  private _wasPositionCentered = false;
  private _editableLayers?: L.FeatureGroup;

  private _drawControl?: L.Control.Draw;
  private _drawControlEditOnly?: L.Control.Draw;

  @Input() mapId = `trapper-map-${trapperMapId++}`;

  @Input()
  public set markers(value: MapMarker[] | null | undefined) {
    if (!value) {
      value = [];
    }
    this._markers$.next(value);
  }

  @Input() zoom = 8;

  @Input() polygonEditingEnabled = false;

  @Output() markerClicked = new BehaviorSubject<MapMarker | null>(null);

  @Output() drawnPolygon = new EventEmitter<geojson.Polygon | null>();
  @Output() mapClick = new EventEmitter<L.LeafletMouseEvent>();

  @Input() locateMeButtonEnabled = false;

  @Input() defaultLocation?: L.LatLng | null;

  _polygon: geojson.Polygon | null = null;

  @Input() set polygon(value: geojson.Polygon | null) {
    this._polygon = value;
    if (!this._map || !this._editableLayers) {
      return;
    }
    this._editableLayers.clearLayers();
    if (value) {
      this.addGeojsonLayer(value);
      this._map.fitBounds(this._editableLayers.getBounds());
    }
    if (this._drawControl && this._drawControlEditOnly) {
      if (value) {
        this._map?.removeControl(this._drawControl);
        this._map?.addControl(this._drawControlEditOnly);
      } else {
        this._map?.removeControl(this._drawControlEditOnly);
        this._map?.addControl(this._drawControl);
      }
    }
  }

  openStreetmapTileLayer = tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    {
      // maxZoom: 18,
      attribution: 'Open Street Map',
      noWrap: true,
    },
  );

  worldImageryTileLayer = tileLayer(
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    {
      // maxZoom: 18,
      attribution: 'Tiles &copy; Esri',
      noWrap: true,
    },
  );

  mapOptions = {
    layers: [
      this.openStreetmapTileLayer,
      // this.worldImageryTileLayer
    ],
    zoom: 15,
    center: latLng(46.879966, -121.726909),
  };

  _layerCache = new Map<string, L.Marker>();

  layers$: Observable<LayerData[]> = this._markers$.pipe(
    map((markers) =>
      markers.filter((marker) => marker.lat !== null && marker.lng !== null),
    ),
    map((markers) => {
      const newMarkers = new Set(markers.map((marker) => marker.id));
      this._layerCache.forEach((layer, id) => {
        if (!newMarkers.has(id)) {
          this._layerCache.delete(id);
        }
      });
      markers.forEach((marker) => {
        if (!marker.id) {
          return;
        }
        let cachedMarker = this._layerCache.get(marker.id);
        if (!cachedMarker) {
          cachedMarker = new Marker(latLng(marker.lat, marker.lng), {
            title: marker.label,
          }).on('click', (e) => {
            this.zone.run(() => {
              this.markerClicked.next(marker);
              DomEvent.stopPropagation(e);
              e.originalEvent.stopPropagation();
              e.originalEvent.preventDefault();
            });
          });
          this._layerCache.set(marker.id, cachedMarker);
        }
        if (marker.isHighlighted) {
          cachedMarker.setIcon(
            new L.DivIcon({
              html: iconLargeSelectedTemplate,
              className: 'trapper-ui-map-marker-icon-large-selected',
              iconSize: [40, 60],
              iconAnchor: [20, 60],
            }),
          );
        } else {
          cachedMarker.setIcon(
            new L.DivIcon({
              html: iconLargeTemplate,
              className: 'trapper-ui-map-marker-icon-large',
              iconSize: [40, 60],
              iconAnchor: [20, 60],
            }),
          );
        }
        cachedMarker.setLatLng(latLng(marker.lat, marker.lng));
      });

      return markers
        .map((marker) => {
          if (!marker.id) {
            return null;
          }
          const layer = this._layerCache.get(marker.id);
          if (!layer) {
            return null;
          }
          return {
            id: marker.id,
            layer,
          };
        })
        .filter((layerData) => layerData !== null) as LayerData[];
    }),
  );

  markers$ = this._markers$.asObservable();

  constructor(
    private zone: NgZone,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  layerTrackBy(index: number, layerData: LayerData) {
    return layerData.id;
  }

  panToMarkersBoundary(markers: MapMarker[]) {
    if (!this._map) {
      return;
    }
    const latLngs = markers.map((marker) => latLng(marker.lat, marker.lng));
    const bounds = L.latLngBounds(latLngs);
    this._map.fitBounds(bounds, { padding: [50, 50], maxZoom: 15 });
  }

  onMapReady(map: L.Map) {
    this._map = map;
    this.bindLeafletMapClickEvent();
    this.addScaleControl();
    this.addDrawControl();
    this.addLayerControl();
    this.addLocateMeButtonOnMap();
    setTimeout(() => map.invalidateSize(), 0);
    if (this.defaultLocation) {
      this.panToLocation(this.defaultLocation.lat, this.defaultLocation.lng);
    }
    this._markers$.pipe(untilDestroyed(this)).subscribe((markers) => {
      if (this._map && markers.length > 0) {
        const highlightedMarker = markers.filter(
          (marker) => marker.isHighlighted,
        )[0];
        if (highlightedMarker) {
          this._map.panTo(latLng(highlightedMarker.lat, highlightedMarker.lng));
        } else if (!this._wasPositionCentered) {
          this._wasPositionCentered = true;
          this.panToMarkersBoundary(markers);
        }
        this._map.invalidateSize({ pan: false });
      }
    });
  }

  addLocateMeButtonOnMap() {
    if (!this._map) {
      return;
    }
    if (!this.locateMeButtonEnabled) {
      return;
    }
    this.translocoService
      .selectTranslate('map.mapLocateMe', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        if (!this._map) {
          return;
        }
        const locateMeButton = L.control.locate({
          position: 'topleft',
          strings: {
            title,
          },
          locateOptions: {
            maxZoom: 16,
            watch: false,
            enableHighAccuracy: true,
          },
          showCompass: true,
        });
        locateMeButton.addTo(this._map);
      });
  }

  bindLeafletMapClickEvent() {
    this._map?.on('click', () => {
      this.zone.run(() => {
        this.markerClicked.next(null);
      });
    });
  }

  addScaleControl() {
    if (!this._map) {
      return;
    }
    L.control.scale().addTo(this._map);
  }

  addLayerControl() {
    if (!this._map) {
      return;
    }
    L.control
      .layers({
        'Open Street Map': this.openStreetmapTileLayer,
        'Satelite Map': this.worldImageryTileLayer,
      })
      .addTo(this._map);
  }

  addDrawControl() {
    if (!this._map) {
      return;
    }
    this.initEditableLayers();
    if (!this._editableLayers) {
      return;
    }
    const drawnItems = new L.FeatureGroup();
    this._map.addLayer(drawnItems);
    if (this.polygonEditingEnabled) {
      this._drawControl = new L.Control.Draw({
        draw: {
          polygon: {
            allowIntersection: false,
          },
          polyline: false,
          circle: false,
          circlemarker: false,
          marker: false,
          rectangle: false,
        },
        edit: {
          featureGroup: this._editableLayers,
          remove: true,
        },
      });

      this._drawControlEditOnly = new L.Control.Draw({
        draw: undefined,
        edit: {
          featureGroup: this._editableLayers,
          remove: true,
        },
      });
    }

    if (this._polygon) {
      this.addGeojsonLayer(this._polygon);
      this._map.fitBounds(this._editableLayers.getBounds());
      if (this._drawControlEditOnly) {
        this._map.addControl(this._drawControlEditOnly);
      }
    } else {
      if (this._drawControl) {
        this._map.addControl(this._drawControl);
      }
    }

    this._map.on(L.Draw.Event.CREATED, (e: L.LeafletEvent) => {
      const createdEvent = e as L.DrawEvents.Created;
      const layer = createdEvent.layer as L.Polygon;
      this._editableLayers?.addLayer(layer);

      if (this._drawControl && this._drawControlEditOnly) {
        this._map?.removeControl(this._drawControl);
        this._map?.addControl(this._drawControlEditOnly);
      }
      this.zone.run(() => {
        const feature = layer.toGeoJSON();
        const geometry = feature.geometry;
        const polygon = geometry as GeoJSON.Polygon;
        this.drawnPolygon.next(polygon);
      });
    });

    this._map.on(L.Draw.Event.EDITED, (e: L.LeafletEvent) => {
      const editedEvent = e as L.DrawEvents.Edited;
      const layers = editedEvent.layers;
      const layer = layers.getLayers()[0] as L.Polygon;
      if (!layer) {
        return;
      }
      this.zone.run(() => {
        const feature = layer.toGeoJSON();
        const geometry = feature.geometry;
        const polygon = geometry as GeoJSON.Polygon;
        this.drawnPolygon.next(polygon);
      });
    });

    this._map.on(L.Draw.Event.DELETED, () => {
      if (this._drawControl && this._drawControlEditOnly) {
        this._map?.removeControl(this._drawControlEditOnly);
        this._map?.addControl(this._drawControl);
      }
      this.zone.run(() => {
        this.drawnPolygon.next(null);
      });
    });
  }

  initEditableLayers() {
    this._editableLayers = new L.FeatureGroup();
    this._map?.addLayer(this._editableLayers);
  }

  addGeojsonLayer(polygon: geojson.Polygon) {
    const layer = L.geoJSON(polygon);
    layer.eachLayer((l) => {
      this._editableLayers?.addLayer(l);
    });
  }

  onMapClick(e: L.LeafletMouseEvent) {
    this.mapClick.next(e);
  }

  panToLocation(lat: number, lng: number) {
    if (!this._map) {
      return;
    }
    this._map.panTo(latLng(lat, lng));
  }
}

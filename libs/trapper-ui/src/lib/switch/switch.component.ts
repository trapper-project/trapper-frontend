import { Component, Input, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgControl } from '@angular/forms';

let trapperSwitchId = 0;

function coerceBooleanProperty(value: unknown): boolean {
  return value != null && `${value}` !== 'false';
}

@Component({
  selector: 'trapper-ui-switch',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
})
export class SwitchComponent {
  checked = false;
  @Input() errors?: string[] | null;
  @Input() labelColor?: 'danger';
  @Input() isRequired = false;
  @Input() trackingId?: string;

  switchId = `trapper-ui-switch-${trapperSwitchId++}`;

  disabled = false;

  private _onChange?: (_val: boolean) => void;
  private _onTouched?: () => void;

  constructor(
    @Self()
    @Optional()
    private ngControl: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  writeValue(obj: boolean | string): void {
    this.checked = coerceBooleanProperty(obj);
  }
  registerOnChange(fn: (_val: boolean) => void): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onTouched() {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  onModelChange(val: boolean | string) {
    this.checked = coerceBooleanProperty(val);
    if (this._onChange) {
      this._onChange(this.checked);
    }
  }

  toggle() {
    this.onModelChange(!this.checked);
  }
}

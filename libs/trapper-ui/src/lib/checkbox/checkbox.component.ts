import { Component, Input, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlValueAccessor, FormsModule, NgControl } from '@angular/forms';

let trapperCheckboxId = 0;

@Component({
  selector: 'trapper-ui-checkbox',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent implements ControlValueAccessor {
  @Input() checked = false;
  @Input() errors?: string[] | null;
  @Input() labelColor?: 'danger';
  @Input() isRequired = false;
  @Input() trackingId?: string;
  @Input() testId?: string;
  @Input() small = true;

  checkboxId = `trapper-ui-checkbox-${trapperCheckboxId++}`;

  isDisabled = false;

  private _onChange?: (_val: boolean) => void;
  private _onTouched?: () => void;

  constructor(
    @Self()
    @Optional()
    private ngControl: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  writeValue(obj: boolean): void {
    this.checked = obj;
  }
  registerOnChange(fn: (_val: boolean) => void): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  onTouched() {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  onModelChange(val: boolean) {
    this.checked = val;
    if (this._onChange) {
      this._onChange(val);
    }
  }
}

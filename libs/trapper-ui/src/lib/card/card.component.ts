import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { inOutAnimation } from '@trapper/animations';
@Component({
  selector: 'trapper-ui-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [inOutAnimation],
})
export class CardComponent {
  @Input() horizontalPadding: 'standard' | 'compact' | 'none' = 'standard';
  @Input() verticalPadding: 'standard' | 'none' | 'compact' = 'standard';
  @Input() verticalMargin: 'large' | 'standard' | 'none' = 'standard';
  @Input() cardWidth: 'sm' | 'md' | 'lg' | 'xxl' | 'full' = 'sm';
  @Input() cardHeight: 'auto' | 'full' = 'auto';
  @Input() loading = false;
  @Input() horizontalAlign: 'left' | 'center' = 'center';
  @Input() rowGap: 'standard' | 'compact' | 'none' = 'standard';
}

import { AfterContentInit, Component, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  ControlValueAccessor,
  NgControl,
} from '@angular/forms';

@Component({
  selector: 'trapper-ui-form-field-hint',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './form-field-hint.component.html',
  styleUrls: ['./form-field-hint.component.scss'],
})
export class FormFieldHintComponent
  implements AfterContentInit, ControlValueAccessor
{
  control?: AbstractControl;

  constructor(@Optional() @Self() public ngControl: NgControl) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  writeValue(): void {
    // void
  }
  registerOnChange(): void {
    // void
  }
  registerOnTouched(): void {
    // void
  }
  setDisabledState?(): void {
    // void
  }

  ngAfterContentInit(): void {
    const control = this.ngControl && this.ngControl.control;
    if (control) {
      this.control = control;
    }
  }
}

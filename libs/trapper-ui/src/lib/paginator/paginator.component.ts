import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { inOutAnimation } from '@trapper/animations';
import {
  SelectBoxInputComponent,
  SelectBoxInputItem,
} from '../select-box-input/select-box-input.component';
import { FormsModule } from '@angular/forms';
import { trapperUiTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'trapper-ui-paginator',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    SelectBoxInputComponent,
    FormsModule,
    TranslocoModule,
  ],
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  animations: [inOutAnimation],
  providers: [trapperUiTranslocoScope],
})
export class PaginatorComponent {
  private _totalPages?: number | undefined;

  @Input() simple = false;

  @Input() total?: number;
  @Input() filtered?: number;
  @Input() itemsPerPage = 10;

  @Input() currentPage?: number;

  @Input()
  public get totalPages(): number | undefined {
    return this._totalPages;
  }
  public set totalPages(value: number | undefined) {
    this._totalPages = value;
    this.preparePageChoices();
  }

  @Output() pageChange = new EventEmitter<number>();

  @Input() currentPageSize = '10';
  @Output() currentPageSizeChange = new EventEmitter<string>();

  pageChoices: number[] = [];

  pageSizeChoices: SelectBoxInputItem[] = [10, 50, 100, 200].map((val) => ({
    name: val.toString(),
    id: val.toString(),
  }));

  next() {
    if (
      this.currentPage !== undefined &&
      this.totalPages !== undefined &&
      this.currentPage < this.totalPages
    ) {
      this.pageChange.emit(this.currentPage + 1);
    }
  }

  prev() {
    if (this.currentPage !== undefined && this.currentPage > 1) {
      this.pageChange.emit(this.currentPage - 1);
    }
  }

  preparePageChoices() {
    if (!this.totalPages || !this.currentPage) {
      return;
    }

    if (this.totalPages == 1) {
      this.pageChoices = [1];
      return;
    }

    if (this.totalPages <= 3) {
      this.pageChoices = Array.from(
        { length: this.totalPages },
        (_, i) => i + 1,
      );
      return;
    }

    const choices = [
      1,
      2,
      3,
      this.currentPage - 1,
      this.currentPage,
      this.currentPage + 1,
      this.totalPages - 1,
      this.totalPages,
    ];

    const finalPageNumbers: number[] = [];
    choices.forEach((choice) => {
      if (choices.length === 0) {
        finalPageNumbers.push(choice);
        return;
      }
      if (finalPageNumbers[finalPageNumbers.length - 1] >= choice) {
        return;
      }
      if (this.totalPages && choice > this.totalPages) {
        return;
      }
      finalPageNumbers.push(choice);
    });

    const choicesWithSeparators: number[] = [];
    finalPageNumbers.forEach((choice, index) => {
      if (index === 0) {
        choicesWithSeparators.push(choice);
        return;
      }
      if (choice - finalPageNumbers[index - 1] > 1) {
        choicesWithSeparators.push(-1);
      }
      choicesWithSeparators.push(choice);
    });

    this.pageChoices = choicesWithSeparators;
  }

  pageClicked(page: number) {
    this.pageChange.emit(page);
  }

  first() {
    this.pageChange.emit(1);
  }

  last() {
    if (this.totalPages) {
      this.pageChange.emit(this.totalPages);
    }
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslocoModule } from '@jsverse/transloco';

interface Bbox {
  left: number;
  top: number;
  width: number;
  height: number;
}

type NullableBbox = Bbox | null;

let placeholderImage: string | null = null;

@Component({
  selector: 'trapper-ui-thumbnail-with-bboxes',
  standalone: true,
  imports: [CommonModule, MatTooltipModule, TranslocoModule],
  templateUrl: './thumbnail-with-bboxes.component.html',
  styleUrls: ['./thumbnail-with-bboxes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThumbnailWithBboxesComponent {
  _thumbnailUrl?: string;
  _bboxes: Bbox[] = [];
  _strokeLineWidth = 2;
  @Input() wide = false;

  thumbnailUrlWithBboxes = '';

  @Input() isSelected = false;
  @Input() isHighlighted = false;
  @Input() rounded = true;

  @Input() set thumbnailUrl(thumbnailUrl: string | undefined) {
    this._thumbnailUrl = thumbnailUrl;
    this._redrawImage();
  }

  @Input() set strokeLineWidth(strokeLineWidth: number) {
    this._strokeLineWidth = strokeLineWidth;
  }

  @Input() set bboxes(bboxes: NullableBbox[]) {
    this._bboxes = bboxes.filter((bbox): bbox is Bbox => bbox !== null);
    this._redrawImage();
  }

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    this._generatePlaceholderImage();
  }

  _generatePlaceholderImage() {
    if (placeholderImage) {
      this.thumbnailUrlWithBboxes = placeholderImage;
      return;
    }
    const canvas = document.createElement('canvas');
    canvas.width = 200;
    canvas.height = 200;
    const ctx = canvas.getContext('2d');
    if (!ctx) {
      return;
    }
    ctx.fillStyle = getComputedStyle(document.body).getPropertyValue(
      '--trapper-color-form-placeholder',
    );
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    placeholderImage = canvas.toDataURL();
    this.thumbnailUrlWithBboxes = placeholderImage;
  }

  _redrawImage() {
    if (this._thumbnailUrl) {
      const image = new Image();
      image.onload = () => {
        this._drawBboxesOnImage(image);
        this.changeDetectorRef.detectChanges();
      };
      image.src = this._thumbnailUrl;
    }
  }

  _drawBboxesOnImage(image: HTMLImageElement) {
    const canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    const ctx = canvas.getContext('2d');
    if (!ctx) {
      return;
    }
    ctx.drawImage(image, 0, 0);
    ctx.strokeStyle = getComputedStyle(document.body).getPropertyValue(
      '--trapper-color-bbox',
    );
    ctx.lineWidth = this._strokeLineWidth;
    this._bboxes.forEach((bbox) => {
      ctx.strokeRect(
        bbox.left * image.width,
        bbox.top * image.height,
        bbox.width * image.width,
        bbox.height * image.height,
      );
    });

    this.thumbnailUrlWithBboxes = canvas.toDataURL();
  }
}

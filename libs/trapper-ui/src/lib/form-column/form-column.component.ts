import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-ui-form-column, form[trapperUiFormColumn]',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './form-column.component.html',
  styleUrls: ['./form-column.component.scss'],
})
export class FormColumnComponent {
  @Input() spacing: 'default' | 'compact' = 'default';
}

import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Optional,
  Output,
  Self,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectComponent, NgSelectModule } from '@ng-select/ng-select';
import { Subject } from 'rxjs';
import { FormsModule, NgControl } from '@angular/forms';
import { TextInputComponent } from '../text-input/text-input.component';

export interface SelectBoxInputItem {
  id: number | string | boolean;
  name: string;
}

@Component({
  selector: 'trapper-ui-select-box-input',
  standalone: true,
  imports: [CommonModule, NgSelectModule, FormsModule, TextInputComponent],
  templateUrl: './select-box-input.component.html',
  styleUrls: ['./select-box-input.component.scss'],
})
export class SelectBoxInputComponent {
  @Input() placeholder = '';
  @Input() searchPlaceholder = '';
  @Input() label = '';
  @Input() isRequired = false;
  @Input() items?: SelectBoxInputItem[] | null = null;
  @Input() help?: string | string[];
  @Input() markNonEmpty = false;
  @Input() loading = false;
  @Input() clearable = true;
  @Input() searchable = false;
  @Input() notFoundText = 'No items found';
  @Input() typeToSearchText = 'Type to search';

  @Input() customOptionTemplate?: TemplateRef<unknown>;
  @Input() customLabelTemplate?: TemplateRef<unknown>;

  @Input() query$ = new Subject<string>();
  @Output() scrolled = new EventEmitter<{ end: number } | null>();

  @ViewChild('select') select?: NgSelectComponent;
  @ViewChild('input') input?: ElementRef;

  @Input() typeahead$?: Subject<string>;

  @Input() testId?: string;

  @Input() multiple = false;

  disabled = false;

  private _value: string[] = [];

  constructor(
    @Self()
    @Optional()
    private ngControl: NgControl,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  set value(value: string[]) {
    this._value = value;
    this.onChange(value);
  }

  get value(): string[] {
    return this._value;
  }

  get hasErrors(): boolean {
    return !!this.ngControl?.invalid;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange = (_val: string[] | null) => {
    undefined;
  };
  onTouched = () => {
    undefined;
  };

  writeValue(value: string[]) {
    this._value = value;
    if (this.changeDetectorRef) {
      this.changeDetectorRef.markForCheck();
    }
  }

  registerOnChange(fn: (_val: string[] | null) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  onInput(event: Event) {
    const element = event.target as HTMLInputElement;
    if (element) {
      this.select?.filter(element.value);
    }
  }

  focus() {
    this.select?.blur();
    setTimeout(() => this.input?.nativeElement.focus(), 10);
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    this.changeDetectorRef.markForCheck();
  }
}

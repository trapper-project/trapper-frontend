import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'trapper-ui-thumbnail-grid-mini',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './thumbnail-grid-mini.component.html',
  styleUrls: ['./thumbnail-grid-mini.component.scss'],
})
export class ThumbnailGridMiniComponent {
  @Input() thumbnails: string[] = [];
}

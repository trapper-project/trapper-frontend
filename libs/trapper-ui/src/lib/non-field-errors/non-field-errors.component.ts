import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-ui-non-field-errors',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './non-field-errors.component.html',
  styleUrls: ['./non-field-errors.component.scss'],
})
export class NonFieldErrorsComponent {
  @Input() errors?: string[] | null;
}

import { Component, Input, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ControlValueAccessor, FormsModule, NgControl } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'trapper-ui-date-input',
  standalone: true,
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatIconModule,
  ],
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss'],
})
export class DateInputComponent implements ControlValueAccessor {
  @Input() label?: string;
  @Input() disabled = false;
  @Input() isRequired = false;
  @Input() placeholder?: string;
  @Input() fontIcon? = 'calendar_today';

  private _onChange?: (s: string) => void;
  private _onTouched?: () => void;

  value = '';

  get hasErrors(): boolean {
    return this.ngControl?.errors !== null && this.ngControl?.pristine === true;
  }

  constructor(
    @Self()
    @Optional()
    private ngControl?: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: () => void): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(s: string) {
    this.value = s;
    if (this._onChange) {
      this._onChange(s);
    }
  }
  onTouched() {
    if (this._onTouched) {
      this._onTouched();
    }
  }
}

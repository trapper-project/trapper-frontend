import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

export type AlertColorVariant = 'info' | 'success' | 'danger';

@Component({
  selector: 'trapper-ui-alert',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent {
  @Input() variant: AlertColorVariant = 'info';
  @Input() hasIcon = false;

  get icon(): string {
    switch (this.variant) {
      case 'info':
        return 'info_outlined';
      case 'success':
        return 'check_circle';
      case 'danger':
        return 'error';
      default:
        return 'info';
    }
  }
}

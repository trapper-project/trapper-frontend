import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { matInfoOutline } from '@ng-icons/material-icons/outline';
import { MatTooltip } from '@angular/material/tooltip';

@Component({
  selector: 'trapper-ui-label',
  standalone: true,
  imports: [CommonModule, NgIconComponent, MatTooltip],
  providers: [
    provideIcons({
      matInfoOutline,
    }),
  ],
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss'],
})
export class LabelComponent {
  @Input() onBorder = false;
  @Input() isDisabled = false;
  @Input() isRequired = false;
  @Input() hint?: string;
}

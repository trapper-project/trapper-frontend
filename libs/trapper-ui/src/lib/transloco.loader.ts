import { TRANSLOCO_SCOPE } from '@jsverse/transloco';
import { availableLanguages } from '@trapper/translations';

export const loader = availableLanguages.reduce(
  (acc, lang) => {
    acc[lang] = () => import(`./i18n/${lang}.json`);
    return acc;
  },
  {} as Record<string, () => Promise<unknown>>,
);

export const trapperUiTranslocoScope = {
  provide: TRANSLOCO_SCOPE,
  useValue: { scope: 'trapperUi', loader },
};

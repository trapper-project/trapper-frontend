import {
  AfterContentInit,
  Component,
  Input,
  Optional,
  Self,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  ControlValueAccessor,
  NgControl,
} from '@angular/forms';
import { inOutAnimation } from '@trapper/animations';

@Component({
  selector: 'trapper-ui-server-errors',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './server-errors.component.html',
  styleUrls: ['./server-errors.component.scss'],
  animations: [inOutAnimation],
})
export class ServerErrorsComponent
  implements AfterContentInit, ControlValueAccessor
{
  @Input() control?: AbstractControl;

  constructor(@Optional() @Self() public ngControl: NgControl) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  writeValue(): void {
    // void
  }
  registerOnChange(): void {
    // void
  }
  registerOnTouched(): void {
    // void
  }
  setDisabledState?(): void {
    // void
  }

  ngAfterContentInit(): void {
    const control = this.ngControl && this.ngControl.control;
    if (control) {
      this.control = control;
    }
  }
}

import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'trapper-ui-simple-checkmark',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './simple-checkmark.component.html',
  styleUrls: ['./simple-checkmark.component.scss'],
})
export class SimpleCheckmarkComponent {
  @Input() value = false;
}

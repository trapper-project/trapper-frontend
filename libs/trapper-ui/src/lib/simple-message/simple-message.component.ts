import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-ui-simple-message',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './simple-message.component.html',
  styleUrls: ['./simple-message.component.scss'],
})
export class SimpleMessageComponent {}

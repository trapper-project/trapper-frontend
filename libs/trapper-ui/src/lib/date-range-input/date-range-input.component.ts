import {
  Component,
  Input,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormControl,
  FormGroup,
  FormsModule,
  NgControl,
  ReactiveFormsModule,
} from '@angular/forms';
import {
  MatDatepickerModule,
  MatDateRangePicker,
} from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { DateFnsModule } from '@angular/material-date-fns-adapter';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { tap } from 'rxjs';
import { LanguageSelectionService, dateLocales } from '@trapper/translations';

export interface DateRange {
  start?: Date | null;
  end?: Date | null;
}

const LOCALE_CHOICES = dateLocales;
@UntilDestroy()
@Component({
  selector: 'trapper-ui-date-range-input',
  standalone: true,
  imports: [
    CommonModule,
    MatDatepickerModule,
    DateFnsModule,
    FormsModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['yyyy-MM-dd'],
        },
        display: {
          dateInput: 'yyyy-MM-dd',
          monthYearLabel: 'MMMM yyyy',
          dateA11yLabel: 'yyyy-MM-dd',
          monthYearA11yLabel: 'MMMM yyyy',
        },
      },
    },
  ],
  templateUrl: './date-range-input.component.html',
  styleUrls: ['./date-range-input.component.scss'],
})
export class DateRangeInputComponent implements OnInit {
  @ViewChild(MatDateRangePicker) dateRangePicker?: MatDateRangePicker<Date>;
  @Input() disabled = false;
  @Input() isRequired = false;
  @Input() placeholder?: string;
  @Input() fontIcon = 'calendar_today';
  @Input() minDate?: Date;
  @Input() maxDate?: Date;

  @Input() testId?: string;

  range = new FormGroup(
    {
      start: new FormControl<Date | null>(null),
      end: new FormControl<Date | null>(null),
    },
    { updateOn: 'blur' },
  );

  private _onChange?: (s: DateRange) => void;
  private _onTouched?: () => void;

  get hasErrors(): boolean {
    return this.ngControl?.errors !== null && this.ngControl?.pristine === true;
  }

  constructor(
    private dateAdapter: DateAdapter<Date>,
    private languageSelectionService: LanguageSelectionService,
    @Self()
    @Optional()
    private ngControl?: NgControl,
  ) {
    this.dateAdapter.setLocale(
      LOCALE_CHOICES[this.languageSelectionService.currentLanguage],
    );

    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.range.valueChanges
      .pipe(
        tap((s) => {
          this.onChange(s);
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.languageSelectionService.localeChange$
      .pipe(
        tap((s) => {
          this.dateAdapter.setLocale(LOCALE_CHOICES[s]);
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  writeValue(obj?: DateRange | null): void {
    if (!obj) {
      this.range.setValue({ start: null, end: null }, { emitEvent: false });
      return;
    } else {
      this.range.setValue(
        {
          start: obj.start || null,
          end: obj.end || null,
        },
        { emitEvent: false },
      );
    }
  }
  registerOnChange(fn: () => void): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(s: DateRange) {
    if (this._onChange) {
      this._onChange(s);
    }
  }
  onTouched() {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  open() {
    this.dateRangePicker?.open();
  }
}

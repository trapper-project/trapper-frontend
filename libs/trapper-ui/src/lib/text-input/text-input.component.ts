import { Component, Input, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlValueAccessor, FormsModule, NgControl } from '@angular/forms';
import { inOutAnimation } from '@trapper/animations';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { bootstrapEye, bootstrapEyeSlash } from '@ng-icons/bootstrap-icons';

export type TextInputTypeChoices = 'text' | 'email' | 'password' | 'number';

@Component({
  selector: 'trapper-ui-text-input',
  standalone: true,
  imports: [CommonModule, FormsModule, NgIconComponent],
  providers: [
    provideIcons({
      bootstrapEye,
      bootstrapEyeSlash,
    }),
  ],
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  animations: [inOutAnimation],
})
export class TextInputComponent implements ControlValueAccessor {
  @Input() label?: string;
  @Input() disabled = false;
  @Input() type: TextInputTypeChoices = 'text';
  @Input() isRequired = false;
  @Input() multiline = false;
  @Input() errors?: string[] | null;
  @Input() helpText?: string;
  @Input() placeholder?: string;
  // @Input() fontIcon?: string;
  @Input() min?: number;
  @Input() max?: number;
  @Input() step?: number;
  @Input() testId?: string;

  private _onChange?: (s: string) => void;
  private _onTouched?: () => void;

  value = '';

  passwordVisible = false;

  get finalType(): TextInputTypeChoices {
    if (this.type === 'password' && this.passwordVisible) {
      return 'text';
    }
    return this.type;
  }

  get hasErrors(): boolean {
    return (
      (this.errors !== undefined &&
        this.errors !== null &&
        this.errors.length > 0) ||
      (this.ngControl?.errors !== null && this.ngControl?.pristine === true)
    );
  }

  constructor(
    @Self()
    @Optional()
    private ngControl?: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: () => void): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(s: string) {
    this.value = s;
    if (this._onChange) {
      this._onChange(s);
    }
  }
  onTouched() {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  togglePasswordVisibility() {
    this.passwordVisible = !this.passwordVisible;
  }
}

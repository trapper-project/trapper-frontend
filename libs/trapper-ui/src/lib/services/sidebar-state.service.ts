import { BreakpointObserver } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject } from 'rxjs';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class SidebarStateService {
  sidebarExpanded = true;
  sidebarExpanded$ = new BehaviorSubject<boolean>(this.sidebarExpanded);

  sidebarPresent = false;
  sidebarPresent$ = new BehaviorSubject<boolean>(this.sidebarPresent);

  wideScreen$ = new BehaviorSubject<boolean>(true);

  currentSidebarOffset$ = new BehaviorSubject<number>(0);
  currentScrollOffset = 0;

  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver
      .observe('(max-width: 1414px)')
      .pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result.matches) {
          this.collapseSidebar();
          this.wideScreen$.next(false);
        } else {
          this.expandSidebar();
          this.wideScreen$.next(true);
        }
      });
  }

  toggleSidebar() {
    this.sidebarExpanded = !this.sidebarExpanded;
    this.currentSidebarOffset$.next(this.currentScrollOffset);
    this.sidebarExpanded$.next(this.sidebarExpanded);
  }

  expandSidebar() {
    this.sidebarExpanded = true;
    this.currentSidebarOffset$.next(this.currentScrollOffset);
    this.sidebarExpanded$.next(this.sidebarExpanded);
  }

  collapseSidebar() {
    this.sidebarExpanded = false;
    this.sidebarExpanded$.next(this.sidebarExpanded);
  }

  setSidebarPresent(present: boolean) {
    this.sidebarPresent = present;
    this.sidebarPresent$.next(this.sidebarPresent);
  }
}

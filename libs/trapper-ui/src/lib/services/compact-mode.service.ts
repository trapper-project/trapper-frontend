import { Injectable } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BehaviorSubject } from 'rxjs';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class CompactModeService {
  compactMode = false;
  compactMode$ = new BehaviorSubject<boolean>(this.compactMode);

  setCompactMode(compact: boolean) {
    this.compactMode = compact;
    this.compactMode$.next(this.compactMode);
  }
}

import {
  AfterContentChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Directive,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule as ExperimentalScrollingModule } from '@angular/cdk-experimental/scrolling';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { inOutAnimation } from '@trapper/animations';
import {
  TRANSLOCO_SCOPE,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { trapperUiTranslocoScope } from '../transloco.loader';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { FormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';

export interface ResponsiveTableColumnInfo {
  name: string;
  key: string;
}

export interface CellTemplateContext<ItemT> {
  item: ItemT;
  column: TableCellDefDirective<ItemT>;
}

@Directive({
  selector: '[trapperUiTableCellDef]',
  standalone: true,
})
export class TableCellDefDirective<ItemT> {
  @Input('trapperUiTableCellDef') name?: string;
  @Input() label?: string;
  @Input() gridWidth = '1fr';
  @Input() className?: string;
  @Input() mainCell = false;
  constructor(public elementRef: TemplateRef<CellTemplateContext<ItemT>>) {}
}

export interface ItemTBase {
  id?: number | string;
  pk?: number;
}

export interface ItemTInternal<ItemT> {
  item: ItemT;
  selectable: boolean;
}

@UntilDestroy()
@Component({
  selector: 'trapper-ui-responsive-table',
  standalone: true,
  imports: [
    CommonModule,
    NgxSkeletonLoaderModule,
    NgScrollbarModule,
    ScrollingModule,
    ExperimentalScrollingModule,
    CheckboxComponent,
    FormsModule,
    MatTooltipModule,
  ],
  templateUrl: './responsive-table.component.html',
  styleUrls: ['./responsive-table.component.scss'],
  animations: [inOutAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [trapperUiTranslocoScope],
})
export class ResponsiveTableComponent<ItemT extends ItemTBase>
  implements AfterContentChecked, OnInit
{
  noItemsMessageDefault = '~~~ No items ~~~';

  // @Input() items: ItemT[] = [];
  _items: ItemTInternal<ItemT>[] = [];
  @Input() isLoading = false;
  @Input() noItemsMessage?: string;

  @Input() gridTemplateColumns = '1fr';

  @Input() selectedItems: Set<ItemT> = new Set();

  @Output() itemClicked = new EventEmitter<ItemT>();
  @Output() selectedItemsChange = new EventEmitter<Set<ItemT>>();

  @Input() clickableRows = false;
  @Input() selectableRows = false;

  private _rowSelectionEnabledFunction: (item: ItemT) => boolean = () => true;
  @Input()
  public set rowSelectionEnabledFunction(value: (item: ItemT) => boolean) {
    this._rowSelectionEnabledFunction = value;
    this.items = this._items.map((item) => item.item);
  }

  @Input() selectionDisabledTooltip = '';

  @Input() enableResponsiveHeader = true;

  @ContentChildren(TableCellDefDirective)
  cellDefs: QueryList<TableCellDefDirective<ItemT>> = new QueryList();

  cellDefsArray: TableCellDefDirective<ItemT>[] = [];
  mainColumns: TableCellDefDirective<ItemT>[] = [];

  tableMode = true;

  @Input() set items(items: ItemT[]) {
    this._items = items.map((item) => ({
      item,
      selectable: this._rowSelectionEnabledFunction(item),
    }));
    this.changeDetectorRef.markForCheck();
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private changeDetectorRef: ChangeDetectorRef,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit() {
    this.breakpointObserver
      .observe('(max-width: 1280px)')
      .pipe(untilDestroyed(this))
      .subscribe((result) => {
        this.tableMode = !result.matches;
        this.changeDetectorRef.markForCheck();
      });

    this.translocoService
      .selectTranslate(
        'responsiveTable.noItemsMessage',
        {},
        this.translocoScope,
      )
      .pipe(untilDestroyed(this))
      .subscribe((message) => {
        this.noItemsMessageDefault = message;
        this.changeDetectorRef.markForCheck();
      });
  }

  ngAfterContentChecked() {
    this.cellDefsArray = this.cellDefs.toArray();
    this.mainColumns = this.cellDefsArray.filter((c) => c.mainCell);
    let gridTemplateColumns = '';
    if (this.selectableRows) {
      gridTemplateColumns += '42px ';
    }
    for (const cellRenderDef of this.cellDefs) {
      if (cellRenderDef.gridWidth) {
        gridTemplateColumns += `${cellRenderDef.gridWidth} `;
      } else {
        gridTemplateColumns += '1fr ';
      }
    }
    gridTemplateColumns = gridTemplateColumns.trim();
    if (gridTemplateColumns !== this.gridTemplateColumns) {
      this.gridTemplateColumns = gridTemplateColumns;
    }
  }

  onItemClicked(item: ItemT) {
    if (!this.clickableRows) {
      return;
    }
    this.itemClicked.emit(item);
  }

  onCheckboxChange(item: ItemT, checked: boolean) {
    if (!this.selectableRows) {
      return;
    }

    if (checked) {
      this.selectedItems = new Set([...this.selectedItems, item]);
    } else {
      this.selectedItems = new Set(
        [...this.selectedItems].filter((i) => i !== item),
      );
    }
    this.selectedItemsChange.emit(this.selectedItems);
  }

  trackByFn(index: number, item: ItemT) {
    return item.id || item.pk || index;
  }

  cellTrackByFn(index: number, cellDef: TableCellDefDirective<ItemT>) {
    return cellDef.name || index;
  }
}

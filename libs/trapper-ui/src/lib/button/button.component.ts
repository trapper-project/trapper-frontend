import { Component, HostBinding, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '../spinner/spinner.component';

export type ButtonVariants =
  | 'primary'
  | 'secondary'
  | 'danger'
  | 'no-outline-link'
  | 'outline'
  | 'outline-primary'
  | 'outline-secondary'
  | 'outline-danger';

@Component({
  selector: 'trapper-ui-button',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() variant: ButtonVariants = 'primary';
  @Input() inProgress = false;
  @Input() disabled = false;
  @Input() testId?: string;

  @HostBinding('class.full-width')
  @Input()
  fullWidth = true;
}

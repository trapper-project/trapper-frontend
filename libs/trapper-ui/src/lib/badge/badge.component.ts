import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EllipsisModule } from 'ngx-ellipsis';

@Component({
  selector: 'trapper-ui-badge',
  standalone: true,
  imports: [CommonModule, EllipsisModule],
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
})
export class BadgeComponent {
  @Input() color = 'var(--trapper-color-badge-default)';
}

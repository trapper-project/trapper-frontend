import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-ui-details-card-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './details-card-item.component.html',
  styleUrls: ['./details-card-item.component.scss'],
})
export class DetailsCardItemComponent {
  @Input() label?: string;
}

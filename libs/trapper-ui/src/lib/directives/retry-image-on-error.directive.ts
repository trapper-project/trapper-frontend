import { Directive, HostListener, Input } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { interval, take, tap } from 'rxjs';

@UntilDestroy(this)
@Directive({
  selector: '[trapperUiRetryImageOnError]',
  standalone: true,
})
export class RetryImageOnErrorDirective {
  private count = 0;

  @Input() retryCount = 3;
  @Input() retryInterval = 2000;

  @HostListener('error', ['$event']) onError(event: Event) {
    const target = event.target as HTMLImageElement;
    const src = target.src;
    if (this.count >= this.retryCount) {
      return;
    }
    interval(this.retryInterval)
      .pipe(
        tap(() => {
          this.count++;
          target.src = '';
          target.src = src;
        }),
        take(1),
        untilDestroyed(this),
      )
      .subscribe();
  }
}

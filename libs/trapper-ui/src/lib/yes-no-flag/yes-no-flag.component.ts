import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'trapper-ui-yes-no-flag',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './yes-no-flag.component.html',
  styleUrls: ['./yes-no-flag.component.scss'],
})
export class YesNoFlagComponent {
  @Input() value = false;
  @Input() yesText = 'Yes';
  @Input() noText = 'No';
  @Input() showText = true;
  @Input()
  swapLabelWithIcon = false;
}

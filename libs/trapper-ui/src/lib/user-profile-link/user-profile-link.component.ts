import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserMiniAvatarComponent } from '../user-mini-avatar/user-mini-avatar.component';

@Component({
  selector: 'trapper-ui-user-profile-link',
  standalone: true,
  imports: [CommonModule, UserMiniAvatarComponent],
  templateUrl: './user-profile-link.component.html',
  styleUrls: ['./user-profile-link.component.scss'],
})
export class UserProfileLinkComponent {
  @Input() userId?: string;
  @Input() name?: string;
  @Input() avatarUrl?: string;
}

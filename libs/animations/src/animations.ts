import { animate, style, transition, trigger } from '@angular/animations';

export const inOutAnimation = trigger('inOutAnimation', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('250ms ease-out', style({ opacity: 1 })),
  ]),
  transition(':leave', [
    style({ opacity: 0 }),
    // animate('100ms ease-in', style({ opacity: 0 })),
  ]),
]);

export const inAnimation = trigger('inOutAnimation', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('250ms ease-out', style({ opacity: 1 })),
  ]),
]);

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { latLng } from 'leaflet';
import { BehaviorSubject, map, retry } from 'rxjs';

export interface CsSiteCustomization {
  project_logo?: string | null;
  project_name?: string | null;
  backend_url?: string | null;
  facebook_url?: string | null;
  twitter_url?: string | null;
  linkedin_url?: string | null;

  default_latitude?: number | null;
  default_longitude?: number | null;

  additional_styles?: string | null;
}

@Injectable({
  providedIn: 'root',
})
export class CsSiteCustomizationService {
  siteCustomization$: BehaviorSubject<CsSiteCustomization | null> =
    new BehaviorSubject<CsSiteCustomization | null>(null);
  additionalStyles$ = this.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.additional_styles ?? ''),
  );

  siteCustomizationReady$ = this.siteCustomization$.pipe(
    map((siteCustomization) => !!siteCustomization),
  );

  constructor(private httpClient: HttpClient) {
    this.getSiteCustomization()
      .pipe(retry({ count: 3, delay: 1000 }))
      .subscribe((res) => {
        this.siteCustomization$.next(res);
      });
  }

  getSiteCustomization() {
    return this.httpClient.get<CsSiteCustomization>('/api/cs/settings/', {
      headers: {
        'X-Skip-Token-Interceptor': 'true',
      },
    });
  }

  defaultLocation$ = this.siteCustomization$.pipe(
    map((siteCustomization) => {
      if (siteCustomization === null) {
        return null;
      }
      if (
        siteCustomization.default_latitude === null ||
        siteCustomization.default_longitude === null
      ) {
        return null;
      }

      if (
        siteCustomization.default_latitude === undefined ||
        siteCustomization.default_longitude === undefined
      ) {
        return null;
      }

      return latLng(
        siteCustomization.default_latitude,
        siteCustomization.default_longitude,
      );
    }),
  );
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PostNavigationScrollToTopStateService {
  enabled = true;
}

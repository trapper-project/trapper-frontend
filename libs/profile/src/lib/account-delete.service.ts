import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AccountDeleteService extends BaseBackendConnector<void, void> {
  endpoint = '/api/accounts/delete-user-account/';

  constructor() {
    super();
  }

  deleteAccount(): Observable<void> {
    return this.delete(this.endpoint);
  }
}

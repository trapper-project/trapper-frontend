import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';

import { Observable } from 'rxjs';

export interface ProfileUpdateModel {
  email: string;
  username: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  institution: string;
  about_me: string;
}

export interface ProfileCurrentValueModel {
  email: string;
  username: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  institution: string;
  about_me: string;
  gdpr: boolean;
  tos: boolean;
  gdpr_accepted_date: string;
  tos_accepted_date: string;
}

@Injectable()
export class ProfileUpdateService extends BaseBackendFormConnector<
  ModelFormGroupControls<ProfileCurrentValueModel>
> {
  endpoint = '/api/accounts/profile/';
  constructor(fb: NonNullableFormBuilder) {
    super(
      fb.group({
        username: fb.control(''),
        first_name: fb.control(''),
        last_name: fb.control(''),
        phone_number: fb.control(''),
        about_me: fb.control(''),
        institution: fb.control(''),
        email: fb.control({ value: '', disabled: true }),
        gdpr: fb.control({ value: false, disabled: true }),
        tos: fb.control({ value: false, disabled: true }),
        gdpr_accepted_date: fb.control({ value: '', disabled: true }),
        tos_accepted_date: fb.control({ value: '', disabled: true }),
      }),
    );
  }

  updateProfile(): Observable<void> {
    return this.performUpdate();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileAuthenticatorListComponent } from './profile-authenticator-list.component';

describe('ProfileAuthenticatorListComponent', () => {
  let component: ProfileAuthenticatorListComponent;
  let fixture: ComponentFixture<ProfileAuthenticatorListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ProfileAuthenticatorListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileAuthenticatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

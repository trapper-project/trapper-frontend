import { Component, Input, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import {
  MfaSetupService,
  RecoveryCodesAuthenticator,
  TotpAuthenticator,
} from '../mfa-setup.service';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoService,
} from '@jsverse/transloco';
import {
  AlertComponent,
  ButtonComponent,
  FormFieldComponent,
  LabelComponent,
  SpinnerComponent,
  TextInputComponent,
} from '@trapper/ui';
import { AllauthService } from '@trapper/backend';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatestWith, map, take } from 'rxjs';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ToastrService } from 'ngx-toastr';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { FormsModule } from '@angular/forms';

@UntilDestroy()
@Component({
  selector: 'trapper-cs-profile-authenticator-list',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoModule,
    ButtonComponent,
    NgxSkeletonLoaderModule,
    FormFieldComponent,
    TextInputComponent,
    LabelComponent,
    QRCodeModule,
    SpinnerComponent,
    FormsModule,
    AlertComponent,
  ],
  templateUrl: './profile-authenticator-list.component.html',
  styleUrl: './profile-authenticator-list.component.scss',
})
export class ProfileAuthenticatorListComponent implements OnInit {
  allauthService = inject(AllauthService);
  mfaSetupService = inject(MfaSetupService);
  translocoService = inject(TranslocoService);
  toastrService = inject(ToastrService);
  scope = inject(TRANSLOCO_SCOPE);
  siteCustomizationService = inject(CsSiteCustomizationService);
  currentProfileService = inject(CurrentUserProfileService);

  inProgress$ = this.mfaSetupService.inProgress$;

  configReady$ = this.allauthService.config$.pipe(map((config) => !!config));

  totpAuthenticator: TotpAuthenticator | null = null;
  recoveryCodesAuthenticator: RecoveryCodesAuthenticator | null = null;

  isTotpSupported = false;
  isRecoveryCodesSupported = false;

  recoveryCodesVisible = false;

  recoveryCodes$ = this.mfaSetupService.recoveryCodes$;

  totpSetupData$ = this.mfaSetupService.totpSetupData$;
  totpSetupErrors$ = this.mfaSetupService.totpSetupErrors$;

  totpSetupInProgress = false;

  enteredCode = '';

  totpUri$ = this.totpSetupData$.pipe(
    combineLatestWith(
      this.siteCustomizationService.siteCustomization$,
      this.currentProfileService.data$,
    ),
    map(([data, customization, currentProfile]) => {
      if (!data || !currentProfile) {
        return null;
      }
      let uri = 'otpauth://totp/';
      uri += encodeURIComponent(customization?.project_name ?? 'Trapper');
      uri += ':' + encodeURIComponent(currentProfile.email);
      uri += '?secret=' + data.secret;
      uri +=
        '&issuer=' +
        encodeURIComponent(customization?.project_name ?? 'Trapper');
      return uri;
    }),
  );

  @Input() set supportedTypes(supportedTypes: string[]) {
    this.isTotpSupported = supportedTypes.includes('totp');
    this.isRecoveryCodesSupported = supportedTypes.includes('recovery_codes');
  }

  ngOnInit(): void {
    this.allauthService.reloadConfig();
    this.allauthService.config$
      .pipe(untilDestroyed(this))
      .subscribe((config) => {
        this.isTotpSupported =
          config?.mfa.supported_types?.includes('totp') ?? false;
        this.isRecoveryCodesSupported =
          config?.mfa.supported_types?.includes('recovery_codes') ?? false;
      });

    this.mfaSetupService.authenticators$
      .pipe(untilDestroyed(this))
      .subscribe((authenticators) => {
        this.recoveryCodesVisible = false;
        this.totpAuthenticator =
          (authenticators?.find(
            (auth) => auth.type === 'totp',
          ) as TotpAuthenticator) || null;
        this.recoveryCodesAuthenticator =
          (authenticators?.find(
            (auth) => auth.type === 'recovery_codes',
          ) as RecoveryCodesAuthenticator) || null;
      });
  }

  showRecoveryCodes(): void {
    this.recoveryCodesVisible = true;
    this.mfaSetupService.fetchSensitiveRecoveryCodes().subscribe();
  }

  regenerateRecoveryCodes(): void {
    this.recoveryCodesVisible = true;
    this.mfaSetupService.regenerateRecoveryCodes().subscribe(() => {
      this.translocoService
        .selectTranslate(
          'profile.mfa.recoveryCodes.regenerated',
          {},
          this.scope,
        )
        .pipe(take(1))
        .subscribe((message) => {
          this.toastrService.success(message);
        });
    });
  }

  startTotpSetup(): void {
    this.totpSetupInProgress = true;
    this.mfaSetupService.fetchTotpSetupData().subscribe();
  }

  setupTotp(): void {
    this.mfaSetupService.setupTotp(this.enteredCode).subscribe(() => {
      this.enteredCode = '';
      this.totpSetupInProgress = false;
    });
  }

  deactivateTotp(): void {
    this.mfaSetupService.deactivateTotp().subscribe();
  }
}

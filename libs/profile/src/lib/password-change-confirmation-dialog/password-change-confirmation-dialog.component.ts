import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';
import { TranslocoModule } from '@jsverse/transloco';
import { csProfileTranslocoScope } from '../transloco.loader';

@Component({
  selector: 'trapper-cs-password-change-confirmation-dialog',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    DialogModule,
    TranslocoModule,
  ],
  templateUrl: './password-change-confirmation-dialog.component.html',
  styleUrls: ['./password-change-confirmation-dialog.component.scss'],
  providers: [csProfileTranslocoScope],
})
export class PasswordChangeConfirmationDialogComponent {
  constructor(private dialogRef: DialogRef) {}

  ok() {
    this.dialogRef.close();
  }
}

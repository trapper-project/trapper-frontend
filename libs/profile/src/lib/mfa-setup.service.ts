import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TRANSLOCO_SCOPE, TranslocoService } from '@jsverse/transloco';
import { ToastrService } from 'ngx-toastr';
import {
  BehaviorSubject,
  NEVER,
  catchError,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs';

export interface AllauthResponse<T> {
  data: T;
  status: number;
}

export interface TotpAuthenticator {
  type: 'totp';
  created_at: number;
  last_used_at: number;
}

export interface RecoveryCodesAuthenticator {
  type: 'recovery_codes';
  created_at: number;
  last_used_at: number | null;
  total_code_count: number;
  unused_code_count: number;
}

export type Authenticator = TotpAuthenticator | RecoveryCodesAuthenticator;

export type AllauthAccountAuthenticatorsResponse = AllauthResponse<
  Authenticator[]
>;

export interface TotpSetupData {
  secret: string;
}

export interface SensitiveRecoveryCodesAuthenticator {
  type: 'recovery_codes';
  created_at: number;
  last_used_at: number | null;
  total_code_count: number;
  unused_code_count: number;
  unused_codes: string[];
}

export type SensitiveRecoveryCodesAuthenticatorResponse =
  AllauthResponse<SensitiveRecoveryCodesAuthenticator>;

@Injectable()
export class MfaSetupService {
  private httpClient = inject(HttpClient);
  private toastrService = inject(ToastrService);
  private router = inject(Router);
  private translocoService = inject(TranslocoService);
  private translocoScope = inject(TRANSLOCO_SCOPE);
  authenticators: Authenticator[] | null = null;
  authenticators$ = new BehaviorSubject<Authenticator[] | null>(null);
  recoveryCodes$ = new BehaviorSubject<string[] | null>(null);

  totpSetupData$ = new BehaviorSubject<TotpSetupData | null>(null);
  totpSetupErrors$ = new BehaviorSubject<string[] | null>(null);

  inProgress$ = new BehaviorSubject<boolean>(false);

  BASE_URL = '/_allauth/browser/v1';

  ACCOUNT_AUTHENTICATORS = this.BASE_URL + '/account/authenticators';
  RECOVERY_CODES = this.BASE_URL + '/account/authenticators/recovery-codes';

  reloadAuthenticators() {
    return this.httpClient
      .get<AllauthAccountAuthenticatorsResponse>(this.ACCOUNT_AUTHENTICATORS)
      .pipe(
        catchError((err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.handleErrorResponse(err);
              return NEVER;
            }
          }
          this.translocoService
            .selectTranslate('profile.mfa.fetchError', {}, this.translocoScope)
            .pipe(take(1))
            .subscribe((message) => {
              this.toastrService.error(message);
            });
          return NEVER;
        }),
        tap((res) => {
          this.authenticators = res.data;
          this.authenticators$.next(res.data);
        }),
      );
  }

  handleErrorResponse(err: HttpErrorResponse) {
    const flows: [{ id: string }] = err.error.data.flows || [];
    const flowIds = flows.map((flow) => flow.id);

    for (const flowId of flowIds) {
      if (flowId === 'reauthenticate') {
        this.router.navigate(['/cs/accounts/flows/reauthenticate'], {
          queryParams: { next: this.router.url, flows: flowIds },
        });
        return;
      }

      if (flowId === 'mfa_authenticate') {
        this.router.navigate(['/cs/accounts/flows/mfa'], {
          queryParams: {
            next: this.router.url,
            flows: flowIds,
            reauthenticate: true,
          },
        });
        return;
      }
    }
    this.router.navigate(['/cs/accounts/login'], {
      queryParams: { next: this.router.url },
    });
  }

  fetchSensitiveRecoveryCodes() {
    this.recoveryCodes$.next(null);
    return this.httpClient
      .get<SensitiveRecoveryCodesAuthenticatorResponse>(this.RECOVERY_CODES)
      .pipe(
        catchError((err) => {
          this.recoveryCodes$.next(null);
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.handleErrorResponse(err);
              return NEVER;
            }

            if (err.status === 400 && err.error.message) {
              this.toastrService.error(err.error.message);
              return NEVER;
            }
          }
          this.translocoService
            .selectTranslate(
              'profile.mfa.recoveryCodes.fetchError',
              {},
              this.translocoScope,
            )
            .pipe(take(1))
            .subscribe((message) => {
              this.toastrService.error(message);
            });
          return NEVER;
        }),
        map((res) => {
          this.recoveryCodes$.next(res.data.unused_codes);
        }),
      );
  }

  regenerateRecoveryCodes() {
    return this.httpClient.post(this.RECOVERY_CODES, {}).pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.handleErrorResponse(err);
            return NEVER;
          }

          if (err.status === 400 && err.error.message) {
            this.toastrService.error(err.error.message);
            return NEVER;
          }
        }
        this.translocoService
          .selectTranslate(
            'profile.mfa.recoveryCodes.regenerationError',
            {},
            this.translocoScope,
          )
          .pipe(take(1))
          .subscribe((message) => {
            this.toastrService.error(message);
          });
        return NEVER;
      }),
      switchMap(() => this.fetchSensitiveRecoveryCodes()),
    );
  }

  deactivateTotp() {
    this.inProgress$.next(true);
    return this.httpClient.delete(this.ACCOUNT_AUTHENTICATORS + '/totp').pipe(
      catchError((err) => {
        this.inProgress$.next(false);
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.handleErrorResponse(err);
            return NEVER;
          }
        }
        this.translocoService
          .selectTranslate(
            'profile.mfa.totp.deactivate.error',
            {},
            this.translocoScope,
          )
          .pipe(take(1))
          .subscribe((message) => {
            this.toastrService.error(message);
          });
        return NEVER;
      }),
      switchMap(() => this.reloadAuthenticators()),
      tap(() => {
        this.inProgress$.next(false);
        this.translocoService
          .selectTranslate(
            'profile.mfa.totp.deactivated',
            {},
            this.translocoScope,
          )
          .pipe(take(1))
          .subscribe((message) => {
            this.toastrService.success(message);
          });
      }),
    );
  }

  fetchTotpSetupData() {
    this.totpSetupData$.next(null);
    return this.httpClient.get(this.ACCOUNT_AUTHENTICATORS + '/totp').pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.handleErrorResponse(err);
            return NEVER;
          }

          if (err.status === 404) {
            this.totpSetupData$.next(err.error.meta);
            return NEVER;
          }
        }
        this.translocoService
          .selectTranslate(
            'profile.mfa.totp.setup.error',
            {},
            this.translocoScope,
          )
          .pipe(take(1))
          .subscribe((message) => {
            this.toastrService.error(message);
          });
        return NEVER;
      }),
      switchMap(() => {
        return this.reloadAuthenticators();
      }),
    );
  }

  setupTotp(code: string) {
    this.inProgress$.next(true);
    return this.httpClient
      .post(this.ACCOUNT_AUTHENTICATORS + '/totp', { code })
      .pipe(
        catchError((err) => {
          this.inProgress$.next(false);
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.handleErrorResponse(err);
              return NEVER;
            }

            if (err.status === 400) {
              const errors: { message: string }[] = err.error.errors;
              this.totpSetupErrors$.next(errors.map((error) => error.message));
              return NEVER;
            }
          }
          this.translocoService
            .selectTranslate(
              'profile.mfa.totp.setup.error',
              {},
              this.translocoScope,
            )
            .pipe(take(1))
            .subscribe((message) => {
              this.toastrService.error(message);
            });
          return NEVER;
        }),
        switchMap(() => this.reloadAuthenticators()),
        tap(() => {
          this.inProgress$.next(false);
          this.totpSetupData$.next(null);
          this.translocoService
            .selectTranslate(
              'profile.mfa.totp.setup.success',
              {},
              this.translocoScope,
            )
            .pipe(take(1))
            .subscribe((message) => {
              this.toastrService.success(message);
            });
        }),
      );
  }
}

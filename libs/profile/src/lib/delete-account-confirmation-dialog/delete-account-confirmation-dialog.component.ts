import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  SimpleMessageComponent,
  TextInputComponent,
} from '@trapper/ui';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';
import { TranslocoModule } from '@jsverse/transloco';
import { csProfileTranslocoScope } from '../transloco.loader';
import { FormsModule } from '@angular/forms';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { tap } from 'rxjs';

@Component({
  selector: 'trapper-cs-delete-account-confirmation-dialog',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    DialogModule,
    TranslocoModule,
    TextInputComponent,
    FormsModule,
  ],
  templateUrl: './delete-account-confirmation-dialog.component.html',
  styleUrls: ['./delete-account-confirmation-dialog.component.scss'],
  providers: [csProfileTranslocoScope],
})
export class DeleteAccountConfirmationDialogComponent implements OnInit {
  constructor(
    private dialogRef: DialogRef,
    private currentUserProfileService: CurrentUserProfileService,
  ) {}

  ngOnInit() {
    this.currentUserProfileService.data$
      .pipe(
        tap((profile) => {
          this.userEmailAddress = profile?.email;
        }),
      )
      .subscribe();
  }

  typedEmail = '';
  userEmailAddress?: string;

  ok() {
    if (this.typedEmail.length === 0) {
      return;
    }

    if (this.typedEmail !== this.userEmailAddress) {
      return;
    }

    this.dialogRef.close(true);
  }

  cancel() {
    this.dialogRef.close(false);
  }
}

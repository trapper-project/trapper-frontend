import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  FormColumnComponent,
  FormFieldComponent,
  FormFieldHintComponent,
  LabelComponent,
  ServerErrorsComponent,
  TextInputComponent,
  YesNoFlagComponent,
} from '@trapper/ui';
import { PasswordChangeConfirmationDialogComponent } from '../password-change-confirmation-dialog/password-change-confirmation-dialog.component';
import { Overlay } from '@angular/cdk/overlay';
import { ProfileUpdateService } from '../profile-update.service';
import { ReactiveFormsModule } from '@angular/forms';
import { map, switchMap, tap } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { ToastrService } from 'ngx-toastr';
import { AvatarChangeDialogComponent } from '../avatar-change-dialog/avatar-change-dialog.component';
import { PasswordChangeService } from '../password-change.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GdprPopupComponent, TosPopupComponent } from '@trapper/cs-consents';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { csProfileTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DeleteAccountConfirmationDialogComponent } from '../delete-account-confirmation-dialog/delete-account-confirmation-dialog.component';
import { AccountDeleteService } from '../account-delete.service';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MfaSetupService } from '../mfa-setup.service';
import { ProfileAuthenticatorListComponent } from '../profile-authenticator-list/profile-authenticator-list.component';
@UntilDestroy()
@Component({
  selector: 'trapper-cs-cs-profile',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    FormColumnComponent,
    TextInputComponent,
    CheckboxComponent,
    ButtonComponent,
    DialogModule,
    ReactiveFormsModule,
    MatIconModule,
    FormFieldComponent,
    ServerErrorsComponent,
    LabelComponent,
    YesNoFlagComponent,
    FormFieldHintComponent,
    TranslocoModule,
    NgxSkeletonLoaderModule,
    ProfileAuthenticatorListComponent,
  ],
  templateUrl: './cs-profile.component.html',
  styleUrls: ['./cs-profile.component.scss'],
  providers: [ProfileUpdateService, MfaSetupService, csProfileTranslocoScope],
})
export class CsProfileComponent implements OnInit {
  formGroup = this.profileUpdateService.formGroup;

  changePasswordFormGroup = this.passwordChangeService.formGroup;

  currentAvatar$ = this.currentUserProfileService.data$.pipe(
    map((data) => data?.avatar || null),
    map((url) => (url ? 'url(' + url + ')' : null)),
  );

  authenticators$ = this.mfaSetupService.authenticators$;

  constructor(
    private dialog: Dialog,
    private overlay: Overlay,
    public profileUpdateService: ProfileUpdateService,
    private changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private currentUserProfileService: CurrentUserProfileService,
    public passwordChangeService: PasswordChangeService,
    public accountDeleteService: AccountDeleteService,
    private route: ActivatedRoute,
    private router: Router,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    private mfaSetupService: MfaSetupService,
    @Inject(TRANSLOCO_SCOPE) private scope: TranslocoScope,
  ) {}

  updateProfile() {
    this.profileUpdateService
      .updateProfile()
      .pipe(
        switchMap(() =>
          this.translocoService.selectTranslate(
            'csProfile.update.success',
            {},
            this.scope,
          ),
        ),
        tap((message) => this.toastr.success(message)),
        switchMap(() => this.currentUserProfileService.reloadUserProfile()),
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        tap(() => {
          this.profileUpdateService.clear();
          this.passwordChangeService.clear();
        }),
      )
      .subscribe();
    this.currentUserProfileService.reloadUserProfile().subscribe();
    this.profileUpdateService.fetchCurrentValue().subscribe();
    this.mfaSetupService.reloadAuthenticators().subscribe();

    this.translocoService
      .selectTranslate('csProfile.title', {}, this.scope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => this.pageTitleService.setSectionTitle(title));
  }

  changePassword() {
    this.passwordChangeService
      .performFullUpdate()
      .pipe(tap(() => this.changePasswordConfirmed()))
      .subscribe();
  }

  changePasswordConfirmed() {
    const dialog = this.dialog.open(PasswordChangeConfirmationDialogComponent, {
      positionStrategy: this.overlay
        .position()
        .global()
        .top('54px')
        .centerHorizontally(),
      disableClose: true,
    });

    dialog.closed
      .pipe(
        tap(() => this.changePasswordFormGroup.reset()),
        tap(() => this.changeDetectorRef.detectChanges()),
      )
      .subscribe();
  }

  deleteAccount() {
    const dialog = this.dialog.open<boolean>(
      DeleteAccountConfirmationDialogComponent,
      {
        positionStrategy: this.overlay
          .position()
          .global()
          .centerHorizontally()
          .centerVertically(),
      },
    );

    dialog.closed
      .pipe(tap((result) => this.handleDeleteAccount(result === true)))
      .subscribe();
  }

  handleDeleteAccount(result: boolean) {
    if (!result) {
      return;
    }
    this.accountDeleteService
      .deleteAccount()
      .pipe(
        switchMap(() =>
          this.translocoService.selectTranslate(
            'csProfile.delete.success',
            {},
            this.scope,
          ),
        ),
        tap((message) => this.toastr.success(message)),
      )
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

  changeAvatar() {
    this.dialog.open(AvatarChangeDialogComponent, {
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    });
  }

  tosPopup() {
    this.dialog.open(TosPopupComponent, {
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    });
  }

  gdprPopup() {
    this.dialog.open(GdprPopupComponent);
  }
}

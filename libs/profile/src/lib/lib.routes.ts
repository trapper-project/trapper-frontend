import { Route } from '@angular/router';
import { CsProfileComponent } from './cs-profile/cs-profile.component';

export const csProfileRoutes: Route[] = [
  { path: '', component: CsProfileComponent },
];

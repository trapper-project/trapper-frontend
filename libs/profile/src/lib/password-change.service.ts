import { Injectable } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import {
  BaseBackendFormConnector,
  ModelFormGroupControls,
} from '@trapper/backend';

export interface PasswordChangePayload {
  old_password: string;
  new_password: string;
  new_password2: string;
}

@Injectable({ providedIn: 'root' })
export class PasswordChangeService extends BaseBackendFormConnector<
  ModelFormGroupControls<PasswordChangePayload>
> {
  endpoint = '/api/accounts/change-password/';

  constructor(fb: NonNullableFormBuilder) {
    super(
      fb.group({
        old_password: fb.control(''),
        new_password: fb.control(''),
        new_password2: fb.control(''),
      }),
    );
  }
}

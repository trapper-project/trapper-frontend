import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  ServerErrorsComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { AvatarChangeService } from './avatar-change.service';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { BehaviorSubject, map, Observable, switchMap, tap } from 'rxjs';
import { DialogRef } from '@angular/cdk/dialog';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { ToastrService } from 'ngx-toastr';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { v4 as uuidv4 } from 'uuid';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { csProfileTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';

function dataUrlToBlobFile(dataUrl: string): File | null {
  const BASE64_MARKER = ';base64,';
  if (dataUrl.indexOf(BASE64_MARKER) !== -1) {
    const parts = dataUrl.split(BASE64_MARKER);
    const contentType = parts[0].split(':')[1];
    // const raw = decodeURIComponent(parts[1]);
    const byteCharacters = atob(parts[1]);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const raw = new Uint8Array(byteNumbers);

    const blob = new Blob([raw], { type: contentType });

    return new File([blob], `avatar_${uuidv4()}.jpg`, {
      type: contentType,
    });
  }
  return null;
}

@UntilDestroy()
@Component({
  selector: 'trapper-cs-avatar-change-dialog',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    ButtonComponent,
    ReactiveFormsModule,
    MatIconModule,
    ImageCropperComponent,
    ServerErrorsComponent,
    TranslocoModule,
  ],
  templateUrl: './avatar-change-dialog.component.html',
  styleUrls: ['./avatar-change-dialog.component.scss'],
  providers: [AvatarChangeService, csProfileTranslocoScope],
})
export class AvatarChangeDialogComponent implements OnInit {
  formGroup = this.fb.group({
    avatar: this.fb.control<Blob | null>(null),
  });
  selectedFiles: FileList | null = null;

  currentAvatar$: Observable<string | null>;
  inProgress$: Observable<boolean>;

  imageChangedEvent?: Event = undefined;
  croppedImage = '';

  avatarFormErrors$ = new BehaviorSubject<string[] | null>(null);

  fileChangeEvent(event: Event): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64 || '';
  }

  constructor(
    public avatarChangeService: AvatarChangeService,
    public currentUserProfileService: CurrentUserProfileService,
    private fb: NonNullableFormBuilder,
    private dialogRef: DialogRef,
    private toastrService: ToastrService,
  ) {
    this.currentAvatar$ = this.currentUserProfileService.data$.pipe(
      map((profile) => profile?.avatar || null),
    );
    this.inProgress$ = this.avatarChangeService.inProgress$;
  }

  ngOnInit(): void {
    this.currentUserProfileService
      .reloadUserProfile()
      .pipe(untilDestroyed(this))
      .subscribe();
    this.avatarChangeService.errors$
      .pipe(
        tap((error) => {
          if (error) {
            if (error.avatar) {
              // this.toastrService.error(error.avatar.join(' '));
              this.avatarFormErrors$.next(error.avatar);
            } else if (error.non_field_errors) {
              this.toastrService.error(error.non_field_errors.join(' '));
            } else if (error.detail) {
              this.toastrService.error(error.detail);
            }
          }
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }

  selectFile(event: Event): void {
    this.imageChangedEvent = event;
    this.avatarFormErrors$.next(null);
  }

  changeAvatar() {
    if (this.croppedImage) {
      const blob = dataUrlToBlobFile(this.croppedImage);
      if (!blob) {
        return;
      }
      this.avatarChangeService
        .changeAvatar({ avatar: blob })
        .pipe(
          switchMap(() => this.currentUserProfileService.reloadUserProfile()),
          tap(() => this.dialogRef.close()),
        )
        .subscribe();
    }
  }

  clearAvatar() {
    this.avatarChangeService
      .clearAvatar()
      .pipe(
        switchMap(() => this.currentUserProfileService.reloadUserProfile()),
        tap(() => this.dialogRef.close()),
      )
      .subscribe();
  }

  cancel() {
    this.dialogRef.close();
  }
}

import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

export interface AvatarPayload {
  avatar: Blob | null;
}

@Injectable()
export class AvatarChangeService extends BaseBackendConnector<
  AvatarPayload,
  unknown
> {
  changeAvatar(payload: AvatarPayload): Observable<void> {
    return this.patch('/api/accounts/profile/', payload, {
      sendAsForm: true,
      sendNulls: true,
    });
  }

  clearAvatar(): Observable<void> {
    return this.patch('/api/accounts/profile/', { avatar: null });
  }
}

import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable, ReplaySubject, filter } from 'rxjs';

interface RecentProjectUpdatePayload {
  default_cs_project?: string | null;
}

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class RecentProjectService extends BaseBackendConnector<
  RecentProjectUpdatePayload,
  unknown
> {
  currentActiveProject$ = new ReplaySubject<string | null>(1);
  constructor(private currentUserProfileService: CurrentUserProfileService) {
    super();
    this.currentUserProfileService.data$
      .pipe(
        filter((data) => !!data),
        untilDestroyed(this),
      )
      .subscribe((data) => {
        this.currentActiveProject$.next(data?.default_cs_project || null);
      });
  }

  reloadRecentProject() {
    this.currentUserProfileService.reloadUserProfile().subscribe();
  }

  updateBackendRecentProject(
    payload: Partial<RecentProjectUpdatePayload>,
  ): Observable<void> {
    return this.put('/api/accounts/set-default-cs-project/', payload);
  }

  updateRecentProject(project: string) {
    this.currentActiveProject$.next(project);
    this.updateBackendRecentProject({
      default_cs_project: project,
    }).subscribe(() => {
      this.currentActiveProject$.next(project);
      this.reloadRecentProject();
    });
  }
}

import { Injectable } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class LogoutService {
  constructor(private httpClient: HttpClient) {}
  logout(): Observable<void> {
    return this.httpClient.post<void>('/api/accounts/logout/', {});
  }
}

import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { catchError, NEVER, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class UnauthorizedInterceptor {
  constructor(private router: Router) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (
          [401].includes(err.status) &&
          !request.url.startsWith('/_allauth')
        ) {
          return this.handleUnauthorized();
        }

        return throwError(() => err);
      }),
    );
  }

  handleUnauthorized(): Observable<HttpEvent<unknown>> {
    if (this.router.url === '/cs/accounts/login') {
      return NEVER;
    }
    const queryParams: { redirect?: string } = {};
    if (this.router.url !== '/') {
      queryParams.redirect = this.router.url;
    }
    this.router.navigate(['/cs/accounts/login'], {
      queryParams,
    });

    return NEVER;
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
import { inject } from '@angular/core';
import { BehaviorSubject, catchError, map, NEVER, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DrfError } from './drf-error';
import { nonFieldError$ } from './field-error-extractor';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UntilDestroy } from '@ngneat/until-destroy';

export interface ConnectorOptions {
  sendAsForm?: boolean;
  sendNulls?: boolean;
  clearPreviousData?: boolean;
}

export type FormValue<T extends AbstractControl> =
  T extends AbstractControl<infer TValue, any> ? TValue : never;
export type FormRawValue<T extends AbstractControl> =
  T extends AbstractControl<any, infer TRawValue> ? TRawValue : never;

export interface DateRange {
  start?: Date | null;
  end?: Date | null;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ModelFormGroupControls<
  T extends Record<string, any>,
  U = DateRange | null,
> = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [K in keyof T]: T[K] extends U
    ? FormControl<T[K]>
    : T[K] extends Record<any, any>
      ? FormGroup<ModelFormGroupControls<T[K]>>
      : FormControl<T[K]>;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ModelFormGroup<T extends Record<string, any>> = FormGroup<
  ModelFormGroupControls<T>
>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function setFormErrors(formGroup: FormGroup, errors: any) {
  Object.keys(errors).forEach((key) => {
    const control = formGroup.get(key);
    const value: unknown = errors[key];
    if (!control || !value) {
      return;
    }

    if (control instanceof FormGroup) {
      setFormErrors(control, value);
    } else {
      control.setErrors({ serverErrors: value });
    }
  });
}

@UntilDestroy()
export abstract class BaseBackendFormConnector<
  TControl extends {
    [K in keyof TControl]: AbstractControl<any>;
  },
  TAdditionalData = Record<string, never>,
> {
  abstract endpoint: string;
  formGroup: FormGroup<TControl>;
  formInitialValue?: FormRawValue<FormGroup<TControl>>;
  inProgress$: Observable<boolean>;
  data$: Observable<
    (FormRawValue<FormGroup<TControl>> & TAdditionalData) | null
  >;
  nonFieldErrors$: Observable<string[] | null>;

  private _inProgress$ = new BehaviorSubject(false);
  private _errors$ = new BehaviorSubject<null | DrfError<
    Record<string, unknown>
  >>(null);
  private _data$ = new BehaviorSubject<
    (FormRawValue<FormGroup<TControl>> & TAdditionalData) | null
  >(null);

  private httpClient: HttpClient = inject(HttpClient);
  protected toastrService: ToastrService = inject(ToastrService);

  constructor(formGroup: FormGroup<TControl>) {
    this.inProgress$ = this._inProgress$.asObservable();
    this.data$ = this._data$.asObservable();
    this.nonFieldErrors$ = nonFieldError$(this._errors$);
    this.formGroup = formGroup;
    this.formInitialValue = this.formGroup.getRawValue();
  }

  private setFormErrors(err: DrfError<Record<string, unknown>>) {
    setFormErrors(this.formGroup, err);
  }

  private toFormData(
    formValue: Record<string, Blob | string | null | undefined>,
    options?: ConnectorOptions,
  ) {
    const formData = new FormData();

    const keys = Object.keys(formValue);

    for (const key of keys) {
      const value = formValue[key];
      if (!options?.sendNulls && (value === null || value === undefined)) {
        continue;
      }
      if (value instanceof Array) {
        value.forEach((element) => {
          formData.append(key, element);
        });
        continue;
      }
      formData.append(key, value || '');
    }

    return formData;
  }

  protected get payload() {
    return this.formGroup.value;
  }

  clear() {
    this._data$.next(null);
    this._errors$.next(null);
    if (this.formInitialValue) {
      this.formGroup.setValue(this.formInitialValue);
    }
  }

  patchFormValue(value: FormRawValue<FormGroup<TControl>>) {
    this.formGroup.patchValue(value);
  }

  wrapRequest(
    fn: () => Observable<FormRawValue<FormGroup<TControl>> & TAdditionalData>,
    options?: ConnectorOptions,
  ) {
    this._inProgress$.next(true);
    this._errors$.next(null);
    this.formGroup.markAsPristine();
    if (options?.clearPreviousData) {
      this._data$.next(null);
    }
    return fn().pipe(
      map((data) => {
        this._inProgress$.next(false);
        this._data$.next(data);
        this.patchFormValue(data);
      }),
      catchError((err) => {
        this._inProgress$.next(false);
        if (err instanceof HttpErrorResponse) {
          if (!err.error || err.status == 500) {
            this.toastrService.error(
              'An unexpected error occurred. Please try again later.',
            );
            return NEVER;
          }
          this.setFormErrors(err.error);
          this._errors$.next(err.error);
          if (err.error.detail) {
            this.toastrService.error(err.error.detail);
          }
        } else {
          this.toastrService.error(
            'An unexpected error occurred. Please try again later.',
          );
          return NEVER;
        }
        return NEVER;
      }),
    );
  }

  private preparePayload(options?: ConnectorOptions) {
    return options?.sendAsForm
      ? this.toFormData(
          this.payload as Record<string, Blob | string | null | undefined>,
          options,
        )
      : this.payload;
  }

  post(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.post<
          FormRawValue<FormGroup<TControl>> & TAdditionalData
        >(endpointUrl, this.preparePayload(options)),
      options,
    );
  }

  put(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.put<
          FormRawValue<FormGroup<TControl>> & TAdditionalData
        >(endpointUrl, this.preparePayload(options)),
      options,
    );
  }

  patch(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.patch<
          FormRawValue<FormGroup<TControl>> & TAdditionalData
        >(endpointUrl, this.preparePayload(options)),
      options,
    );
  }

  get(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.get<
          FormRawValue<FormGroup<TControl>> & TAdditionalData
        >(endpointUrl),
      options,
    );
  }

  performUpdate(options?: ConnectorOptions): Observable<void> {
    return this.patch(this.endpoint, options);
  }

  fetchCurrentValue(options?: ConnectorOptions): Observable<void> {
    return this.get(this.endpoint, options);
  }

  performCreate(options?: ConnectorOptions): Observable<void> {
    return this.post(this.endpoint, options);
  }

  performFullUpdate(options?: ConnectorOptions): Observable<void> {
    return this.put(this.endpoint, options);
  }
}

import { inject } from '@angular/core';
import {
  BehaviorSubject,
  catchError,
  map,
  NEVER,
  Observable,
  Subject,
} from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DrfError } from './drf-error';
import { extractFieldError$, nonFieldError$ } from './field-error-extractor';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { setFormErrors } from './base-backend-form-connector';

export interface ConnectorOptions {
  sendAsForm?: boolean;
  sendNulls?: boolean;
  clearPreviousData?: boolean;
}

export class BaseBackendConnector<T, U> {
  inProgress$: Observable<boolean>;
  errors$: Observable<null | DrfError<T>>;
  data$: Observable<U | null>;
  nonFieldErrors$: Observable<string[] | null>;
  notFound$: Observable<void>;

  private _inProgress$ = new BehaviorSubject(false);
  private _errors$ = new BehaviorSubject<null | DrfError<T>>(null);
  private _notFound$ = new Subject<void>();
  private _data$ = new BehaviorSubject<U | null>(null);

  private httpClient: HttpClient = inject(HttpClient);
  protected toastrService: ToastrService = inject(ToastrService);

  private _formGroup?: FormGroup;

  constructor() {
    this.inProgress$ = this._inProgress$.asObservable();
    this.errors$ = this._errors$.asObservable();
    this.data$ = this._data$.asObservable();
    this.nonFieldErrors$ = nonFieldError$(this.errors$);
    this.notFound$ = this._notFound$.asObservable();
  }

  private toFormData(
    formValue: Record<string, Blob | string | null | undefined>,
    options?: ConnectorOptions,
  ) {
    const formData = new FormData();

    const keys = Object.keys(formValue);

    for (const key of keys) {
      const value = formValue[key];
      if (!options?.sendNulls && (value === null || value === undefined)) {
        continue;
      }
      if (value instanceof Array) {
        value.forEach((element) => {
          formData.append(key, element);
        });
        continue;
      }
      formData.append(key, value || '');
    }

    return formData;
  }

  clear() {
    this._data$.next(null);
    this._errors$.next(null);
  }

  fieldError$(fieldName: keyof T) {
    return extractFieldError$(this.errors$, fieldName);
  }

  wrapRequest(fn: () => Observable<U>, options?: ConnectorOptions) {
    this._inProgress$.next(true);
    this._errors$.next(null);
    if (options?.clearPreviousData) {
      this._data$.next(null);
    }
    if (this._formGroup) {
      this._formGroup.markAsPristine();
    }
    return fn().pipe(
      map((data) => {
        this._inProgress$.next(false);
        this._data$.next(data);
      }),
      catchError((err) => {
        this._inProgress$.next(false);
        if (err instanceof HttpErrorResponse) {
          if (!err.error || err.status == 500) {
            this.toastrService.error(
              'An unexpected error occurred. Please try again later.',
            );
            return NEVER;
          }
          this._errors$.next(err.error);
          if (err.status === 404) {
            this._notFound$.next();
          }
          if (err.error.detail) {
            this.toastrService.error(err.error.detail);
          }
          if (this._formGroup) {
            setFormErrors(this._formGroup, err.error);
          }
        } else {
          this.toastrService.error(
            'An unexpected error occurred. Please try again later.',
          );
          return NEVER;
        }
        return NEVER;
      }),
    );
  }

  post(
    endpointUrl: string,
    payload: T,
    options?: ConnectorOptions,
  ): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.post<U>(
          endpointUrl,
          options?.sendAsForm
            ? this.toFormData(
                payload as Record<string, Blob | string | null | undefined>,
                options,
              )
            : payload,
        ),
      options,
    );
  }

  put(
    endpointUrl: string,
    payload: T,
    options?: ConnectorOptions,
  ): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.put<U>(
          endpointUrl,
          options?.sendAsForm
            ? this.toFormData(
                payload as Record<string, Blob | string | null | undefined>,
                options,
              )
            : payload,
        ),
      options,
    );
  }

  patch(
    endpointUrl: string,
    payload: T,
    options?: ConnectorOptions,
  ): Observable<void> {
    return this.wrapRequest(
      () =>
        this.httpClient.patch<U>(
          endpointUrl,
          options?.sendAsForm
            ? this.toFormData(
                payload as Record<string, Blob | string | null | undefined>,
                options,
              )
            : payload,
        ),
      options,
    );
  }

  get(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(() => this.httpClient.get<U>(endpointUrl), options);
  }

  delete(endpointUrl: string, options?: ConnectorOptions): Observable<void> {
    return this.wrapRequest(
      () => this.httpClient.delete<U>(endpointUrl),
      options,
    );
  }

  setFormGroup(formGroup: FormGroup) {
    this._formGroup = formGroup;
  }
}

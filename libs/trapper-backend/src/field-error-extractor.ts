import { map, Observable } from 'rxjs';
import { DrfError } from './drf-error';

export function extractFieldError$<T>(
  errors$: Observable<DrfError<T> | undefined | null>,
  fieldName: keyof DrfError<T>,
) {
  return errors$.pipe(
    map((errors) => {
      if (!errors) {
        return null;
      }

      if (fieldName === 'detail' && errors.detail) {
        return [errors.detail];
      }
      return errors[fieldName] || null;
    }),
  );
}

export function nonFieldError$(
  error$: Observable<DrfError<unknown> | undefined | null>,
) {
  return extractFieldError$(error$, 'non_field_errors');
}

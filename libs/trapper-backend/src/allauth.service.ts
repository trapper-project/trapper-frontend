import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { DrfError } from './drf-error';
import { ModelFormGroup, setFormErrors } from './base-backend-form-connector';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, NEVER, catchError, map, tap } from 'rxjs';

export interface AllauthConfigAccount {
  authentication_method: string;
  is_open_for_signup: boolean;
}

export interface AllauthConfigSocialAccountProvider {
  id: string;
  name: string;
  flows: string[];
  client_id: string;
}

export interface AllauthConfigMfa {
  supported_types: string[];
}

export interface AllauthConfig {
  account: AllauthConfigAccount;
  socialaccount: {
    providers: AllauthConfigSocialAccountProvider[];
  };
  mfa: AllauthConfigMfa;
}

function processAllauthBadRequest<T extends Record<string, string | null>>(
  err: HttpErrorResponse,
  form: ModelFormGroup<T>,
) {
  console.error(err.error);
  const collectedErrors: Record<string, string[]> = {};
  const formControlNames = Object.keys(form.controls);
  for (const error of err.error.errors) {
    const param = error.param;
    if (param && formControlNames.includes(param)) {
      collectedErrors[param] = [error.message];
    } else {
      if (!collectedErrors['non_field_errors']) {
        collectedErrors['non_field_errors'] = [];
      }
      collectedErrors['non_field_errors'].push(error.message);
    }
  }
  setFormErrors(form, collectedErrors);

  return collectedErrors as DrfError<T>;
}

@Injectable({
  providedIn: 'root',
})
export class AllauthService {
  private httpClient = inject(HttpClient);
  private router = inject(Router);
  private toastrService = inject(ToastrService);
  config: AllauthConfig | null = null;
  config$ = new BehaviorSubject<AllauthConfig | null>(null);

  BASE_URL = '/_allauth/browser/v1';

  SESSION_STATUS = this.BASE_URL + '/auth/session';

  LOGIN = this.BASE_URL + '/auth/login';
  REAUTHENTICATE = this.BASE_URL + '/auth/reauthenticate';

  MFA_AUTHENTICATE = this.BASE_URL + '/auth/2fa/authenticate';
  MFA_REAUTHENTICATE = this.BASE_URL + '/auth/2fa/reauthenticate';

  PROVIDER_SIGNUP = this.BASE_URL + '/auth/provider/signup';
  REDIRECT_TO_PROVIDER = this.BASE_URL + '/auth/provider/redirect';
  PROVIDER_TOKEN = this.BASE_URL + '/auth/provider/token';

  providers$ = this.config$.pipe(
    map((config) => {
      if (!config) {
        return null;
      }
      return config?.socialaccount.providers;
    }),
  );

  loginInProgress$ = new BehaviorSubject<boolean>(false);
  mfaInProgress$ = new BehaviorSubject<boolean>(false);
  reauthenticateInProgress$ = new BehaviorSubject<boolean>(false);

  loginErrors$ = new BehaviorSubject<DrfError<{
    email: string;
    password: string;
  }> | null>(null);

  mfaErrors$ = new BehaviorSubject<DrfError<{
    code: string;
  }> | null>(null);

  reloadConfig() {
    this.config = null;
    this.config$.next(null);

    this.httpClient
      .get<{ data: AllauthConfig }>('/_allauth/browser/v1/config')
      .pipe(map((res) => res.data))
      .subscribe((config) => {
        this.config = config;
        this.config$.next(config);
      });
  }

  private getCSRFTokenFromCookie() {
    return (
      document.cookie
        .split('; ')
        .find((row) => row.startsWith('csrftoken='))
        ?.split('=')[1] ?? ''
    );
  }

  private getPostLoginFullUrl() {
    const origin = window.location.origin;
    const baseHref = document.querySelector('base')?.getAttribute('href') ?? '';
    return origin + baseHref + '/cs/accounts/flows/social-login';
  }

  startSocialLoginFlow(providerId: string) {
    const provider = this.config?.socialaccount.providers.find(
      (p) => p.id === providerId,
    );
    if (!provider) {
      throw new Error('Provider not found');
    }

    const actionUrl = this.REDIRECT_TO_PROVIDER;

    const form = document.createElement('form');
    form.method = 'POST';
    form.action = actionUrl;
    const data: Record<string, string> = {
      provider: providerId,
      process: 'login',
      callback_url: this.getPostLoginFullUrl(),
      csrfmiddlewaretoken: this.getCSRFTokenFromCookie(),
    };
    Object.entries(data).forEach(([k, v]) => {
      const input = document.createElement('input');
      input.type = 'hidden';
      input.name = k;
      input.value = v;
      form.appendChild(input);
    });
    document.body.appendChild(form);
    form.submit();
  }

  startMfaFlow() {
    this.router.navigate(['/cs/accounts/flows/mfa']);
  }

  login(form: ModelFormGroup<{ email: string; password: string }>) {
    form.markAsPristine();
    const email = form.get('email')?.value || '';
    const password = form.get('password')?.value || '';
    this.loginErrors$.next(null);
    this.loginInProgress$.next(true);
    return this.httpClient
      .post(this.LOGIN, {
        email,
        password,
      })
      .pipe(
        tap(() => {
          this.loginInProgress$.next(false);
        }),
        catchError((err) => {
          this.loginInProgress$.next(false);
          if (err instanceof HttpErrorResponse && err.status === 401) {
            for (const flow of err.error.data.flows) {
              if (flow.id === 'mfa_authenticate' && flow.is_pending) {
                this.startMfaFlow();
                return NEVER;
              }
            }
          } else if (err instanceof HttpErrorResponse && err.status === 400) {
            const errors = processAllauthBadRequest(err, form);
            this.loginErrors$.next(errors);
            return NEVER;
          }
          throw err;
        }),
      );
  }

  reauthenticate(form: ModelFormGroup<{ password: string }>) {
    form.markAsPristine();
    const password = form.get('password')?.value || '';
    this.loginErrors$.next(null);
    this.reauthenticateInProgress$.next(true);
    return this.httpClient
      .post(this.REAUTHENTICATE, {
        password,
      })
      .pipe(
        tap(() => {
          this.reauthenticateInProgress$.next(false);
        }),
        catchError((err) => {
          this.reauthenticateInProgress$.next(false);
          if (err instanceof HttpErrorResponse && err.status === 400) {
            const errors = processAllauthBadRequest(err, form);
            this.loginErrors$.next(errors);
            return NEVER;
          }
          throw err;
        }),
      );
  }

  mfaAuthenticate(form: ModelFormGroup<{ code: string | null }>) {
    form.markAsPristine();
    const code = form.get('code')?.value || '';
    this.mfaErrors$.next(null);
    this.mfaInProgress$.next(true);
    return this.httpClient
      .post(this.MFA_AUTHENTICATE, {
        code,
      })
      .pipe(
        tap(() => {
          this.mfaInProgress$.next(false);
        }),
        catchError((err) => {
          this.mfaInProgress$.next(false);
          if (err instanceof HttpErrorResponse && err.status === 400) {
            const errors = processAllauthBadRequest(err, form);
            this.mfaErrors$.next(errors);
            return NEVER;
          }
          throw err;
        }),
      );
  }

  mfaReauthenticate(form: ModelFormGroup<{ code: string | null }>) {
    form.markAsPristine();
    const code = form.get('code')?.value || '';
    this.mfaErrors$.next(null);
    this.reauthenticateInProgress$.next(true);
    return this.httpClient
      .post(this.MFA_REAUTHENTICATE, {
        code,
      })
      .pipe(
        tap(() => {
          this.reauthenticateInProgress$.next(false);
        }),
        catchError((err) => {
          this.reauthenticateInProgress$.next(false);
          if (err instanceof HttpErrorResponse && err.status === 400) {
            const errors = processAllauthBadRequest(err, form);
            this.mfaErrors$.next(errors);
            return NEVER;
          }
          throw err;
        }),
      );
  }

  checkSession() {
    return this.httpClient
      .get(this.SESSION_STATUS)
      .pipe(
        catchError((err) => {
          if (err instanceof HttpErrorResponse && err.status === 410) {
            this.router.navigate(['/cs/accounts/login']);
            return NEVER;
          }

          if (err instanceof HttpErrorResponse && err.status === 401) {
            if (err.error.meta.is_authenticated) {
              // reauthenticaton required
              this.router.navigate(['/cs/accounts/login']);
              return NEVER;
            }
            for (const flow of err.error.data.flows) {
              if (!flow.is_pending) {
                continue;
              }
              if (flow.id === 'mfa_authenticate') {
                this.startMfaFlow();
                return NEVER;
              } else {
                this.toastrService.error(`Unexpected pending flow: ${flow.id}`);
                this.router.navigate(['/cs/accounts/login']);
                return NEVER;
              }
            }
          }
          this.toastrService.error(
            'An unexpected error occurred. Please try again later.',
          );
          this.router.navigate(['/cs/accounts/login']);
          return NEVER;
        }),
      )
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

  activate2fa() {
    return this.httpClient.post(this.MFA_REAUTHENTICATE, {});
  }
}

export type DrfError<T> = {
  [field in keyof T | 'non_field_errors']?: string[] | undefined;
} & {
  detail?: string;
};

export type DrfErrorOrNull<T> = DrfError<T> | null;

import {
  AfterViewInit,
  Component,
  HostBinding,
  NgZone,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationEnd, Router, RouterModule } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { FooterComponent } from '../footer/footer.component';
import { NgScrollbar, NgScrollbarModule } from 'ngx-scrollbar';
import { trapperCsLayoutTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  BehaviorSubject,
  delay,
  distinctUntilChanged,
  map,
  tap,
  filter,
  fromEvent,
} from 'rxjs';
import { CompactModeService, SidebarStateService } from '@trapper/ui';

import { PostNavigationScrollToTopStateService } from '@trapper/post-navigation-scroll-to-top';

@UntilDestroy()
@Component({
  selector: 'trapper-cs-cs-layout',
  standalone: true,
  templateUrl: './cs-layout.component.html',
  styleUrls: ['./cs-layout.component.scss'],
  imports: [
    CommonModule,
    RouterModule,
    HeaderComponent,
    FooterComponent,
    NgScrollbarModule,
    TranslocoModule,
    ScrollingModule,
  ],
  providers: [trapperCsLayoutTranslocoScope],
})
export class CsLayoutComponent implements AfterViewInit {
  @ViewChild(NgScrollbar) scrollbarRef?: NgScrollbar;
  lastScrollTop = 0;

  navHeaderHidden$ = new BehaviorSubject<boolean>(false);

  stickyHeader = true;

  @HostBinding('class.compact-mode') compactMode = false;

  _scrolledToTop$ = new BehaviorSubject<boolean>(true);
  scrolledToTop$ = this._scrolledToTop$.pipe(
    distinctUntilChanged(),
    tap((scrolledToTop) => {
      if (scrolledToTop) {
        this.stickyHeader = true;
      }
    }),
  );

  constructor(
    private zone: NgZone,
    private sidebarStateService: SidebarStateService,
    private router: Router,
    private postNavigationScrollToTopStateService: PostNavigationScrollToTopStateService,
    private compactModeService: CompactModeService,
  ) {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        filter(() => !!this.scrollbarRef),
        tap(() => {
          if (!this.postNavigationScrollToTopStateService.enabled) {
            return;
          }
          this.scrollbarRef?.scrollTo({ top: 0, duration: 500 });
        }),
        untilDestroyed(this),
      )
      .subscribe();

    this.compactModeService.compactMode$
      .pipe(untilDestroyed(this))
      .subscribe((compactMode) => {
        this.compactMode = compactMode;
      });
  }

  ngAfterViewInit(): void {
    if (this.scrollbarRef) {
      fromEvent(this.scrollbarRef.viewport.nativeElement, 'scroll')
        .pipe(
          map((event) => {
            const scrollViewport = event.target as HTMLElement;
            if (!scrollViewport) {
              return null;
            }
            const scrollPosition = scrollViewport.scrollTop;
            this.sidebarStateService.currentScrollOffset = scrollPosition;
            return scrollPosition;
          }),
          filter(
            (scrollPosition): scrollPosition is number =>
              scrollPosition !== null,
          ),
          delay(250),
          map((scrollPosition) => {
            const delta = 5;
            const navbarHeight = 68;

            const scrollDelta = Math.abs(this.lastScrollTop - scrollPosition);

            if (scrollDelta <= delta) {
              return null;
            }

            const isScrolledDown = scrollPosition > this.lastScrollTop;
            const scrolledMoreThanNavbarHeight = scrollPosition > navbarHeight;

            this.lastScrollTop = scrollPosition;

            if (!scrolledMoreThanNavbarHeight) {
              this._scrolledToTop$.next(true);
            } else {
              this._scrolledToTop$.next(false);
            }

            return isScrolledDown && scrolledMoreThanNavbarHeight;
          }),
          filter(
            (isScrolledUp): isScrolledUp is boolean => isScrolledUp !== null,
          ),
          distinctUntilChanged(),
          tap((forceShowHeader) => {
            this.zone.run(() => {
              this.navHeaderHidden$.next(forceShowHeader);
            });
          }),
          untilDestroyed(this),
        )
        .subscribe();
    } else {
      console.warn('scrollbarRef not available');
    }
  }
}

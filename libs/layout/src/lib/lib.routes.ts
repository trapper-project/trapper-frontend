import { Route } from '@angular/router';
import { CsLayoutComponent } from './cs-layout/cs-layout.component';

export const csLayoutRoutes: Route[] = [
  {
    path: '',
    component: CsLayoutComponent,
    children: [
      {
        path: 'projects',
        loadChildren: () => import('@trapper/cs-projects'),
      },
      {
        path: 'accounts',
        loadChildren: () =>
          import('@trapper/cs-accounts').then((m) => m.routes),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('@trapper/profile').then((m) => m.csProfileRoutes),
      },
      {
        path: '**',
        redirectTo: 'projects/active-project',
      },
    ],
  },
];

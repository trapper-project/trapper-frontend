import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { CdkMenuModule } from '@angular/cdk/menu';
import { LogoutService } from '@trapper/auth-token';
import { Router, RouterModule } from '@angular/router';
import {
  LanguageSelectionService,
  languageDisplayNames,
} from '@trapper/translations';
import { WorkspaceSelectorService } from '@trapper/workspace-selector';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { CompactModeService, SidebarStateService } from '@trapper/ui';
import { BreakpointObserver } from '@angular/cdk/layout';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';
import { fromEvent, map } from 'rxjs';
import { TranslocoModule } from '@jsverse/transloco';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import {
  matDarkMode,
  matInvertColors,
  matLightMode,
  matContrast,
} from '@ng-icons/material-icons/baseline';

@UntilDestroy()
@Component({
  selector: 'trapper-cs-header',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    CdkMenuModule,
    RouterModule,
    MatIconModule,
    TranslocoModule,
    MatTooltipModule,
    NgIconComponent,
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [
    provideIcons({
      matLightMode,
      matDarkMode,
      matInvertColors,
      matContrast,
    }),
  ],
})
export class HeaderComponent implements OnInit {
  currentUser$ = this.userProfileService.data$;

  sidebarExpanded$ = this.sidebarStateService.sidebarExpanded$;
  sidebarPresent$ = this.sidebarStateService.sidebarPresent$;

  isWideScreen = false;
  isCompactMode = false;

  supportedLanguages = Object.entries(languageDisplayNames).map(
    ([code, label]) => ({
      code,
      label,
    }),
  );

  logo$ = this.siteCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.project_logo),
  );

  backendURL$ = this.siteCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.backend_url),
  );

  @Input() stickyHeader = false;
  @Output() stickyHeaderChange = new EventEmitter<boolean>();

  @Input() navHeaderHidden = false;
  @Input() scrolledToTop = true;

  colorScheme: 'light' | 'dark' | null = 'light';
  defaultColorScheme: 'light' | 'dark' = 'light';

  constructor(
    private userProfileService: CurrentUserProfileService,
    private router: Router,
    private languageSelectionService: LanguageSelectionService,
    private workspaceSelectorService: WorkspaceSelectorService,
    private logoutService: LogoutService,
    private sidebarStateService: SidebarStateService,
    private compactModeService: CompactModeService,
    private breakpointObserver: BreakpointObserver,
    private siteCustomizationService: CsSiteCustomizationService,
  ) {}

  ngOnInit(): void {
    this.breakpointObserver
      .observe('(max-width: 1414px)')
      .subscribe((result) => {
        this.isWideScreen = !result.matches;
      });

    this.compactModeService.compactMode$
      .pipe(untilDestroyed(this))
      .subscribe((isCompact) => {
        this.isCompactMode = isCompact;
      });

    const colorScheme = localStorage.getItem('TRAPPER_COLOR_SCHEME');
    if (colorScheme === 'light' || colorScheme === 'dark') {
      this.setColorScheme(colorScheme);
    } else {
      this.setColorScheme(null);
    }

    const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
    if (mediaQuery) {
      fromEvent(mediaQuery, 'change')
        .pipe(
          map((event) => (event as MediaQueryListEvent).matches),
          untilDestroyed(this),
        )
        .subscribe((isDark) => {
          this.defaultColorScheme = isDark ? 'dark' : 'light';
          if (this.colorScheme === null) {
            this.applyDefaultColorScheme();
          }
        });
    }
  }

  logOut() {
    this.logoutService.logout().subscribe(() => {
      this.userProfileService.reloadUserProfile().subscribe();
      this.router.navigate(['/cs/accounts/login']);
    });
  }

  get currentLanguage() {
    return this.languageSelectionService.currentLanguage;
  }

  get currentLanguageLabel() {
    return this.supportedLanguages.find(
      (lang) => lang.code === this.currentLanguage,
    )?.label;
  }

  setLanguage(lang: string) {
    this.languageSelectionService.setLanguage(lang);
  }

  get workspaceSelectorEnabled() {
    return this.workspaceSelectorService.workspaceSelectorEnabled;
  }

  toggleSidebar() {
    this.sidebarStateService.toggleSidebar();
  }

  toggleStickyHeader() {
    this.stickyHeader = !this.stickyHeader;
    this.stickyHeaderChange.emit(this.stickyHeader);
  }

  private applyColorScheme(scheme: 'light' | 'dark') {
    document.documentElement.setAttribute('data-theme', scheme);
  }

  private applyDefaultColorScheme() {
    this.applyColorScheme(this.defaultColorScheme);
  }

  setColorScheme(scheme: 'light' | 'dark' | null) {
    this.colorScheme = scheme;
    if (scheme === null) {
      localStorage.removeItem('TRAPPER_COLOR_SCHEME');
      this.applyDefaultColorScheme();
      return;
    }
    localStorage.setItem('TRAPPER_COLOR_SCHEME', scheme);
    this.applyColorScheme(scheme);
  }

  toggleColorScheme() {
    switch (this.colorScheme) {
      case 'light':
        this.setColorScheme(null);
        break;
      case 'dark':
        this.setColorScheme('light');
        break;
      default:
        this.setColorScheme('dark');
    }
  }
}

import { Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoModule } from '@jsverse/transloco';
import { CsSiteCustomizationService } from '@trapper/cs-site-customization';
import { map, shareReplay } from 'rxjs';
import { VersionInfoService } from '@trapper/version-info';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CompactModeService } from '@trapper/ui';

@Component({
  selector: 'trapper-cs-footer',
  standalone: true,
  imports: [CommonModule, TranslocoModule, MatIconModule, MatTooltipModule],
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  versionInfo$ = this.versionInfoService.versionInfo$.pipe(
    map(
      (versionInfo) =>
        `FE: ${versionInfo.fe_version}\nBE: ${versionInfo.be_version}`,
    ),
    shareReplay(1),
  );
  currentYear = new Date().getFullYear();
  projectName$ = this.csCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.project_name || 'Trapper'),
    shareReplay(1),
  );

  facebook_url$ = this.csCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.facebook_url),
    shareReplay(1),
  );

  twitter_url$ = this.csCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.twitter_url),
    shareReplay(1),
  );

  linkedin_url$ = this.csCustomizationService.siteCustomization$.pipe(
    map((siteCustomization) => siteCustomization?.linkedin_url),
    shareReplay(1),
  );

  compactMode$ = this.compactModeService.compactMode$;

  @HostBinding('class.compact-mode') compactMode = false;

  constructor(
    private csCustomizationService: CsSiteCustomizationService,
    private versionInfoService: VersionInfoService,
    private compactModeService: CompactModeService,
  ) {}

  ngOnInit(): void {
    this.compactModeService.compactMode$.subscribe((compactMode) => {
      this.compactMode = compactMode;
    });
  }
}

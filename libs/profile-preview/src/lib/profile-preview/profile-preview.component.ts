import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { ProfilePreviewDataService } from '../profile-preview-data.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatIconModule } from '@angular/material/icon';
import { CardComponent, DetailsCardItemComponent } from '@trapper/ui';
import { map } from 'rxjs';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { inOutAnimation } from '@trapper/animations';
import { profilePreviewTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-profile-preview',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    CardComponent,
    DetailsCardItemComponent,
    NgxSkeletonLoaderModule,
    TranslocoModule,
  ],
  templateUrl: './profile-preview.component.html',
  styleUrls: ['./profile-preview.component.scss'],
  providers: [ProfilePreviewDataService, profilePreviewTranslocoScope],
  animations: [inOutAnimation],
})
export class ProfilePreviewComponent implements OnInit {
  @HostBinding('@inOutAnimation') inOutAnimation = true;
  userId?: string | number;
  data$ = this.profilePreviewDataService.data$;
  currentAvatar$ = this.data$.pipe(
    map((data) => data?.avatar),
    map((avatar) => `url(${avatar})`),
  );
  constructor(
    @Inject(DIALOG_DATA) data: { userId: string | number },
    private profilePreviewDataService: ProfilePreviewDataService,
    private dialogRef: DialogRef,
  ) {
    this.userId = data.userId;
  }

  ngOnInit(): void {
    if (!this.userId) {
      console.warn('No user id provided');
      return;
    }
    this.profilePreviewDataService
      .fetchUser(this.userId)
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  close() {
    this.dialogRef.close();
  }
}

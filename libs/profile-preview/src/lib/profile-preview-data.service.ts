import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

export interface ProfilePreviewResponse {
  id: number;
  name: string;
  first_name: string;
  last_name: string;
  avatar: string;
  username: string;
  institution: string;
  about_me: string;
  classification_projects: string[];
}

@Injectable()
export class ProfilePreviewDataService extends BaseBackendConnector<
  unknown,
  ProfilePreviewResponse
> {
  fetchUser(userId: number | string): Observable<void> {
    return this.get(`/api/accounts/profile/${userId}/`);
  }
}

import { Injectable } from '@angular/core';

import { Dialog } from '@angular/cdk/dialog';
import { ProfilePreviewComponent } from './profile-preview/profile-preview.component';
import { Overlay } from '@angular/cdk/overlay';
@Injectable()
export class ProfilePreviewService {
  constructor(
    private dialog: Dialog,
    private overlay: Overlay,
  ) {}

  open(userId: number | string) {
    this.dialog.open(ProfilePreviewComponent, {
      data: { userId },
      panelClass: 'dialog',
      width: 'min(1280px, 100%)',
      maxHeight: '100vh',
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    });
  }
}

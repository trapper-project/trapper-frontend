import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from '@angular/cdk/dialog';
import { ProfilePreviewService } from './profile-preview.service';

@NgModule({
  imports: [CommonModule, DialogModule],
  exports: [],
  providers: [ProfilePreviewService],
})
export class CsProfilePreviewModule {}

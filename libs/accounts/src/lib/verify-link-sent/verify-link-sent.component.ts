import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { RouterModule } from '@angular/router';
import { AccountsCardFooterClipartComponent } from '../accounts-card-footer-clipart/accounts-card-footer-clipart.component';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-verify-link-sent',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    RouterModule,
    SimpleMessageComponent,
    AccountsCardFooterClipartComponent,
    TranslocoModule,
  ],
  providers: [trapperCsAccountsTranslocoScope],
  templateUrl: './verify-link-sent.component.html',
  styleUrls: ['./verify-link-sent.component.scss'],
})
export class VerifyLinkSentComponent implements OnInit {
  constructor(
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit(): void {
    this.translocoService
      .selectTranslate('verifyLinkSent.pageTitle', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setTitle(title);
      });
  }
}

export default VerifyLinkSentComponent;

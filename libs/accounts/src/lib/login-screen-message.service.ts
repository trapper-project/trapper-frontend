import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoginScreenMessageService {
  private _message$ = new BehaviorSubject<string | null>(null);

  message$: Observable<string | null>;

  constructor() {
    this.message$ = this._message$.asObservable();
  }

  setMessage(msg: string | null) {
    this._message$.next(msg);
  }

  clearMessage() {
    this._message$.next(null);
  }
}

import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertComponent,
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  FormFieldComponent,
  LabelComponent,
  NonFieldErrorsComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoService,
} from '@jsverse/transloco';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AllauthService } from '@trapper/backend';
import { map, switchMap, tap } from 'rxjs';
import { extractFieldError$ } from '@trapper/backend';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { PageTitleService } from '@trapper/page-title-management';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-mfa',
  standalone: true,
  templateUrl: './mfa.component.html',
  styleUrl: './mfa.component.scss',
  imports: [
    CommonModule,
    FormFieldComponent,
    TranslocoModule,
    ReactiveFormsModule,
    ButtonComponent,
    TextInputComponent,
    CardComponent,
    FormColumnComponent,
    AlertComponent,
    LabelComponent,
    NonFieldErrorsComponent,
    RouterModule,
    ServerErrorsComponent,
  ],
  providers: [trapperCsAccountsTranslocoScope],
})
export class MfaComponent implements OnInit {
  allAuthService = inject(AllauthService);
  profileService = inject(CurrentUserProfileService);
  router = inject(Router);
  translocoService = inject(TranslocoService);
  translocoScope = inject(TRANSLOCO_SCOPE);
  pageTitleService = inject(PageTitleService);
  route = inject(ActivatedRoute);

  postLoginRoute = '/';
  formGroup = new FormGroup({
    code: new FormControl(''),
  });

  reauthenticate = false;

  inProgress$ = this.allAuthService.mfaInProgress$;

  nonFieldErrors$ = this.allAuthService.mfaErrors$.pipe(
    map((errors) => {
      if (!errors) {
        return null;
      }
      if (errors.detail) {
        return [errors.detail];
      }
      return errors.non_field_errors || null;
    }),
  );

  ngOnInit() {
    this.translocoService
      .selectTranslate<string>('mfaLogin.pageTitle', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setSectionTitle(title);
      });

    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this.postLoginRoute = params.get('next') || '/';
      this.reauthenticate = !!params.get('reauthenticate');
    });
  }

  fieldError$(fieldName: 'code') {
    return extractFieldError$(this.allAuthService.mfaErrors$, fieldName);
  }
  errors$ = this.allAuthService.mfaErrors$;

  mfaAuthenticate() {
    this.allAuthService
      .mfaAuthenticate(this.formGroup)
      .pipe(
        switchMap(() => this.profileService.reloadUserProfile()),
        tap(() => this.router.navigate([this.postLoginRoute])),
      )
      .subscribe();
  }

  cancel() {
    this.router.navigate(['/']);
  }
}

export default MfaComponent;

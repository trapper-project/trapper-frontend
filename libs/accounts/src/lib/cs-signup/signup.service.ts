import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

export interface SignupFormModel {
  email: string;
  username: string;
  first_name: string;
  last_name: string;
  password: string;
  password2: string;
  phone_number: string;
  institution: string;
  about_me: string;
  gdpr: boolean;
  tos: boolean;
}

@Injectable({ providedIn: 'root' })
export class SignupService extends BaseBackendConnector<
  SignupFormModel,
  unknown
> {
  signUp(payload: SignupFormModel): Observable<void> {
    return this.post('/api/accounts/register/', payload);
  }
}

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  FormColumnComponent,
  TextInputComponent,
} from '@trapper/ui';
import { Router, RouterModule } from '@angular/router';
import { SignupFormModel, SignupService } from './signup.service';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { tap } from 'rxjs';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { GdprPopupComponent, TosPopupComponent } from '@trapper/cs-consents';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-signup',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    TextInputComponent,
    CheckboxComponent,
    FormColumnComponent,
    RouterModule,
    ReactiveFormsModule,
    DialogModule,
    TranslocoModule,
  ],
  providers: [SignupService, trapperCsAccountsTranslocoScope],
  templateUrl: './cs-signup.component.html',
  styleUrls: ['./cs-signup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CsSignupComponent implements OnInit {
  formGroup = this.fb.group({
    email: this.fb.control(''),
    username: this.fb.control(''),
    first_name: this.fb.control(''),
    last_name: this.fb.control(''),
    password: this.fb.control(''),
    password2: this.fb.control(''),
    phone_number: this.fb.control(''),
    institution: this.fb.control(''),
    about_me: this.fb.control(''),
    gdpr: this.fb.control(false),
    tos: this.fb.control(false),
  });
  constructor(
    private signUpService: SignupService,
    private fb: NonNullableFormBuilder,
    private router: Router,
    private dialog: Dialog,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit() {
    this.translocoService
      .selectTranslate('signup.title', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((description) => {
        this.pageTitleService.setTitle(description);
      });
  }

  signUp() {
    this.signUpService
      .signUp(this.formGroup.getRawValue())
      .pipe(
        tap(() =>
          this.router.navigate(['/cs/accounts/verification-link-sent']),
        ),
      )
      .subscribe();
  }

  fieldError$(fieldName: keyof SignupFormModel) {
    return this.signUpService.fieldError$(fieldName);
  }

  tosPopup() {
    this.dialog.open(TosPopupComponent);
  }

  gdprPopup() {
    this.dialog.open(GdprPopupComponent);
  }
}

export default CsSignupComponent;

import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '@trapper/ui';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'trapper-accounts-social-login-flow',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './social-login-flow.component.html',
  styleUrl: './social-login-flow.component.scss',
})
export class SocialLoginFlowComponent implements OnInit {
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private toastr = inject(ToastrService);

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((params) => {
      const error = params.get('error');
      if (error === 'cancelled') {
        console.warn('User cancelled social login flow');
        this.router.navigate(['/cs/accounts/login']);
      } else if (error) {
        this.toastr.error(
          'An error occurred during social login (error: ' + error + ')',
        );
        this.router.navigate(['/cs/accounts/login']);
      } else {
        this.router.navigate(['/cs/accounts/flows']);
      }
    });
  }
}

export default SocialLoginFlowComponent;

import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  FormFieldComponent,
  LabelComponent,
  NonFieldErrorsComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AllauthService, extractFieldError$ } from '@trapper/backend';
import { map, switchMap, tap } from 'rxjs';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-reauthenticate',
  standalone: true,
  imports: [
    CommonModule,
    ButtonComponent,
    CardComponent,
    FormColumnComponent,
    NonFieldErrorsComponent,
    ReactiveFormsModule,
    FormFieldComponent,
    LabelComponent,
    RouterModule,
    TranslocoModule,
    TextInputComponent,
    ServerErrorsComponent,
  ],
  providers: [trapperCsAccountsTranslocoScope],
  templateUrl: './reauthenticate.component.html',
  styleUrl: './reauthenticate.component.scss',
})
export class ReauthenticateComponent implements OnInit {
  private allauthService = inject(AllauthService);
  private router = inject(Router);
  private profileService = inject(CurrentUserProfileService);
  private translocoService = inject(TranslocoService);
  private translocoScope = inject(TRANSLOCO_SCOPE);
  private pageTitleService = inject(PageTitleService);
  route = inject(ActivatedRoute);

  inProgress$ = this.allauthService.reauthenticateInProgress$;
  formGroupPassword: FormGroup = new FormGroup({
    password: new FormControl<string>(''),
  });

  formGroupMfa: FormGroup = new FormGroup({
    code: new FormControl<string>(''),
  });

  postLoginRoute = '/';
  flows: string[] = [];
  flow = 'reauthenticate';

  // nonFieldErrors$ = this.loginService.nonFieldErrors$;
  nonFieldErrors$ = this.allauthService.loginErrors$.pipe(
    map((errors) => {
      if (!errors) {
        return null;
      }
      if (errors.detail) {
        return [errors.detail];
      }
      return errors.non_field_errors || null;
    }),
  );

  ngOnInit(): void {
    this.translocoService
      .selectTranslate<string>(
        'reauthenticate.pageTitle',
        {},
        this.translocoScope,
      )
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setSectionTitle(title);
      });
    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this.postLoginRoute = params.get('next') || '/';
      this.flows = params.getAll('flows');
      const flow = params.get('flow');
      if (flow) {
        this.flow = flow;
      } else {
        this.flow = this.flows[0];
      }
    });
  }

  fieldError$(fieldName: 'password') {
    return extractFieldError$(this.allauthService.loginErrors$, fieldName);
  }

  reauthenticatePassword() {
    this.allauthService
      .reauthenticate(this.formGroupPassword)
      .pipe(
        switchMap(() => this.profileService.reloadUserProfile()),
        tap(() => this.router.navigate([this.postLoginRoute])),
      )
      .subscribe();
  }

  reauthenticateMfa() {
    this.allauthService
      .mfaReauthenticate(this.formGroupMfa)
      .pipe(
        switchMap(() => this.profileService.reloadUserProfile()),
        tap(() => this.router.navigate([this.postLoginRoute])),
      )
      .subscribe();
  }

  reauthenticate() {
    if (this.flow === 'reauthenticate') {
      this.reauthenticatePassword();
    } else if (this.flow === 'mfa_authenticate') {
      this.reauthenticateMfa();
    }
  }
}

export default ReauthenticateComponent;

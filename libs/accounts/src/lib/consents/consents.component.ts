import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '@trapper/ui';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Dialog } from '@angular/cdk/dialog';
import { GdprUpdateComponent, TosUpdateComponent } from '@trapper/cs-consents';
import { EMPTY, mergeMap, switchMap, tap } from 'rxjs';
import { Router } from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-consents',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './consents.component.html',
  styleUrl: './consents.component.scss',
})
export class ConsentsComponent implements OnInit {
  private currentProfileService = inject(CurrentUserProfileService);
  private dialog = inject(Dialog);
  private router = inject(Router);
  currentProfile$ = this.currentProfileService.data$;

  dialogOpen = false;

  ngOnInit(): void {
    this.currentProfileService.reloadUserProfile().subscribe();
    this.currentProfileService.data$
      .pipe(
        mergeMap((profile) => {
          if (!profile) {
            return EMPTY;
          }

          if (this.dialogOpen) {
            return EMPTY;
          }

          if (!profile.gdpr) {
            const ref = this.dialog.open<{ gdpr: boolean }>(
              GdprUpdateComponent,
              {
                disableClose: true,
              },
            );
            this.dialogOpen = true;

            return ref.closed.pipe(
              tap(() => {
                this.dialogOpen = false;
              }),
              switchMap(() => this.currentProfileService.reloadUserProfile()),
            );
          }

          if (!profile.tos) {
            const ref = this.dialog.open<{ tos: boolean }>(TosUpdateComponent, {
              disableClose: true,
            });
            this.dialogOpen = true;

            return ref.closed.pipe(
              tap(() => {
                this.dialogOpen = false;
              }),
              switchMap(() => this.currentProfileService.reloadUserProfile()),
            );
          }

          this.router.navigate(['/']);
          return EMPTY;
        }),
        untilDestroyed(this),
      )
      .subscribe();
  }
}

export default ConsentsComponent;

import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent, SimpleMessageComponent } from '@trapper/ui';
import { AccountsCardFooterClipartComponent } from '../accounts-card-footer-clipart/accounts-card-footer-clipart.component';
import { RouterModule } from '@angular/router';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-password-reset-confirmation',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    AccountsCardFooterClipartComponent,
    RouterModule,
    TranslocoModule,
  ],
  providers: [trapperCsAccountsTranslocoScope],
  templateUrl: './password-reset-confirmation.component.html',
  styleUrls: ['./password-reset-confirmation.component.scss'],
})
export class PasswordResetConfirmationComponent implements OnInit {
  constructor(
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit(): void {
    this.translocoService
      .selectTranslate(
        'passwordResetConfirmation.pageTitle',
        {},
        this.translocoScope,
      )
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setTitle(title);
      });
  }
}

export default PasswordResetConfirmationComponent;

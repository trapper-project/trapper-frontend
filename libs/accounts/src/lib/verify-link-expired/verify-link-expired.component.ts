import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AccountLinkResetService } from './account-link-reset.service';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-verify-link-expired',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    SimpleMessageComponent,
    RouterModule,
    TranslocoModule,
  ],
  providers: [trapperCsAccountsTranslocoScope, AccountLinkResetService],
  templateUrl: './verify-link-expired.component.html',
  styleUrls: ['./verify-link-expired.component.scss'],
})
export class VerifyLinkExpiredComponent implements OnInit {
  uid?: string | null;
  constructor(
    private toastr: ToastrService,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    private accountLinkRestService: AccountLinkResetService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.translocoService
      .selectTranslate('verifyLinkExpired.pageTitle', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setTitle(title);
      });

    this.activatedRoute.paramMap
      .pipe(untilDestroyed(this))
      .subscribe((params) => {
        this.uid = params.get('uid');
      });

    this.accountLinkRestService.nonFieldErrors$
      .pipe(untilDestroyed(this))
      .subscribe((error) => {
        if (error) {
          this.toastr.error(error.join('\n'));
        }
      });
  }
  resend() {
    if (!this.uid) {
      return;
    }
    this.accountLinkRestService
      .resendActivationLink(this.uid)
      .subscribe(() =>
        this.router.navigate(['/cs/accounts/verification-link-sent']),
      );
  }
}

export default VerifyLinkExpiredComponent;

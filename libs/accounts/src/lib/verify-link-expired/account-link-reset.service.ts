import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { catchError, map, Observable, of } from 'rxjs';

export class AccountLinkResetPayload {}

@Injectable()
export class AccountLinkResetService extends BaseBackendConnector<
  void,
  unknown
> {
  resendActivationLink(uid: string): Observable<boolean> {
    return this.post(`/api/accounts/resend-activation/${uid}/`).pipe(
      map(() => true),
      catchError(() => {
        return of(false);
      }),
    );
  }
}

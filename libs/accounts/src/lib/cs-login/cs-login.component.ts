import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlertComponent,
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  FormFieldComponent,
  LabelComponent,
  NonFieldErrorsComponent,
  ServerErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { NEVER, catchError, map, switchMap, take, tap } from 'rxjs';
import { LoginScreenMessageService } from '../login-screen-message.service';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { DialogModule } from '@angular/cdk/dialog';
import { CurrentUserProfileService } from '@trapper/current-profile-data';
import { PageTitleService } from '@trapper/page-title-management';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AllauthService } from '@trapper/backend';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { extractFieldError$ } from '@trapper/backend';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-login',
  standalone: true,
  templateUrl: './cs-login.component.html',
  styleUrls: ['./cs-login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    CardComponent,
    TextInputComponent,
    ButtonComponent,
    FormColumnComponent,
    NonFieldErrorsComponent,
    RouterModule,
    ReactiveFormsModule,
    AlertComponent,
    FormFieldComponent,
    ServerErrorsComponent,
    TranslocoModule,
    LabelComponent,
    DialogModule,
    NgxSkeletonLoaderModule,
  ],
  providers: [trapperCsAccountsTranslocoScope],
})
export class CsLoginComponent implements OnInit {
  inProgress$ = this.allauthService.loginInProgress$;
  formGroup: FormGroup = new FormGroup({
    email: new FormControl<string>(''),
    password: new FormControl<string>(''),
  });

  // nonFieldErrors$ = this.loginService.nonFieldErrors$;
  nonFieldErrors$ = this.allauthService.loginErrors$.pipe(
    map((errors) => {
      if (!errors) {
        return null;
      }
      if (errors.detail) {
        return [errors.detail];
      }
      return errors.non_field_errors || null;
    }),
  );
  message$ = this.loginScreenMessageService.message$;

  postLoginRoute = '/';

  socialLoginProviders$ = this.allauthService.providers$;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private profileService: CurrentUserProfileService,
    private loginScreenMessageService: LoginScreenMessageService,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
    private allauthService: AllauthService,
  ) {}

  ngOnInit(): void {
    this.allauthService.reloadConfig();
    this.profileService.clear();

    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this.postLoginRoute = params.get('redirect') || '/';
    });

    this.translocoService
      .selectTranslate<string>('login.pageTitle', {}, this.translocoScope)
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setSectionTitle(title);
      });
  }

  login() {
    this.loginScreenMessageService.clearMessage();
    this.allauthService
      .login(this.formGroup)
      .pipe(
        catchError((error) => {
          if (error.status === 401) {
            this.translocoService
              .selectTranslate<string>(
                'login.errors.accountInactive',
                {},
                this.translocoScope,
              )
              .pipe(take(1))
              .subscribe((message) => {
                this.loginScreenMessageService.setMessage(message);
              });
          } else {
            this.translocoService
              .selectTranslate<string>(
                'login.errors.generic',
                {},
                this.translocoScope,
              )
              .pipe(take(1))
              .subscribe((message) => {
                this.loginScreenMessageService.setMessage(message);
              });
          }
          return NEVER;
        }),
        switchMap(() => this.profileService.reloadUserProfile()),
        tap(() => this.router.navigate([this.postLoginRoute])),
      )
      .subscribe();
  }

  fieldError$(fieldName: 'email' | 'password') {
    return extractFieldError$(this.allauthService.loginErrors$, fieldName);
  }

  socialLogin(provider: string) {
    this.allauthService.startSocialLoginFlow(provider);
  }
}

export default CsLoginComponent;

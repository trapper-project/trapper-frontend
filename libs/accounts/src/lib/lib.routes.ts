import { Route } from '@angular/router';

export const routes: Route[] = [
  {
    path: 'signup',
    loadComponent: () => import('./cs-signup/cs-signup.component'),
  },
  {
    path: 'login',
    loadComponent: () => import('./cs-login/cs-login.component'),
  },
  {
    path: 'consents',
    loadComponent: () => import('./consents/consents.component'),
  },
  {
    path: 'flows/mfa',
    loadComponent: () => import('./mfa/mfa.component'),
  },
  {
    path: 'flows/social-login',
    loadComponent: () => import('./social-login/social-login-flow.component'),
  },
  {
    path: 'flows/reauthenticate',
    loadComponent: () => import('./reauthenticate/reauthenticate.component'),
  },
  {
    path: 'flows',
    loadComponent: () => import('./flows/flows.component'),
  },
  {
    path: 'verification-link-sent',
    loadComponent: () =>
      import('./verify-link-sent/verify-link-sent.component'),
  },
  {
    path: 'verification-link-expired/:uid',
    loadComponent: () =>
      import('./verify-link-expired/verify-link-expired.component'),
  },
  {
    path: 'password-reset',
    loadComponent: () => import('./password-reset/password-reset.component'),
  },
  {
    path: 'password-reset-confirm',
    loadComponent: () =>
      import(
        './password-reset-confirmation/password-reset-confirmation.component'
      ),
  },
  {
    path: 'password-reset-set-password/:uid/:token',
    loadComponent: () =>
      import(
        './password-reset-set-password/password-reset-set-password.component'
      ),
  },
  {
    path: 'password-reset/:uid/:token',
    loadComponent: () =>
      import('./password-reset-validate/password-reset-validate.component'),
  },
  {
    path: 'activate/:uid/:token',
    loadComponent: () => import('./activate/activate.component'),
  },
  {
    path: '',
    redirectTo: '../',
    pathMatch: 'prefix',
  },
  {
    path: '**',
    redirectTo: '../',
  },
];

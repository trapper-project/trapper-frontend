import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '@trapper/ui';
import { AllauthService } from '@trapper/backend';

@Component({
  selector: 'trapper-accounts-flows',
  standalone: true,
  imports: [CommonModule, SpinnerComponent],
  templateUrl: './flows.component.html',
  styleUrl: './flows.component.scss',
})
export class FlowsComponent implements OnInit {
  private allauthService = inject(AllauthService);

  ngOnInit(): void {
    this.allauthService.checkSession();
  }
}

export default FlowsComponent;

import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, tap } from 'rxjs';
import { PasswordResetValidationService } from './password-reset-validation.service';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { untilDestroyed } from '@ngneat/until-destroy';

@Component({
  selector: 'trapper-accounts-password-reset-validate',
  standalone: true,
  imports: [CommonModule, MatProgressSpinnerModule, TranslocoModule],
  providers: [trapperCsAccountsTranslocoScope],
  templateUrl: './password-reset-validate.component.html',
  styleUrls: ['./password-reset-validate.component.scss'],
})
export class PasswordResetValidateComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private passwordResetValidation: PasswordResetValidationService,
    private router: Router,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap((param) => {
          return this.passwordResetValidation
            .validatePasswordResetData(param['uid'], param['token'])
            .pipe(
              tap((result) => {
                if (result) {
                  this.router.navigate([
                    '/cs/accounts/password-reset-set-password/',
                    param['uid'],
                    param['token'],
                  ]);
                }
              }),
            );
        }),
      )
      .subscribe();

    this.translocoService
      .selectTranslate(
        'passwordResetValidate.pageTitle',
        {},
        this.translocoScope,
      )
      .pipe(untilDestroyed(this))
      .subscribe((title) => {
        this.pageTitleService.setTitle(title);
      });
  }
}

export default PasswordResetValidateComponent;

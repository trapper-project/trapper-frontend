import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PasswordResetValidationService {
  constructor(private httpClient: HttpClient) {}

  validatePasswordResetData(uid: string, token: string): Observable<boolean> {
    return this.httpClient
      .post(`/api/accounts/password-reset/${uid}/${token}/`, {
        new_password: '',
      })
      .pipe(
        map(() => true),
        catchError(() => {
          return of(true);
        }),
      );
  }
}

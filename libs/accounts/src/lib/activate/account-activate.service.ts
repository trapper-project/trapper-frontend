import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { catchError, map, Observable, of } from 'rxjs';

export class AccountActivationPayload {}

@Injectable()
export class AccountActivateService extends BaseBackendConnector<
  void,
  unknown
> {
  activateAccount(uid: string, token: string): Observable<boolean> {
    return this.post(`/api/accounts/activate/${uid}/${token}/`).pipe(
      map(() => true),
      catchError(() => {
        return of(false);
      }),
    );
  }
}

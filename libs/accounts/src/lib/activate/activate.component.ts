import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, switchMap, take, tap } from 'rxjs';
import { LoginScreenMessageService } from '../login-screen-message.service';
import { AccountActivateService } from './account-activate.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SpinnerComponent } from '@trapper/ui';
import { inOutAnimation } from '@trapper/animations';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { untilDestroyed } from '@ngneat/until-destroy';

@Component({
  selector: 'trapper-accounts-activate',
  standalone: true,
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    SpinnerComponent,
    TranslocoModule,
  ],
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss'],
  providers: [AccountActivateService, trapperCsAccountsTranslocoScope],
  animations: [inOutAnimation],
})
export class ActivateComponent implements OnInit {
  uid?: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loginMessageService: LoginScreenMessageService,
    private accountActivateService: AccountActivateService,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private scope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.route.params
      .pipe(
        tap((params) => {
          this.uid = params['uid'];
        }),
        switchMap((params) =>
          this.accountActivateService.activateAccount(
            params['uid'],
            params['token'],
          ),
        ),
        tap((result) => {
          if (!result) {
            this.router.navigate([
              '/cs/accounts/verification-link-expired/',
              this.uid,
            ]);
          } else {
            this.translocoService
              .selectTranslate('csAccounts.emailVerifiedAlert', {}, this.scope)
              .pipe(take(1), untilDestroyed(this))
              .subscribe((alert) => {
                this.loginMessageService.setMessage(alert);
              });
            this.router.navigate(['/cs/accounts/login']);
          }
        }),
      )
      .subscribe();

    this.accountActivateService.errors$
      .pipe(
        filter(Boolean),
        tap(() =>
          this.router.navigate([
            '/cs/accounts/verification-link-expired',
            this.uid,
          ]),
        ),
      )
      .subscribe();
  }
}

export default ActivateComponent;

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'trapper-accounts-accounts-card-footer-clipart',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './accounts-card-footer-clipart.component.html',
  styleUrls: ['./accounts-card-footer-clipart.component.scss'],
})
export class AccountsCardFooterClipartComponent {}

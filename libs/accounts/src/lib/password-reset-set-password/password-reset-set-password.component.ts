import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  NonFieldErrorsComponent,
  TextInputComponent,
} from '@trapper/ui';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { PasswordResetSetPasswordService } from './password-reset-set-password.service';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { PageTitleService } from '@trapper/page-title-management';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-password-reset-set-password',
  standalone: true,
  providers: [PasswordResetSetPasswordService, trapperCsAccountsTranslocoScope],
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    TextInputComponent,
    RouterModule,
    ReactiveFormsModule,
    NonFieldErrorsComponent,
    TranslocoModule,
  ],
  templateUrl: './password-reset-set-password.component.html',
  styleUrls: ['./password-reset-set-password.component.scss'],
})
export class PasswordResetSetPasswordComponent implements OnInit {
  formGroup = this.fb.group({
    new_password: this.fb.control(''),
    new_password2: this.fb.control(''),
  });
  constructor(
    private fb: NonNullableFormBuilder,
    public srvc: PasswordResetSetPasswordService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private translocoService: TranslocoService,
    private pageTitleService: PageTitleService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}
  ngOnInit(): void {
    this.route.params
      .pipe(
        tap((params) =>
          this.srvc.setUidAndToken(params['uid'], params['token']),
        ),
      )
      .subscribe();
    this.translocoService
      .selectTranslate(
        'passwordResetSetPassword.pageTitle',
        {},
        this.translocoScope,
      )
      .pipe(
        tap((title) => this.pageTitleService.setTitle(title)),
        untilDestroyed(this),
      )
      .subscribe();
  }

  resetPassword() {
    this.srvc
      .resetPassword(this.formGroup.getRawValue())
      .pipe(
        tap(() => {
          this.toastr.success(
            'Your password has been changed. You can log in now!',
          );
          this.router.navigate(['/cs/accounts/login']);
        }),
      )
      .subscribe();
  }
}

export default PasswordResetSetPasswordComponent;

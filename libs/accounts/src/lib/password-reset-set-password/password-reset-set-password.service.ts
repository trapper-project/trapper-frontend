import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';

export interface PasswordResetSetPasswordPayload {
  new_password: string;
  new_password2: string;
}

@Injectable({ providedIn: 'root' })
export class PasswordResetSetPasswordService extends BaseBackendConnector<
  PasswordResetSetPasswordPayload,
  unknown
> {
  private uid = '';
  private token = '';

  setUidAndToken(uid: string, token: string): void {
    this.uid = uid;
    this.token = token;
  }

  resetPassword(payload: PasswordResetSetPasswordPayload) {
    return this.post(
      `/api/accounts/password-reset/${this.uid}/${this.token}/`,
      payload,
    );
  }
}

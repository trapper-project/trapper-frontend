import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';

export interface PasswordResetRequestPayload {
  email: string;
}

@Injectable({ providedIn: 'root' })
export class PasswordResetService extends BaseBackendConnector<
  PasswordResetRequestPayload,
  unknown
> {
  requestPasswordReset(payload: PasswordResetRequestPayload) {
    return this.post('/api/accounts/request-password-reset/', payload);
  }
}

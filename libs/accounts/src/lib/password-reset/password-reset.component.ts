import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  FormColumnComponent,
  SimpleMessageComponent,
  TextInputComponent,
} from '@trapper/ui';
import { Router, RouterModule } from '@angular/router';
import { PasswordResetService } from './password-reset.service';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { tap } from 'rxjs';
import { PageTitleService } from '@trapper/page-title-management';
import {
  TRANSLOCO_SCOPE,
  TranslocoModule,
  TranslocoScope,
  TranslocoService,
} from '@jsverse/transloco';
import { trapperCsAccountsTranslocoScope } from '../transloco.loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-accounts-password-reset',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    TextInputComponent,
    ButtonComponent,
    SimpleMessageComponent,
    FormColumnComponent,
    RouterModule,
    ReactiveFormsModule,
    TranslocoModule,
  ],
  providers: [trapperCsAccountsTranslocoScope],
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
})
export default class PasswordResetComponent implements OnInit {
  formGroup = this.fb.group({
    email: this.fb.control(''),
  });
  constructor(
    public passwordResetService: PasswordResetService,
    private fb: NonNullableFormBuilder,
    private router: Router,
    private pageTitleService: PageTitleService,
    private translocoService: TranslocoService,
    @Inject(TRANSLOCO_SCOPE) private translocoScope: TranslocoScope,
  ) {}

  ngOnInit(): void {
    this.translocoService
      .selectTranslate('passwordReset.pageTitle', {}, this.translocoScope)
      .pipe(
        untilDestroyed(this),
        tap((title) => this.pageTitleService.setTitle(title)),
      )
      .subscribe();
  }

  requestPasswordReset() {
    this.passwordResetService
      .requestPasswordReset(this.formGroup.getRawValue())
      .pipe(
        tap(() =>
          this.router.navigate(['/cs/accounts/password-reset-confirm']),
        ),
      )
      .subscribe();
  }
}

import { sv, enUS, pl, de } from 'date-fns/locale';

export const availableLanguages = ['en', 'sv', 'pl', 'de'] as const;

export const fullLanguageCodes = {
  en: 'EN-us',
  sv: 'sv',
  pl: 'pl',
  de: 'de',
};

export const dateLocales = {
  en: enUS,
  sv: sv,
  pl: pl,
  de: de,
};

export const languageDisplayNames = {
  en: 'ENG',
  sv: 'SWE',
  pl: 'PL',
  de: 'DE',
};

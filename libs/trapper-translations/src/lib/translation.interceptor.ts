import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LanguageSelectionService } from './language-selection.service';

@Injectable()
export class TranslationInterceptor {
  constructor(private languageSelectionService: LanguageSelectionService) {}
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    const lang = this.languageSelectionService.currentLanguage;
    const langCode = this.languageSelectionService.currentLanguageCode;
    const d = new Date();
    d.setTime(d.getTime() + 30 * 24 * 60 * 60 * 1000);
    const expires = 'expires=' + d.toUTCString();
    document.cookie = 'trapper_language=' + lang + ';' + expires + ';path=/';
    return next.handle(
      request.clone({
        setHeaders: {
          'Accept-Language': langCode + ';q=0.9,en;q=0.8',
        },
        withCredentials: true,
      }),
    );
  }
}

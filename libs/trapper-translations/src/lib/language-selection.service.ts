import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  dateLocales,
  availableLanguages,
  fullLanguageCodes,
} from './trapper-languages.config';

export type LanguageChoicesT = (typeof availableLanguages)[number];
const KEY = 'trapper_current_language';

const languageCodeToLanguage = Object.fromEntries(
  Object.entries(fullLanguageCodes).map(([key, value]) => [value, key]),
);

@Injectable({ providedIn: 'root' })
export class LanguageSelectionService {
  localeChange$ = new BehaviorSubject<LanguageChoicesT>(this.currentLanguage);

  defaultLanguage: LanguageChoicesT | undefined = availableLanguages[0];

  constructor() {
    // default language based on browser language
    const browserLang = navigator.language.split('-')[0];
    if (this.isValidLanguage(browserLang)) {
      this.defaultLanguage = browserLang;
    }
  }

  get currentLanguage(): LanguageChoicesT {
    const lang = this.getSavedLanguage();
    return lang;
  }

  get currentDateLocale() {
    const lang = this.currentLanguage;
    const locale = dateLocales[lang];

    return locale;
  }

  get currentLanguageCode() {
    return fullLanguageCodes[this.currentLanguage];
  }

  setLanguage(lang: string) {
    if (availableLanguages.includes(lang as LanguageChoicesT)) {
      this.storeLanguageSelection(lang);
      this.localeChange$.next(lang as LanguageChoicesT);
    }
  }

  isValidLanguage(lang: string): lang is LanguageChoicesT {
    return availableLanguages.includes(lang as LanguageChoicesT);
  }

  storeLanguageSelection(lang: string) {
    if (!this.isValidLanguage(lang)) {
      return;
    }
    const d = new Date();
    d.setTime(d.getTime() + 30 * 24 * 60 * 60 * 1000);
    const expires = 'expires=' + d.toUTCString();
    document.cookie = 'trapper_language=' + lang + ';' + expires + ';path=/';

    localStorage.setItem(KEY, lang);
  }

  getSavedLanguage(): LanguageChoicesT {
    let cookieLang: string | null | undefined = document.cookie
      .split(';')
      .find((c) => c.trim().startsWith('trapper_language='))
      ?.split('=')[1];

    if (cookieLang && !this.isValidLanguage(cookieLang)) {
      // maybe this is a language code
      cookieLang = languageCodeToLanguage[cookieLang];
    }

    if (!cookieLang || !this.isValidLanguage(cookieLang)) {
      console.log('no cookie language', cookieLang);
      // fallback to localStorage
      cookieLang = localStorage.getItem(KEY);
    }

    if (!cookieLang || !this.isValidLanguage(cookieLang)) {
      this.storeLanguageSelection(
        this.defaultLanguage || availableLanguages[0],
      );
      // for some reason, defaultLanguage might be undefined
      return this.defaultLanguage || availableLanguages[0];
    }

    return cookieLang;
  }
}

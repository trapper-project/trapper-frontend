import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

export interface TosUpdateModel {
  tos: boolean;
}

@Injectable({ providedIn: 'root' })
export class TosUpdateService extends BaseBackendConnector<
  TosUpdateModel,
  void
> {
  updateTos(payload: TosUpdateModel): Observable<void> {
    return this.patch('/api/accounts/profile/', payload);
  }
}

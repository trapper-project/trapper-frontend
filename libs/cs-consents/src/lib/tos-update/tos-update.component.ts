import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  FormColumnComponent,
  ModalComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { RouterModule } from '@angular/router';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TosUpdateService } from './tos-update.service';
import { map, switchMap } from 'rxjs';
import { TosService } from '../legal-docs.service';
import { LanguageSelectionService } from '@trapper/translations';
import { DialogRef } from '@angular/cdk/dialog';
import { trapperCsConsentsTranslocoScope } from '../transloco.loader';
import { TranslocoModule } from '@jsverse/transloco';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-tos-update',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    CheckboxComponent,
    FormColumnComponent,
    RouterModule,
    ButtonComponent,
    ReactiveFormsModule,
    TranslocoModule,
    ModalComponent,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './tos-update.component.html',
  styleUrls: ['./tos-update.component.scss'],
  providers: [trapperCsConsentsTranslocoScope],
})
export class TosUpdateComponent implements OnInit {
  formGroup = this.fb.group({
    tos: false,
  });
  content$ = this.tosService.data$.pipe(map((data) => data?.content || null));

  constructor(
    private fb: NonNullableFormBuilder,
    public tosUpdateService: TosUpdateService,
    private tosService: TosService,
    private languageSelectionService: LanguageSelectionService,
    private dialogRef: DialogRef,
  ) {}

  ngOnInit(): void {
    this.languageSelectionService.localeChange$
      .pipe(
        switchMap(() => this.tosService.fetchTos()),
        untilDestroyed(this),
      )
      .subscribe();
  }

  updateTos() {
    this.tosUpdateService
      .updateTos(this.formGroup.getRawValue())
      .subscribe(() => {
        this.dialogRef.close();
      });
  }
}

export default TosUpdateComponent;

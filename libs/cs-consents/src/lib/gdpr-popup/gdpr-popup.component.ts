import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { map, switchMap } from 'rxjs';
import { ButtonComponent, CardComponent, ModalComponent } from '@trapper/ui';
import { DialogRef } from '@angular/cdk/dialog';
import { GdrpService } from '../legal-docs.service';
import { LanguageSelectionService } from '@trapper/translations';
import { TranslocoModule } from '@jsverse/transloco';
import { trapperCsConsentsTranslocoScope } from '../transloco.loader';

@Component({
  selector: 'trapper-frontend-gdpr-popup',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    ModalComponent,
    TranslocoModule,
  ],
  templateUrl: './gdpr-popup.component.html',
  styleUrls: ['./gdpr-popup.component.scss'],
  providers: [trapperCsConsentsTranslocoScope],
})
export class GdprPopupComponent implements OnInit {
  content$ = this.gdprService.data$.pipe(map((data) => data?.content || null));
  constructor(
    private gdprService: GdrpService,
    private dialogRef: DialogRef,
    private languageSelectionService: LanguageSelectionService,
  ) {}
  ngOnInit(): void {
    this.gdprService.fetchGdpr().subscribe();
    this.languageSelectionService.localeChange$
      .pipe(switchMap(() => this.gdprService.fetchGdpr()))
      .subscribe();
  }

  close() {
    this.dialogRef.close();
  }
}

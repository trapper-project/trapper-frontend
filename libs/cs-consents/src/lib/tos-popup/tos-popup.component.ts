import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TosService } from '../legal-docs.service';
import { map, switchMap } from 'rxjs';
import { ButtonComponent, CardComponent, ModalComponent } from '@trapper/ui';
import { DialogRef } from '@angular/cdk/dialog';
import { LanguageSelectionService } from '@trapper/translations';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { TranslocoModule } from '@jsverse/transloco';
import { trapperCsConsentsTranslocoScope } from '../transloco.loader';
import { UntilDestroy } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-tos-popup',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    ButtonComponent,
    NgScrollbarModule,
    ModalComponent,
    TranslocoModule,
  ],
  templateUrl: './tos-popup.component.html',
  styleUrls: ['./tos-popup.component.scss'],
  providers: [trapperCsConsentsTranslocoScope],
})
export class TosPopupComponent implements OnInit {
  content$ = this.tosService.data$.pipe(map((data) => data?.content || null));

  constructor(
    private tosService: TosService,
    private dialogRef: DialogRef,
    private languageSelectionService: LanguageSelectionService,
  ) {}
  ngOnInit(): void {
    this.tosService.fetchTos().subscribe();
    this.languageSelectionService.localeChange$
      .pipe(switchMap(() => this.tosService.fetchTos()))
      .subscribe();
  }

  close() {
    this.dialogRef.close();
  }
}

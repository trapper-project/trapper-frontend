import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonComponent,
  CardComponent,
  CheckboxComponent,
  FormColumnComponent,
  ModalComponent,
  SimpleMessageComponent,
} from '@trapper/ui';
import { RouterModule } from '@angular/router';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { map, switchMap } from 'rxjs';
import { GdprUpdateService } from './gdpr-update.service';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { GdrpService } from '../legal-docs.service';
import { LanguageSelectionService } from '@trapper/translations';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DialogRef } from '@angular/cdk/dialog';
import { TranslocoModule } from '@jsverse/transloco';
import { trapperCsConsentsTranslocoScope } from '../transloco.loader';

@UntilDestroy()
@Component({
  selector: 'trapper-frontend-gdpr-update',
  standalone: true,
  imports: [
    CommonModule,
    CardComponent,
    SimpleMessageComponent,
    CheckboxComponent,
    FormColumnComponent,
    RouterModule,
    ButtonComponent,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    TranslocoModule,
    ModalComponent,
  ],
  templateUrl: './gdpr-update.component.html',
  styleUrls: ['./gdpr-update.component.scss'],
  providers: [trapperCsConsentsTranslocoScope],
})
export class GdprUpdateComponent implements OnInit {
  formGroup = this.fb.group({
    gdpr: false,
  });

  content$ = this.gdprService.data$.pipe(map((data) => data?.content || null));

  constructor(
    private fb: NonNullableFormBuilder,
    public gdprUpdateService: GdprUpdateService,
    private gdprService: GdrpService,
    private languageSelectionService: LanguageSelectionService,
    private dialogRef: DialogRef,
  ) {}

  ngOnInit(): void {
    this.languageSelectionService.localeChange$
      .pipe(
        switchMap(() => this.gdprService.fetchGdpr()),
        untilDestroyed(this),
      )
      .subscribe();
  }

  updateGdpr() {
    this.gdprUpdateService
      .updateGdpr(this.formGroup.getRawValue())
      .subscribe(() => {
        this.dialogRef.close();
      });
  }
}

export default GdprUpdateComponent;

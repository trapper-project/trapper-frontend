import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';
import { Observable } from 'rxjs';

export interface GdprUpdateModel {
  gdpr: boolean;
}

@Injectable({ providedIn: 'root' })
export class GdprUpdateService extends BaseBackendConnector<
  GdprUpdateModel,
  void
> {
  updateGdpr(payload: GdprUpdateModel): Observable<void> {
    return this.patch('/api/accounts/profile/', payload);
  }
}

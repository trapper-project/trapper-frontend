import { Injectable } from '@angular/core';
import { BaseBackendConnector } from '@trapper/backend';

export interface DocDto {
  content: string;
}

@Injectable({ providedIn: 'root' })
export class GdrpService extends BaseBackendConnector<void, DocDto> {
  fetchGdpr() {
    return this.get('/api/cs/gdpr/');
  }
}

@Injectable({ providedIn: 'root' })
export class TosService extends BaseBackendConnector<void, DocDto> {
  fetchTos() {
    return this.get('/api/cs/tos/');
  }
}

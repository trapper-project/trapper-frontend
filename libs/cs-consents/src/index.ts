export * from './lib/gdpr-update/gdpr-update.component';
export * from './lib/gdpr-popup/gdpr-popup.component';
export * from './lib/tos-popup/tos-popup.component';
export * from './lib/tos-update/tos-update.component';

module.exports = {
  rootTranslationsPath: 'apps/trapper-frontend-cs/src/assets/i18n/',
  langs: ['en', 'sv', 'pl', 'de'],
  scopePathMap: {
    csProjects: 'libs/cs-projects/src/lib/i18n',
    csLayout: 'libs/layout/src/lib/i18n',
    csAccounts: 'libs/accounts/src/lib/i18n',
    trapperUi: 'libs/trapper-ui/src/lib/i18n',
    csProfilePreview: 'libs/profile-preview/src/lib/i18n',
    csProfile: 'libs/profile/src/lib/i18n',
    csConsents: 'libs/cs-consents/src/lib/i18n',
    resourceAnnotator: 'libs/resource-annotator/src/lib/i18n',
  },
  keysManager: {
    defaultValue: '~{{key}}~',
    output: 'lib/i18n',
  },
};

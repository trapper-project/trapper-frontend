<p align="center">
  <img src="https://gitlab.com/trapper-project/trapper/-/raw/master/trapper/trapper-project/trapper/apps/common/static/images/logo/logo_text.png">
</p>

<div align="center"> 
<font size="6"> Smart and innovative frontend UI for Citizen Science projects </font>
<br>
<hr> 
<img src="https://img.shields.io/badge/Angular-16-blue"/>
<a href="https://trapper-project.readthedocs.io/en/latest/"><img src="https://img.shields.io/badge/Trapper-Documentation-yellow" /></a>
<a href="https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571"><img src="https://img.shields.io/badge/Trapper-Paper-blue.svg" /></a>
<a href="https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A"><img src="https://img.shields.io/badge/Trapper-Slack-orange" /></a>
<a href="https://gitlab.com/oscf/trapper-frontend/-/blob/main/LICENSE"><img src="https://img.shields.io/badge/Licence-GPLv3-pink" /></a>
<br><br>
</div>

## Table of Contents

[Trapper Frontend](https://gitlab.com/oscf/trapper-frontend/#trapper-frontend) | [Local development](https://gitlab.com/oscf/trapper-frontend/#local-development) | [Demo](https://gitlab.com/oscf/trapper-frontend/#-demo) | [Documentation](https://gitlab.com/oscf/trapper-frontend/#-documentation) | [Who is using TRAPPER?](https://gitlab.com/oscf/trapper-frontend/#-who-is-using-trapper) | [Funders and Partners](https://gitlab.com/oscf/trapper-frontend/#-funders-and-partners) | [Support](https://gitlab.com/oscf/trapper-frontend/#-support) | [License](https://gitlab.com/oscf/trapper-frontend/#-license)

## 🐗 Trapper Frontend

This is the Citizen Science frontend for the [TRAPPER project](https://gitlab.com/oscf/trapper-cs). It is a Angular application leveraging NX to manage the monorepo. At this moment, this repo contains the following applications:

- trapper-frontend-cs: Citizen Science edition of the frontend

## 🏗️ Local development

### Prerequisites

You will need NodeJS 18 and NPM installed on your machine.

First, install dependencies using `npm install`.

### Running the application

To run the application, use `npm run start`. This will start the application on port 4200.

By default, the application will connect to the backend running on `https://dev.trapper-project.org/`. To change this, you can set the `TRAPPER_BACKEND_URL` environment variable.

### Formating

To format the code, use `npm run format`.
Code formatting is done using Prettier. It is enforced in the CI pipeline.

### Linting

To lint the code, use `npm run lint`.
Linting is done using ESLint. It is enforced in the CI pipeline.

### Building

To build the application, use `npm run build`.

### Deploying

You can deploy the application using docker image build by the CI pipeline. The image is available on the [GitLab container registry](https://gitlab.com/oscf/trapper-frontend/container_registry).

You can specify the backend URL using the `TRAPPER_BACKEND_URL` environment variable. The correct format for url is `${protocol}://${host}:${port}`. For example, `https://dev.trapper-project.org`, without a trailing slash.

You can run the image using the following command:

```bash
docker pull registry.gitlab.com/oscf/trapper-frontend:main
docker run -p 8042:80 -e TRAPPER_BACKEND_URL=https://dev.trapper-project.org registry.gitlab.com/oscf/trapper-frontend:main
```

## 🌐 Demo
[Trapper Expert ](https://demo.trapper-project.org) demo instance does not include **Trapper Citizen Science** or **Trapper AI** yet.

## 📝 Documentation

[TRAPPER](https://trapper-project.readthedocs.io) documentation.

If you are using TRAPPER, please cite our work:

> Bubnicki, J.W., Churski, M. and Kuijper, D.P.J. (2016), trapper: an open source web-based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. https://doi.org/10.1111/2041-210X.12571

For more news about TRAPPER please visit the [Open Science Conservation Fund (OSCF) website](https://os-conservation.org) and [OSCF LinkedIn profile](https://www.linkedin.com/company/os-conservation/).

## 🏢 Who is using TRAPPER?
* Mammal Research Institute Polish Academy of Sciences;
* Karkonosze National Park;
* Swedish University of Agricultural Sciences;
* Svenska Jägareförbundet;
* Meles Wildbiologie;
* University of Freiburg Wildlife Ecology and Management;
* Bavarian Forest National Park;
* Georg-August-Universität Göttingen;
* KORA - Carnivore Ecology and Wildlife Management;
* and many more individual scientists and ecologies;

## 💲 Funders and Partners
<p align="center">
  <img src="TRAPPER-funds.png">
</p>
<p align="center">
  <img src="TRAPPER-partners.png">
</p>

## 🤝 Support

Feel free to add a new issue with a respective title and description on the [TRAPPER issue tracker](https://gitlab.com/trapper-project/trapper/-/issues). If you already found a solution to your problem, we would be happy to review your pull request.

If you prefer direct contact, please let us know: `contact@os-conservation.org`

We also have [TRAPPER Mailing List](https://groups.google.com/d/forum/trapper-project) and [TRAPPER Slack](https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A).

## 📜 License

Read more in [TRAPPER License](https://gitlab.com/oscf/trapper-frontend/-/blob/main/LICENSE).
